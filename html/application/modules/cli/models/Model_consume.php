<?php
if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
class Model_consume extends CI_Model {
	protected $error = FALSE, $error_msg = array();
	protected $base_config;
	protected $db_api;
	function __construct() {
		parent::__construct();
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		$this->load->helper('base/base_config');
		
		$this->load->library('adminpanel/Lib_Adminpanel', array(), 'adminpanel');
		$this->load->library('adminpanel/Lib_Firebase', '', 'firebase');
		
		// Load database
		$this->db_api = $this->load->database('api', TRUE);
	}
	function consume_stories_categories_data($search_text = '', $start = 0, $perpage = 100) {
		$collectData = array(
			'search_text'			=> (is_string($search_text) ? $search_text :''),
			'collect'				=> array(),
			'start'					=> (is_numeric($start) ? (int)$start : 0),
			'perpage'				=> (is_numeric($perpage) ? (int)$perpage : 100),
		);	
		try {
			$collectData['collect']['consume_categories'] = $this->firebase->get_categories_data_from_firebase($collectData['search_text'], $collectData['start'], $collectData['perpage']);
		} catch (Exception $ex) {
			$this->error = true;
			$this->error_msg[] = "Exception error while consume categories data from firebase: {$ex->getMessage()}";
		}
		if (!$this->error) {
			return $collectData;
		}
		return false;
	}
	//----------------------------------------------------------------
	function get_consume_index_by($by_type, $by_value = null) {
		$by_type = (is_string($by_type) ? strtolower($by_type) : 'categories');
		$value = "";
		if (isset($by_value)) {
			if (is_numeric($by_value)) {
				$value = (int)$by_value;
			} else if (is_string($by_value)) {
				$value = sprintf("%s", $by_value);
			} else {
				$value = "";
			}
		}
		$value = ((is_string($value) || is_numeric($value)) ? $value : '');
		switch (strtolower($by_type)) {
			case 'categories':
			case 'stories':
				$value = sprintf("%s", $value);
			break;
			case 'seq':
			default:
				if (!preg_match('/^[1-9][0-9]*$/', $by_value)) {
					$value = 0;
				} else {
					$value = sprintf('%d', $value);
				}
			break;
		}
		$this->db_api->select('*')->from('consume_firebase');
		$this->db_api->where('firebase_env', ConstantConfig::THIS_SERVER_MODE);
		switch (strtolower($by_type)) {
			case 'categories':
				$this->db_api->where('consume_key', $value);
			break;
			case 'stories':
				$this->db_api->where('consume_key', $value);
			break;
			default:
				$this->db_api->where('seq', $value);
			break;
		}
		try {
			$sql_query = $this->db_api->get();
		} catch (Exception $ex) {
			return false;
		}
		return $sql_query->row();
	}
	
	
	//------------------------------------------------------------------------------------
	public function insert_consume_logs_by_table($table, $input_params) {
		$table = (is_string($table) ? strtolower($table) : 'categories_logs');
		$this->db_api->insert($table, $input_params);
		return $this->db_api->insert_id();
	}
	public function get_consume_logs_by_table_seq($table, $seq = 0) {
		$table = (is_string($table) ? strtolower($table) : 'categories_logs');
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$this->db_api->where('seq', $seq);
		return $this->db_api->get($table)->row();
	}
	public function update_consume_logs_by_table_seq($table, $seq, $update_params = array()) {
		$table = (is_string($table) ? strtolower($table) : 'consume_firebase');
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$this->db_api->where('seq', $seq);
		$this->db_api->update($table, $update_params);
		return $this->db_api->affected_rows();
	}
	public function delete_consume_logs_by_table_type_seq($table, $type, $seq) {
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$table = (is_string($table) ? strtolower($table) : 'categories_logs');
		$type = (is_string($type) ? strtolower($type) : 'lower');
		$sql = sprintf("DELETE FROM %s", $this->db_api->escape_str($table));
		switch (strtolower($type)) {
			case 'upper':
				$sql .= sprintf(" WHERE seq > '%d'", $seq);
			break;
			case 'lower':
			default:
				$sql .= sprintf(" WHERE seq < '%d'", $seq);
			break;
		}
		try {
			$sql_query = $this->db_api->query($sql);
		} catch (Exception $ex) {
			return false;
		}
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}