<?php
if (!defined('BASEPATH')) {
	Exit('No direct script access allowed');
}

class Consume_firebase_database extends MY_Controller {
	public $isAdmin = FALSE;
	public $isMerchant = FALSE;
	protected $DateObject;
	protected $email_vendor;
	public $error = FALSE, $error_msg = array();
	protected $firebase_credential;
	protected $base_config = array();
	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
		# Load Base Config
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		
		$this->load->model('adminpanel/Model_adminpanel', 'mod_adminpanel');
		$this->load->model('adminpanel/Model_firebase', 'mod_firebase');
		$this->load->model('adminpanel/Model_configuration', 'mod_config');
		$this->load->library('adminpanel/Lib_Rc4Crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('3', '4'))) {
			$this->isAdmin = TRUE;
		}
		if (!$this->isAdmin) {
			if (in_array($this->mod_adminpanel->localdata['account_role'], array('2', '5'))) {
				$this->isMerchant = TRUE;
			}
		}
		$this->DateObject = $this->mod_adminpanel->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'));
		$this->email_vendor = $this->base_config['email_vendor'];
		$this->firebase_credential = $this->base_config['firebase_credential'];
		//-----
		$this->load->model('cli/Model_consume', 'mod_consume');
	}
	
	function consume_categories() {
		$collectData = array(
			'page'				=> 'digitale-home',
			'collect'			=> array(),
			'base_home_path'	=> 'home',
			'search_text'		=> '',
			'table'				=> 'consume_firebase',
		);
		try {
			$collectData['collect']['stories_categories'] = $this->mod_consume->consume_stories_categories_data($collectData['search_text'], 0, 100);
		} catch (Exception $ex) {
			$this->error = true;
			$this->error_msg[] = "Exception error while get consume categories from mod-consume: {$ex->getMessage()}";
		}
		if (!$this->error) {
			if (!isset($collectData['collect']['stories_categories']['collect']['consume_categories'])) {
				$this->error = true;
				$this->error_msg[] = "False return from mod-consume.";
			} else {
				if (!is_array($collectData['collect']['stories_categories']['collect']['consume_categories'])) {
					$this->error = true;
					$this->error_msg[] = "Categories consume from mod-consume not in array datatype.";
				}
			}
		}
		if (!$this->error) {
			try {
				$collectData['collect']['firebase_consume'] = $this->mod_consume->get_consume_index_by('categories', 'stories_categories');
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get consume-index data with exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			if (!isset($collectData['collect']['firebase_consume']->seq)) {
				$this->error = true;
				$this->error_msg[] = "No index-data return from database.";
			} else {
				$collectData['collect']['query_params'] = array(
					'consume_datetime'			=> $this->DateObject->format('Y-m-d H:i:s'),
				);
				try {
					$collectData['collect']['query_params']['consume_data'] = json_encode($collectData['collect']['stories_categories']['collect']['consume_categories'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot generate json-encoded for insert-params: {$ex->getMessage()}";
				}
			}
		}
		// INSERT NEW CONSUMED DATA
		if (!$this->error) {
			try {
				$collectData['collect']['insert_logs_seq'] = $this->mod_consume->insert_consume_logs_by_table($collectData['collect']['firebase_consume']->consume_table, $collectData['collect']['query_params']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot insert new consume logs with exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			if ((int)$collectData['collect']['insert_logs_seq'] === 0) {
				$this->error = true;
				$this->error_msg[] = "Return from insert logsis zero value.";
			} else {
				$collectData['update_params'] = array(
					'consume_last_seq'			=> (int)$collectData['collect']['insert_logs_seq'],
					'consume_last_datetime'		=> $this->DateObject->format('Y-m-d H:i:s'),
				);
				try {
					$collectData['collect']['updated_consume_index'] = $this->mod_consume->update_consume_logs_by_table_seq($collectData['table'], $collectData['collect']['firebase_consume']->seq, $collectData['update_params']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot update consume index with exception: {$ex->getMessage()}";
				}
			}
		}
		// Delete old data
		if (!$this->error) {
			if ((int)$collectData['collect']['updated_consume_index'] > 0) {
				try {
					$collectData['collect']['delete_data'] = $this->mod_consume->delete_consume_logs_by_table_type_seq($collectData['collect']['firebase_consume']->consume_table, 'lower', $collectData['update_params']['consume_last_seq']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot delete old logs data with exception: {$ex->getMessage()}";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Consume index seq return zero value.";
			}
		}
		
		if (!$this->error) {
			if ((int)$collectData['collect']['updated_consume_index'] > 0) {
				echo "DONE WITH AFFECTED ROWS\r\n";
			} else {
				echo "NO UPDATE AFFECTED\r\n";
			}
		} else {
			print_r($this->error_msg);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}