<?php
if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
class Lib_Firebase {
	private $CI;
	public $endpoint;
	public $error = FALSE, $error_msg = array();
	public $UA = "Api.Context/UA (By imzers[at]gmail.com)";
	protected $base_config;
	protected $firebase_credential;
	function __construct() {
		$this->CI = &get_instance();
		$this->base_config = $this->CI->config->item('base_config');
		$this->firebase_credential = $this->base_config['firebase_credential'];
		# Set Endpoint
		$this->set_endpoint(base_firebase_url('firebase/'));
		
		# Headers
		$this->set_headers();
		$this->add_headers('Content-type', 'application/json;charset=utf-8');
		$this->add_headers('Authorization', 'Basic ' . base64_encode("{$this->firebase_credential['USER']}:{$this->firebase_credential['TOKEN']}"));
	}
	
	
	
	
	function set_endpoint($endpoint) {
		$this->endpoint = $endpoint;
		return $this;
	}
	
	
	
	
	function get_data_by($by_type, $by_value, $input_params) {
		$action = 'GET';
		$url = $this->endpoint;
		$by_type = (is_string($by_type) ? strtolower($by_type) : '');
		$by_value = (is_string($by_value) ? $by_value : '');
		switch (strtolower($by_type)) {
			case 'collection':
				$url .= "{$by_value}";
			break;
			case 'sequence':
				$url .= "{$by_value}";
			break;
			default:
				// Default
			break;
		}
		$queryString = (isset($input_params['query_string']) ? $input_params['query_string'] : '');
		if (is_array($queryString)) {
			$queryString = http_build_query($queryString);
		}
		if (strlen($queryString) > 0) {
			$url .= "?";
			$url .= $queryString;
		}
		
		
		
		$headers = $this->create_curl_headers($this->headers);
		$params = array();
		
		if (!$this->error) {
			try {
				$get_data = $this->create_curl_request($action, $url, $this->UA, $headers, $params, 30);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Catch exception while doing get-data: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			return $get_data;
		}
	}
	
	//-------------------------------------------------------
	function get_categories_count($queryString = '')  {
		$categories = array();
		$categories_subs = array();
		$get_categories = $this->get_categories();
		$returnData = array();
		//--- Categories
		if ($get_categories['result'] == TRUE) {
			if (isset($get_categories['data'])) {
				if (is_array($get_categories['data']) && (count($get_categories['data']) > 0)) {
					$i = 0;
					foreach ($get_categories['data'] as $key => $val) {
						$categories[$i] = $key;
						$categories_subs[$i] = array();
						if (is_array($val)) {
							if (count($val) > 0) {
								foreach ($val as $subKey => $subVal) {
									$categories_subs[$i][] = $subKey;
								}
							}
						}
						$i += 1;
					}
				}
			}
		}
		$data = array(
			'kategori'			=> $categories,
			'sub-kategori'		=> $categories_subs,
		);
		$data['categories'] = array();
		if (count($data['kategori']) > 0) {
			foreach ($data['kategori'] as $ke => $val) {
				$data['categories'][$ke] = array(
					'title'							=> $val,
					'items'							=> array(),
				);
				if (count($data['sub-kategori'][$ke]) > 0) {
					foreach ($data['sub-kategori'][$ke] as $v) {
						$data['categories'][$ke]['items'][] = $v;
					}
				}
			}
		}
		$queryString = (is_string($queryString) ? strtolower($queryString) : '');
		$data['query_string'] = $queryString;
		if (strlen($queryString) === 0) {
			$data['data'] = $data['categories'];
			$data['count'] = count($data['data']);
		} else {
			$data['data'] = $this->get_search_word($data['query_string'], $data['categories']);
			$data['count'] = count($data['data']);
		}
		return $data;
    }
	function get_categories_data($queryString = '', $page, $limit) {
        $categories = array();
		$categories_subs = array();
		$get_categories = $this->get_categories();
		$returnData = array();
		//--- Categories
		if ($get_categories['result'] == TRUE) {
			if (isset($get_categories['data'])) {
				if (is_array($get_categories['data']) && (count($get_categories['data']) > 0)) {
					$i = 0;
					foreach ($get_categories['data'] as $key => $val) {
						$categories[$i] = $key;
						$categories_subs[$i] = array();
						if (is_array($val)) {
							if (count($val) > 0) {
								foreach ($val as $subKey => $subVal) {
									$categories_subs[$i][] = $subKey;
								}
							}
						}
						$i += 1;
					}
				}
			}
		}
		$data = array(
			'kategori'			=> $categories,
			'sub-kategori'		=> $categories_subs,
		);
		$data['categories'] = array();
		if (count($data['kategori']) > 0) {
			foreach ($data['kategori'] as $ke => $val) {
				$data['categories'][$ke] = array(
					'title'							=> $val,
					'items'							=> array(),
				);
				if (count($data['sub-kategori'][$ke]) > 0) {
					foreach ($data['sub-kategori'][$ke] as $v) {
						$data['categories'][$ke]['items'][] = $v;
					}
				}
			}
		}
		$data['get_categories'] = $get_categories;
		$queryString = (is_string($queryString) ? strtolower($queryString) : '');
		$data['query_string'] = $queryString;
		if (strlen($queryString) === 0) {
			$data['data'] = array_slice($data['categories'], $page, $limit);
			$data['count'] = count($data['data']);
		} else {
			$data['data'] = $this->get_search_word($data['query_string'], $data['categories']);
			$data['data'] = array_slice($data['data'], $page, $limit);
			$data['count'] = count($data['data']);
		}
        return $data;
    }
	function get_categories_data_from_firebase($queryString = '', $page, $limit) {
        $categories = array();
		$categories_subs = array();
		$get_categories = $this->get_categories();
		$returnData = array();
		//--- Categories
		if ($get_categories['result'] == TRUE) {
			if (isset($get_categories['data'])) {
				if (is_array($get_categories['data']) && (count($get_categories['data']) > 0)) {
					$i = 0;
					foreach ($get_categories['data'] as $key => $val) {
						$categories[$i] = $key;
						$categories_subs[$i] = array();
						if (is_array($val)) {
							if (count($val) > 0) {
								foreach ($val as $subKey => $subVal) {
									$categories_subs[$i][] = $subKey;
								}
							}
						}
						$i += 1;
					}
				}
			}
		}
		$data = array(
			'kategori'			=> $categories,
			'sub-kategori'		=> $categories_subs,
		);
		$data['categories'] = array();
		if (count($data['kategori']) > 0) {
			foreach ($data['kategori'] as $ke => $val) {
				$data['categories'][$ke] = array(
					'title'							=> $val,
					'items'							=> array(),
				);
				if (count($data['sub-kategori'][$ke]) > 0) {
					foreach ($data['sub-kategori'][$ke] as $v) {
						$data['categories'][$ke]['items'][] = $v;
					}
				}
			}
		}
		$data['get_categories'] = $get_categories;
		$queryString = (is_string($queryString) ? strtolower($queryString) : '');
		$data['query_string'] = $queryString;
		if (strlen($queryString) === 0) {
			$data['data'] = array_slice($data['categories'], $page, $limit);
			$data['count'] = count($data['data']);
		} else {
			$data['data'] = $this->get_search_word($data['query_string'], $data['categories']);
			$data['data'] = array_slice($data['data'], $page, $limit);
			$data['count'] = count($data['data']);
		}
        return $data;
    }
	function get_categories_data_single($queryString = '') {
		$categories = array();
		$categories_subs = array();
		$get_categories = $this->get_categories();
		$returnData = array();
		//--- Categories
		if ($get_categories['result'] == TRUE) {
			if (isset($get_categories['data'])) {
				if (count($get_categories['data']) > 0) {
					$i = 0;
					foreach ($get_categories['data'] as $key => $val) {
						$categories[$i] = $key;
						$categories_subs[$i] = array();
						if (is_array($val)) {
							if (count($val) > 0) {
								foreach ($val as $subKey => $subVal) {
									$categories_subs[$i][] = $subKey;
								}
							}
						}
						$i += 1;
					}
				}
			}
		}
		$data = array(
			'kategori'			=> $categories,
			'sub-kategori'		=> $categories_subs,
		);
		$data['categories'] = array();
		if (count($data['kategori']) > 0) {
			$for_i = 0;
			foreach ($data['kategori'] as $ke => $val) {
				$data['categories'][$for_i] = array(
					'title'							=> $val,
					'items'							=> array(),
				);
				if (count($data['sub-kategori'][$ke]) > 0) {
					foreach ($data['sub-kategori'][$ke] as $v) {
						$data['categories'][$for_i]['items'][] = $v;
					}
				}
				$for_i += 1;
			}
		}
		$data['get_categories'] = $get_categories;
		$queryString = (is_string($queryString) ? strtolower($queryString) : '');
		$data['query_string'] = $queryString;
		if (strlen($queryString) === 0) {
			$data['data'] = (isset($data['categories'][0]) ? $data['categories'][0] : null);
			$data['count'] = count($data['data']);
		} else {
			$data['categories'] = $this->get_search_word($data['query_string'], $data['categories']);
			$data['data'] = array();
			if (count($data['categories']) > 0) {
				$for_i = 0;
				foreach ($data['categories'] as $keval) {
					$data['data'][] = $keval;
				}
			}
			$data['data'] = (isset($data['data'][0]) ? $data['data'][0] : null);
			$data['count'] = count($data['data']);
		}
        return $data;
	}
	
	
	
	function get_subcategories_data($queryString = '') {
        $categories = array();
		$categories_subs = array();
		$get_categories = $this->get_categories_subs($queryString);
		$returnData = array();
		
		
		return $get_categories;
		
		
		
		//--- Categories
		if ($get_categories['result'] == TRUE) {
			if (isset($get_categories['data'])) {
				if (count($get_categories['data']) > 0) {
					$i = 0;
					foreach ($get_categories['data'] as $key => $val) {
						$categories[$i] = $key;
						$categories_subs[$i] = array();
						if (is_array($val)) {
							if (count($val) > 0) {
								foreach ($val as $subKey => $subVal) {
									$categories_subs[$i][] = $subKey;
								}
							}
						}
						$i += 1;
					}
				}
			}
		}
		$data = array(
			'kategori'			=> $categories,
			'sub-kategori'		=> $categories_subs,
		);
		$data['categories'] = array();
		if (count($data['kategori']) > 0) {
			foreach ($data['kategori'] as $ke => $val) {
				$data['categories'][$ke] = array(
					'title'							=> $val,
					'items'							=> array(),
				);
				if (count($data['sub-kategori'][$ke]) > 0) {
					foreach ($data['sub-kategori'][$ke] as $v) {
						$data['categories'][$ke]['items'][] = $v;
					}
				}
			}
		}
		$data['get_categories'] = $get_categories;
		$queryString = (is_string($queryString) ? strtolower($queryString) : '');
		$data['query_string'] = $queryString;
		if (strlen($queryString) === 0) {
			$data['data'] = array_slice($data['categories'], $page, $limit);
			$data['count'] = count($data['data']);
		} else {
			$data['data'] = $this->get_search_word($data['query_string'], $data['categories']);
			$data['data'] = array_slice($data['data'], $page, $limit);
			$data['count'] = count($data['data']);
		}
        return $data;
    }
	
	function get_categories() {
		$error = FALSE;
		$error_msg = array();
		//-----
		if (!$error) {
			try {
				$collection_data = $this->get_data_by('collection', 'kategori', array());
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Error exception while get-data: {$ex->getMessage()}";
			}
		}
		if (!$error) {
			if (!isset($collection_data['response']['body'])) {
				$error = true;
				$error_msg[] = "There is no body index on collection-data";
			}
		}
		if (!$error) {
			try {
				$response_body = json_decode($collection_data['response']['body'], true);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Error cannot decode body of curl-response: {$ex->getMessage()}";
			}
		}
		
		if (!$error) {
			return array(
				'result'			=> true,
				'data'				=> $response_body,
				'error'				=> false,
				'curl'				=> $collection_data,
			);
		} else {
			return array(
				'result'			=> false,
				'data'				=> null,
				'error'				=> $error_msg,
				'curl'				=> $collection_data,
			);
		}
	}
	function get_categories_subs($queryString = '') {
		$queryString = (is_string($queryString) ? $queryString : '');
		$error = FALSE;
		$error_msg = array();
		$queryParams = array(
			'query_string'			=> array(),
		);
		//-----
		if (!$error) {
			if (strlen($queryString) > 0) {
				$queryParams['query_string']["kategori"] = urlencode($queryString);
			}
		}
		if (!$error) {
			try {
				$collection_data = $this->get_data_by('collection', 'kategori', $queryParams);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Error exception while get-data: {$ex->getMessage()}";
			}
		}
		if (!$error) {
			if (!isset($collection_data['response']['body'])) {
				$error = true;
				$error_msg[] = "There is no body index on collection-data";
			}
		}
		if (!$error) {
			try {
				$response_body = json_decode($collection_data['response']['body'], true);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Error cannot decode body of curl-response: {$ex->getMessage()}";
			}
		}
		
		if (!$error) {
			return array(
				'result'			=> true,
				'data'				=> $response_body,
				'error'				=> false,
				'curl'				=> $collection_data,
			);
		} else {
			return array(
				'result'			=> false,
				'data'				=> null,
				'error'				=> $error_msg,
				'curl'				=> $collection_data,
			);
		}
	}
	//==========================================================
	function get_stories_data_single($queryString = '') {
		$stories = array();
		$stories_files = array();
		$get_stories = $this->get_stories_single($queryString);
		$returnData = array();
		//--- Stories
		if ($get_stories['result'] == TRUE) {
			if (isset($get_stories['data'])) {
				if (count($get_stories['data']) > 0) {
					$i = 0;
					$stories[$i] = $get_stories['data'];
					$stories_files[$i] = array();
					if (isset($get_stories['data']['files'])) {
						if (is_array($get_stories['data']['files']) && (count($get_stories['data']['files']) > 0)) {
							foreach ($get_stories['data']['files'] as $fileKey => &$fileVal) {
								$fileVal['key'] = $fileKey;
								$stories_files[$i][] = $fileVal;
							}
						}
					}
				}
			}
		}
		$data = array(
			'cerita'			=> $stories,
			'cerita-file'		=> $stories_files,
		);
		$data['stories'] = array();
		if (count($data['cerita']) > 0) {
			$for_i = 0;
			foreach ($data['cerita'] as $ke => $val) {
				$data['stories'][$for_i] = array(
					'title'							=> $val,
					'items'							=> array(),
				);
				if (count($data['cerita-file'][$ke]) > 0) {
					foreach ($data['cerita-file'][$ke] as $v) {
						$data['stories'][$for_i]['items'][] = $v;
					}
				}
				$for_i += 1;
			}
		}
		$data['get_stories'] = $get_stories;
		$queryString = (is_string($queryString) ? strtolower($queryString) : '');
		$data['query_string'] = $queryString;
		
		$data['data'] = (isset($data['stories'][0]) ? $data['stories'][0] : null);
		$data['count'] = count($data['data']);
        return $data;
	}
	function get_stories() {
		$error = FALSE;
		$error_msg = array();
		//-----
		if (!$error) {
			try {
				$collection_data = $this->get_data_by('collection', 'cerita', array());
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Error exception while get-data stories: {$ex->getMessage()}";
			}
		}
		if (!$error) {
			if (!isset($collection_data['response']['body'])) {
				$error = true;
				$error_msg[] = "There is no body index on collection-data of stories";
			}
		}
		if (!$error) {
			try {
				$response_body = json_decode($collection_data['response']['body'], true);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Error cannot decode body of curl-response stories: {$ex->getMessage()}";
			}
		}
		
		if (!$error) {
			return array(
				'result'			=> true,
				'data'				=> $response_body,
				'error'				=> false,
				'curl'				=> $collection_data,
			);
		} else {
			return array(
				'result'			=> false,
				'data'				=> null,
				'error'				=> $error_msg,
				'curl'				=> $collection_data,
			);
		}
	}
	function get_stories_single($stories_seq = 0) {
		$error = FALSE;
		$error_msg = array();
		//-----
		if (!$error) {
			$query_params = array(
				'query_string'			=> array(
					'cerita'					=> $stories_seq
				),
			);
		}
		if (!$error) {
			try {
				$collection_data = $this->get_data_by('sequence', 'cerita', $query_params);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Error exception while get-data stories: {$ex->getMessage()}";
			}
		}
		if (!$error) {
			if (!isset($collection_data['response']['body'])) {
				$error = true;
				$error_msg[] = "There is no body index on collection-data of stories";
			}
		}
		if (!$error) {
			try {
				$response_body = json_decode($collection_data['response']['body'], true);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Error cannot decode body of curl-response stories: {$ex->getMessage()}";
			}
		}
		
		
		if (!$error) {
			return array(
				'result'			=> true,
				'data'				=> $response_body,
				'error'				=> false,
				'curl'				=> $collection_data,
			);
		} else {
			return array(
				'result'			=> false,
				'data'				=> null,
				'error'				=> $error_msg,
				'curl'				=> $collection_data,
			);
		}
	}
	function get_stories_count($queryString = '')  {
		$categories = array();
		$categories_subs = array();
		$get_stories = $this->get_stories();
		$returnData = array();
		$stories = array();
		//--- Categories
		if ($get_stories['result'] == TRUE) {
			if (isset($get_stories['data'])) {
				if (count($get_stories['data']) > 0) {
					$i = 0;
					foreach ($get_stories['data'] as $key => $val) {
						$stories[$i] = $val;
						if (isset($val['files'])) {
							if (is_array($val['files']) && (count($val['files']) > 0)) {
								foreach ($val['files'] as $fileKey => $fileVal) {
									unset($val['files'][$fileKey]);
									if (is_string($fileVal) && (strlen($fileVal) > 0)) {
										$val['files'][] = $fileVal;
									}
								}
							}
						}
						$i += 1;
					}
				}
			}
		}
		$data = array(
			'cerita'			=> $stories,
		);
		$data['stories'] = array();
		if (count($data['cerita']) > 0) {
			foreach ($data['cerita'] as $ke => $val) {
				$data['stories'][$ke] = array(
					'title'							=> (isset($val['judul']) ? $val['judul'] : ''),
					'items'							=> (isset($val['files']) ? $val['files'] : array()),
					'content'						=> $val,
				);
			}
		}
		$queryString = (is_string($queryString) ? strtolower($queryString) : '');
		$data['query_string'] = $queryString;
		if (strlen($queryString) === 0) {
			$data['data'] = $data['stories'];
			$data['count'] = count($data['stories']);
		} else {
			$data['data'] = $this->get_search_word($data['query_string'], $data['stories']);
			$data['count'] = count($data['data']);
		}
		return $data;
    }
	function get_stories_data($queryString = '', $page, $limit) {
        $categories = array();
		$categories_subs = array();
		$get_stories = $this->get_stories();
		$returnData = array();
		$stories = array();
		//--- Stories
		if ($get_stories['result'] == TRUE) {
			if (isset($get_stories['data'])) {
				if (count($get_stories['data']) > 0) {
					$i = 0;
					foreach ($get_stories['data'] as $key => &$val) {
						if (is_array($val)) {
							$val['index_key'] = $key;
						}
						/*
						if (isset($val['files'])) {
							if (is_array($val['files']) && (count($val['files']) > 0)) {
								foreach ($val['files'] as $fileKey => $fileVal) {
									unset($val['files'][$fileKey]);
									if (is_string($fileVal) && (strlen($fileVal) > 0)) {
										$val['files'][] = $fileVal;
									}
								}
							}
						}
						*/
						$stories[$i] = $val;
						$i += 1;
					}
				}
			}
		}
		$data = array(
			'cerita'			=> $stories,
		);
		$data['stories'] = array();
		if (count($data['cerita']) > 0) {
			foreach ($data['cerita'] as $ke => $val) {
				$data['stories'][$ke] = array(
					'title'							=> (isset($val['judul']) ? $val['judul'] : ''),
					'items'							=> (isset($val['files']) ? $val['files'] : array()),
					'content'						=> $val,
				);
			}
		}
		$data['get_stories'] = $get_stories;
		$queryString = (is_string($queryString) ? strtolower($queryString) : '');
		$data['query_string'] = $queryString;
		if (strlen($queryString) === 0) {
			$data['data'] = array_slice($data['stories'], $page, $limit);
			$data['count'] = count($data['data']);
		} else {
			$data['data'] = $this->get_search_word($data['query_string'], $data['stories']);
			$data['data'] = array_slice($data['data'], $page, $limit);
			$data['count'] = count($data['data']);
		}
        return $data;
    }
	//=========================================================
	// Search words inside array-object
	function get_search_word($searchword, $array_object) {
		if (!is_array($array_object)) {
			return false;
		}
		if (count($array_object) === 0) {
			return false;
		}
		$matches = array();
		foreach($array_object as $k => $v) {
			if (isset($v['title'])) {
				if (preg_match("/\b{$searchword}\b/i", $v['title'])) {
					$matches[$k] = $v;
				}
			}
		}
		return $matches;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	//=======================================================================================================================
	function create_curl_request($action, $url, $UA, $headers = null, $params = array(), $timeout = 30) {
		$cookie_file = (dirname(__FILE__).'/cookies.txt');
		$url = str_replace( "&amp;", "&", urldecode(trim($url)) );
		$ch = curl_init();
		switch (strtolower($action)) {
			case 'get':
				if ((is_array($params)) && (count($params) > 0)) {
					$Querystring = http_build_query($params);
					$url .= "?";
					$url .= $Querystring;
				}
			break;
			case 'post':
			default:
				$url .= "";
			break;
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		if ($headers != null) {
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		} else {
			curl_setopt($ch, CURLOPT_HEADER, false);
		}
		curl_setopt($ch, CURLOPT_USERAGENT, $UA);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_COOKIE, $cookie_file);
		//curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		$post_fields = NULL;
		switch (strtolower($action)) {
			case 'get':
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			break;
			case 'put':
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			break;
			case 'delete':
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
			break;
			case 'post':
			default:
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			break;
		}
		switch (strtolower($action)) {
			case 'get':
				curl_setopt($ch, CURLOPT_POST, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, null);
			break;
			case 'put':
			case 'post':
			default:
				if ((is_array($params)) && (count($params) > 0) && (is_array($headers) && count($headers) > 0)) {
					foreach ($headers as $heval) {
						$getContentType = explode(":", $heval);
						if (strtolower($getContentType[0]) !== 'content-type') {
							continue;
						}
						switch (strtolower(trim($getContentType[0]))) {
							case 'content-type':
								if (isset($getContentType[1])) {
									if (is_string($getContentType[1])) {
										if (strpos('application/xml', strtolower(trim($getContentType[1]))) !== FALSE) {
											$post_fields = $post_fields;
										} else if (strpos('application/json', strtolower(trim($getContentType[1]))) !== FALSE) {
											if (is_array($params)) {
												$post_fields = json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
											} else {
												$post_fields = $params;
											}
										} else if (strpos('application/x-www-form-urlencoded', strtolower(trim($getContentType[1]))) !== FALSE) {
											$post_fields = http_build_query($params);
										} else if (strpos('multipart/form-data', strtolower(trim($getContentType[1]))) !== FALSE) {
											$post_fields = http_build_query($params);
										} else {
											$post_fields = http_build_query($params);
										}
									}
								}
							break;
							default:
								$post_fields = http_build_query($params);
							break;
						}
					}
				} else if ((!empty($params)) || ($params != '')) {
					$post_fields = $params;
				}
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
			break;
		}
		// Get Response
		$response = curl_exec($ch);
		$mixed_info = curl_getinfo($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$header_string = substr($response, 0, $header_size);
		$header_content = $this->get_headers_from_curl_response($header_string);
		$header_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if (count($header_content) > 1) {
			$header_content = end($header_content);
		}
		$body = substr($response, $header_size);
		curl_close ($ch);
		$return = array(
			'request'		=> array(
				'method'			=> $action,
				'host'				=> $url,
				'header'			=> $headers,
				'body'				=> $post_fields,
			),
			'response'		=> array(),
		);
		if (!empty($response) || $response != '') {
			$return['response']['code'] = (int)$header_code;
			$return['response']['header'] = array(
				'size' => $header_size, 
				'string' => $header_string,
				'content' => $header_content,
			);
			$return['response']['body'] = $body;
			return $return;
		}
		return false;
	}
	private static function get_headers_from_curl_response($headerContent) {
		$headers = array();
		// Split the string on every "double" new line.
		$arrRequests = explode("\r\n\r\n", $headerContent);
		// Loop of response headers. The "count($arrRequests) - 1" is to 
		// avoid an empty row for the extra line break before the body of the response.
		for ($index = 0; $index < (count($arrRequests) - 1); $index++) {
			foreach (explode("\r\n", $arrRequests[$index]) as $i => $line) {
				if ($i === 0) {
					$headers[$index]['http_code'] = $line;
				} else {
					list ($key, $value) = explode(': ', $line);
					$headers[$index][$key] = $value;
				}
			}
		}
		return $headers;
	}
	public function create_curl_headers($headers = array()) {
		$headers_keys = array();
		$curlheaders = array();
		$i = 0;
		foreach ($headers as $ke => $val) {
			if (!in_array($ke, $headers_keys)) {
				$curlheaders[$i] = "{$ke}: {$val}";
			}
			$headers_keys[] = $ke;
			$i++;
		}
		$new_i = ($i + 1);
		return $curlheaders;
	}
	//------- utils
	function sanitize_file_name( $filename ) {
		$filename_raw = $filename;
		$special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}");
		foreach ($special_chars as $chr) {
			$filename = str_replace($chr, '', $filename);
		}
		$filename = preg_replace('/[\s-]+/', '-', $filename);
		$filename = trim($filename, '.-_');
		$filename;
	}
	function sanitize_url_parameter($params_input = array()) {
		$sanitized = [];
		if (count($params_input) > 0) {
			foreach ($params_input as $key => $keval) {
				if (!is_array($keval) || (!is_object($keval))) {
					//$keval = filter_var($keval, FILTER_SANITIZE_STRING);
					$keval = filter_var($keval, FILTER_SANITIZE_URL);
				}
				$sanitized[$key] = $keval;
			}
		}
		return $sanitized;
	}
	//------
	function set_headers($headers = array()) {
		$this->headers = $headers;
		return $this;
	}
	function reset_headers() {
		$this->headers = null;
		return $this;
	}
	function add_headers($key, $val) {
		if (!isset($this->headers)) {
			$this->headers = $this->get_firebase_headers();
		}
		if (!isset($this->headers[$key])) {
			$add_header = array($key => $val);
			$this->headers = array_merge($add_header, $this->headers);
		} else {
			unset($this->headers[$key]);
			$add_header = array($key => $val);
			$this->headers = array_merge($add_header, $this->headers);
		}
	}
	function get_firebase_headers() {
		return $this->headers;
	}
	// Utils
	function permalink($url) {
		$url = strtolower($url);
		$url = preg_replace('/&.+?;/', '', $url);
		$url = preg_replace('/\s+/', '_', $url);
		$url = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '_', $url);
		$url = preg_replace('|%|', '_', $url);
		$url = preg_replace('/&#?[a-z0-9]+;/i', '', $url);
		$url = preg_replace('/[^%A-Za-z0-9 \_\-]/', '_', $url);
		$url = preg_replace('|_+|', '-', $url);
		$url = preg_replace('|-+|', '-', $url);
		$url = trim($url, '-');
		$url = (strlen($url) > 128) ? substr($url, 0, 128) : $url;
		return $url;
	}
}




