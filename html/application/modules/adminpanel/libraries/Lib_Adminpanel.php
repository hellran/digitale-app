<?php
if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
class Lib_Adminpanel {
	private static $instance;
	protected $CI;
	public $db_dashboard;
	public $db_report;
	private $databases = array();
	public $userdata;
	public $localdata;
	public function __construct($configs = array()) {
		$this->CI =& get_instance();
		$this->CI->load->helper('base/base_config');
		$this->CI->load->helper('base/base_query');
		$this->db_dashboard = $this->CI->load->database('dashboard', TRUE);
		$this->db_report = $this->CI->load->database('report', TRUE);
		//$this->databases['version'] = $this->get_database('version');
		$this->userdata_start();
	}
	public static function get_instance($configs = array()) {
        if (!self::$instance) {
            self::$instance = new Lib_Adminpanel($configs);
        }
        return self::$instance;
    }
	public function get_database($database_name) {
		$sql = "SELECT db_host, db_port, db_user, db_pass, db_name FROM dashboard_databases WHERE LOWER(database_name) = LOWER('{$database_name}')";
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
	}
	function userdata_start() {
		$this->userdata = (($this->CI->session->userdata('gg_login_account') != FALSE) ? $this->get_login_data($this->CI->session->userdata('gg_login_account')) : NULL);
		$this->localdata = (isset($this->userdata->local_seq) ? $this->get_login_data_userdata($this->userdata->local_seq) : NULL);
	}
	//--
	public static function db_connect($db_params) {
		$instance = self::get_instance();
		return $instance->CI->load->database($db_params, TRUE);
	}
	//------------------------------------------------------------------
	
	function get_login_data($uid, $login_server = null) {
		if (!$login_server) {
			$login_server = 'goodgames';
		}
		$loginTable = "gg_account";
		switch (strtolower($login_server)) {
			case 'local':
			case 'localhost':
				$loginTable = "local_account";
			break;
			case 'goodgames':
			default:
				$loginTable = "gg_account";
			break;
		}
		$this->db_dashboard->select('*');
		$this->db_dashboard->from($loginTable);
		if (is_numeric($uid)) {
			$this->db_dashboard->where('seq', $uid);
		} else if (is_string($uid)) {
			switch (strtolower($login_server)) {
				case 'local':
				case 'localhost':
					$this->db_dashboard->where('LOWER(account_email)', strtolower($uid));
				break;
				case 'goodgames':
				default:
					$this->db_dashboard->where('LOWER(login_username)', strtolower($uid));
				break;
			}
		}
		//$sql_wheres = array('login_username' => $uid);
		return $this->db_dashboard->get()->row();
	}
	function get_login_data_userdata($local_seq) {
		$local_seq = (is_numeric($local_seq) ? (int)$local_seq : 0);
		if ((int)$local_seq === 0) {
			return false;
		}
		$sql = sprintf("SELECT a.*, r.role_id, r.role_code, r.role_name FROM local_account AS a LEFT JOIN dashboard_roles AS r ON r.role_id = a.account_role WHERE (a.seq = '%d') LIMIT 1",
			$local_seq
		);
		try {
			$sql_query = $this->db_dashboard->query($sql);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		if (count($sql_query->result_array()) > 0) {
			foreach ($sql_query->result_array() as $keval) {
				return $keval;
			}
		}
	}
	function update_gg_login_account($account_seq) {
		$account_params = array(
			'login_datetime_first'			=> NULL,
		);
		$this->db_dashboard->where('seq', $account_seq);
		$this->db_dashboard->set('login_datetime_last', 'NOW()', FALSE);
		$this->db_dashboard->update('gg_account');
	}
	function update_local_login_account($account_seq) {
		$this->db_dashboard->where('seq', $account_seq);
		$this->db_dashboard->set('login_datetime', 'NOW()', FALSE);
		$this->db_dashboard->update('local_account');
	}
	//-------------------
	// Utils
	//-------------------
	function create_unique_datetime($timezone = "Asia/Bangkok") {
		$microtime = microtime(true);
		$micro = sprintf("%06d",($microtime - floor($microtime)) * 1000000);
		$DateObject = new DateTime(date("Y-m-d H:i:s.{$micro}", $microtime));
		$DateObject->setTimezone(new DateTimeZone($timezone));
		return $DateObject->format('YmdHisu');
	}
	function create_dateobject($timezone, $format, $date) {
		$DateObject = new DateTime();
		$DateObject->setTimezone(new DateTimeZone($timezone));
		$DateObject->createFromFormat($format, $date);
		return $DateObject;
	}
	
	
	//-------------------------------------------------------------------------------------------
	// Pagination
	public function generate_listeo_pagination_dashboard($self, &$page, $per_page, $rows_count, &$start) {
		$sum_pages = ceil($rows_count / $per_page);
		if ($sum_pages < 2) { $sum_pages = 1; }
		$page = isset($page) ? intval($page) : 1;
		if ($page < 1 || $page > $sum_pages) { $page = 1; }
		$start = ceil(($page * $per_page) - $per_page);
		$page_display = "";

		$page_display .= '<ul>';
		$page_display .= '<li class="arrow first">' . '<a href="' . sprintf($self, ($sum_pages / $sum_pages)) . '" rel="first"><i class="fa fa-angle-double-left"></i></a></li>';
		if ($sum_pages <= 0) {
			if ($page > 1) {
				$page_display .= '<li><a href="' . sprintf($self, ($page - 1)) . '"><i class="sl sl-icon-arrow-left"></i></a></li>';
			} else {
				$page_display .= '<li><a href="#"><i class="sl sl-icon-arrow-left"></i></a></li>';
			}
			$i = 1;
			while ($i <= $sum_pages) {
				$page_display .= '<li><a href="' . sprintf($self, $i) . '" class="current-page">' . $i . '</a></li>';
				$i++;
			}
			$page_display .= '<li><a href="' . sprintf($self, ($page + 1)) . '"><i class="sl sl-icon-arrow-right"></i></a></li>';
		} else {
			if ($page > 1) {
				$page_display .= '<li><a href="' . sprintf($self, ($page - 1)) . '"><i class="sl sl-icon-arrow-left"></i></a></li>';
			} else {
				$page_display .= '<li><a href="#"><i class="sl sl-icon-arrow-left"></i></a></li>';
			}
			for ($i = ($page - 2); $i < $page; $i++) {
				if ($i > 0) {
					$page_display .= '<li><a href="' . sprintf($self, $i) . '">' . $i . '</a></li>';
				}
			}
			$page_display .= '<li><a class="current-page" href="' . sprintf($self, $i) . '">' . $page . '</a></li>';
			for ($i = ($page + 1); $i <= ($page + 2); $i++) {
				if ($i <= $sum_pages) {
					$page_display .= '<li><a href="' . sprintf($self, $i) . '">' . $i . '</a></li>';
				}
			}
			if (($page + 1) > $sum_pages) {
				$page_display .= '<li><span><i class="sl sl-icon-arrow-right"></i></span></li>';
			} else {
				$page_display .= '<li><a href="' . sprintf($self, ($page + 1)) . '"><i class="sl sl-icon-arrow-right"></i></a></li>';
			}
		}
		$page_display .= '<li class="arrow last"><a href="' . sprintf($self, $sum_pages) . '" rel="last"><i class="fa fa-angle-double-right"></i></a></li>';
		$page_display .= '</ul>';
		return $page_display;
	}
	public function generate_listeo_pagination_home($self, &$page, $per_page, $rows_count, &$start) {
		$sum_pages = ceil($rows_count / $per_page);
		if ($sum_pages < 2) { $sum_pages = 1; }
		$page = isset($page) ? intval($page) : 1;
		if ($page < 1 || $page > $sum_pages) { $page = 1; }
		$start = ceil(($page * $per_page) - $per_page);
		$page_display = "";
		$page_display .= '<ul>';
		$page_display .= '<li class="arrow first">' . '<a href="' . sprintf($self, ($sum_pages / $sum_pages)) . '" rel="first"><i class="fa fa-angle-double-left"></i></a></li>';
		if ($sum_pages <= 0) {
			if ($page > 1) {
				$page_display .= '<li><a href="' . sprintf($self, ($page - 1)) . '"><i class="sl sl-icon-arrow-left"></i></a></li>';
			} else {
				$page_display .= '<li><a href="#"><i class="sl sl-icon-arrow-left"></i></a></li>';
			}
			$i = 1;
			while ($i <= $sum_pages) {
				$page_display .= '<li><a href="' . sprintf($self, $i) . '" class="current-page">' . $i . '</a></li>';
				$i++;
			}
			$page_display .= '<li><a href="' . sprintf($self, ($page + 1)) . '"><i class="sl sl-icon-arrow-right"></i></a></li>';
		} else {
			if ($page > 1) {
				$page_display .= '<li><a href="' . sprintf($self, ($page - 1)) . '"><i class="sl sl-icon-arrow-left"></i></a></li>';
			} else {
				$page_display .= '<li><a href="#"><i class="sl sl-icon-arrow-left"></i></a></li>';
			}
			for ($i = ($page - 2); $i < $page; $i++) {
				if ($i > 0) {
					$page_display .= '<li><a href="' . sprintf($self, $i) . '">' . $i . '</a></li>';
				}
			}
			$page_display .= '<li><a class="current-page" href="' . sprintf($self, $i) . '">' . $page . '</a></li>';
			for ($i = ($page + 1); $i <= ($page + 2); $i++) {
				if ($i <= $sum_pages) {
					$page_display .= '<li><a href="' . sprintf($self, $i) . '">' . $i . '</a></li>';
				}
			}
			if (($page + 1) > $sum_pages) {
				$page_display .= '<li><span><i class="sl sl-icon-arrow-right"></i></span></li>';
			} else {
				$page_display .= '<li><a href="' . sprintf($self, ($page + 1)) . '"><i class="sl sl-icon-arrow-right"></i></a></li>';
			}
		}
		$page_display .= '<li class="arrow last"><a href="' . sprintf($self, $sum_pages) . '" rel="last"><i class="fa fa-angle-double-right"></i></a></li>';
		$page_display .= '</ul>';
		return $page_display;
	}
}



