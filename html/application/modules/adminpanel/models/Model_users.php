<?php
if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
class Model_users extends CI_Model{
	protected $db_dashboard;
	protected $db_report;
	private $databases = array();
	public $userdata;
	public $localdata;
	function __construct() {
		parent::__construct();
		$this->load->library('Lib_Adminpanel', array(), 'adminpanel');
		$this->db_dashboard = $this->adminpanel->db_dashboard;
		$this->db_report = $this->adminpanel->db_report;
		//$this->databases['version'] = $this->adminpanel->get_database('version');
		$this->userdata = $this->adminpanel->userdata;
		$this->localdata = $this->adminpanel->localdata;
	}
	private function get_database($database_name) {
		$sql = "SELECT db_host, db_port, db_user, db_pass, db_name FROM dashboard_databases WHERE LOWER(database_name) = LOWER('{$database_name}')";
		$sql_query = $this->db_report->query($sql);
		return $sql_query->result();
	}
	function userdata_start() {
		$this->userdata = (($this->session->userdata('gg_login_account') != FALSE) ? $this->get_login_data($this->session->userdata('gg_login_account')) : NULL);
	}
	//--------------------------------------------
	function get_login_data($uid, $login_server = null) {
		if (!$login_server) {
			$login_server = 'goodgames';
		}
		$loginTable = "gg_account";
		switch (strtolower($login_server)) {
			case 'local':
				$loginTable = "local_account";
			break;
			case 'goodgames':
			default:
				$loginTable = "gg_account";
			break;
		}
		$this->db_dashboard->select('*');
		$this->db_dashboard->from($loginTable);
		if (is_numeric($uid)) {
			$this->db_dashboard->where('seq', $uid);
		} else if (is_string($uid)) {
			$this->db_dashboard->where('LOWER(login_username)', strtolower($uid));
		}
		//$sql_wheres = array('login_username' => $uid);
		return $this->db_dashboard->get()->row();
	}
	function get_login_data_userdata($local_seq) {
		$local_seq = (is_numeric($local_seq) ? (int)$local_seq : 0);
		if ((int)$local_seq === 0) {
			return false;
		}
		$sql = sprintf("SELECT a.*, r.role_id, r.role_code, r.role_name FROM local_account AS a LEFT JOIN dashboard_roles AS r ON r.role_id = a.account_role WHERE (a.seq = '%d') LIMIT 1",
			$local_seq
		);
		try {
			$sql_query = $this->db_dashboard->query($sql);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		if (count($sql_query->result_array()) > 0) {
			foreach ($sql_query->result_array() as $keval) {
				return $keval;
			}
		}
	}
	//-------------------------------------------------------
	function userListingCount($searchText = '')  {
		$this->db_dashboard->select('COUNT(account.seq) AS total_accounts');
        $this->db_dashboard->from('local_account AS account');
		$this->db_dashboard->join('dashboard_roles AS role', 'role.seq = account.account_role', 'LEFT');
        if (!empty($searchText)) {
			if (is_string($searchText)) {
				$searchLike = "(
					(CONCAT('', account.account_username, '')  LIKE '%{$searchText}%')
					OR (CONCAT('', account.account_email, '') LIKE '%{$searchText}%')
					OR (CONCAT('', account.account_fullname, '') LIKE '%{$searchText}%')
				)";
            $this->db_dashboard->where($searchLike);
			}
        }
        $this->db_dashboard->where('account.account_delete_status', 0);
        $this->db_dashboard->where('account.account_role <', 4);
		return $this->db_dashboard->get()->row();
    }
	function userListing($searchText = '', $page, $segment) {
        $this->db_dashboard->select('account.*, role.role_id, role.role_code, role.role_name');
        $this->db_dashboard->from('local_account AS account');
		$this->db_dashboard->join('dashboard_roles AS role', 'role.seq = account.account_role', 'LEFT');
        if (!empty($searchText)) {
			if (is_string($searchText)) {
				$searchLike = "(
					(CONCAT('', account.account_username, '')  LIKE '%{$searchText}%')
					OR (CONCAT('', account.account_email, '') LIKE '%{$searchText}%')
					OR (CONCAT('', account.account_fullname, '') LIKE '%{$searchText}%')
				)";
				$this->db_dashboard->where($searchLike);
			}
        }
        $this->db_dashboard->where('account.account_delete_status', 0);
        $this->db_dashboard->where('account.account_role <', 4);
        $this->db_dashboard->limit($page, $segment);
        $query = $this->db_dashboard->get();
        
        return $query->result(); 
    }
	//-----------------------------------------------------------------------------------------
	function get_dashboard_roles() {
		$this->db_dashboard->select('DISTINCT role_id AS role_id, role_code, role_name', FALSE);
		$this->db_dashboard->from('dashboard_roles');
		$this->db_dashboard->where('role_id <', 4);
		$this->db_dashboard->order_by('role_name', 'ASC');
		$sql_query = $this->db_dashboard->get();
		return $sql_query->result();
	}
	function get_local_user_by($by_value, $by_type = null) {
		if (!isset($by_type)) {
			$by_type = 'email';
		}
		$sql_wheres = array();
		switch (strtolower($by_type)) {
			case 'email':
			default:
				$sql_wheres['account_email'] = $by_value;
			break;
			case 'username':
				$sql_wheres['account_username'] = $by_value;
			break;
			case 'seq':
			case 'id':
				if (is_numeric($by_value)) {
					$sql_wheres['seq'] = $by_value;
				} else {
					$sql_wheres['account_username'] = $by_value;
				}
			break;
			case 'activation':
				if (is_string($by_value)) {
					$sql_wheres['account_activation_code'] = $by_value;
				}
			break;
		}
		$this->db_dashboard->select('*');
		$this->db_dashboard->from('local_account');
		$this->db_dashboard->where($sql_wheres);
		$sql_query = $this->db_dashboard->get();
		return $sql_query->result();
	}
	function get_local_user_match_by($by_value, $by_type = null, $seq = 0) {
		if (!isset($by_type)) {
			$by_type = 'email';
		}
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$sql_wheres = array();
		switch (strtolower($by_type)) {
			case 'email':
			default:
				$sql_wheres['account_email'] = $by_value;
			break;
			case 'username':
				$sql_wheres['account_username'] = $by_value;
			break;
			case 'seq':
			case 'id':
				if (is_numeric($by_value)) {
					$sql_wheres['seq'] = $by_value;
				} else {
					$sql_wheres['account_username'] = $by_value;
				}
			break;
		}
		$this->db_dashboard->select('*');
		$this->db_dashboard->from('local_account');
		$this->db_dashboard->where('seq !=', $seq);
		$this->db_dashboard->where($sql_wheres);
		$sql_query = $this->db_dashboard->get();
		return $sql_query->result();
	}
	function add_user($input_params) {
		$this->db_dashboard->trans_start();
        $this->db_dashboard->insert('local_account', $input_params);
        $db_insert_id = $this->db_dashboard->insert_id();
        $this->db_dashboard->trans_complete();
        return (int)$db_insert_id;
	}
	function edit_user($seq, $input_params) {
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$this->db_dashboard->where('seq', $seq);
		$this->db_dashboard->update('local_account', $input_params);
        return (int)$seq;
	}
	function insert_local_user_properties($seq, $input_params) {
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$query_params = array(
			'local_seq'					=> $seq,
			'properties_key'			=> (isset($input_params['properties_key']) ? $input_params['properties_key'] : ''),
			'properties_value'			=> (isset($input_params['properties_value']) ? $input_params['properties_value'] : ''),
			'properties_datetime'		=> $this->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'))->format('Y-m-d H:i:s'),
		);
		$query_params['properties_key'] = substr($query_params['properties_key'], 0, 64);
		$this->db_dashboard->trans_start();
		$this->db_dashboard->insert('local_account_properties', $query_params);
		$db_insert_id = $this->db_dashboard->insert_id();
		$this->db_dashboard->trans_complete();
		return (int)$db_insert_id;
	}
	function update_local_user_properties($seq, $input_params) {
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$query_params = array(
			'local_seq'					=> $seq,
			'properties_key'			=> (isset($input_params['properties_key']) ? $input_params['properties_key'] : ''),
			'properties_value'			=> (isset($input_params['properties_value']) ? $input_params['properties_value'] : ''),
			'properties_datetime'		=> $this->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'))->format('Y-m-d H:i:s'),
		);
		$query_params['properties_key'] = substr($query_params['properties_key'], 0, 64);
		$sql = sprintf("INSERT INTO local_account_properties(local_seq, properties_key, properties_value, properties_datetime) VALUES('%d', '%s', '%s', '%s') ON DUPLICATE KEY UPDATE properties_value = '%s', properties_datetime = NOW()",
			$query_params['local_seq'],
			$query_params['properties_key'],
			$query_params['properties_value'],
			$query_params['properties_datetime'],
			$query_params['properties_value']
		);
		$this->db_dashboard->trans_start();
		$sql_query = $this->db_dashboard->query($sql);
		$db_insert_id = $this->db_dashboard->insert_id();
		$this->db_dashboard->trans_complete();
		return (int)$db_insert_id;
	}
	function get_local_user_properties($local_seq) {
		$local_seq = (is_numeric($local_seq) ? (int)$local_seq : 0);
		$this->db_dashboard->select('*');
		$this->db_dashboard->from('local_account_properties');
		$this->db_dashboard->where('local_seq', $local_seq);
		$this->db_dashboard->order_by('properties_key', 'ASC');
		$sql_query = $this->db_dashboard->get();
		return $sql_query->result();
	}
	//-------------------------------------------
	function paginationCompress($link, $count, $perPage = 10) {
		$this->load->library('pagination');
	
		$config['base_url'] = base_url($link);
		$config['total_rows'] = $count;
		$config['uri_segment'] = PAGINATION_SEGMENT;
		$config['per_page'] = $perPage;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['first_tag_open'] = '<li class="arrow">';
		$config['first_link'] = 'First';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="arrow">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="arrow">';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="arrow">';
		$config['last_link'] = 'Last';
		$config['last_tag_close'] = '</li>';
	
		$this->pagination->initialize($config);
		$page = $config['per_page'];
		$segment = $this->uri->segment(PAGINATION_SEGMENT);
	
		return array (
			'page'		=> $page,
			'segment'	=> $segment
		);
	}
	
	//--------------------------------------------------
	// Utils
	//--------------------------------------------------
	function get_province($input_params = array()) {
		$query_params = array(
			'country_code'				=> (isset($input_params['country_code']) ? $input_params['country_code'] : '360'),
			'province_code'				=> (isset($input_params['province_code']) ? $input_params['province_code'] : ''),
			'city_code'					=> (isset($input_params['city_code']) ? $input_params['city_code'] : ''),
			'district_code'				=> (isset($input_params['district_code']) ? $input_params['district_code'] : ''),
			'area_code'					=> (isset($input_params['area_code']) ? $input_params['area_code'] : ''),
		);
		$sql = sprintf("SELECT province_code, province_name FROM dashboard_address_area WHERE country_code = '%d' GROUP BY province_code, province_name ORDER BY province_code ASC",
			$query_params['country_code']
		);
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
	}
	function get_city($input_params = array()) {
		$query_params = array(
			'country_code'				=> (isset($input_params['country_code']) ? $input_params['country_code'] : '360'),
			'province_code'				=> (isset($input_params['province_code']) ? $input_params['province_code'] : ''),
			'city_code'					=> (isset($input_params['city_code']) ? $input_params['city_code'] : ''),
			'district_code'				=> (isset($input_params['district_code']) ? $input_params['district_code'] : ''),
			'area_code'					=> (isset($input_params['area_code']) ? $input_params['area_code'] : ''),
		);
		$sql = sprintf("SELECT d.country_code, d.province_code, d.city_code, d.city_name FROM dashboard_address_area AS d WHERE (d.country_code = '%d' AND d.province_code = '%s') GROUP BY d.city_code ORDER BY d.city_name ASC",
			$query_params['country_code'],
			$query_params['province_code']
		);
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
	}
	function get_district($input_params = array()) {
		$query_params = array(
			'country_code'				=> (isset($input_params['country_code']) ? $input_params['country_code'] : '360'),
			'province_code'				=> (isset($input_params['province_code']) ? $input_params['province_code'] : ''),
			'city_code'					=> (isset($input_params['city_code']) ? $input_params['city_code'] : ''),
			'district_code'				=> (isset($input_params['district_code']) ? $input_params['district_code'] : ''),
			'area_code'					=> (isset($input_params['area_code']) ? $input_params['area_code'] : ''),
		);
		$sql = sprintf("SELECT d.country_code, d.province_code, d.city_code, d.city_name, d.district_code, d.district_name FROM dashboard_address_area AS d WHERE (d.country_code = '%d' AND d.province_code = '%s' AND d.city_code = '%s') GROUP BY d.district_code ORDER BY d.district_name ASC",
			$query_params['country_code'],
			$query_params['province_code'],
			$query_params['city_code']
		);
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
	}
	function get_area($input_params = array()) {
		$query_params = array(
			'country_code'				=> (isset($input_params['country_code']) ? $input_params['country_code'] : '360'),
			'province_code'				=> (isset($input_params['province_code']) ? $input_params['province_code'] : ''),
			'city_code'					=> (isset($input_params['city_code']) ? $input_params['city_code'] : ''),
			'district_code'				=> (isset($input_params['district_code']) ? $input_params['district_code'] : ''),
			'area_code'					=> (isset($input_params['area_code']) ? $input_params['area_code'] : ''),
		);
		$sql = sprintf("SELECT d.country_code, d.province_code, d.province_name, d.city_code, d.city_name, d.district_code, d.district_name, d.area_code, d.area_name FROM dashboard_address_area AS d WHERE (d.country_code = '%d' AND d.province_code = '%s' AND d.city_code = '%s' AND d.district_code) GROUP BY d.area_name ORDER BY d.area_name ASC",
			$query_params['country_code'],
			$query_params['province_code'],
			$query_params['city_code'],
			$query_params['district_code']
		);
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
	}
	function create_unique_datetime($timezone) {
		return $this->adminpanel->create_unique_datetime($timezone);
	}
	function create_dateobject($timezone, $format, $date) {
		return $this->adminpanel->create_dateobject($timezone, $format, $date);
	}
	
}




