<?php
if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
class Model_firebase extends CI_Model {
	protected $db_dashboard;
	protected $db_report;
	private $databases = array();
	public $userdata;
	public $localdata;
	function __construct() {
		parent::__construct();
		$this->load->library('adminpanel/Lib_Adminpanel', array(), 'adminpanel');
		$this->load->helper('base/base_config');
		$this->db_dashboard = $this->adminpanel->db_dashboard;
		$this->db_report = $this->adminpanel->db_report;
		
		$this->userdata = $this->adminpanel->userdata;
		$this->localdata = $this->adminpanel->localdata;
	}
	
	
	
	function get_stories_params_by($by_type, $by_key, $by_value, $input_params = array(), $paging = array(), $order = null) {
		$by_type = (is_string($by_type) ? $by_type : '');
		$by_type = sprintf('%s', $by_type);
		if (strlen($by_type) === 0) {
			$by_type = 'data'; // Get data by seq as default type-params
		}
		$by_key = (is_string($by_key) ? $by_key : '');
		$by_key = sprintf('%s', $by_key);
		if (strlen($by_key) === 0) {
			$by_key = 'seq'; // Get data by seq as default type-params
		}
		$by_value = ((is_string($by_value) || is_numeric($by_value)) ? $by_value : '');
		$by_value = sprintf("%s", $by_value);
		//==========
		// Build SQL
		//==========
		$sql_wheres = array();
		$sql_natives = array();
		switch (strtolower($by_type)) {
			case 'data':
			default:
				$this->db_dashboard->select('*');
				switch (strtolower($by_key)) {
					case 'seq':
					default:
						$by_value = sprintf("%s", $by_value);
						$sql_wheres['seq'] = $by_value;
					break;
					case 'collection':
						$sql_wheres['stories_collection'] = $by_value;
					break;
					case 'stories_seq':
						$sql_wheres['stories_seq'] = $by_value;
					break;
					case 'create':
						$sql_wheres['stories_create_by'] = $by_value;
					break;
					case 'update':
						$sql_wheres['stories_update_by'] = $by_value;
					break;
					case 'daterange':
						//$sql_wheres['stories_create_datetime'] = $by_value;
					break;
					case 'datecreate':
						//$sql_wheres['stories_create_datetime'] = $by_value;
					break;
					case 'dateupdate':
						//$sql_wheres['stories_update_datetime'] = $by_value;
					break;
				}
			break;
			case 'stories':
				switch (strtolower($by_key)) {
					case 'seq':
					default:
						switch (strtolower($by_value)) {
							case 'min':
								$this->db_dashboard->select('MIN(seq) AS value', FALSE);
							break;
							case 'max':
								$this->db_dashboard->select('MAX(seq) AS value', FALSE);
							break;
							case 'distinct':
								$this->db_dashboard->select('DISTINCT seq AS value', FALSE);
							break;
							case 'data':
							default:
								$this->db_dashboard->select('*');
								if (is_array($input_params) && (count($input_params) > 0)) {
									$sql_wheres['seq'] = (isset($input_params['seq']) ? $input_params['seq'] : 0);
								}
							break;
						}
					break;
					case 'collection':
						switch (strtolower($by_value)) {
							case 'min':
								$this->db_dashboard->select('MIN(stories_collection) AS value', FALSE);
							break;
							case 'max':
								$this->db_dashboard->select('MAX(stories_collection) AS value', FALSE);
							break;
							case 'distinct':
								$this->db_dashboard->select('DISTINCT stories_collection AS value', FALSE);
							break;
							case 'data':
							default:
								$this->db_dashboard->select('*');
								if (is_array($input_params) && (count($input_params) > 0)) {
									$sql_wheres['stories_collection'] = (isset($input_params['stories_collection']) ? $input_params['stories_collection'] : 0);
								}
							break;
						}
					break;
					case 'stories_seq':
						switch (strtolower($by_value)) {
							case 'min':
								$this->db_dashboard->select('MIN(CAST(stories_seq AS UNSIGNED)) AS value', FALSE);
							break;
							case 'max':
								$this->db_dashboard->select('MAX(CAST(stories_seq AS UNSIGNED)) AS value', FALSE);
							break;
							case 'distinct':
								$this->db_dashboard->select('DISTINCT stories_seq AS value', FALSE);
							break;
							case 'data':
							default:
								$this->db_dashboard->select('*');
								if (is_array($input_params) && (count($input_params) > 0)) {
									$sql_wheres['stories_seq'] = (isset($input_params['stories_seq']) ? $input_params['stories_seq'] : 0);
								}
							break;
						}
					break;
					case 'create':
						switch (strtolower($by_value)) {
							case 'min':
								$this->db_dashboard->select('MIN(stories_create_by) AS value', FALSE);
							break;
							case 'max':
								$this->db_dashboard->select('MAX(stories_create_by) AS value', FALSE);
							break;
							case 'distinct':
								$this->db_dashboard->select('DISTINCT stories_create_by AS value', FALSE);
							break;
							case 'data':
							default:
								$this->db_dashboard->select('*');
								if (is_array($input_params) && (count($input_params) > 0)) {
									$sql_wheres['stories_create_by'] = (isset($input_params['stories_create_by']) ? $input_params['stories_create_by'] : 0);
								}
							break;
						}
					break;
					case 'update':
						switch (strtolower($by_value)) {
							case 'min':
								$this->db_dashboard->select('MIN(stories_update_by) AS value', FALSE);
							break;
							case 'max':
								$this->db_dashboard->select('MAX(stories_update_by) AS value', FALSE);
							break;
							case 'distinct':
								$this->db_dashboard->select('DISTINCT stories_update_by AS value', FALSE);
							break;
							case 'data':
							default:
								$this->db_dashboard->select('*');
								if (is_array($input_params) && (count($input_params) > 0)) {
									$sql_wheres['stories_update_by'] = (isset($input_params['stories_update_by']) ? $input_params['stories_update_by'] : 0);
								}
							break;
						}
					break;
					case 'daterange':
						
					break;
					case 'datecreate':
						//$sql_wheres['stories_create_datetime'] = $by_value;
					break;
					case 'dateupdate':
						//$sql_wheres['stories_update_datetime'] = $by_value;
					break;
				}
			break;
		}
		$this->db_dashboard->from('firebase_stories');
		if (count($sql_wheres) > 0) {
			$query_params = array(
				'seq'						=> (isset($sql_wheres['seq']) ? $sql_wheres['seq'] : 0),
				'stories_collection'		=> (isset($sql_wheres['stories_collection']) ? $sql_wheres['stories_collection'] : ''),
				'stories_seq'				=> (isset($sql_wheres['stories_seq']) ? $sql_wheres['stories_seq'] : ''),
				'stories_create_by'			=> (isset($sql_wheres['stories_create_by']) ? $sql_wheres['stories_create_by'] : 0),
				'stories_update_by'			=> (isset($sql_wheres['stories_update_by']) ? $sql_wheres['stories_update_by'] : 0),
				'stories_create_datetime'	=> (isset($sql_wheres['stories_create_datetime']) ? $sql_wheres['stories_create_datetime'] : date('Y-m-d H:i:s')),
				'stories_update_datetime'	=> (isset($sql_wheres['stories_update_datetime']) ? $sql_wheres['stories_update_datetime'] : date('Y-m-d H:i:s')),
			);
		} else {
			$query_params = array();
		}
		switch (strtolower($by_type)) {
			case 'data':
			default:
				// Manipulate datetime range or formatting
				switch (strtolower($by_key)) {
					case 'daterange':
					case 'datecreate':
					case 'dateupdate':
						$query_params['stories_create_datetime'] = (isset($query_params['stories_create_datetime']) ? date('Y-m-d', strtotime($query_params['stories_create_datetime'])) : date('Y-m-d'));
						$query_params['stories_update_datetime'] = (isset($query_params['stories_update_datetime']) ? date('Y-m-d', strtotime($query_params['stories_update_datetime'])) : date('Y-m-d'));
						$this->db_dashboard->where($by_key, $query_params[$by_key]);
					break;
					case 'create':
						$this->db_dashboard->where('stories_create_by', $query_params['stories_create_by']);
						if (is_array($input_params) && (count($input_params) > 0)) {
							foreach ($input_params as $inputKey => $inputVal) {
								$this->db_dashboard->where($inputKey, $inputVal);
							}
						}
					break;
					case 'collection':
						$this->db_dashboard->where('stories_collection', $query_params['stories_collection']);
						if (is_array($input_params) && (count($input_params) > 0)) {
							foreach ($input_params as $inputKey => $inputVal) {
								$this->db_dashboard->where($inputKey, $inputVal);
							}
						}
					break;
					case 'seq':
					default:
						$this->db_dashboard->where($by_key, $query_params[$by_key]);
					break;
				}
			break;
			case 'stories':
				switch (strtolower($by_key)) {
					default:
						$this->db_dashboard->where($query_params);
					break;
				}
			break;
		}
		if ($order != null) {
			$query_order = array(
				'by'		=> 'seq',
				'sort'		=> 'DESC',
			);
			if (isset($order['order_by'])) {
				if (is_string($order['order_by'])) {
					$query_order['by'] = $order['order_by'];
				}
			}
			if (isset($order['order_sort'])) {
				if (is_string($order['order_sort'])) {
					$query_order['sort'] = strtoupper($order['order_sort']);
				}
			}
			$this->db_dashboard->order_by($query_order['by'], $query_order['sort']);
		}
		if (isset($paging['start']) && isset($paging['perpage'])) {
			$this->db_dashboard->limit($paging['perpage'], $paging['start']);
		}
		$sql_query = $this->db_dashboard->get();
		return $sql_query->result();
	}
	function insert_stories_params_by($by_type, $by_key, $by_value, $input_params = array()) {
		$by_type = (is_string($by_type) ? $by_type : '');
		$by_type = sprintf('%s', $by_type);
		if (strlen($by_type) === 0) {
			$by_type = 'data'; // Insert data
		}
		$by_key = (is_string($by_key) ? $by_key : '');
		$by_key = sprintf('%s', $by_key);
		if (strlen($by_key) === 0) {
			$by_key = 'duplicate'; // Insert On Duplicate Key
		}
		$by_value = ((is_string($by_value) || is_numeric($by_value)) ? $by_value : '');
		$by_value = sprintf("%s", $by_value);
		//==========
		// Build SQL
		//==========
		$sql_params = array();
		$sql_native_insert = "";
		if (count($input_params) > 0) {
			$sql_native_insert .= "INSERT INTO firebase_stories(";
		}
		switch (strtolower($by_type)) {
			case 'data':
			default:
				$sql_params['stories_collection'] = (isset($input_params['stories_collection']) ? $input_params['stories_collection'] : '');
				$sql_params['stories_seq'] = (isset($input_params['stories_seq']) ? $input_params['stories_seq'] : '0');
				$sql_params['stories_url_host'] = (isset($input_params['stories_url_host']) ? $input_params['stories_url_host'] : '');
				$sql_params['stories_url_path'] = (isset($input_params['stories_url_path']) ? $input_params['stories_url_path'] : '');
				$sql_params['stories_url_full'] = (isset($input_params['stories_url_full']) ? $input_params['stories_url_full'] : '');
				$sql_params['stories_create_by'] = (isset($input_params['stories_create_by']) ? $input_params['stories_create_by'] : '0');
				$sql_params['stories_create_datetime'] = (isset($input_params['stories_create_datetime']) ? $input_params['stories_create_datetime'] : date('Y-m-d H:i:s'));
				$sql_params['stories_update_by'] = (isset($input_params['stories_update_by']) ? $input_params['stories_update_by'] : '0');
				$sql_params['stories_update_datetime'] = (isset($input_params['stories_update_datetime']) ? $input_params['stories_update_datetime'] : date('Y-m-d H:i:s'));
				$sql_params['stories_status'] = 'reviewed';
			break;
			case 'stories':
				// Nothing to do
			break;
		}
		if (count($sql_params) > 0) {
			$i = 0;
			foreach ($sql_params as $key => $val) {
				if ($i === 0) {
					$sql_native_insert .= sprintf("%s", $key);
				} else {
					$sql_native_insert .= sprintf(", %s", $key);
				}
				$i++;
			}
			$sql_native_insert .= ")";
			$sql_native_insert .= " VALUES(";
		}
		if (count($sql_params) > 0) {
			$i = 0;
			foreach ($sql_params as $keval) {
				if ($i === 0) {
					$sql_native_insert .= sprintf("'%s'", $keval);
				} else {
					$sql_native_insert .= sprintf(", '%s'", $keval);
				}
				$i++;
			}
			$sql_native_insert .= ")";
			$sql_native_insert .= sprintf(" ON DUPLICATE KEY UPDATE stories_url_host = '%s', stories_url_path = '%s', stories_url_full = '%s', stories_status = '%s', stories_update_by = '%d', stories_update_datetime = NOW()",
				(isset($sql_params['stories_url_host']) ? $sql_params['stories_url_host'] : ''),
				(isset($sql_params['stories_url_path']) ? $sql_params['stories_url_path'] : ''),
				(isset($sql_params['stories_url_full']) ? $sql_params['stories_url_full'] : ''),
				(isset($sql_params['stories_status']) ? $sql_params['stories_status'] : ''),
				(isset($sql_params['stories_update_by']) ? $sql_params['stories_update_by'] : '0')
			);
		}
		if (count($sql_params) > 0) {
			$this->db_dashboard->trans_start();
			$sql_query = $this->db_dashboard->query($sql_native_insert);
			//$this->db_dashboard->insert('firebase_stories', $sql_params);
			$db_insert_id = $this->db_dashboard->insert_id();
			$this->db_dashboard->trans_complete();
			return (int)$db_insert_id;
		} else {
			return 0;
		}
	}
	function get_latest_published_stories($limit = 32) {
		$this->db_dashboard->select('*')->from('firebase_stories');
		$this->db_dashboard->where('collection', 'cerita');
		$this->db_dashboard->where('stories_status', 'published');
		$this->db_dashboard->order_by('stories_update_datetime', 'DESC')->limit($limit);
		return $this->db_dashboard->get()->result();
	}
	
	
	
	
}








