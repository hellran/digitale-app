<?php 
if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
class Model_adminpanel extends CI_Model{
	protected $db_dashboard;
	protected $db_report;
	private $databases = array();
	protected $api_service;
	protected $guestlogin;
	public $userdata;
	public $localdata;
	protected $base_config;
	protected $error = false, $error_msg = array();
	protected $DateObject;
	function __construct() {
		parent::__construct();
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		$this->load->library('adminpanel/Lib_Adminpanel', array(), 'adminpanel');
		$this->load->library('adminpanel/Lib_imzcustom', false, 'lib_imzcustom');
		$this->load->helper('base/base_config');
		$this->db_dashboard = $this->adminpanel->db_dashboard;
		$this->db_report = $this->adminpanel->db_report;
		$this->load->library('adminpanel/Lib_Rc4Crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		//--
		$this->api_services = array(
			'lists'		=> 'http://project.localhost/platform/DataCenter.svc?singleWsdl',
			'api'		=> array(
				'json'		=> 'http://project.localhost/WCF_GG_Platform_V2_Authentication.svc/json?',
				'xml'		=> 'http://project.localhost/WCF_GG_Platform_V2_Authentication.svc/xml?',
			),
		);
		$this->guestlogin = array(
			'id'		=> '08900090-54b3-e7c7-0000-000046bffd97',
			'timestamp'	=> time(),
			'appid'		=> '84',
			'appsecret'	=> 'APP_SECRET',
		);
		$this->userdata = $this->adminpanel->userdata;
		$this->localdata = $this->adminpanel->localdata;
		$this->DateObject = $this->create_dateobject("Asia/Bangkok", 'Y-m-d H:i:s', date('Y-m-d H:i:s'));
	}
	//--------------------------------------------------------------------------------------------------------
	// Convert to Codeigniter
	//--------------------------------------------------------------------------------------------------------
	private function get_firebase_login_data_by_seq($seq = 0) {
		$this->db_dashboard->where('seq', $seq);
		return $this->db_dashboard->get('local_account_firebase')->row();
	}
	private function insert_firebase_login_data($insert_params) {
		$this->db_dashboard->set('log_datetime', 'NOW()', FALSE);
		$this->db_dashboard->set('local_seq', 0);
		$this->db_dashboard->insert('local_account_firebase', $insert_params);
		return $this->db_dashboard->insert_id();
	}
	private function update_firebase_login_data_by_seq($seq, $update_params = array()) {
		$this->db_dashboard->where('seq', $seq);
		$this->db_dashboard->update('local_account_firebase', $update_params);
		return $this->db_dashboard->affected_rows();
	}
	function insert_local_data_by_firebase($firebase_seq, $query_params) {
		$this->load->model('adminpanel/Model_users', 'mod_users');
		$collectData = array();
		try {
			$account_activation_ending = new DateTime($query_params['account_activation_ending']);
		} catch (Exception $ex) {
			$this->error = true;
			$this->error_msg[] = "account-activation-ending: {$ex->getMessage()}";
		}
		if (!$this->error) {
			$query_params['account_activation_ending'] = $account_activation_ending->add(new DateInterval('P7D'));
			$query_params['account_activation_ending'] = $account_activation_ending->format('Y-m-d H:i:s');
			try {
				$subscription_starting = new DateTime($query_params['subscription_starting']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "account-activation-ending: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			$query_params['subscription_starting'] = $subscription_starting->format('Y-m-d H:i:s');
			try {
				$subscription_expiring = new DateTime($query_params['subscription_expiring']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "subscription-expiring: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			$query_params['subscription_expiring'] = $subscription_expiring->add(new DateInterval('P1Y')); // Add 1 Year For Default
			$query_params['subscription_expiring'] = $subscription_expiring->format('Y-m-d H:i:s');
			$input_params = array(
				'user_password'				=> 'firebase',
			);
			try {
				$input_params['unique_datetime'] = $this->create_unique_datetime("Asia/Bangkok");
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot create new unique datetime: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			$query_params['account_hash'] = $this->rc4crypt->bEncryptRC4($input_params['unique_datetime']);
			try {
				$input_params['account_password'] = ("{$this->rc4crypt->bDecryptRC4($query_params['account_hash'])}|{$input_params['user_password']}");
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = $ex->getMessage();
			}
		}
		if (!$this->error) {
			$query_params['account_password'] = sha1($input_params['account_password']);
			$query_params['account_activation_code'] = md5($query_params['account_hash']);
			if (strtoupper($query_params['account_active']) === strtoupper('Y')) {
				$query_params['account_activation_status'] = 'Y';
			} else {
				$query_params['account_activation_status'] = 'N';
			}
			if (strtoupper($query_params['account_activation_status']) === strtoupper('Y')) {
				$query_params['account_activation_datetime'] = $this->DateObject->format('Y-m-d H:i:s');
				$query_params['account_activation_by'] = 'register-system';
			}
		}
		//--------------------------------------
		//Check username or email exists
		if (!$this->error) {
			try {
				$collectData['local_users'] = $this->mod_users->get_local_user_by($query_params['account_username'], 'username');
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot check username is exists from model.";
			}
		}
		if (!$this->error) {
			if (count($collectData['local_users']) > 0) {
				$this->error = true;
				$this->error_msg[] = "Username already taken.";
			}
		}
		if (!$this->error) {
			try {
				$collectData['local_emails'] = $this->mod_users->get_local_user_by($query_params['account_email'], 'email');
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot check email is exists from model.";
			}
		}
		if (!$this->error) {
			if (count($collectData['local_emails']) > 0) {
				$this->error = true;
				$this->error_msg[] = "Email already taken.";
			}
		}
		# Finally add user to database
		if (!$this->error) {
			try {
				$collectData['new_account_seq'] = $this->add_user($query_params);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot insert new user to local-account.";
			}
		}
		# Update firebase local-seq
		if (!$this->error) {
			if ($collectData['new_account_seq'] > 0) {
				$firebase_seq = (is_numeric($firebase_seq) ? (int)$firebase_seq : 0);
				$firebase_update_params = array(
					'local_seq'				=> $collectData['new_account_seq'],
				);
				try {
					$this->update_firebase_login_data_by_seq($firebase_seq, $firebase_update_params);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot update firebase local seq with exception: {$ex->getMessage()}";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "New local seq is 0";
			}
		}
		if (!$this->error) {
			return $collectData['new_account_seq'];
		} else {
			return false;
		}
	}
	
	function firebase_input($input_params = null) {
		$collectData = array();
		if (!isset($input_params)) {
			$collectData['input_params'] = $this->lib_imzcustom->php_input_request();
		} else {
			$collectData['input_params'] = $input_params;
		}
		return $collectData;
	}
	function firebase_login($query_params = null) {
		$collectData = array();
		if (!isset($query_params)) {
			return false;
		}
		$this->db_dashboard->select('*')->from('local_account_firebase');
		if (is_array($query_params) && (count($query_params) > 0)) {
			foreach ($query_params as $query_key => $query_val) {
				$query_key = (is_string($query_key) ? strtolower($query_key) : 'null');
				if (strtolower($query_key) !== 'firebase_fullname') {
					$this->db_dashboard->where($query_key, $query_val);
				}
			}
		}
		try {
			$sql_query = $this->db_dashboard->get();
		} catch (Exception $ex) {
			$this->error = true;
			$this->error_msg[] = "Cannot query get firebase data with exception: {$ex->getMessage()}";
		}
		if (!$this->error) {
			if ($sql_query->row() != FALSE) {
				$row = $sql_query->row();
				$collectData['new_firebase_seq'] = $row->seq;
			} else {
				$insert_params = array(
					'firebase_provider'			=> (isset($query_params['firebase_provider']) ? $query_params['firebase_provider'] : ''),
					'firebase_identity'			=> (isset($query_params['firebase_identity']) ? (is_string($query_params['firebase_identity']) ? $query_params['firebase_identity'] : '') : ''),
					'firebase_uid'				=> (isset($query_params['firebase_uid']) ? (is_string($query_params['firebase_uid']) ? $query_params['firebase_uid'] : '') : ''),
					'firebase_email'			=> (isset($query_params['firebase_email']) ? (is_string($query_params['firebase_email']) ? $query_params['firebase_email'] : '') : ''),
					'firebase_fullname'			=> (isset($query_params['firebase_fullname']) ? (is_string($query_params['firebase_fullname']) ? $query_params['firebase_fullname'] : '') : ''),
				);
				if (!is_string($insert_params['firebase_provider'])) {
					$this->error = true;
					$this->error_msg[] = "Firebase provider should be string datatype.";
				} else {
					if (!in_array(strtolower($insert_params['firebase_provider']), array('email', 'password', 'google.com'))) {
						$this->error = true;
						$this->error_msg[] = "Firebase provider not allowed.";
					} else {
						try {
							$collectData['new_firebase_seq'] = $this->insert_firebase_login_data($insert_params);
						} catch (Exception $ex) {
							$this->error = true;
							$this->error_msg[] = "Exception error add new firebase account: {$ex->getMessage()}";
						}
					}
				}
			}
		}
		if (!$this->error) {
			if ((int)$collectData['new_firebase_seq'] > 0) {
				try {
					$collectData['firebase_login_data'] = $this->get_firebase_login_data_by_seq($collectData['new_firebase_seq']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get firebase login data with exception: {$ex->getMessage()}";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "NO firebase data insert or data maded.";
			}
		}
		if (!$this->error) {
			if (!isset($collectData['firebase_login_data']->firebase_uid)) {
				$this->error = true;
				$this->error_msg[] = "No firebase login-data get from database.";
			} else {
				try {
					switch (strtolower($collectData['firebase_login_data']->firebase_provider)) {
						case 'password':
						default:
							$collectData['local_data'] = $this->get_login_data($collectData['firebase_login_data']->firebase_uid, 'firebase');
						break;
						case 'google.com':
							$collectData['local_data'] = $this->get_login_data($collectData['firebase_login_data']->firebase_uid, 'firebase');
						break;
					}
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get local-account data.";
				}
			}
		}
		//----------------------------------------------------------------------------
		// Check if login is correct
		if (!$this->error) {
			if (!isset($collectData['local_data']->seq)) {
				$collectData['local_user_params'] = array(
					'account_username'				=> $collectData['firebase_login_data']->firebase_uid,
					'account_email'					=> $collectData['firebase_login_data']->firebase_email,
					'account_firebase_seq'			=> $collectData['firebase_login_data']->seq,
					'account_firebase_uid'			=> $collectData['firebase_login_data']->firebase_uid,
					'account_hash'					=> 'FIREBASE',
					'account_password'				=> $collectData['firebase_login_data']->firebase_identity,
					'account_inserting_datetime'	=> $this->DateObject->format('Y-m-d H:i:s'),
					'account_inserting_remark'		=> 'register from firebase',
					'account_activation_code'		=> md5("{$collectData['firebase_login_data']->firebase_uid}-{$this->DateObject->format('Y-m-d H:i:s')}"),
					'account_activation_starting'	=> $this->DateObject->format('Y-m-d H:i:s'),
					'account_activation_ending'		=> $this->DateObject->format('Y-m-d H:i:s'),
					'account_activation_datetime'	=> NULL,
					'account_activation_status'		=> 'N', // N as default
					'account_activation_by'			=> '',
					'account_active'				=> 'N',
					'account_role'					=> 1,
					'account_nickname'				=> base_permalink($collectData['firebase_login_data']->firebase_identity),
					'account_fullname'				=> $collectData['firebase_login_data']->firebase_fullname,
					'account_address'				=> '',
					'account_phonenumber'			=> '',
					'account_phonemobile'			=> '',
					'account_delete_status'			=> 0,
					'account_delete_datetime'		=> NULL,
					'account_delete_by'				=> '',
					'account_edited_datetime'		=> NULL,
					'account_edited_by'				=> NULL,
					'subscription_starting'			=> $this->DateObject->format('Y-m-d H:i:s'),
					'subscription_expiring'			=> $this->DateObject->format('Y-m-d H:i:s'),
				);
				try {
					$collectData['local_data_seq'] = $this->insert_local_data_by_firebase($collectData['firebase_login_data']->seq, $collectData['local_user_params']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Exception error while insert new local data with firebase: {$ex->getMessage()}";
				}
			} else {
				$collectData['local_data_seq'] = $collectData['local_data']->seq;
			}
		}
		if (!$this->error) {
			// Get newest local-data by seq
			if ((int)$collectData['local_data_seq'] === 0) {
				$this->error = true;
				$this->error_msg[] = "No insert local-data maded by firebase.";
			} else {
				try {
					$collectData['user_data'] = $this->get_local_data_by_seq($collectData['local_data_seq']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get local data by seq with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (!isset($collectData['user_data']->seq)) {
				$this->error = true;
				$this->error_msg[] = "User data fresh not have seq.";
			} else {
				try {
					$collectData['firebase_logged_data'] = $this->get_firebase_login_data_by_seq($collectData['firebase_login_data']->seq);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get fresh updated firebase local data with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (!isset($collectData['firebase_logged_data']->local_seq)) {
				$this->error = true;
				$this->error_msg[] = "Local data of firebase logged data not have local-seq.";
			} else {
				if ($collectData['firebase_logged_data']->local_seq !== $collectData['user_data']->seq) {
					$this->error = true;
					$this->error_msg[] = "Firebase data login seem not correct, please check your firebase login.";
				} else {
					//------------------------------
					// Made Logged-in
					//------------------------------
					try {
						$collectData['gg_account_login_seq'] = $this->insert_gg_account_login($collectData['user_data']);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "not get return from insert_gg_account_login: {$ex->getMessage()}";
					}
				}
			}
		}
		if (!$this->error) {
			if ($collectData['gg_account_login_seq'] === 0) {
				$this->error = true;
				$this->error_msg[] = "returning gg-account-login-seq is 0.";
			}
		}
		//---------------------------------------------------------------------
		// Login Created
		if (!$this->error) {
			$this->update_gg_login_account($collectData['gg_account_login_seq']);
			$this->update_local_login_account($collectData['user_data']->seq);
			$this->session->set_userdata('gg_login_account', $collectData['gg_account_login_seq']);
		}
		//---------------------------------------------------------------------
		$collectData['return'] = array();
		if (!$this->error) {
			$collectData['return']['success'] = true;
			$collectData['return']['error'] = null;
		} else {
			$collectData['return']['success'] = false;
			$collectData['return']['error'] = $this->error_msg;
		}
		return $collectData['return'];
	}
	
	
	
	
	
	
	function local_login($input_params = null) {
		$collectData = array();
		//-- Global error
		$error = false;
		$error_msg = array();
		if (!isset($input_params)) {
			$collectData['input_params'] = $this->lib_imzcustom->php_input_request();
		} else {
			$collectData['input_params'] = $input_params;
		}
		$collectData['login_params'] = array(
			'user_email'		=> (isset($collectData['input_params']['body']['user_email']) ? $collectData['input_params']['body']['user_email'] : ''),
			'user_username'		=> (isset($collectData['input_params']['body']['user_username']) ? $collectData['input_params']['body']['user_username'] : ''),
			'user_password'		=> (isset($collectData['input_params']['body']['user_password']) ? $collectData['input_params']['body']['user_password'] : ''),
		);
		if (!$error) {
			try {
				$collectData['local_data'] = $this->get_login_data($collectData['login_params']['user_email'], 'localhost');
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Cannot get local-account data.";
			}
		}
		if (!$error) {
			if (count($collectData['local_data']) === 0) {
				$error = true;
				$error_msg[] = "local-data is empty or not found.";
			} else {
				if (!isset($collectData['local_data']->seq)) {
					$error = true;
					$error_msg[] = "local-data not have sequence.";
				}
			}
		}
		if (!$error) {
			if (strtoupper($collectData['local_data']->account_active) !== strtoupper('Y')) {
				$error = true;
				$error_msg[] = "Account not active yet.";
			}
		}
		if (!$error) {
			$collectData['login_params']['account_hash'] = $this->rc4crypt->bDecryptRC4($collectData['local_data']->account_hash);
			if (sha1("{$collectData['login_params']['account_hash']}|{$collectData['login_params']['user_password']}") !== $collectData['local_data']->account_password) {
				$error = true;
				$error_msg[] = "Password is not match.";
				$error_msg[] = $collectData;
			}
		}
		if (!$error) {
			try {
				$collectData['gg_account_login_seq'] = $this->insert_gg_account_login($collectData['local_data']);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "not get return from insert_gg_account_login: {$ex->getMessage()}";
			}
		}
		if (!$error) {
			if ($collectData['gg_account_login_seq'] === 0) {
				$error = true;
				$error_msg[] = "returning gg-account-login-seq is 0.";
			}
		}
		//---------------------------------------------------------------------
		// Login Created
		if (!$error) {
			$this->update_gg_login_account($collectData['gg_account_login_seq']);
			$this->update_local_login_account($collectData['local_data']->seq);
			$this->session->set_userdata('gg_login_account', $collectData['gg_account_login_seq']);
		}
		//---------------------------------------------------------------------
		$return = array();
		if (!$error) {
			$return['success'] = true;
			$return['error'] = null;
		} else {
			$return['success'] = false;
			$return['error'] = $error_msg;
		}
		return $return;
	}
	private function insert_gg_account_login($input_params) {
		//-- Global error
		$error = false;
		$error_msg = array();
		$new_insert_id = 0;
		$query_params = array(
			'login_id'				=> (isset($input_params->seq) ? $input_params->seq : ''),
			'login_email'			=> (isset($input_params->account_email) ? $input_params->account_email : ''),
			'login_username'		=> (isset($input_params->account_username) ? $input_params->account_username : ''),
			'login_nickname'		=> (isset($input_params->account_nickname) ? $input_params->account_nickname : ''),
			'local_seq'				=> (isset($input_params->seq) ? $input_params->seq : ''),
			'login_datetime_first'	=> (isset($input_params->login_datetime) ? $input_params->login_datetime : date('Y-m-d H:i:s')),
			'login_datetime_last'	=> (isset($input_params->login_datetime) ? $input_params->login_datetime : date('Y-m-d H:i:s')),
		);
		$query_params['login_username'] = (is_string($query_params['login_username']) ? strtolower($query_params['login_username']) : '');
		$query_params['login_username'] .= "@localhost";
		//$query_params['login_username'] .= "@ggpassport";
		if (!$error) {
			try {
				$gg_data = $this->get_login_data($query_params['login_username'], 'goodgames');
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Cannot get data of login-data by get-login-data function: {$ex->getMessage()}";
			}
		}
		if (!$error) {
			if (isset($gg_data->seq)) {
				if ((int)$gg_data->seq > 0) {
					$new_insert_id = (int)$gg_data->seq;
				}
			}
		}
		if (!$error) {
			if ($new_insert_id === 0) {
				$this->db_dashboard->insert('gg_account', $query_params);
				$new_insert_id = $this->db_dashboard->insert_id();
			}
		}
		return $new_insert_id;
	}
	private function get_local_data_by_seq($seq) {
		$this->db_dashboard->where('seq', $seq);
		return $this->db_dashboard->get('local_account')->row();
	}
	
	
	
	
	
	
	/******************************************************************************************************
	* Queries by lib_imzers
	******************************************************************************************************/
	// Un-used just prevend un-exists instance
	function sql_addslashes($string, $db_driver = 'mysql') {
		return $string;
	}
	function save_token_data($session, $code, $token, $datetime) {
		$insert_params = array(
			'login_session'					=> $session,
			'login_code'					=> $code,
			'login_token'					=> $token,
		);
		$this->db_dashboard->set('login_datetime', 'NOW()', FALSE);
		try {
			$this->db_dashboard->insert('gg_account_log', $insert_params);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		return $this->db_dashboard->insert_id();
	}
	function update_token_data($seq, $account_seq) {
		$update_params = array(
			'account_seq'					=> $account_seq,
		);
		$this->db_dashboard->where('seq', $seq);
		$this->db_dashboard->set('account_seq', $account_seq);
		$this->db_dashboard->update('gg_account_log', $update_params);
	}
	function get_token_data($uid) {
		$token_data = FALSE;
		$sql = "SELECT * FROM gg_account_log WHERE";
		$sql .= (is_numeric($uid) ? " seq = {$this->sql_addslashes($uid, 'mysql')}" : " login_session = '{$this->sql_addslashes($uid, 'mysql')}'");
		$sql .= " ORDER BY login_datetime DESC LIMIT 1";
		$sql_query = $this->db_dashboard->query($sql);
		foreach ($sql_query->result() as $row) {
			$token_data = json_decode(json_encode($row), true);
		}
		return $token_data;
	}
	function get_login_data($uid, $login_server = null) {
		if (!$login_server) {
			$login_server = 'goodgames';
		}
		$loginTable = "gg_account";
		switch (strtolower($login_server)) {
			case 'local':
			case 'localhost':
				$loginTable = "local_account";
			break;
			case 'firebase':
				$loginTable = "local_account";
			break;
			case 'goodgames':
			default:
				$loginTable = "gg_account";
			break;
		}
		$this->db_dashboard->select('*');
		$this->db_dashboard->from($loginTable);
		switch (strtolower($login_server)) {
			case 'local':
			case 'localhost':
				if (is_numeric($uid)) {
					$this->db_dashboard->where('seq', $uid);
				} else if (is_string($uid)) {
					$this->db_dashboard->where('LOWER(account_email)', strtolower($uid));
				} else {
					$this->db_dashboard->where('LOWER(account_email)', strtolower($uid));
				}
			break;
			case 'firebase':
				$this->db_dashboard->where('LOWER(account_firebase_uid)', strtolower($uid));
			break;
			case 'goodgames':
			default:
				$this->db_dashboard->where('LOWER(login_username)', strtolower($uid));
			break;
		}
		//$sql_wheres = array('login_username' => $uid);
		return $this->db_dashboard->get()->row();
	}
	private function update_gg_login_account($account_seq) {
		$account_params = array(
			'login_datetime_first'			=> NULL,
		);
		$this->db_dashboard->where('seq', $account_seq);
		$this->db_dashboard->set('login_datetime_last', 'NOW()', FALSE);
		$this->db_dashboard->update('gg_account');
	}
	private function update_local_login_account($account_seq) {
		$this->db_dashboard->where('seq', $account_seq);
		$this->db_dashboard->set('login_datetime', 'NOW()', FALSE);
		$this->db_dashboard->update('local_account');
	}
	function update_local_account_data($seq, $query_params) {
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$this->db_dashboard->where('seq', $seq);
		$this->db_dashboard->update('local_account', $query_params);
	}
	//--------------------------------------------
	function get_login_data_userdata($local_seq) {
		$local_seq = (is_numeric($local_seq) ? (int)$local_seq : 0);
		if ((int)$local_seq === 0) {
			return false;
		}
		$sql = sprintf("SELECT a.*, r.role_id, r.role_code, r.role_name FROM local_account AS a LEFT JOIN dashboard_roles AS r ON r.role_id = a.account_role WHERE (a.seq = '%d') LIMIT 1",
			$local_seq
		);
		try {
			$sql_query = $this->db_dashboard->query($sql);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		if (count($sql_query->result_array()) > 0) {
			foreach ($sql_query->result_array() as $keval) {
				return $keval;
			}
		}
	}
	//--------------------------------------------------------------------------------------
	function get_datetime() {
		return date('Y-m-d H:i:s');
	}
	//-------------
	function get_local_users($type = 'total') {
		switch ($type) {
			case 'total':
				$sql = "SELECT COUNT(seq) AS total_users FROM local_account";
				$sql_query = $this->db_dashboard->query($sql);
				return $sql_query->result_array();
			break;
		}
	}
	function get_local_user_by($by_value, $by_type = null) {
		if (!isset($by_type)) {
			$by_type = 'email';
		}
		$sql_wheres = array();
		switch (strtolower($by_type)) {
			case 'email':
			default:
				$sql_wheres['account_email'] = $by_value;
			break;
			case 'username':
				$sql_wheres['account_username'] = $by_value;
			break;
			case 'seq':
			case 'id':
				if (is_numeric($by_value)) {
					$sql_wheres['seq'] = $by_value;
				} else {
					$sql_wheres['account_username'] = $by_value;
				}
			break;
			case 'activation':
				if (is_string($by_value)) {
					$sql_wheres['account_activation_code'] = $by_value;
				}
			break;
		}
		$this->db_dashboard->select('*');
		$this->db_dashboard->from('local_account');
		$this->db_dashboard->where($sql_wheres);
		$sql_query = $this->db_dashboard->get();
		return $sql_query->result();
	}
	function get_local_user_properties_by($local_seq, $by_value, $by_type = null) {
		if (!isset($by_type)) {
			$by_type = 'activation';
		}
		$sql_wheres = array();
		switch (strtolower($by_type)) {
			case 'properties-value':
				$local_seq = (is_string($local_seq) ? strtolower($local_seq) : '');
				$sql_wheres['properties_key'] = $local_seq;
			break;
			default:
				$local_seq = (is_numeric($local_seq) ? (int)$local_seq : 0);
				$sql_wheres['local_seq'] = $local_seq;
				$sql_wheres['properties_key'] = '';
			break;
		}
		switch (strtolower($by_type)) {
			case 'activation':
			default:
				$sql_wheres['properties_key'] = 'user-request-activation-code';
			break;
			case 'password-count':
				$sql_wheres['properties_key'] = 'user-request-password-count';
				if (isset($sql_wheres['properties_value'])) {
					unset($sql_wheres['properties_value']);
				}
			break;
			case 'password-string':
				$sql_wheres['properties_key'] = 'user-request-password-string';
			break;
			case 'password-hash':
				$sql_wheres['properties_key'] = 'user-request-password-hash';
			break;
			case 'seq':
				if (isset($sql_wheres['local_seq'])) {
					unset($sql_wheres['local_seq']);
				}
				if (isset($sql_wheres['properties_key'])) {
					unset($sql_wheres['properties_key']);
				}
				$by_value = (is_numeric($by_value) ? (int)$by_value : 0);
				$sql_wheres['seq'] = $by_value;
			break;
			case 'properties-value':
				if (isset($sql_wheres['local_seq'])) {
					unset($sql_wheres['local_seq']);
				}
				$by_value = (is_string($by_value) ? $by_value : '');
				$sql_wheres['properties_value'] = sprintf('%s', $by_value);
				$sql_wheres['properties_value'] = substr($sql_wheres['properties_value'], 0, 64);
			break;
		}
		$this->db_dashboard->select('*');
		$this->db_dashboard->from('local_account_properties');
		$this->db_dashboard->where($sql_wheres);
		$sql_query = $this->db_dashboard->get();
		return $sql_query->result();
	}
	function set_local_user_properties_by($local_seq, $by_value, $by_type = null) {
		$local_seq = (is_numeric($local_seq) ? (int)$local_seq : 0);
		if (!isset($by_type)) {
			$by_type = 'email';
		}
		$input_params = array();
		$input_params['local_seq'] = $local_seq;
		$input_params['properties_key'] = "";
		switch (strtolower($by_type)) {
			case 'activation':
			default:
				$by_value = (is_numeric($by_value) ? sprintf('%s', $by_value) : '1');
				$input_params['properties_key'] = 'user-request-activation-code';
			break;
			case 'password-count':
				$by_value = (is_numeric($by_value) ? sprintf('%s', $by_value) : '1');
				$input_params['properties_key'] = 'user-request-password-count';
			break;
			case 'password-string':
				$input_params['properties_key'] = 'user-request-password-string';
			break;
			case 'password-hash':
				$input_params['properties_key'] = 'user-request-password-hash';
			break;
		}
		$by_value = (is_string($by_value) ? $by_value : '');
		$input_params['properties_value'] = sprintf('%s', $by_value);
		$this->db_dashboard->trans_start();
		$sql = "INSERT INTO local_account_properties(local_seq, properties_key, properties_value, properties_datetime) VALUES (?, ?, ?, NOW()) ON DUPLICATE KEY UPDATE local_seq = VALUES(local_seq), properties_key = VALUES(properties_key), properties_value= ?, properties_datetime = NOW()";
		$this->db_dashboard->query($sql, array($input_params['local_seq'], $input_params['properties_key'], $input_params['properties_value'], $input_params['properties_value']));
		$db_insert_id = $this->db_dashboard->insert_id();
		//$this->db_dashboard->set('properties_datetime', 'NOW()', FALSE);
        //$this->db_dashboard->insert('local_account_properties', $input_params);
		$this->db_dashboard->trans_complete();
		return (int)$db_insert_id;
	}
	//-------------------------------------------------------------------------------
	function get_dashboard_roles() {
		$this->db_dashboard->select('DISTINCT role_id AS role_id, role_code, role_name', FALSE);
		$this->db_dashboard->from('dashboard_roles');
		$this->db_dashboard->where('role_id <', 4);
		$this->db_dashboard->order_by('role_name', 'ASC');
		$sql_query = $this->db_dashboard->get();
		return $sql_query->result();
	}
	function add_user($input_params) {
		$this->db_dashboard->trans_start();
        $this->db_dashboard->insert('local_account', $input_params);
        $db_insert_id = $this->db_dashboard->insert_id();
        $this->db_dashboard->trans_complete();
        return (int)$db_insert_id;
	}
	function edit_user($seq, $input_params) {
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$this->db_dashboard->where('seq', $seq);
		$this->db_dashboard->update('local_account', $input_params);
        return (int)$seq;
	}
	function insert_local_user_properties($seq, $input_params) {
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$query_params = array(
			'local_seq'					=> $seq,
			'properties_key'			=> (isset($input_params['properties_key']) ? $input_params['properties_key'] : ''),
			'properties_value'			=> (isset($input_params['properties_value']) ? $input_params['properties_value'] : ''),
			'properties_datetime'		=> $this->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'))->format('Y-m-d H:i:s'),
		);
		$query_params['properties_key'] = substr($query_params['properties_key'], 0, 64);
		$this->db_dashboard->trans_start();
		$this->db_dashboard->insert('local_account_properties', $query_params);
		$db_insert_id = $this->db_dashboard->insert_id();
		$this->db_dashboard->trans_complete();
		return (int)$db_insert_id;
	}
	function update_local_user_properties($seq, $input_params) {
		$seq = (is_numeric($seq) ? (int)$seq : 0);
		$query_params = array(
			'local_seq'					=> $seq,
			'properties_key'			=> (isset($input_params['properties_key']) ? $input_params['properties_key'] : ''),
			'properties_value'			=> (isset($input_params['properties_value']) ? $input_params['properties_value'] : ''),
			'properties_datetime'		=> $this->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'))->format('Y-m-d H:i:s'),
		);
		$query_params['properties_key'] = substr($query_params['properties_key'], 0, 64);
		$sql = sprintf("INSERT INTO local_account_properties(local_seq, properties_key, properties_value, properties_datetime) VALUES('%d', '%s', '%s', '%s') ON DUPLICATE KEY UPDATE properties_value = '%s', properties_datetime = NOW()",
			$query_params['local_seq'],
			$query_params['properties_key'],
			$query_params['properties_value'],
			$query_params['properties_datetime'],
			$query_params['properties_value']
		);
		$this->db_dashboard->trans_start();
		$sql_query = $this->db_dashboard->query($sql);
		$db_insert_id = $this->db_dashboard->insert_id();
		$this->db_dashboard->trans_complete();
		return (int)$db_insert_id;
	}
	function get_local_user_properties($local_seq) {
		$local_seq = (is_numeric($local_seq) ? (int)$local_seq : 0);
		$this->db_dashboard->select('*');
		$this->db_dashboard->from('local_account_properties');
		$this->db_dashboard->where('local_seq', $local_seq);
		$this->db_dashboard->order_by('properties_key', 'ASC');
		$sql_query = $this->db_dashboard->get();
		return $sql_query->result();
	}
	//-------------------------------------------------------------------------------
	function get_province($input_params = array()) {
		$query_params = array(
			'country_code'				=> (isset($input_params['country_code']) ? $input_params['country_code'] : '360'),
			'province_code'				=> (isset($input_params['province_code']) ? $input_params['province_code'] : ''),
			'city_code'					=> (isset($input_params['city_code']) ? $input_params['city_code'] : ''),
			'district_code'				=> (isset($input_params['district_code']) ? $input_params['district_code'] : ''),
			'area_code'					=> (isset($input_params['area_code']) ? $input_params['area_code'] : ''),
		);
		$sql = sprintf("SELECT province_code, province_name FROM dashboard_address_area WHERE country_code = '%d' GROUP BY province_code, province_name ORDER BY province_code ASC",
			$query_params['country_code']
		);
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
	}
	function get_city($input_params = array()) {
		$query_params = array(
			'country_code'				=> (isset($input_params['country_code']) ? $input_params['country_code'] : '360'),
			'province_code'				=> (isset($input_params['province_code']) ? $input_params['province_code'] : ''),
			'city_code'					=> (isset($input_params['city_code']) ? $input_params['city_code'] : ''),
			'district_code'				=> (isset($input_params['district_code']) ? $input_params['district_code'] : ''),
			'area_code'					=> (isset($input_params['area_code']) ? $input_params['area_code'] : ''),
		);
		$sql = sprintf("SELECT d.country_code, d.province_code, d.city_code, d.city_name FROM dashboard_address_area AS d WHERE (d.country_code = '%d' AND d.province_code = '%s') GROUP BY d.city_code ORDER BY d.city_name ASC",
			$query_params['country_code'],
			$query_params['province_code']
		);
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
	}
	function get_district($input_params = array()) {
		$query_params = array(
			'country_code'				=> (isset($input_params['country_code']) ? $input_params['country_code'] : '360'),
			'province_code'				=> (isset($input_params['province_code']) ? $input_params['province_code'] : ''),
			'city_code'					=> (isset($input_params['city_code']) ? $input_params['city_code'] : ''),
			'district_code'				=> (isset($input_params['district_code']) ? $input_params['district_code'] : ''),
			'area_code'					=> (isset($input_params['area_code']) ? $input_params['area_code'] : ''),
		);
		$sql = sprintf("SELECT d.country_code, d.province_code, d.city_code, d.city_name, d.district_code, d.district_name FROM dashboard_address_area AS d WHERE (d.country_code = '%d' AND d.province_code = '%s' AND d.city_code = '%s') GROUP BY d.district_code ORDER BY d.district_name ASC",
			$query_params['country_code'],
			$query_params['province_code'],
			$query_params['city_code']
		);
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
	}
	function get_area($input_params = array()) {
		$query_params = array(
			'country_code'				=> (isset($input_params['country_code']) ? $input_params['country_code'] : '360'),
			'province_code'				=> (isset($input_params['province_code']) ? $input_params['province_code'] : ''),
			'city_code'					=> (isset($input_params['city_code']) ? $input_params['city_code'] : ''),
			'district_code'				=> (isset($input_params['district_code']) ? $input_params['district_code'] : ''),
			'area_code'					=> (isset($input_params['area_code']) ? $input_params['area_code'] : ''),
		);
		$sql = sprintf("SELECT d.country_code, d.province_code, d.province_name, d.city_code, d.city_name, d.district_code, d.district_name, d.area_code, d.area_name FROM dashboard_address_area AS d WHERE (d.country_code = '%d' AND d.province_code = '%s' AND d.city_code = '%s' AND d.district_code) GROUP BY d.area_name ORDER BY d.area_name ASC",
			$query_params['country_code'],
			$query_params['province_code'],
			$query_params['city_code'],
			$query_params['district_code']
		);
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
	}
	//--------------------------------------------------
	// Utils
	//--------------------------------------------------
	function create_unique_datetime($timezone) {
		return $this->adminpanel->create_unique_datetime($timezone);
	}
	function create_dateobject($timezone, $format, $date) {
		return $this->adminpanel->create_dateobject($timezone, $format, $date);
	}
	//-- Email
	function send_email($mail_type, $mail_account, $input_params) {
		$config = array();
		// Mail Type Avaiable: google and mailgun
		$mail_type = (is_string($mail_type) ? strtolower($mail_type) : 'google');
		$config['useragent'] = 'CI Imzers Mailer';
		$config['protocol'] = 'smtp';
		
		$config['smtp_host'] = $mail_account['smtp_host'];
		$config['smtp_user'] = $mail_account['smtp_user'];
		$config['smtp_pass'] = $mail_account['smtp_pass'];
		$config['smtp_port'] = $mail_account['smtp_port'];
		
		$config['smtp_timeout'] = 5;
		$config['wordwrap'] = TRUE;
		$config['wrapchars'] = 76;
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['validate'] = FALSE;
		$config['priority'] = 3;
		$config['crlf'] = "\r\n";
		$config['newline'] = "\r\n";
		$config['bcc_batch_mode'] = FALSE;
		$config['bcc_batch_size'] = 200;
		$this->load->library('email'); // Note: no $config param needed
		$this->email->initialize($config);
		$this->email->from($input_params['sender_address'], $input_params['sender_name']);
		$this->email->to($input_params['email_address']);
		$this->email->reply_to($input_params['sender_address'], $input_params['sender_name']);
		$this->email->subject($input_params['email_subject']);
		$this->email->message($input_params['email_body']);
		$this->email->send();
	}
	function execute_send_email($mail_type, $input_params) {
		$mail_account = array();
		$mail_type = (is_string($mail_type) ? strtolower($mail_type) : $this->email_vendor);
		$query_params = array();
		$mail_account['smtp_host'] = $this->base_config['email_vendors'][$mail_type]['mail_hostname'];
		$mail_account['smtp_user'] = $this->base_config['email_vendors'][$mail_type]['mail_username'];
		$mail_account['smtp_pass'] = $this->base_config['email_vendors'][$mail_type]['mail_password'];
		$mail_account['smtp_port'] = $this->base_config['email_vendors'][$mail_type]['mail_port'];
		$input_params['sender_address'] = $this->base_config['email_vendors'][$mail_type]['mail_address'];
		$input_params['sender_name'] = $this->base_config['email_vendors'][$mail_type]['mail_name'];
		
		$query_params['sender_address'] = (isset($input_params['sender_address']) ? $input_params['sender_address'] : '');
		$query_params['sender_name'] = (isset($input_params['sender_name']) ? $input_params['sender_name'] : '');
		$query_params['email_address'] = (isset($input_params['account_email']) ? $input_params['account_email'] : '');
		$query_params['email_subject'] = (isset($input_params['account_action_subject']) ? $input_params['account_action_subject'] : '');
		$query_params['email_body'] = (isset($input_params['account_action_body']) ? $input_params['account_action_body'] : '');
		try {
			$this->send_email($mail_type, $mail_account, $query_params);
		} catch (Exception $ex) {
			exit("Cannot running function to send email: {$ex->getMessage()}");
		}
	}
	//-----------------------------------------------------------------------------------
	public function get_pagination_start($page, $per_page, $rows_count) {
		$end_page = ceil($rows_count / $per_page);
		if ($end_page < 2) { $end_page = 1; }
		$page = isset($page) ? intval($page) : 1;
		//if ($page < 1 || $page > $end_page) { $page = 1; }
		$start = ceil(($page * $per_page) - $per_page);
		if ($start > 0) {
			return $start;
		}
		return 0;
	}
	function generate_pagination_for($for_page, $url, $page, $per_page, $rows_count, $start) {
		$for_page = (is_string($for_page) ? strtolower($for_page) : 'dashboard');
		switch (strtolower($for_page)) {
			case 'home':
				return $this->adminpanel->generate_listeo_pagination_home("{$url}/%d", $page, $per_page, $rows_count, $start);
			break;
			case 'dashboard':
			default:
				return $this->adminpanel->generate_listeo_pagination_dashboard("{$url}/%d", $page, $per_page, $rows_count, $start);
			break;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function __destruct() {
		if ($this->db_dashboard) {
			$this->db_dashboard->close();
		}
		if ($this->db_report != NULL) {
			$this->db_report->close();
		}
	}
	
}