<?php 
if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
class Model_configuration extends CI_Model {
	protected $db_dashboard;
	protected $db_report;
	private $databases = array();
	public $userdata;
	public $localdata;
	function __construct() {
		parent::__construct();
		$this->load->library('adminpanel/Lib_Adminpanel', array(), 'adminpanel');
		$this->load->helper('base/base_config');
		$this->db_dashboard = $this->adminpanel->db_dashboard;
		$this->db_report = $this->adminpanel->db_report;
		
		$this->userdata = $this->adminpanel->userdata;
		$this->localdata = $this->adminpanel->localdata;
	}
	
	function get_config_item_by($by_key, $by_value = null) {
		$by_key = (isset($by_key) ? $by_key : '');
		if (is_string($by_key)) {
			$by_key = strtolower($by_key);
		} else {
			$by_key = '';
		}
		if (!isset($by_value)) {
			$by_value = '0';
		}
		$by_value = (is_string($by_value) ? $by_value : (is_numeric($by_value) ? (int)$by_value : 0));
		if (strlen($by_key) > 0) {
			switch ($by_key) {
				case 'seq':
					$by_value = (is_numeric($by_value) ? $by_value : 0);
					$sql = sprintf("SELECT * FROM dashboard_configuration WHERE seq = '%d'", $by_value);
				break;
				case 'code':
				default:
					$by_value = strtolower($by_value);
					$sql = sprintf("SELECT * FROM dashboard_configuration WHERE LOWER(config_code) = '%s'", $by_value);
				break;
			}
			try {
				$sql_query = $this->db_dashboard->query($sql);
			} catch (Exception $ex) {
				return array(
					'result'			=> false,
					'data'				=> $ex,
				);
			}
			if ($sql_query) {
				return array(
					'result'			=> true,
					'data'				=> $sql_query->result(),
				);
			}
		} else {
			return array(
				'result'			=> false,
				'data'				=> null,
			);
		}
	}
	function set_config_item_by($by_key, $by_value = null, $input_params = array()) {
		$by_key = (isset($by_key) ? $by_key : '');
		if (is_string($by_key)) {
			$by_key = strtolower($by_key);
		} else {
			$by_key = '';
		}
		if (!isset($by_value)) {
			$by_value = 0;
		}
		$by_value = (is_string($by_value) ? $by_value : '');
		$query_wheres = array();
		$query_params = array(
			'config_name'				=> (isset($input_params['config_name']) ? $input_params['config_name'] : ''),
			'config_description'		=> (isset($input_params['config_description']) ? $input_params['config_description'] : ''),
			'config_value'				=> (isset($input_params['config_value']) ? $input_params['config_value'] : ''),
			'config_datetime_update'	=> date('Y-m-d H:i:s'),
			'config_by'					=> (isset($this->adminpanel->localdata['account_email']) ? $this->adminpanel->localdata['account_email'] : 'system'),
			'config_active'				=> (isset($input_params['config_active']) ? $input_params['config_active'] : ''),
		);
		if (strlen($by_key) > 0) {
			switch ($by_key) {
				case 'seq':
					$by_value = (is_numeric($by_value) ? $by_value : 0);
					$query_wheres['seq'] = sprintf("%d", $by_value);
				break;
				case 'code':
				default:
					$by_value = strtolower($by_value);
					$query_wheres['config_code'] = sprintf("%s", $by_value);
				break;
			}
			if (count($query_wheres) > 0) {
				foreach ($query_wheres as $key => $val) {
					$this->db_dashboard->where($key, $val);
				}
			}
			//Update
			$this->db_dashboard->update('dashboard_configuration', $query_params);
			return true;
		} else {
			return false;
		}
	}
	function insert_config_item_by($by_value = null, $input_params = array()) {
		if (!isset($by_value)) {
			$by_value = '';
		}
		$by_value = (is_string($by_value) ? $by_value : '');
		$query_wheres = array();
		$query_params = array(
			'config_code'				=> '', // Set Later
			'config_name'				=> (isset($input_params['config_name']) ? $input_params['config_name'] : ''),
			'config_description'		=> (isset($input_params['config_description']) ? $input_params['config_description'] : ''),
			'config_value'				=> (isset($input_params['config_value']) ? $input_params['config_value'] : ''),
			'config_datetime_insert'	=> date('Y-m-d H:i:s'),
			'config_datetime_update'	=> date('Y-m-d H:i:s'),
			'config_by'					=> (isset($this->adminpanel->localdata['account_email']) ? $this->adminpanel->localdata['account_email'] : 'system'),
			'config_active'				=> (isset($input_params['config_active']) ? $input_params['config_active'] : 0),
		);
		$query_params['config_code'] = (isset($by_value) ? $by_value : '');
		$query_params['config_code'] = $this->make_permalink($query_params['config_code']);
		$db_insert_id = 0;
		if (strlen($by_value) > 0) {
			$query_params['config_code'] = substr($query_params['config_code'], 0, 64);
			//Insert
			$this->db_dashboard->trans_start();
			$this->db_dashboard->insert('dashboard_configuration', $query_params);
			$db_insert_id = $this->db_dashboard->insert_id();
			$this->db_dashboard->trans_complete();
		}
		return $db_insert_id;
	}
	//--
	function get_count_configs($searchText = '') {
		$this->db_dashboard->select('COUNT(conf.seq) AS value');
        $this->db_dashboard->from('dashboard_configuration AS conf');
        if (!empty($searchText)) {
			if (is_string($searchText)) {
				$searchLike = "(
					(CONCAT('', conf.config_code, '')  LIKE '%{$searchText}%')
					OR (CONCAT('', conf.config_name, '') LIKE '%{$searchText}%')
				)";
				$this->db_dashboard->where($searchLike);
			}
        }
		return $this->db_dashboard->get()->row();
	}
	function get_data_configs($searchText = '', $page, $limit) {
		$sql = "SELECT conf.* FROM dashboard_configuration AS conf";
        if (!empty($searchText)) {
			if (is_string($searchText)) {
				$searchLike = "(
					(CONCAT('', conf.config_code, '')  LIKE '%{$searchText}%')
					OR (CONCAT('', conf.config_name, '') LIKE '%{$searchText}%')
				)";
				$sql .= " WHERE {$searchLike}";
			}
        }
		$sql .= " LIMIT {$page}, {$limit}";
		$sql_query = $this->db_dashboard->query($sql);
		return $sql_query->result();
		
	}
	
	
	
	
	
	//================ Utils
	private function make_permalink($url) {
		$url = strtolower($url);
		$url = preg_replace('/&.+?;/', '', $url);
		$url = preg_replace('/\s+/', '_', $url);
		$url = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '_', $url);
		$url = preg_replace('|%|', '_', $url);
		$url = preg_replace('/&#?[a-z0-9]+;/i', '', $url);
		$url = preg_replace('/[^%A-Za-z0-9 \_\-]/', '_', $url);
		$url = preg_replace('|_+|', '-', $url);
		$url = preg_replace('|-+|', '-', $url);
		$url = trim($url, '-');
		$url = (strlen($url) > 128) ? substr($url, 0, 128) : $url;
		return $url;
	}
	
	
	
	
	
	
	
	
}






