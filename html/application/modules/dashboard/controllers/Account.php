<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {
	public $isAdmin = FALSE;
	public $isMerchant = FALSE;
	protected $DateObject;
	protected $email_vendor;
	public $error = FALSE, $error_msg = array();
	protected $firebase_credential;
	protected $base_config = array();
	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
		# Load Base Config
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		
		$this->load->model('adminpanel/Model_adminpanel', 'mod_adminpanel');
		$this->load->model('adminpanel/Model_firebase', 'mod_firebase');
		$this->load->model('adminpanel/Model_configuration', 'mod_config');
		$this->load->library('adminpanel/Lib_Rc4Crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('3', '4'))) {
			$this->isAdmin = TRUE;
		}
		if (!$this->isAdmin) {
			if (in_array($this->mod_adminpanel->localdata['account_role'], array('2', '5'))) {
				$this->isMerchant = TRUE;
			}
		}
		$this->DateObject = $this->mod_adminpanel->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'));
		$this->load->library('adminpanel/Lib_Firebase', '', 'firebase');
		//
		$this->email_vendor = $this->base_config['email_vendor'];
		$this->firebase_credential = $this->base_config['firebase_credential'];
	}
	private function take_configuration_item($config_code) {
		$items = false;
		try {
			$config_items = $this->mod_config->get_config_item_by('code', $config_code);
		} catch (Exception $ex) {
			return false;
		}
		if (isset($config_items['data'])) {
			if (is_array($config_items['data']) && (count($config_items['data']) > 0)) {
				foreach ($config_items['data'] as $keval) {
					$items = $keval;
					break;
				}
			}
		}
		return $items;
	}
	//------------------------------------------------------------------------------------------------------------
	function loginaction() {
		$input_params = array();
		$this->form_validation->set_rules('user_server', 'Login Server', 'required|max_length[32]');
		$user_server = $this->input->post('user_server');
		if (strlen($user_server) === 0) {
			$user_server = 'goodgames';
		}
		switch (strtolower($user_server)) {
			case 'local':
				$this->form_validation->set_rules('user_email', 'Email', 'trim|valid_email|max_length[128]|xss_clean');
			break;
			case 'goodgames':
			default:
				$this->form_validation->set_rules('user_username', 'Username', 'trim|required|max_length[128]|xss_clean');
			break;
		}
		$this->form_validation->set_rules('user_password', 'Password', 'required|max_length[32]');
		if ($this->form_validation->run() == FALSE) {
            $login = FALSE;
			$this->session->set_flashdata('error', 'Some required field is not filled.');
			redirect(base_url('home'));
			exit;
        } else {
			$input_params['body'] = array(
				'user_email'				=> $this->input->post('user_email'),
				'user_username'				=> $this->input->post('user_username'),
				'user_password'				=> $this->input->post('user_password'),
			);
			switch (strtolower($user_server)) {
				case 'local':
					$input_params['body']['loginLocal'] = 'true';
					$login = $this->mod_adminpanel->local_login($input_params);
				break;
				case 'goodgames':
				default:
					$input_params['body']['loginGoodgames'] = 'true';
					$login = $this->mod_adminpanel->gg_login($input_params);
				break;
			}
		}
		
		if ($login != FALSE) {
			if (isset($login['success'])) {
				if ($login['success'] === TRUE) {
					$sessionArray = array(
						'isLoggedIn' 	=> TRUE,
					);
					$this->session->set_userdata($sessionArray);
					$this->session->set_flashdata('success', "Success Logged In");
				} else {
					$this->session->set_flashdata('error', "Failed login, please check your username and password");
				}
			} else {
				$this->session->set_flashdata('error', "Something error persist");
			}
		} else {
			$this->session->set_flashdata('error', 'Account not found');
		}
		
		
		redirect(base_url('home'));
	}
	function logout() {
		$this->mod_adminpanel->userdata = null;
		$this->mod_adminpanel->localdata = null;
		if ($this->session->userdata('gg_login_account') != FALSE) {
			$this->session->unset_userdata('gg_login_account');
		}
		if ($this->session->userdata('local_login_account') != FALSE) {
			$this->session->unset_userdata('local_login_account');
		}
		$this->session->sess_destroy();
		redirect(base_url('home/logout'));
		exit;
	}
	
	function registerUsername() {
		$username = $this->input->post('user_name');
		$username = (is_string($username) ? $username : '');
		$safe_username = "";
		if (strlen($username) > 0) {
			$safe_username = $this->firebase->permalink($username);
		}
		$return = array('username' => $safe_username);
		echo json_encode($return);
	}
	function registeraction() {
		//--------------------
		$this->load->model('adminpanel/Model_users', 'mod_users');
		//--------------------
		$error = FALSE;
		$error_msg = [];
		$collectData = array();
		$collectData['page'] = 'dashboard-register';
		$collectData['collect'] = array();
		if ($this->session->userdata('gg_login_account') != FALSE) {
			redirect(base_url('dashboard'));
			exit;
		}
		//----------------------------------------------------------------------------------------------------------
		$collectData['collect']['roles'] = $this->mod_adminpanel->get_dashboard_roles();
		if (!$this->isAdmin) {
			$this->form_validation->set_rules('user_fullname','Full Name','trim|required|max_length[128]|xss_clean');
			//$this->form_validation->set_rules('user_nickname','Nickname','trim|required|max_length[128]|xss_clean');
			$this->form_validation->set_rules('user_username','Username','trim|required|max_length[64]|xss_clean');
			$this->form_validation->set_rules('user_email','Email','trim|required|valid_email|xss_clean|max_length[128]');
			$this->form_validation->set_rules('user_password','Password','required|max_length[64]');
			$this->form_validation->set_rules('user_password_confirm','Password Confirm','required|matches[user_password]|max_length[64]');
			$this->form_validation->set_rules('user_phonenumber', 'Phone Number', 'xss_clean');
			$this->form_validation->set_rules('user_phonemobile', 'Mobile Number', 'xss_clean');
			if($this->form_validation->run() == FALSE) {
				$validation_error_msg = validation_errors('<li>', '</li>');
				$this->session->set_flashdata('error', 'Something error with required fields: ' . $validation_error_msg);
				redirect(base_url('home'));
				exit;
			} else {
				$input_params = array(
					'user_fullname'						=> $this->input->post('user_fullname'),
					'user_nickname'						=> $this->input->post('user_username'),
					'user_username'						=> $this->input->post('user_username'), // required unique
					'user_email'						=> $this->input->post('user_email'), // required unique
					'user_password'						=> $this->input->post('user_password'),
					'user_password_confirm'				=> $this->input->post('user_password_confirm'),
					'user_phonenumber'					=> $this->input->post('user_phonenumber'),
					'user_phonemobile'					=> $this->input->post('user_phonemobile'),
					'user_address'						=> $this->input->post('user_address'),
					'user_role'							=> '1',
					'subscription_starting'				=> $this->DateObject->format('Y-m-d H:i:s'),
					'subscription_expiring'				=> '', // Later
					'account_activation_ending'			=> '', // Later -> get 1 week
					'account_active'					=> 'N',
					'account_remark'					=> '',
				);
				$input_params['user_email'] = filter_var($input_params['user_email'], FILTER_VALIDATE_EMAIL);
				$input_params['user_phonenumber'] = sprintf('%s', $input_params['user_phonenumber']);
				$input_params['user_phonemobile'] = sprintf('%s', $input_params['user_phonemobile']);
				$input_params['user_role'] = sprintf('%d', $input_params['user_role']);
				$input_params['user_fullname'] = sprintf('%s', $input_params['user_fullname']);
				
				
				$query_params = array(
					'account_username'				=> ((strlen($input_params['user_username']) > 0) ? strtolower($input_params['user_username']) : ''),
					'account_email'					=> ((strlen($input_params['user_email']) > 0) ? strtolower($input_params['user_email']) : ''),
					'account_hash'					=> '', // Later
					'account_password'				=> '', // Later
					'account_inserting_datetime'	=> $this->DateObject->format('Y-m-d H:i:s'),
					'account_inserting_remark'		=> ((strlen($input_params['account_remark']) > 0) ? $input_params['account_remark'] : ''),
					'account_activation_code'		=> '', // Later,
					'account_activation_starting'	=> $this->DateObject->format('Y-m-d H:i:s'),
					'account_activation_ending'		=> $this->DateObject->format('Y-m-d H:i:s'), // Later will add 7 days
					'account_activation_datetime'	=> NULL,
					'account_activation_status'		=> 'N', // N as default
					'account_activation_by'			=> '', // Should be user itself or by administrator (Set Later)
					'account_active'				=> ((strlen($input_params['account_active']) > 0) ? strtoupper($input_params['account_active']) : 'N'),
					'account_role'					=> ((strlen($input_params['user_role']) > 0) ? $input_params['user_role'] : 0),
					'account_nickname'				=> ((strlen($input_params['user_nickname']) > 0) ? $input_params['user_nickname'] : ''),
					'account_fullname'				=> ((strlen($input_params['user_fullname']) > 0) ? ucwords(strtolower($input_params['user_fullname'])) : ''),
					'account_address'				=> ((strlen($input_params['user_address']) > 0) ? $input_params['user_address'] : ''),
					'account_phonenumber'			=> ((strlen($input_params['user_phonenumber']) > 0) ? $input_params['user_phonenumber'] : ''),
					'account_phonemobile'			=> ((strlen($input_params['user_phonemobile']) > 0) ? $input_params['user_phonemobile'] : ''),
					'account_delete_status'			=> 0, // 0 as default(not deleted)
					'account_delete_datetime'		=> NULL,
					'account_delete_by'				=> '', // Should be user itself or system or by administrator (Set Later)
					'account_edited_datetime'		=> NULL,
					'account_edited_by'				=> NULL,
					'subscription_starting'			=> $this->DateObject->format('Y-m-d H:i:s'), // NOW()
					'subscription_expiring'			=> $this->DateObject->format('Y-m-d H:i:s'), // Current NOW() and will add 1 Year
				);
				//-------------------------------------
				if (!$error) {
					if ($input_params['user_password'] !== $input_params['user_password_confirm']) {
						$error = true;
						$error_msg[] = "Password and confirm password not match";
					}
				}
				if (!$error) {
					try {
						$account_activation_ending = new DateTime($query_params['account_activation_ending']);
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "account-activation-ending: {$ex->getMessage()}";
					}
				}
				if (!$error) {
					$query_params['account_activation_ending'] = $account_activation_ending->add(new DateInterval('P7D')); // Add 7 Days for date-activation-expired
					$query_params['account_activation_ending'] = $account_activation_ending->format('Y-m-d H:i:s');
					try {
						$subscription_starting = new DateTime($query_params['subscription_starting']);
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "account-activation-ending: {$ex->getMessage()}";
					}
				}
				if (!$error) {
					$query_params['subscription_starting'] = $subscription_starting->format('Y-m-d H:i:s');
					try {
						$subscription_expiring = new DateTime($query_params['subscription_expiring']);
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "subscription-expiring: {$ex->getMessage()}";
					}
				}
				if (!$error) {
					$query_params['subscription_expiring'] = $subscription_expiring->add(new DateInterval('P1Y')); // Add 1 Year For Default
					$query_params['subscription_expiring'] = $subscription_expiring->format('Y-m-d H:i:s');
				}
				if (!$error) {
					try {
						$input_params['unique_datetime'] = $this->mod_adminpanel->create_unique_datetime("Asia/Bangkok");
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot create new unique datetime: {$ex->getMessage()}";
					}
				}
				//-------------------------------------
				if (!$error) {
					$query_params['account_hash'] = $this->rc4crypt->bEncryptRC4($input_params['unique_datetime']);
					try {
						$input_params['account_password'] = ("{$this->rc4crypt->bDecryptRC4($query_params['account_hash'])}|{$input_params['user_password']}");
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = $ex->getMessage();
					}
				}
				if (!$error) {
					$query_params['account_password'] = sha1($input_params['account_password']);
					$query_params['account_activation_code'] = md5($query_params['account_hash']);
				}
				if (!$error) {
					if (strtoupper($query_params['account_active']) === strtoupper('Y')) {
						$query_params['account_activation_status'] = 'Y';
					} else {
						$query_params['account_activation_status'] = 'N';
					}
				}
				if (!$error) {
					if (strtoupper($query_params['account_activation_status']) === strtoupper('Y')) {
						$query_params['account_activation_datetime'] = $this->DateObject->format('Y-m-d H:i:s');
						$query_params['account_activation_by'] = 'register-system';
					}
				}
				//-- Check username or email exists
				if (!$error) {
					try {
						$local_users = $this->mod_users->get_local_user_by($query_params['account_username'], 'username');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot check username is exists from model.";
					}
				}
				if (!$error) {
					if (count($local_users) > 0) {
						$error = true;
						$error_msg[] = "Username already taken.";
					}
				}
				if (!$error) {
					try {
						$local_users = $this->mod_users->get_local_user_by($query_params['account_email'], 'email');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot check email is exists from model.";
					}
				}
				if (!$error) {
					if (count($local_users) > 0) {
						$error = true;
						$error_msg[] = "Email already taken.";
					}
				}
				//-- Add User to Database
				if (!$error) {
					try {
						$new_account_seq = $this->mod_adminpanel->add_user($query_params);
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot insert new user to local-account.";
					}
				}
				if (!$error) {
					$local_data_properties = array();
					$local_data_properties['user_address_province'] = $this->input->post('user_address_province');
					$local_data_properties['user_address_city'] = $this->input->post('user_address_city');
					$local_data_properties['user_address_district'] = $this->input->post('user_address_district');
					$local_data_properties['user_address_area'] = $this->input->post('user_address_area');
					$local_data_properties['user_address'] = $this->input->post('user_address');
					if ((int)$new_account_seq > 0) {
						foreach ($local_data_properties as $key => $val) {
							if (strlen($val) > 0) {
								$local_properties_params = array(
									'properties_key'				=> strtolower($key),
									'properties_value'				=> $val,
								);
								$new_propertie_seq = $this->mod_adminpanel->insert_local_user_properties($new_account_seq, $local_properties_params);
							}
						}
					}
				}
				# Send email
				if (!$error) {
					$query_params['account_action_subject'] = "New User Registration";
					$query_params['account_action_body'] = "";
					try {
						$local_data = $this->mod_adminpanel->get_local_user_by($new_account_seq, 'seq');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot get new registration of local-user data.";
					}
				}
				if (!$error) {
					//-----------------------
					$query_params['account_action_body'] .= "You just register on " . show_header_title('adminpanel/form-login') . "<br/>";
					$query_params['account_action_body'] .= "Active your account by visit activation code below:<br/>";
					$query_params['account_action_body'] .= "<a href='" . base_url("dashboard/account/activation/{$local_data[0]->account_activation_code}") . "'>" . base_url("dashboard/account/activation/{$local_data[0]->account_activation_code}") . "</a><br/>";
					
					$query_params['account_action_body'] .= "<br/>----<br/>";
					$query_params['account_action_body'] .= show_header_title('adminpanel/form-login') . "<br/>";
					$query_params['account_action_body'] .= show_footer_string('user/user-includes/version');
					//-----------------------
				}
				# Send-email action
				if (!$error) {
					$this->mod_adminpanel->execute_send_email($this->email_vendor, $query_params);
				}
				if (!$error) {
					$this->session->set_flashdata('success', 'Success register new user, please check your email for activation code.');
					redirect(base_url('home'));
				} else {
					$error_to_show = "";
					foreach ($this->error_msg as $keval) {
						if (is_string($keval) || is_numeric($keval)) {
							$error_to_show .= sprintf("%s", $keval);
						} else if (is_object($keval) || is_array($keval)) {
							$error_to_show .= json_encode($keval);
						} else {
							$error_to_show .= "-";
						}
					}
					$this->session->set_flashdata('error', $error_to_show);
					redirect(base_url('home'));
					exit;
				}
			}
		}
		
		exit;
	}
	function login() {
		$collectData = array(
			'page'					=> 'form-login',
			'collect'				=> array(),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'Dashboard: Login',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if ($collectData['collect']['userdata'] != FALSE) {
				$this->error = true;
				$this->error_msg[] = "User already logged-in.";
				redirect(base_url('home'));
				exit;
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$collectData['take_config_items'] = array(
					'copyright',
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
					'telp-redaksi',
					'alamat-redaksi',
					'home-description-head',
					'home-description-body',
					'home-stage-kiri',
					'home-stage-kanan',
					'home-stage-tengah',
					# HOME Categories
					'dongeng-pengantar-tidur',
					'seri-pengembangan-karakter',
					'cerita-berdasarkan-umur',
				);
				foreach ($collectData['take_config_items'] as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		//-------------------------------------------------------------------------------------------------------
		
			
		
		
		//--------------
		// Debug
		/*
		echo "<pre>";
		if (!$this->error) {
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		*/
		
		
		
		if (!$this->error) {
			$this->load->view($collectData['base_home_path'] . '/home.php', $collectData);
			
		} else {
			$action_message = "";
			foreach ($this->error_msg as $error_msg) {
				$action_message .= "- {$error_msg}<br/>";
			}
			$this->session->set_flashdata('action_message', $action_message);
			redirect(base_url("{$collectData['base_home_path']}/home/error"));
			exit;
		}
		
	}
	function loginfirebase() {
		$collectData = array(
			'page'					=> 'form-login-firebase',
			'collect'				=> array(),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'Dashboard: Login',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if ($collectData['collect']['userdata'] != FALSE) {
				$this->error = true;
				$this->error_msg[] = "User already logged-in.";
				$collectData['response'] = array(
					'success'					=> true,
					'msg'						=> 'User alredy logged-in',
				);
				echo json_encode($collectData['response']);
				exit;
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$collectData['take_config_items'] = array(
					'copyright',
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
					'telp-redaksi',
					'alamat-redaksi',
					'home-description-head',
					'home-description-body',
					'home-stage-kiri',
					'home-stage-kanan',
					'home-stage-tengah',
					# HOME Categories
					'dongeng-pengantar-tidur',
					'seri-pengembangan-karakter',
					'cerita-berdasarkan-umur',
				);
				foreach ($collectData['take_config_items'] as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		//---------------------------------------------------------------------------------------------------------
		if (!$this->error) {
			try {
				$collectData['collect']['firebase_input_params'] = $this->mod_adminpanel->firebase_input();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get post-params from mod-adminpanel with exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			if (!isset($collectData['collect']['firebase_input_params']['input_params']['body'])) {
				$this->error = true;
				$this->error_msg[] = "There is no input-params body.";
			} else {
				if (is_array($collectData['collect']['firebase_input_params']['input_params']['body']) && (count($collectData['collect']['firebase_input_params']['input_params']['body']) > 0)) {
					$collectData['query_params'] = array(
						'firebase_provider'				=> (isset($collectData['collect']['firebase_input_params']['input_params']['body']['firebase_provider']) ? $collectData['collect']['firebase_input_params']['input_params']['body']['firebase_provider'] : ''),
						'firebase_identity'				=> (isset($collectData['collect']['firebase_input_params']['input_params']['body']['firebase_identity']) ? $collectData['collect']['firebase_input_params']['input_params']['body']['firebase_identity'] : ''),
						'firebase_uid'					=> (isset($collectData['collect']['firebase_input_params']['input_params']['body']['firebase_uid']) ? $collectData['collect']['firebase_input_params']['input_params']['body']['firebase_uid'] : ''),
						'firebase_email'				=> (isset($collectData['collect']['firebase_input_params']['input_params']['body']['firebase_email']) ? $collectData['collect']['firebase_input_params']['input_params']['body']['firebase_email'] : ''),
						'firebase_fullname'				=> (isset($collectData['collect']['firebase_input_params']['input_params']['body']['firebase_fullname']) ? $collectData['collect']['firebase_input_params']['input_params']['body']['firebase_fullname'] : ''),
					);
					$collectData['query_params']['firebase_provider'] = ((is_string($collectData['query_params']['firebase_provider']) || is_numeric($collectData['query_params']['firebase_provider'])) ? sprintf("%s", $collectData['query_params']['firebase_provider']) : '');
					$collectData['query_params']['firebase_identity'] = ((is_string($collectData['query_params']['firebase_identity']) || is_numeric($collectData['query_params']['firebase_identity'])) ? sprintf("%s", $collectData['query_params']['firebase_identity']) : '');
					$collectData['query_params']['firebase_uid'] = ((is_string($collectData['query_params']['firebase_uid']) || is_numeric($collectData['query_params']['firebase_uid'])) ? sprintf("%s", $collectData['query_params']['firebase_uid']) : '');
					$collectData['query_params']['firebase_email'] = ((is_string($collectData['query_params']['firebase_email']) || is_numeric($collectData['query_params']['firebase_email'])) ? sprintf("%s", $collectData['query_params']['firebase_email']) : '');
					$collectData['query_params']['firebase_fullname'] = ((is_string($collectData['query_params']['firebase_fullname']) || is_numeric($collectData['query_params']['firebase_email'])) ? sprintf("%s", $collectData['query_params']['firebase_fullname']) : '');
					try {
						$collectData['collect']['firebase_login'] = $this->mod_adminpanel->firebase_login($collectData['query_params']);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Exception error while call firebase-login from mod-adminpanel: {$ex->getMessage()}";
					}
				} else {
					$this->error = true;
					$this->error_msg[] = "No input-params body content.";
				}
			}
		}
		if (!$this->error) {
			$collectData['response'] = array(
				'success'				=> false,
			);
			if ($collectData['collect']['firebase_login'] != FALSE) {
				if (isset($collectData['collect']['firebase_login']['success'])) {
					if ($collectData['collect']['firebase_login']['success'] === TRUE) {
						$sessionArray = array(
							'isLoggedIn' 	=> TRUE,
						);
						$this->session->set_userdata($sessionArray);
						$collectData['response']['success'] = true;
						$collectData['response']['msg'] = "Success Logged In";
					} else {
						$collectData['response']['msg'] = "Failed login, please check your username and password";
						if (isset($collectData['collect']['firebase_login']['error'])) {
							$collectData['response']['errors'] = $collectData['collect']['firebase_login']['error'];
						}
					}
				} else {
					$collectData['response']['msg'] = "Something error persist";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Account not found";
			}
		}
		//---------------------------------------------------------------------------------------------------------
		if (!$this->error) {
			echo json_encode($collectData['response']);
		} else {
			$collectData['response'] = array(
				'success'				=> false,
				'msg'					=> $this->error_msg,
			);
			echo json_encode($collectData['response']);
		}	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//============================================================================================
	private function set_stories_categories($stories_seq, $categories) {
		$stories_categories = array(
			'tmp'						=> array(),
			'data'						=> array(),
		);
		if (isset($categories['categories'])) {
			if (is_array($categories['categories']) && (count($categories['categories']) > 0)) {
				$for_i = 0;
				$stories_categories['tmp'][$for_i] = array(
					'title'							=> '',
					'subtitles'						=> array(),
					'subtitles_items'				=> array(),
				);
				foreach ($categories['categories'] as $keval) {
					$stories_categories['tmp'][$for_i]['title'] = (isset($keval['title']) ? $keval['title'] : '');
					if (isset($keval['items'])) {
						$val_i = 0;
						if (count($keval['items']) > 0) {
							foreach ($keval['items'] as $val) {
								if (isset($categories['get_categories']['data'][$keval['title']][$val])) {
									if (array_key_exists($stories_seq, $categories['get_categories']['data'][$keval['title']][$val])) {
										if (!empty($categories['get_categories']['data'][$keval['title']][$val][$stories_seq]) && (is_array($categories['get_categories']['data'][$keval['title']][$val][$stories_seq]))) {
											$stories_categories['tmp'][$for_i]['subtitles'][] = $val;
											$stories_categories['tmp'][$for_i]['subtitles_items'][] = count($categories['get_categories']['data'][$keval['title']][$val]);
										}
									}
								}
								$val_i++;
							}
						}
					}
					$for_i += 1;
				}
			}
		}
		//-------
		if (count($stories_categories['tmp']) > 0) {
			foreach ($stories_categories['tmp'] as &$keval) {
				if (isset($keval['subtitles'])) {
					if (is_array($keval['subtitles'])) {
						$keval['count'] = count($keval['subtitles']);
						if ($keval['count'] > 0) {
							$stories_categories['data'][] = $keval;
						}
					}
				}
			}
		}
		//---
		
		return $stories_categories;
	}
	
	
	//------------------------------------------------------------------------
	function passwordForget() {
		$error = FALSE;
		$error_msg = [];
		$collectData = array(
			'page'						=> 'form-password-forget',
			'collect'					=> array(),
			'base_home_path'			=> 'home',
			'base_dashboard_path'		=> 'dashboard',
		);
		if (!$this->mod_adminpanel->localdata) {
			$collectData['collect']['address-province'] = $this->mod_adminpanel->get_province(360);
			$this->load->view("{$collectData['base_dashboard_path']}/dashboard.php", $collectData);
		} else {
			redirect(base_url('home'));
			exit;
		}
	}
	function passwordForgetAction() {
		$error = FALSE;
		$error_msg = [];
		$collectData = array();
		$collectData['page'] = 'form-password-forget';
		$collectData['collect'] = array();
		if ($this->session->userdata('gg_login_account') != FALSE) {
			redirect(base_url('dashboard'));
			exit;
		}
		//----------------------------------------------------------------------------------------------------------
		$collectData['collect']['roles'] = $this->mod_adminpanel->get_dashboard_roles();
		if (!$this->isAdmin) {
			$this->form_validation->set_rules('user_email','Email','trim|required|valid_email|xss_clean|max_length[128]');
			if($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('error', 'Something error with required fields');
				redirect(base_url('adminpanel/passwordForget'));
				exit;
			} else {
				$input_params = array(
					'user_email'					=> $this->input->post('user_email'), // required unique
				);
				$input_params['user_email'] = filter_var($input_params['user_email'], FILTER_VALIDATE_EMAIL);
	
				
				$query_params = array(
					'account_email'					=> ((strlen($input_params['user_email']) > 0) ? strtolower($input_params['user_email']) : ''),
					'account_inserting_remark'		=> 'Request new password',
					'account_activation_code'		=> md5($this->mod_adminpanel->create_unique_datetime("Asia/Bangkok")),
				);
				//-------------------------------------
				//-- Check email exists
				if (!$error) {
					try {
						$local_users = $this->mod_adminpanel->get_local_user_by($query_params['account_email'], 'email');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot check email is exists from model while doing activation request.";
					}
				}
				if (!$error) {
					if (count($local_users) === 0) {
						$error = true;
						$error_msg[] = "Email is not registered on system.";
					}
				}
				if (!$error) {
					if (!isset($local_users[0])) {
						$error = true;
						$error_msg[] = "Un-expected data index from models.";
					}
				}
				if (!$error) {
					$local_user_properties = array();
					$query_params['account_salt'] = $this->rc4crypt->bDecryptRC4($local_users[0]->account_hash);
					$query_params['account_password_string'] = "{$query_params['account_salt']}|{$query_params['account_activation_code']}";
					$query_params['account_password_hash'] = sha1($query_params['account_password_string']);
				}
				//# Check amount of max-allowed request new-password per-day
				# password-count
				if (!$error) {
					try {
						$local_user_properties['password-count'] = $this->mod_adminpanel->get_local_user_properties_by($local_users[0]->seq, '', 'password-count');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot get user properties by password-count: {$ex->getMessage()}";
					}
				}
				if (!$error) {
					if (count($local_user_properties['password-count']) === 0) {
						try {
							$set_local_user_properties_seq = $this->mod_adminpanel->set_local_user_properties_by($local_users[0]->seq, 1, 'password-count');
						} catch (Exception $ex) {
							$error = true;
							$error_msg[] = "Cannot set local-user-propeties with key password-count: {$ex->getMessage()}";
						}
					} else {
						$set_local_user_properties_seq = 0;
					}
				}
				if ((int)$set_local_user_properties_seq > 0) {
					try {
						$local_user_properties['password-count'] = $this->mod_adminpanel->get_local_user_properties_by($local_users[0]->seq, $set_local_user_properties_seq, 'seq');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot get user properties by seq on password-count: {$ex->getMessage()}";
					}
				}
				if (!$error) {
					if (!isset($local_user_properties['password-count'][0])) {
						$error = true;
						$error_msg[] = "local-user properties data no as expected index format by password-count";
					}
				}
				# password-string
				if (!$error) {
					try {
						$local_user_properties['password-string'] = $this->mod_adminpanel->get_local_user_properties_by($local_users[0]->seq, $query_params['account_password_string'], 'password-string');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot get user properties by password-string: {$ex->getMessage()}";
					}
				}
				if (!$error) {
					if (count($local_user_properties['password-string']) === 0) {
						try {
							$set_local_user_properties_seq = $this->mod_adminpanel->set_local_user_properties_by($local_users[0]->seq, $query_params['account_password_string'], 'password-string');
						} catch (Exception $ex) {
							$error = true;
							$error_msg[] = "Cannot set local-user-propeties with key password-string: {$ex->getMessage()}";
						}
					} else {
						$set_local_user_properties_seq = 0;
					}
				}
				if ((int)$set_local_user_properties_seq > 0) {
					try {
						$local_user_properties['password-string'] = $this->mod_adminpanel->get_local_user_properties_by($local_users[0]->seq, $set_local_user_properties_seq, 'seq');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot get user properties by seq on password-string: {$ex->getMessage()}";
					}
				}
				if (!$error) {
					if (!isset($local_user_properties['password-string'][0])) {
						$error = true;
						$error_msg[] = "local-user properties data no as expected index format by password-string";
					}
				}
				# password-hash
				if (!$error) {
					try {
						$local_user_properties['password-hash'] = $this->mod_adminpanel->get_local_user_properties_by($local_users[0]->seq, $query_params['account_password_hash'], 'password-hash');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot get user properties by password-hash: {$ex->getMessage()}";
					}
				}
				if (!$error) {
					if (count($local_user_properties['password-hash']) === 0) {
						try {
							$set_local_user_properties_seq = $this->mod_adminpanel->set_local_user_properties_by($local_users[0]->seq, $query_params['account_password_hash'], 'password-hash');
						} catch (Exception $ex) {
							$error = true;
							$error_msg[] = "Cannot set local-user-propeties with key password-hash: {$ex->getMessage()}";
						}
					} else {
						$set_local_user_properties_seq = 0;
					}
				}
				if ((int)$set_local_user_properties_seq > 0) {
					try {
						$local_user_properties['password-hash'] = $this->mod_adminpanel->get_local_user_properties_by($local_users[0]->seq, $set_local_user_properties_seq, 'seq');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot get user properties by seq on password-hash: {$ex->getMessage()}";
					}
				}
				if (!$error) {
					if (!isset($local_user_properties['password-hash'][0])) {
						$error = true;
						$error_msg[] = "local-user properties data no as expected index format by password-hash";
					}
				}
				//--------------------------------------------------------------------------------------------------
				// set only 5 time per-day
				if (!$error) {
					if (intval($local_user_properties['password-count'][0]->properties_value) > 5) {
						$error = true;
						$error_msg[] = "Only 5 times per-day for requesting new password, please try again on another day or tomorrow.";
					}
				}
				if (!$error) {
					$request_password_count = 1;
					if (date('Ymd', strtotime($local_user_properties['password-count'][0]->properties_datetime)) === date('Ymd')) {
						$request_password_count = (intval($local_user_properties['password-count'][0]->properties_value) + 1);
					}
					try {
						$set_local_user_properties_seq = $this->mod_adminpanel->set_local_user_properties_by($local_users[0]->seq, $request_password_count, 'password-count');
					} catch (Exception $ex) {
						$error = true;
						$error_msg[] = "Cannot set local-user-propeties by update on password-count: {$ex->getMessage()}";
					}
				}
				//-------------------------------------
				# Send email
				if (!$error) {
					$query_params['account_action_subject'] = 'New Password Request';
					$query_params['account_action_body'] = '';
					//-----------------------
					$query_params['account_action_body'] .= "You just request new password code on " . show_header_title('adminpanel/form-login') . "<br/>";
					$query_params['account_action_body'] .= "Please change your new password by visit activation code below:<br/>";
					$query_params['account_action_body'] .= "<a href='" . base_url("adminpanel/passwordNew/{$local_user_properties['password-hash'][0]->properties_value}") . "'>" . base_url("adminpanel/passwordNew/{$local_user_properties['password-hash'][0]->properties_value}") . "</a><br/>";
					
					$query_params['account_action_body'] .= "<br/>----<br/>";
					$query_params['account_action_body'] .= show_header_title('adminpanel/form-login') . "<br/>";
					$query_params['account_action_body'] .= show_footer_string('user/user-includes/version');
					//-----------------------
				}
				# Send-email action
				if (!$error) {
					$this->send_email($this->email_vendor, $query_params);
				}
				//---- Redirect back to request-password page
				if (!$error) {
					$this->session->set_flashdata('success', 'New password activation code email at ' . $local_users[0]->account_email);
					redirect(base_url('adminpanel/passwordForget'));
				} else {
					$error_to_show = "";
					foreach ($error_msg as $keval) {
						$error_to_show .= $keval;
					}
					$this->session->set_flashdata('error', $error_to_show);
					redirect(base_url('adminpanel/passwordForget'));
				}
			}
		}
	}
	function passwordNew($activation_code = '') {
		$error = false;
		$error_msg = [];
		//----
		$activation_code = (is_string($activation_code) ? strtolower($activation_code) : '');
		$activation_code = sprintf('%s', $activation_code);
		if (strlen($activation_code) === 0) {
			$this->session->set_flashdata('error', 'Password activation code should not be empty.');
			redirect(base_url('adminpanel/passwordForget'));
			exit;
		}
		$query_params = array();
		$input_params = array(
			'account_activation_code'		=> substr($activation_code, 0, 64),
		);
		//---------------------------------------------------------------------
		$local_user_properties = array();
		# get local-properties-data by password-hash
		if (!$error) {
			try {
				$local_user_properties['password-hash'] = $this->mod_adminpanel->get_local_user_properties_by('user-request-password-hash', $input_params['account_activation_code'], 'properties-value');
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Cannot get user properties by password-hash by user-request-password-hash on properties-value: {$ex->getMessage()}";
			}
		}
		if (!$error) {
			if (!isset($local_user_properties['password-hash'][0])) {
				$error = true;
				$error_msg[] = "local-user properties data no as expected index format by password-hash";
			}
		}
		if (!$error) {
			if ((int)$local_user_properties['password-hash'][0]->local_seq === 0) {
				$error = true;
				$error_msg[] = "local-seq from local-properties-data is 0.";
			}
		}
		if (!$error) {
			try {
				$local_users = $this->mod_adminpanel->get_local_user_by($local_user_properties['password-hash'][0]->local_seq, 'seq');
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Cannot check local-user by get_local_user_by on passwordNew.";
			}
		}
		if (!$error) {
			if (count($local_users) === 0) {
				$error = true;
				$error_msg[] = "Local user is not exists on system.";
			}
		}
		if (!$error) {
			if (!isset($local_users[0])) {
				$error = true;
				$error_msg[] = "Un-expected data index from models by get local-user-data by sequence.";
			}
		}
		# get local-properties-data by password-string
		if (!$error) {
			$query_params['account_salt'] = $this->rc4crypt->bDecryptRC4($local_users[0]->account_hash);
		}
		if (!$error) {
			try {
				$local_user_properties['password-string'] = $this->mod_adminpanel->get_local_user_properties_by($local_users[0]->seq, $query_params['account_salt'], 'password-string');
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Cannot get user properties by password-string on passwordNew: {$ex->getMessage()}";
			}
		}
		if (!$error) {
			if (!isset($local_user_properties['password-string'][0])) {
				$error = true;
				$error_msg[] = "local-user properties data no as expected index format by password-string on passwordNew";
			}
		}
		if (!$error) {
			if (sha1($local_user_properties['password-string'][0]->properties_value) !== $local_user_properties['password-hash'][0]->properties_value) {
				$error = true;
				$error_msg[] = "Hashing password string should be same with password hash by salt.";
			}
		}
		//---------------------------------------------------------------------------------------------------
		if (!$error) {
			$query_params['account_salt_new'] = $this->mod_adminpanel->create_unique_datetime("Asia/Bangkok");
			$query_params['account_hash'] = $this->rc4crypt->bEncryptRC4($query_params['account_salt_new']);
			$query_params['account_password_new'] = $this->rc4crypt->bEncryptRC4(time());
			// set userdata of new password
			$this->session->set_userdata('tmp_password', $query_params['account_password_new']);
			$query_params['account_password_string'] = "{$query_params['account_salt_new']}|{$query_params['account_password_new']}";
			$query_params['account_password'] = sha1($query_params['account_password_string']);
			//----
			$query_params['account_edited_datetime'] = $this->DateObject->format('Y-m-d H:i:s');
			$query_params['account_edited_by'] = $local_users[0]->account_username;
			$query_params['account_inserting_remark'] = "New request password confirmed.";
			//--
			try {
				$password_hash_seq = $this->mod_adminpanel->set_local_user_properties_by($local_users[0]->seq, $query_params['account_password'], 'password-hash');
				$password_string_seq = $this->mod_adminpanel->set_local_user_properties_by($local_users[0]->seq, $query_params['account_password_string'], 'password-string');
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Cannot re-set local-user-propeties with key password-hash and password-string: {$ex->getMessage()}";
			}
		}
		//----
		if (!$error) {
			if (isset($query_params['account_salt'])) {
				unset($query_params['account_salt']);
			}
			if (isset($query_params['account_salt_new'])) {
				unset($query_params['account_salt_new']);
			}
			if (isset($query_params['account_password_new'])) {
				unset($query_params['account_password_new']);
			}
			if (isset($query_params['account_password_string'])) {
				unset($query_params['account_password_string']);
			}
			$this->mod_adminpanel->update_local_account_data($local_users[0]->seq, $query_params);
			
			//---
			// Local login
			//---
			$login_params = array(
				'body'			=> array(
					'user_email'		=> $local_users[0]->account_email,
					'user_username'		=> $local_users[0]->account_username,
					'user_password'		=> $this->session->userdata('tmp_password'),
				)
			);
			try {
				$local_login = $this->mod_adminpanel->local_login($login_params);
			} catch (Exception $ex) {
				$error = true;
				$error_msg[] = "Cannot doing local-login using temporary passwords.";
			}
		}
		if (!$error) {
			if (isset($local_login['success'])) {
				if ($local_login['success'] != TRUE) {
					$error = true;
					$error_msg[] = "Local login failed.";
				}
			} else {
				$error = true;
				$error_msg[] = "Local login by succee failed.";
			}
		}
		if (!$error) {
			$this->session->set_flashdata('success', "Reset password success, please change your password with your new password.");
			redirect(base_url('adminpanel/passwordChange'));
		} else {
			if ($this->session->userdata('tmp_password') != FALSE) {
				$this->session->set_userdata('tmp_password', NULL);
			}
			$error_to_show = "";
			foreach ($error_msg as $keval) {
				$error_to_show .= $keval;
			}
			$this->session->set_flashdata('error', $error_to_show);
			redirect(base_url('adminpanel/passwordForget'));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
















