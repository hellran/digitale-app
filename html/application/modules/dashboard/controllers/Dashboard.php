<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	public $isAdmin = FALSE;
	public $isMerchant = FALSE;
	protected $DateObject;
	protected $email_vendor;
	public $error = FALSE, $error_msg = array();
	protected $firebase_credential;
	protected $base_config = array();
	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
		# Load Base Config
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		
		$this->load->model('adminpanel/Model_adminpanel', 'mod_adminpanel');
		$this->load->model('adminpanel/Model_firebase', 'mod_firebase');
		$this->load->model('adminpanel/Model_configuration', 'mod_config');
		$this->load->library('adminpanel/Lib_Rc4Crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('3', '4'))) {
			$this->isAdmin = TRUE;
		}
		if (!$this->isAdmin) {
			if (in_array($this->mod_adminpanel->localdata['account_role'], array('2', '5'))) {
				$this->isMerchant = TRUE;
			}
		}
		$this->DateObject = $this->mod_adminpanel->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'));
		$this->load->library('adminpanel/Lib_Firebase', '', 'firebase');
		//
		$this->email_vendor = $this->base_config['email_vendor'];
		$this->firebase_credential = $this->base_config['firebase_credential'];
	}
	private function take_configuration_item($config_code) {
		$items = false;
		try {
			$config_items = $this->mod_config->get_config_item_by('code', $config_code);
		} catch (Exception $ex) {
			return false;
		}
		if (isset($config_items['data'])) {
			if (is_array($config_items['data']) && (count($config_items['data']) > 0)) {
				foreach ($config_items['data'] as $keval) {
					$items = $keval;
					break;
				}
			}
		}
		return $items;
	}
	//------------------------------------------------------------------------------------------------------------
	function index() {
		$collectData = array(
			'page'					=> 'dashboard-home',
			'collect'				=> array(),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'Dashboard Home',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view dashboard page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		//----------------------------------------------------------------------------------------------------
		// Get Created Stories
		if (!$this->error) {
			try {
				$collectData['collect']['my_stories'] = $this->mod_firebase->get_stories_params_by('data', 'create', (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0));
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error exception get my-stories: {$ex->getMessage()}";
			}
		}
		//---------------------------------------------
		// Debug
		/*
		echo "<pre>";
		if (!$this->error) {
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//---------------------------------------------
		if (!$this->error) {
			$this->load->view("{$collectData['base_dashboard_path']}/dashboard.php", $collectData);
		} else {
			$error_to_show = "";
			foreach ($this->error_msg as $keval) {
				if (is_string($keval) || is_numeric($keval)) {
					$error_to_show .= sprintf("%s", $keval);
				} else if (is_object($keval) || is_array($keval)) {
					$error_to_show .= json_encode($keval);
				} else {
					$error_to_show .= "-";
				}
			}
			$this->session->set_flashdata('error', $error_to_show);
			redirect(base_url('home/home/index'));
			exit;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}