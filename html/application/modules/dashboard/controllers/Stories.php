<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stories extends MY_Controller {
	public $isSuper = FALSE;
	public $isAdmin = FALSE;
	public $isMerchant = FALSE;
	protected $DateObject;
	protected $email_vendor;
	public $error = FALSE, $error_msg = array();
	protected $firebase_credential;
	protected $base_config = array();
	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
		# Load Base Config
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		
		$this->load->model('adminpanel/Model_adminpanel', 'mod_adminpanel');
		$this->load->model('adminpanel/Model_firebase', 'mod_firebase');
		$this->load->model('adminpanel/Model_configuration', 'mod_config');
		$this->load->library('adminpanel/Lib_Rc4Crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('4'))) {
			$this->isSuper = TRUE;
			$this->isAdmin = TRUE;
			$this->isMerchant = TRUE;
		}
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('3', '4'))) {
			$this->isAdmin = TRUE;
			$this->isMerchant = TRUE;
		}
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('2', '5'))) {
			$this->isMerchant = TRUE;
		}
		$this->DateObject = $this->mod_adminpanel->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'));
		$this->load->library('adminpanel/Lib_Firebase', '', 'firebase');
		//
		$this->email_vendor = $this->base_config['email_vendor'];
		$this->firebase_credential = $this->base_config['firebase_credential'];
	}
	private function take_configuration_item($config_code) {
		$items = false;
		try {
			$config_items = $this->mod_config->get_config_item_by('code', $config_code);
		} catch (Exception $ex) {
			return false;
		}
		if (isset($config_items['data'])) {
			if (is_array($config_items['data']) && (count($config_items['data']) > 0)) {
				foreach ($config_items['data'] as $keval) {
					$items = $keval;
					break;
				}
			}
		}
		return $items;
	}
	private function get_stories_categories_from_consumed_firebase() {
		// Stories categories from consume
		$this->load->model('cli/Model_consume', 'mod_consume');
		$collectData = array(
			'collect'			=> array(),
		);
		try {
			$collectData['consume_index'] = $this->mod_consume->get_consume_index_by('categories', 'stories_categories');
		} catch (Exception $ex) {
			$this->error = true;
			$this->error_msg[] = "Cannot get stories-categories from mod-consume with exception: {$ex->getMessage()}";
		}
		if (!$this->error) {
			if (isset($collectData['consume_index']->consume_last_seq)) {
				try {
					$collectData['categories_data'] = $this->mod_consume->get_consume_logs_by_table_seq($collectData['consume_index']->consume_table, $collectData['consume_index']->consume_last_seq);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get categories-data by mod-consume with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (isset($collectData['categories_data']->consume_data)) {
				return json_decode($collectData['categories_data']->consume_data, true);
			}
		}
		return false;
	}
	//----------------------------------------------------------------------------------
	// Show Index
	function index($pgnumber = 0) {
		$collectData = array(
			'page'					=> 'stories-index',
			'collect'				=> array(),
			'pgnumber'				=> (is_numeric($pgnumber) ? (int)$pgnumber : 0),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'Dashboard: Stories',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		//==================
		// Firebase content
		//==================
		# Kategori
		if (!$this->error) {
			try {
				$collectData['collect']['categories'] = $this->firebase->get_categories_data('', 0, 100);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception on add-categories page: {$ex->getMessage()}";
			}
		}
		// Get Created Stories
		if (!$this->error) {
			try {
				$collectData['collect']['my_stories'] = $this->mod_firebase->get_stories_params_by('data', 'create', (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0));
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error exception get my-stories: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			$collectData['collect']['invalid_characters'] = array("$", "%", "#", "<", ">", "|");
			$collectData['collect']['my_stories_count'] = count($collectData['collect']['my_stories']);
			if ($collectData['collect']['my_stories_count'] > 0) {
				/*
				foreach ($collectData['collect']['my_stories'] as &$storyVal) {
					$storyVal->stories_fulldata = $this->firebase->get_stories_data_single($storyVal->stories_seq);
				}
				*/
			}
		}
		//------------------------------------
		// Make pagination start and per-page
		if (!$this->error) {
			if (isset($collectData['collect']['my_stories_count'])) {
				if ((int)$collectData['collect']['my_stories_count'] > 0) {
					$collectData['pagination'] = array(
						'page'		=> (isset($collectData['pgnumber']) ? $collectData['pgnumber'] : 1),
						'start'		=> 0,
					);
					$collectData['pagination']['page'] = (is_numeric($collectData['pagination']['page']) ? sprintf("%d", $collectData['pagination']['page']) : 1);
					if ($collectData['pagination']['page'] > 0) {
						$collectData['pagination']['page'] = (int)$collectData['pagination']['page'];
					} else {
						$collectData['pagination']['page'] = 1;
					}
					$collectData['pagination']['start'] = $this->mod_adminpanel->get_pagination_start($collectData['pagination']['page'], base_config('rows_per_page'), $collectData['collect']['my_stories_count']);
				} else {
					$collectData['pagination'] = array(
						'page'		=> 1,
						'start'		=> 0,
					);
				}
				
				//-------------------------------------------------------------
				// Get stories by paging
				$collectData['get_paging_params'] = array(
					'start'			=> $collectData['pagination']['start'], 
					'perpage'		=> base_config('rows_per_page'),
				);
				try {
					$collectData['collect']['stories_data'] = $this->mod_firebase->get_stories_params_by('data', 'create', $collectData['collect']['userdata']['seq'], array(), $collectData['get_paging_params']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Error while get stories data by paging with exception: {$ex->getMessage()}";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Should have value as total rows.";
			}
		}
		# Generate pagination
		if (!$this->error) {
			/*
			if (is_array($collectData['collect']['stories_data']) && (count($collectData['collect']['stories_data']) > 0)) {
				foreach ($collectData['collect']['stories_data'] as &$storiesdata) {
					$storiesdata->stories_fulldata = $this->firebase->get_stories_data_single($storiesdata->stories_seq);
				}
			}
			*/
			try {
				$collectData['collect']['pagination'] = $this->mod_adminpanel->generate_pagination_for('dashboard', base_url("{$collectData['base_dashboard_path']}/stories/index"), $collectData['pagination']['page'], base_config('rows_per_page'), $collectData['collect']['my_stories_count'], $collectData['pagination']['start']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error while get pagination with exception: {$ex->getMessage()}";
			}
		}
		
		
		
		//--------------------------------------------
		// DEBUG
		/*
		echo "<pre>";
		if (!$this->error) {
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//--------------------------------------------
		
		if (!$this->error) {
			$this->load->view("{$collectData['base_dashboard_path']}/dashboard.php", $collectData);
		} else {
			$error_to_show = "";
			foreach ($this->error_msg as $keval) {
				if (is_string($keval) || is_numeric($keval)) {
					$error_to_show .= sprintf("%s", $keval);
				} else if (is_object($keval) || is_array($keval)) {
					$error_to_show .= json_encode($keval);
				} else {
					$error_to_show .= "-";
				}
			}
			$this->session->set_flashdata('error', $error_to_show);
			redirect(base_url('dashboard'));
			exit;
		}
	}
	//--------------------------------------------------------------------------------------------------
	function published($pgnumber = 0) {
		$collectData = array(
			'page'					=> 'stories-lists-published',
			'this_method'			=> 'published',
			'collect'				=> array(),
			'pgnumber'				=> (is_numeric($pgnumber) ? (int)$pgnumber : 0),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'My Stories: Published',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		$this->get_data_of_published_or_reviewed_or_drafted($collectData, $collectData['pgnumber']);
	}
	function drafted($pgnumber = 0) {
		$collectData = array(
			'page'					=> 'stories-lists-drafted',
			'this_method'			=> 'drafted',
			'collect'				=> array(),
			'pgnumber'				=> (is_numeric($pgnumber) ? (int)$pgnumber : 0),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'My Stories: Drafted',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		$this->get_data_of_published_or_reviewed_or_drafted($collectData, $collectData['pgnumber']);
	}
	function reviewed($pgnumber = 0) {
		$collectData = array(
			'page'					=> 'stories-lists-reviewed',
			'this_method'			=> 'reviewed',
			'collect'				=> array(),
			'pgnumber'				=> (is_numeric($pgnumber) ? (int)$pgnumber : 0),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'My Stories: Reviewed',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		$this->get_data_of_published_or_reviewed_or_drafted($collectData, $collectData['pgnumber']);
	}
	private function get_data_of_published_or_reviewed_or_drafted($collectData, $pgnumber = 0) {
		if (!isset($collectData['this_method'])) {
			$this->error = true;
			$this->error_msg[] = "There is no method from collectData.";
		}
		// Get Created Stories
		if (!$this->error) {
			try {
				$collectData['collect']['raw_stories'] = $this->mod_firebase->get_stories_params_by('data', 'create', (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0));
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error exception get raw-stories: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			$collectData['collect']['my_stories'] = array();
			$collectData['collect']['invalid_characters'] = array("$", "%", "#", "<", ">", "|");
			$collectData['collect']['raw_stories_count'] = count($collectData['collect']['raw_stories']);
			if ($collectData['collect']['raw_stories_count'] > 0) {
				$my_stories_i = 0;
				foreach ($collectData['collect']['raw_stories'] as $storyVal) {
					if (strtolower($storyVal->stories_status) === $collectData['this_method']) {
						$collectData['collect']['my_stories'][$my_stories_i] = $storyVal;
					}
					$my_stories_i++;
				}
			}
			$collectData['collect']['my_stories_count'] = count($collectData['collect']['my_stories']);
		}
		//------------------------------------
		// Make pagination start and per-page
		if (!$this->error) {
			if (isset($collectData['collect']['my_stories_count'])) {
				if ((int)$collectData['collect']['my_stories_count'] > 0) {
					$collectData['pagination'] = array(
						'page'		=> (isset($collectData['pgnumber']) ? $collectData['pgnumber'] : 1),
						'start'		=> 0,
					);
					$collectData['pagination']['page'] = (is_numeric($collectData['pagination']['page']) ? sprintf("%d", $collectData['pagination']['page']) : 1);
					if ($collectData['pagination']['page'] > 0) {
						$collectData['pagination']['page'] = (int)$collectData['pagination']['page'];
					} else {
						$collectData['pagination']['page'] = 1;
					}
					$collectData['pagination']['start'] = $this->mod_adminpanel->get_pagination_start($collectData['pagination']['page'], base_config('rows_per_page'), $collectData['collect']['my_stories_count']);
				} else {
					$collectData['pagination'] = array(
						'page'		=> 1,
						'start'		=> 0,
					);
				}
				
				//-------------------------------------------------------------
				// Get stories by paging
				$collectData['get_paging_params'] = array(
					'start'			=> $collectData['pagination']['start'], 
					'perpage'		=> base_config('rows_per_page'),
				);
				$collectData['query_stories_params'] = array(
					'paging'		=> array(
						'start'			=> $collectData['pagination']['start'], 
						'perpage'		=> base_config('rows_per_page'),
					),
					'sorting'		=> array(
						'order_by'			=> 'stories_create_datetime',
						'order_sort'		=> 'DESC',
					),
					'conditions'	=> array(
						'stories_status'	=> $collectData['this_method'],
					),
				);
				try {
					$collectData['collect']['stories_data'] = $this->mod_firebase->get_stories_params_by('data', 'create', $collectData['collect']['userdata']['seq'], $collectData['query_stories_params']['conditions'], $collectData['query_stories_params']['paging'], $collectData['query_stories_params']['sorting']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Error while get stories data by paging with exception: {$ex->getMessage()}";
				}
				
			} else {
				$this->error = true;
				$this->error_msg[] = "Should have value as total rows.";
			}
		}
		# Generate pagination
		if (!$this->error) {
			try {
				$collectData['collect']['pagination'] = $this->mod_adminpanel->generate_pagination_for('dashboard', base_url("{$collectData['base_dashboard_path']}/stories/{$collectData['this_method']}"), $collectData['pagination']['page'], base_config('rows_per_page'), $collectData['collect']['my_stories_count'], $collectData['pagination']['start']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error while get pagination with exception: {$ex->getMessage()}";
			}
		}
		//--------------------------------------------
		// DEBUG
		/*
		echo "<pre>";
		if (!$this->error) {
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//--------------------------------------------
		if (!$this->error) {
			$this->load->view("{$collectData['base_dashboard_path']}/dashboard.php", $collectData);
		} else {
			$error_to_show = "";
			foreach ($this->error_msg as $keval) {
				if (is_string($keval) || is_numeric($keval)) {
					$error_to_show .= sprintf("%s", $keval);
				} else if (is_object($keval) || is_array($keval)) {
					$error_to_show .= json_encode($keval);
				} else {
					$error_to_show .= "-";
				}
			}
			$this->session->set_flashdata('error', $error_to_show);
			redirect(base_url('dashboard'));
			exit;
		}
	}
	//--------------------------------------------------------------------------------------------------
	// Show Add
	function add($page) {
		$this->publish($page);
	}
	function publish($page = 'add') {
		$collectData = array(
			'page'							=> 'stories-add',
			'collect'						=> array(),
			'title'							=> 'Dashboard: Add Stories',
			'base_home_path'				=> 'home',
			'base_dashboard_path'			=> 'dashboard',
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		$page = (is_string($page) ? $page : 'index');
		$page = strtolower($page);
		$collectData['this_page'] = $page;
		$publish_get_item = $this->input->get('cerita');
		if ($publish_get_item != FALSE) {
			$publish_get_item = ((strlen($publish_get_item) > 0) ? $publish_get_item : 0);
			$publish_get_item = ((is_numeric($publish_get_item) || is_string($publish_get_item)) ? $publish_get_item : '');
		}
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		//==================
		// Firebase content
		//==================
		# Kategori
		if (!$this->error) {
			try {
				$collectData['collect']['categories'] = $this->firebase->get_categories_data('', 0, 100);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception on add-categories page: {$ex->getMessage()}";
			}
		}
		//----------------------------------------------------------------------------------------------
		// Switch page
		switch ($collectData['this_page']) {
			case 'edit':
				$collectData['page'] = 'stories-edit';
			break;
			case 'add':
			case 'index':
			default:
				$collectData['page'] = 'stories-add';
			break;
		}
		//----------
		// Debug
		//----------
		/*
		echo "<pre>";
		if (!$this->error) {
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//------------------------------------------------------------

		
		
		
		
		
		if (!$this->error) {
			$this->load->view("{$collectData['base_dashboard_path']}/dashboard.php", $collectData);
		} else {
			$error_to_show = "";
			foreach ($this->error_msg as $keval) {
				if (is_string($keval) || is_numeric($keval)) {
					$error_to_show .= sprintf("%s", $keval);
				} else if (is_object($keval) || is_array($keval)) {
					$error_to_show .= json_encode($keval);
				} else {
					$error_to_show .= "-";
				}
			}
			$this->session->set_flashdata('error', $error_to_show);
			redirect(base_url('dashboard'));
			exit;
		}
		
		
		
		
	}
	//---------------------------------------------------------------------------------------
	function publishThumbnail() {
		$collectData = array(
			'page'					=> 'stories-publish-thumbnail',
			'collect'				=> array(),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'Dashboard: Stories',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		$upload_img = $this->base_config['upload_img'];
		$collectData['upload_img'] = $upload_img;
		# Load Codeigniter Lib
		$this->load->library('upload', $upload_img['local']);
		$this->load->library('ftp');
		$collectData['firebase_credential'] = $this->firebase_credential;
		//---------------------------------------------------------------------------------------------------------
		$collectData['publish_thumbnail'] = '';
		
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		//=======================
		// Upload process
		if (!$this->error) {
			if (!isset($_FILES['publish_thumbnail']['tmp_name'])) {
				$this->error = true;
				$this->error_msg[] = "There is no image uploaded";
			} else {
				if ($_FILES['publish_thumbnail']['tmp_name'] != null) {
					// Check Image Size and Image Pixels
					$collectData['image_upload_data'] = array(
						'size'			=> (isset($_FILES['publish_thumbnail']['size']) ? $_FILES['publish_thumbnail']['size'] : 0),
						'max_size'		=> (isset($upload_img['local']['max_size']) ? $upload_img['local']['max_size'] : 0),
					);
					if ($collectData['image_upload_data']['size'] === 0) {
						$this->error = true;
						$this->error_msg[] = "Cannot get size of image.";
					}
					if ($collectData['image_upload_data']['max_size'] === 0) {
						$this->error = true;
						$this->error_msg[] = "Cannot get max-size configuration.";
					}
					if (intval($collectData['image_upload_data']['size']) > $collectData['image_upload_data']['max_size']) {
						$this->error = true;
						$this->error_msg[] = "Max Image size is: {$collectData['image_upload_data']['max_size']}";
					}
					list($collectData['image_upload_data']['width'], $collectData['image_upload_data']['height']) = getimagesize($_FILES['publish_thumbnail']['tmp_name']);
					if (intval($collectData['image_upload_data']['width']) > $upload_img['local']['max_width']) {
						$this->error = true;
						$this->error_msg[] = "Max Width of image is: {$upload_img['local']['max_width']}";
					}
					if (intval($collectData['image_upload_data']['height']) > $upload_img['local']['max_height']) {
						$this->error = true;
						$this->error_msg[] = "Max Height of image is: {$upload_img['local']['max_height']}";
					}
				} else {
					$this->error = true;
					$this->error_msg[] = "Image tmp-name is null";
				}
			}
		}
		if (!$this->error) {
			$collectData['upload_data'] = array();
			if ($this->upload->do_upload('publish_thumbnail')) {
				try {
					$collectData['upload_data']['source'] = $this->upload->data();
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get image-uploaded data with exception: {$ex->getMessage()}";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Failed while uploading image-file.";
			}
		}
		if (!$this->error) {
			//------------------------------------------------------------
			// Process to resize
			//------------------------------------------------------------
			$this->load->library('image_lib');
			if ($this->upload->do_upload('publish_thumbnail')) {
				if (isset($collectData['upload_data']['source']['full_path'])) {
					$collectData['upload_img']['resize']['library']['source_image'] = $collectData['upload_data']['source']['full_path'];
					$this->image_lib->initialize($collectData['upload_img']['resize']['library']);
					if (!$this->image_lib->resize()) {
						$this->error = true;
						$this->error_msg[] = "Cannot using image-library for resizing image";
					} else {
						if (isset($this->image_lib->full_dst_path)) {
							if (!file_exists($this->image_lib->full_dst_path)) {
								$this->error = true;
								$this->error_msg[] = "File of resized image not really exists.";
							} else {
								// Delete Original
								try {
									unlink($this->image_lib->full_src_path);
								} catch (Exception $ex) {
									$this->error = true;
									$this->error_msg[] = "Error delete original source images.";
								}
								// Do CDN Upload Here
								try {
									$this->ftp->connect($upload_img['cdn']);
									$collectData['cdn_upload_file_name'] = basename($this->image_lib->full_dst_path);
									$collectData['cdn_upload'] = $this->ftp->upload($this->image_lib->full_dst_path, "{$upload_img['cdn']['cdn_full_path']}/{$collectData['cdn_upload_file_name']}"); 
									$this->ftp->close();
								} catch (Exception $ex) {
									$this->error = true;
									$this->error_msg[] = "Error exception while uploading to FTP: {$ex->getMessage()}";
								}
								// Delete Thumbs
								try {
									unlink($this->image_lib->full_dst_path);
								} catch (Exception $ex) {
									$this->error = true;
									$this->error_msg[] = "Error exception unlink tmp images: {$ex->getMessage()}.";
								}
								// Set to database insert
								$collectData['publish_thumbnail'] .= sprintf("%s/%s",
									$upload_img['cdn']['cdn_full_url'],
									$collectData['cdn_upload_file_name']
								);
							}
						}
					}
				} else {
					$this->error = true;
					$this->error_msg[] = "Uploaded image not get full-path of image";
				}
			}
		}
		
		// Return response:
		if (!$this->error) {
			echo $collectData['publish_thumbnail'];
		} else {
			echo json_encode($this->error_msg);
		}
	}
	function edit($stories_seq = 0) {
		$collectData = array(
			'page'					=> 'stories-edit',
			'collect'				=> array(),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'Dashboard: Edit Stories',
			'base_config'			=> $this->base_config,
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		$upload_img = $this->base_config['upload_img'];
		# Load Codeigniter Lib
		$this->load->library('upload', $upload_img['local']);
		$this->load->library('ftp');
		//-----------------------------------------------------------------------------------------------------
		if (isset($stories_seq)) {
			$stories_seq = ((is_string($stories_seq) || is_numeric($stories_seq)) ? $stories_seq : '0');
			$stories_seq = strtolower(sprintf('%s', $stories_seq));
			$stories_seq = ((strlen($stories_seq) > 0) ? $stories_seq : '0');
			$stories_seq = sprintf('%d', $stories_seq);
		} else {
			$stories_seq = null;
		}
		$collectData['stories_seq'] = $stories_seq;
		$collectData['collect']['stories_seq'] = sprintf("%s", $stories_seq);
		$collectData['firebase_credential'] = $this->firebase_credential;
		$collectData['publish_thumbnail'] = '';
		$collectData['publish_cover'] = '';
		//------------------------------------------------------------------------------------------------------
        if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
				'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
				'line'					=> $this->take_configuration_item('footer-line-id'),
				'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
			);
		}
		//=========================================================================================================
		// Get Created Stories
		if (!$this->error) {
			$collectData['collect']['created_stories'] = array();
			try {
				$collectData['collect']['my_stories'] = $this->mod_firebase->get_stories_params_by('data', 'create', (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0));
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error exception get my-stories.";
			}
		}
		if (!$this->error) {
			if (is_array($collectData['collect']['my_stories']) && (count($collectData['collect']['my_stories']) > 0)) {
				foreach ($collectData['collect']['my_stories'] as $story) {
					$collectData['collect']['created_stories'][] = (isset($story->stories_seq) ? $story->stories_seq : false);
				}
			}
			if (count($collectData['collect']['created_stories']) === 0) {
				$this->error = true;
				$this->error_msg[] = "You have not any stories created.";
			} else {
				if ($this->isAdmin !== TRUE) {
					if (!in_array($collectData['stories_seq'], $collectData['collect']['created_stories'])) {
						$this->error = true;
						$this->error_msg[] = "This story does not belong to you.";
					}
				}
			}
		}
		// Firebase content
		# Kategori
		if (!$this->error) {
			try {
				$collectData['collect']['categories'] = $this->firebase->get_categories_data('', 0, 100);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception on add-categories page: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			if (intval($collectData['stories_seq']) > -1) {
				try {
					$collectData['collect']['stories'] = $this->firebase->get_stories_data_single($collectData['stories_seq']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Exception error get single stories sequence.";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Stories sequence cannot be in negatif value.";
			}
		}
		if (!$this->error) {
			if (isset($collectData['collect']['stories']['cerita'])) {
				if (is_array($collectData['collect']['stories']['cerita']) && (count($collectData['collect']['stories']['cerita']) > 0)) {
					foreach ($collectData['collect']['stories']['cerita'] as $cerita) {
						$collectData['collect']['stories_data'] = $cerita;
					}
				} else {
					$this->error = true;
					$this->error_msg[] = "Stories from firebase database not in properly format, should be on array object.";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Stories return empty value.";
			}
		}
		//----------------
		// Debug
		/*
		echo "<pre>";
		if (!$this->error) {
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		if (!$this->error) {
			$this->load->view("{$collectData['base_dashboard_path']}/dashboard.php", $collectData);
		} else {
			$error_to_show = "";
			foreach ($this->error_msg as $keval) {
				if (is_string($keval) || is_numeric($keval)) {
					$error_to_show .= sprintf("%s", $keval);
				} else if (is_object($keval) || is_array($keval)) {
					$error_to_show .= json_encode($keval);
				} else {
					$error_to_show .= "-";
				}
			}
			$this->session->set_flashdata('error', $error_to_show);
			redirect(base_url('dashboard/stories/index'));
			exit;
		}
	}
	//=========================================================================
	// Action
	//=========================================================================
	function addaction($stories_seq = null) {
		$collectData = array(
			'page'					=> 'stories-publish-story',
			'collect'				=> array(),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'Dashboard: Add Stories',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		$upload_img = $this->base_config['upload_img'];
		# Load Codeigniter Lib
		$this->load->library('upload', $upload_img['local']);
		$this->load->library('ftp');
		//-----------------------------------------------------------------------------------------------------
		if (isset($stories_seq)) {
			$stories_seq = ((is_string($stories_seq) || is_numeric($stories_seq)) ? $stories_seq : '0');
			$stories_seq = strtolower(sprintf('%s', $stories_seq));
			$stories_seq = ((strlen($stories_seq) > 0) ? $stories_seq : '0');
			$stories_seq = sprintf('%d', $stories_seq);
		} else {
			$stories_seq = null;
		}
		$collectData['stories_seq'] = $stories_seq;
		$collectData['firebase_credential'] = $this->firebase_credential;
		$collectData['publish_thumbnail'] = '';
		$collectData['publish_cover'] = '';
		//------------------------------------------------------------------------------------------------------
        if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
				'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
				'line'					=> $this->take_configuration_item('footer-line-id'),
				'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
			);
		}
		//=========================================================================================================
		if (!$this->error) {
			$this->form_validation->set_rules('publish_judul', 'Judul', 'required|max_length[512]|xss_clean');
			$this->form_validation->set_rules('publish_sinopsis', 'Deskripsi', 'required|max_length[2048]|xss_clean');
			$this->form_validation->set_rules('publish_penulis', 'Penulis', 'required|max_length[256]|xss_clean');
			$this->form_validation->set_rules('publish_editor', 'Editor', 'required|max_length[256]|xss_clean');
			$this->form_validation->set_rules('publish_ilustrator', 'Ilustrator', 'required|max_length[256]|xss_clean');
			$this->form_validation->set_rules('publish_thumbnail', 'Images Thumbnail', 'required|xss_clean');
			if($this->form_validation->run() == false || $this->session->flashdata('error') != null) {
				$this->error = true;
				$this->error_msg[] = validation_errors('-', '<br/>');
				$this->session->set_flashdata('error', validation_errors('-', '<br/>'));
				redirect(base_url("dashboard/stories/index"));
				exit;
			}
		}
		//------------- Starting Create Data Object to Post
		if (!$this->error) {
			$collectData['params_query'] = array(
				'seq'						=> 0,
				'data'						=> array(),
				'stories_seq'				=> 0,
			);
		}
		if ($stories_seq != null) {
			$collectData['params_query']['stories_seq'] += (int)$stories_seq;
		} else {
			# Get unique stories-seq from dashboard database
			if (!$this->error) {
				try {
					$stories_max_seq = $this->mod_firebase->get_stories_params_by('stories', 'stories_seq', 'max');
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get maximum stories-seq from dashboard database: {$ex->getMessage()}.";
				}
			}
			if (!$this->error) {
				if (count($stories_max_seq) > 0) {
					foreach ($stories_max_seq as $seqVal) {
						$collectData['params_query']['stories_seq'] = (int)$seqVal->value;
					}
				}
			}
			if (!$this->error) {
				if ($collectData['params_query']['stories_seq'] != null) {
					try {
						$stories_max_data = $this->mod_firebase->get_stories_params_by('data', 'stories_seq', $collectData['params_query']['stories_seq']);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Cannot get data by maximum stories-seq from dashboard database: {$ex->getMessage()}.";
					}
				} else {
					$collectData['params_query']['seq'] = 0;
				}
			}
			if (!$this->error) {
				if ($collectData['params_query']['stories_seq'] != null) {
					if (count($stories_max_data) > 0) {
						foreach ($stories_max_data as $seqVal) {
							$collectData['params_query']['seq'] = (isset($seqVal->seq) ? (int)$seqVal->seq : 0);
						}
					}
				}
			}
			//==============================================================
			if (!$this->error) {
				if ((int)$collectData['params_query']['seq'] === 0) {
					try {
						$collectData['params_query']['firebase'] = $this->firebase->get_stories();
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Error getting firebase stories from firebase library.";
					}
				}
			}
			if (!$this->error) {
				$firebase_stories = array();
				if ((int)$collectData['params_query']['seq'] === 0) {
					if (isset($collectData['params_query']['firebase']['data']) && (count($collectData['params_query']['firebase']['data']) > 0)) {
						foreach ($collectData['params_query']['firebase']['data'] as $key => &$val) {
							$val['key'] = $key;
						}
					}
				}
			}
			if (!$this->error) {
				$i = 0;
				if ((int)$collectData['params_query']['seq'] === 0) {
					if (isset($collectData['params_query']['firebase']['data']) && (count($collectData['params_query']['firebase']['data']) > 0)) {
						foreach ($collectData['params_query']['firebase']['data'] as $keval) {
							$firebase_stories[$i]['stories_collection'] = 'cerita';
							$firebase_stories[$i]['stories_seq'] = (isset($keval['key']) ? $keval['key'] : 0);
							$firebase_stories[$i]['stories_url_host'] = FIREBASE_URL_HOSTNAME;
							$firebase_stories[$i]['stories_url_path'] = "{$firebase_stories[$i]['stories_collection']}/{$firebase_stories[$i]['stories_seq']}";
							$firebase_stories[$i]['stories_url_full'] = (FIREBASE_URL_PROTOCOL . "{$firebase_stories[$i]['stories_url_host']}/" . $this->base_config['firebase_database_prefix'] . "/{$firebase_stories[$i]['stories_collection']}/{$firebase_stories[$i]['stories_seq']}");
							
							$firebase_stories[$i]['stories_create_by'] = (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0);
							$firebase_stories[$i]['stories_update_by'] = (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0);
							$firebase_stories[$i]['stories_create_datetime'] = $this->DateObject->format('Y-m-d H:i:s');
							$firebase_stories[$i]['stories_update_datetime'] = $this->DateObject->format('Y-m-d H:i:s');
							$i += 1;
						}
					}
				}
			}
			if (!$this->error) {
				if ((int)$collectData['params_query']['seq'] === 0) {
					if (count($firebase_stories) > 0) {
						foreach ($firebase_stories as $keval) {
							// Insert if duplicate, update some parameters, return seq
							$collectData['params_query']['seq'] = $this->mod_firebase->insert_stories_params_by('data', 'duplicate', 'update', $keval);
						}
					}
				}
			}
			// Already get $collectData['params_query']['seq'] > 0
			if (!$this->error) {
				if ((int)$collectData['params_query']['seq'] > 0) {
					try {
						$collectData['params_query']['stories_data'] = $this->mod_firebase->get_stories_params_by('data', 'seq', $collectData['params_query']['seq']);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Error for get data of max seq from firebase-stories: {$ex->getMessage()}";
					}
				} else {
					$this->error = true;
					$this->error_msg[] = "Last seq of firebase-stories must be more than 0.";
				}
			}
			if (!$this->error) {
				if (count($collectData['params_query']['stories_data']) > 0) {
					foreach ($collectData['params_query']['stories_data'] as $keval) {
						$collectData['params_query']['stories_seq'] = (isset($keval->stories_seq) ? (int)$keval->stories_seq : 0);
					}
				}
				// Make new stories-seq by +1
				$collectData['params_query']['stories_seq'] += 1;
			}
		}
		//==================================
		// Main form for PUT into Firebase
		//==================================
		if (!$this->error) {
			$collectData['params_query']['form'] = array(
				'put_action'						=> 'PUT',
				'put_url'							=> base_firebase_url("firebase/cerita"),
				'put_data'							=> array(),
			);
			$collectData['params_query']['form']['put_url'] .= "/{$collectData['params_query']['stories_seq']}";
		}
		# Form Data
		if (!$this->error) {
			$collectData['params_query']['form']['put_data'] = array(
				'deskripsi'				=> $this->input->post('publish_sinopsis'),
				'judul'					=> $this->input->post('publish_judul'),
				'pembaca'				=> 0, // Integer of jumlah-pembaca
				'penulis'				=> $this->input->post('publish_penulis'),
				'penyunting'			=> $this->input->post('publish_editor'),
				'illustrator'			=> $this->input->post('publish_ilustrator'),
				'publisher'				=> '', // Should be logged-in User Fullname
				'sampul'				=> $this->input->post('publish_thumbnail'),
				'thumbnail'				=> $this->input->post('publish_thumbnail'),
				'waktu'					=> 0, // Temporary using 0
				'publish_status'		=> 'NO',
				'licence_type'			=> $this->input->post('publish_licence_type'),
				'licence_url'			=> $this->input->post('publish_licence_url'),
				'tags'					=> $this->input->post('publish_tags'),
			);
			if (isset($collectData['collect']['userdata']['account_fullname'])) {
				$collectData['params_query']['form']['put_data']['publisher'] = $collectData['collect']['userdata']['account_fullname'];
			}
		}
		if (!$this->error) {
			$collectData['params_query']['form']['put_parent'] = array('kategori' => array());
			$collectData['params_query']['form']['put_parent']['collection'] = array();
			$collectData['params_query']['form']['put_parent_data'] = array(
				'judul'					=> (isset($collectData['params_query']['form']['put_data']['judul']) ? $collectData['params_query']['form']['put_data']['judul'] : ''),
				'publisher'				=> (isset($collectData['params_query']['form']['put_data']['publisher']) ? $collectData['params_query']['form']['put_data']['publisher'] : ''),
				'thumbnail'				=> (isset($collectData['params_query']['form']['put_data']['thumbnail']) ? $collectData['params_query']['form']['put_data']['thumbnail'] : ''),
				'publish_status'		=> (isset($collectData['params_query']['form']['put_data']['publish_status']) ? $collectData['params_query']['form']['put_data']['publish_status'] : 'NO'),
				'penulis'				=> (isset($collectData['params_query']['form']['put_data']['penulis']) ? $collectData['params_query']['form']['put_data']['penulis'] : ''),
				'penyunting'			=> (isset($collectData['params_query']['form']['put_data']['penyunting']) ? $collectData['params_query']['form']['put_data']['penyunting'] : ''),
				'illustrator'			=> (isset($collectData['params_query']['form']['put_data']['illustrator']) ? $collectData['params_query']['form']['put_data']['illustrator'] : ''),
				'sampul'				=> (isset($collectData['params_query']['form']['put_data']['sampul']) ? $collectData['params_query']['form']['put_data']['sampul'] : ''),
				'licence_type'			=> (isset($collectData['params_query']['form']['put_data']['licence_type']) ? $collectData['params_query']['form']['put_data']['licence_type'] : ''),
				'licence_url'			=> (isset($collectData['params_query']['form']['put_data']['licence_url']) ? $collectData['params_query']['form']['put_data']['licence_url'] : ''),
				'tags'					=> (isset($collectData['params_query']['form']['put_data']['tags']) ? $collectData['params_query']['form']['put_data']['tags'] : ''),
			);
			$collectData['params_query']['form']['put_parent_data']['tags'] = explode(",", $collectData['params_query']['form']['put_parent_data']['tags']);
			if (count($collectData['params_query']['form']['put_parent_data']['tags']) === 0) {
				$collectData['params_query']['form']['put_parent_data']['tags'] = array();
			} else {
				foreach ($collectData['params_query']['form']['put_parent_data']['tags'] as $tag) {
					$tag = preg_replace('/\s+/', ' ',	$tag);
				}
			}
			//----
			$collectData['params_query']['form']['put_data']['kategori'] = $this->input->post('publish_kategori');
			$collectData['params_query']['form']['put_data']['kategori_sub'] = $this->input->post('publish_subkategori');
			if (is_array($collectData['params_query']['form']['put_data']['kategori_sub']) && (count($collectData['params_query']['form']['put_data']['kategori_sub']) > 0)) {
				$for_i = 0;
				foreach ($collectData['params_query']['form']['put_data']['kategori_sub'] as $subkatKey => $subkatVal) {
					$kategori_sub_urlpath = "";
					$collectData['params_query']['form']['put_parent']['kategori'][$for_i] = array();
					if (isset($collectData['params_query']['form']['put_data']['kategori'][$subkatKey])) {
						if (is_string($collectData['params_query']['form']['put_data']['kategori'][$subkatKey])) {
							$kategori_sub_urlpath .= $collectData['params_query']['form']['put_data']['kategori'][$subkatKey];
						}
					}
					if (is_array($subkatVal) && (count($subkatVal) > 0)) {
						foreach ($subkatVal as $subkatKeval) {
							$collectData['params_query']['form']['put_parent']['kategori'][$for_i][] = ("{$kategori_sub_urlpath}/{$subkatKeval}");
						}
					}
					$for_i++;
				}
			}
		}
		//-----------------------------------------------------------------
		if (!$this->error) {
			if (count($collectData['params_query']['form']['put_parent']['kategori']) > 0) {
				foreach ($collectData['params_query']['form']['put_parent']['kategori'] as $keval) {
					if (is_array($keval) && (count($keval) > 0)) {
						foreach ($keval as $val) {
							$collectData['params_query']['form']['put_parent']['collection'][] = (is_string($val) ? $val : '');
						}
					}
				}
			}
		}
		if (!$this->error) {
			if (count($collectData['params_query']['form']['put_parent']['collection']) > 0) {
				foreach ($collectData['params_query']['form']['put_parent']['collection'] as &$val) {
					$val .= (isset($collectData['params_query']['stories_seq']) ? "/{$collectData['params_query']['stories_seq']}" : '0');
				}
			}
		}
		//-----------------------------------------------------------------
		# Remove un-necessary index
		if (!$this->error) {
			if (isset($collectData['params_query']['form']['put_data']['kategori'])) {
				unset($collectData['params_query']['form']['put_data']['kategori']);
			}
			if (isset($collectData['params_query']['form']['put_data']['kategori_sub'])) {
				unset($collectData['params_query']['form']['put_data']['kategori_sub']);
			}
		}
		//-----------------------------------------------------------------
		if (!$this->error) {
			if ($stories_seq != null) {
				$collectData['params_query']['form']['put_data']['files'] = array();
				try {
					$collectData['params_query']['stories'] = $this->firebase->get_stories_data_single($stories_seq);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Exception error while get edited stories seq data from firebase.";
				}
			}
		}
		if (!$this->error) {
			if ($stories_seq != null) {
				if (isset($collectData['params_query']['stories']['cerita'])) {
					if (is_array($collectData['params_query']['stories']['cerita']) && (count($collectData['params_query']['stories']['cerita']) > 0)) {
						foreach ($collectData['params_query']['stories']['cerita'] as $keval) {
							if (isset($keval['files'])) {
								$collectData['params_query']['form']['put_data']['files'] = $keval['files'];
							}
							if (strlen($collectData['params_query']['form']['put_data']['thumbnail']) === 0) {
								$collectData['params_query']['form']['put_data']['thumbnail'] = (isset($keval['thumbnail']) ? $keval['thumbnail'] : '');
							}
							if (strlen($collectData['params_query']['form']['put_parent_data']['thumbnail']) === 0) {
								$collectData['params_query']['form']['put_parent_data']['thumbnail'] = (isset($keval['thumbnail']) ? $keval['thumbnail'] : '');
							}
							if (strlen($collectData['params_query']['form']['put_data']['sampul']) === 0) {
								$collectData['params_query']['form']['put_data']['sampul'] = (isset($keval['sampul']) ? $keval['sampul'] : '');
							}
						}
					}
				}
			}
		}
		# Kategori
		if (!$this->error) {
			try {
				$collectData['collect']['categories'] = $this->firebase->get_categories_data('', 0, 100);
				$collectData['collect']['categories'] = $this->get_stories_categories_from_consumed_firebase();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception on add-categories page: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			//===========================
			// Make categories of stories
			try {
				$collectData['collect']['cerita_stories_categories'] = $this->set_stories_categories($collectData['params_query']['stories_seq'], $collectData['collect']['categories']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error while set-story-categories with exception: {$ex->getMessage}";
			}
		}
		//--------------------------------------------------------
		// Rewrite Stories Files
		//--------------------------------------------------------
		if (!$this->error) {
			$collectData['params_query']['form']['put_data']['files'] = array();
			$collectData['collect']['stories_files'] = array(
				'texts'			=> $this->input->post('publish_item_teks'),
				'images'		=> $this->input->post('publish_item_gambar'),
			);
			if (!is_array($collectData['collect']['stories_files']['texts'])) {
				$this->error = true;
				$this->error_msg[] = "Stories texts should be in array datatype.";
			}
			if (!is_array($collectData['collect']['stories_files']['images'])) {
				$this->error = true;
				$this->error_msg[] = "Stories images should be in array datatype.";
			}
		}
		if (!$this->error) {
			if (count($collectData['collect']['stories_files']['texts']) !== count($collectData['collect']['stories_files']['images'])) {
				$this->error = true;
				$this->error_msg[] = "Count of texts and images should be same.";
			} else {
				if (count($collectData['collect']['stories_files']['texts']) > 0) {
					$items_i = 0;
					foreach ($collectData['collect']['stories_files']['texts'] as $textsKey => $textsVal) {
						$collectData['params_query']['form']['put_data']['files'][$items_i] = array(
							'gambar' 	=> (isset($collectData['collect']['stories_files']['images'][$textsKey]) ? $collectData['collect']['stories_files']['images'][$textsKey] : ''),
							'teks'		=> $textsVal,
						);
						$items_i += 1;
					}
				}
			}
		}		
		//------------------------------------
		// Debug
		//------------------------------------
		/*
		if (!$this->error) {
			
			echo "<pre>";
			print_r($collectData);
			
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//------------------------------------
		
		
		
		
		# PUT to Firebase (cerita)
		if (!$this->error) {
			$action = $collectData['params_query']['form']['put_action'];
			$url = $collectData['params_query']['form']['put_url'];
			$authorize_header = base64_encode("{$this->firebase_credential['USER']}:{$this->firebase_credential['TOKEN']}");
			$this->firebase->add_headers('Authorization', "Basic {$authorize_header}");
			$headers = $this->firebase->create_curl_headers($this->firebase->headers);
			$content = json_encode($collectData['params_query']['form']['put_data'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			
			try {
				$collectData['params_query']['curl_internal'] = $this->firebase->create_curl_request($collectData['params_query']['form']['put_action'], $collectData['params_query']['form']['put_url'], $this->firebase->UA, $headers, $content, 15);
			} catch (Exception $ex){
				$this->error = true;
				$this->error_msg[] = "Error exception while doing PUT data to firebase.";
			}
		}
		# UPDATE to Local Database
		if (!$this->error) {
			if (isset($collectData['params_query']['curl_internal']['response']['code'])) {
				if ((int)$collectData['params_query']['curl_internal']['response']['code'] == 200) {
					$query_params = array(
						'stories_collection'		=> 'cerita',
						'stories_seq'				=> (isset($collectData['params_query']['stories_seq']) ? $collectData['params_query']['stories_seq'] : ''),
						'stories_url_host' 			=> FIREBASE_URL_HOSTNAME,
						'stories_url_path'			=> '',
						'stories_url_full'			=> '',
						'stories_status'			=> 'reviewed',
						'stories_create_by'			=> (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0),
						'stories_update_by'			=> (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0),
						'stories_create_datetime'	=> $this->DateObject->format('Y-m-d H:i:s'),
						'stories_update_datetime'	=> $this->DateObject->format('Y-m-d H:i:s'),
					);
					$query_params['stories_url_path'] .= "{$query_params['stories_collection']}/{$query_params['stories_seq']}";
					$query_params['stories_url_full'] .= (FIREBASE_URL_PROTOCOL . "{$query_params['stories_url_host']}/" . $this->base_config['firebase_database_prefix'] . "/{$query_params['stories_url_path']}");
					try {
						$collectData['params_query']['new_stories_seq'] = $this->mod_firebase->insert_stories_params_by('data', 'duplicate', 'update', $query_params);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Error update new stories-seq to local database.";
					}
				}
			}
		}
		# PUT to Firebase (kategori)
		if (!$this->error) {
			$collectData['collect']['category_full_path_url'] = array();
			if (is_array($collectData['collect']['cerita_stories_categories']['data']) && (count($collectData['collect']['cerita_stories_categories']['data']) > 0)) {
				foreach ($collectData['collect']['cerita_stories_categories']['data'] as $catKey => $catVal) {
					$category_parent_title = $catVal['title'];
					if (isset($catVal['subtitles'])) {
						if (is_array($catVal['subtitles'])) {
							if (count($catVal['subtitles']) > 0) {
								foreach ($catVal['subtitles'] as $subcatVal) {
									$collectData['collect']['category_full_path_url'][] = ($category_parent_title . '/' . $subcatVal . '/' . $collectData['stories_seq']);
								}
							}
						}
					}
				}
			}
			//----------- Remove from current categories
			if (count($collectData['collect']['category_full_path_url']) > 0) {
				$collectData['params_query']['curl_parent_internal'] = array();
				foreach ($collectData['collect']['category_full_path_url'] as $keval) {
					$remove_from_categories = array(
						'action'					=> 'PUT',
						'url'						=> '',
						'headers'					=> $this->firebase->create_curl_headers($this->firebase->headers),
						'content'					=> array(
							'collection'					=> '',
							//'data'						=> NULL,
						),
					);
					$remove_from_categories['url'] = base_firebase_url("firebase/kategori/put/");
					$remove_from_categories['url'] .= base_permalink($keval);
					$remove_from_categories['content']['collection'] = ($keval . '/' . $collectData['params_query']['stories_seq']);
					try {
						$collectData['params_query']['curl_parent_internal'][] = $this->firebase->create_curl_request($remove_from_categories['action'], $remove_from_categories['url'], $this->firebase->UA, $remove_from_categories['headers'], json_encode($remove_from_categories['content'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), 15);
					} catch (Exception $ex){
						$this->error = true;
						$this->error_msg[] = "Error exception while doing PUT data to firebase on remove-from-kategori.";
					}
				}
			}
			//----------- Add to categories
			$authorize_header = base64_encode("{$this->firebase_credential['USER']}:{$this->firebase_credential['TOKEN']}");
			$this->firebase->add_headers('Authorization', "Basic {$authorize_header}");
			$put_to_categories = array(
				'action'					=> 'PUT',
				'url'						=> '',
				'headers'					=> $this->firebase->create_curl_headers($this->firebase->headers),
				'content'					=> array(
					'collection'					=> array(),
					'data'							=> (isset($collectData['params_query']['form']['put_parent_data']) ? $collectData['params_query']['form']['put_parent_data'] : array()),
				),
			);
			if (count($collectData['params_query']['form']['put_parent']['collection']) > 0) {
				$collectData['params_query']['curl_parent_internal'] = array();
				foreach ($collectData['params_query']['form']['put_parent']['collection'] as $keval) {
					$put_to_categories['url'] = base_firebase_url("firebase/kategori/put/");
					$put_to_categories['url'] .= base_permalink($keval);
					$put_to_categories['content']['collection'] = $keval;
					//$put_to_categories['content'] = json_encode($put_to_categories['content'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
					try {
						$collectData['params_query']['curl_parent_internal'][] = $this->firebase->create_curl_request($put_to_categories['action'], $put_to_categories['url'], $this->firebase->UA, $put_to_categories['headers'], json_encode($put_to_categories['content'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), 15);
					} catch (Exception $ex){
						$this->error = true;
						$this->error_msg[] = "Error exception while doing PUT data to firebase.";
					}
				}
			}
		}
		//===========
		// Debug
		/*
		if (!$this->error) {
			echo "<pre>";
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//===========
		
		if (!$this->error) {
			if ((int)$collectData['params_query']['new_stories_seq'] > 0) {
				
						
					
				$this->session->set_flashdata('success', 'Terimakasih! Cerita yang anda publish, telah kami terima. Kami akan konfirmasi melalui email ketika cerita telah tampil di aplikasi.');
				redirect(base_url("dashboard/stories/edit/{$query_params['stories_seq']}?time=" . time()));
			} else {
				$this->session->set_flashdata('error', 'Something error with requirement fields.');
				redirect(base_url("dashboard/stories/index"));
			}
		}
		
		
		exit;
	}
	function editaction($stories_seq = null) {
		$collectData = array(
			'page'					=> 'stories-edit',
			'collect'				=> array(),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'Dashboard: Edit Stories',
		);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			} else {
				$collectData['collect']['configuration'] = array(
					'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
					'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
					'line'					=> $this->take_configuration_item('footer-line-id'),
					'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
				);
				$take_config_items = array(
					'nama-pendiri',
					'email-pendiri',
					'email-redaksi',
				);
				foreach ($take_config_items as $keval) {
					$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
				}
			}
		}
		$upload_img = $this->base_config['upload_img'];
		# Load Codeigniter Lib
		$this->load->library('upload', $upload_img['local']);
		$this->load->library('ftp');
		//-----------------------------------------------------------------------------------------------------
		if (isset($stories_seq)) {
			$stories_seq = ((is_string($stories_seq) || is_numeric($stories_seq)) ? $stories_seq : '0');
			$stories_seq = strtolower(sprintf('%s', $stories_seq));
			$stories_seq = ((strlen($stories_seq) > 0) ? $stories_seq : '0');
			$stories_seq = sprintf('%d', $stories_seq);
		} else {
			$stories_seq = null;
		}
		$collectData['stories_seq'] = $stories_seq;
		$collectData['collect']['stories_seq'] = sprintf("%s", $stories_seq);
		$collectData['firebase_credential'] = $this->firebase_credential;
		$collectData['publish_thumbnail'] = '';
		$collectData['publish_cover'] = '';
		//------------------------------------------------------------------------------------------------------
        if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
				'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
				'line'					=> $this->take_configuration_item('footer-line-id'),
				'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
			);
		}
		//=========================================================================================================
		// Get Created Stories
		// Check if Stories is Owned by User
		//=========================================================================================================
		if (!$this->error) {
			$collectData['collect']['created_stories'] = array();
			try {
				$collectData['collect']['my_stories'] = $this->mod_firebase->get_stories_params_by('data', 'create', (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0));
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error exception get my-stories.";
			}
		}
		if (!$this->error) {
			if (is_array($collectData['collect']['my_stories']) && (count($collectData['collect']['my_stories']) > 0)) {
				foreach ($collectData['collect']['my_stories'] as $story) {
					$collectData['collect']['created_stories'][] = (isset($story->stories_seq) ? $story->stories_seq : false);
				}
			}
			if (count($collectData['collect']['created_stories']) === 0) {
				$this->error = true;
				$this->error_msg[] = "You have not any stories created.";
			} else {
				if ($this->isAdmin !== TRUE) {
					if (!in_array($collectData['stories_seq'], $collectData['collect']['created_stories'])) {
						$this->error = true;
						$this->error_msg[] = "This story does not belong to you.";
					}
				}
			}
		}
		//=========================================================================================================
		// Firebase content
		# Kategori
		if (!$this->error) {
			try {
				$collectData['collect']['categories'] = $this->firebase->get_categories_data('', 0, 100);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception on add-categories page: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			if (intval($collectData['stories_seq']) > -1) {
				try {
					$collectData['collect']['stories'] = $this->firebase->get_stories_data_single($collectData['stories_seq']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Exception error get single stories sequence.";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Stories sequence cannot be in negatif value.";
			}
		}
		
		
		
		//=========================================================================================================
		// FORM ACTION
		//=========================================================================================================
		if (!$this->error) {
			$this->form_validation->set_rules('publish_publisher', 'Publisher', 'required|max_length[512]|xss_clean');
			$this->form_validation->set_rules('publish_judul', 'Judul', 'required|max_length[512]|xss_clean');
			$this->form_validation->set_rules('publish_sinopsis', 'Deskripsi', 'required|max_length[2048]|xss_clean');
			$this->form_validation->set_rules('publish_penulis', 'Penulis', 'required|max_length[256]|xss_clean');
			$this->form_validation->set_rules('publish_editor', 'Editor', 'required|max_length[256]|xss_clean');
			$this->form_validation->set_rules('publish_ilustrator', 'Ilustrator', 'required|max_length[256]|xss_clean');
			$this->form_validation->set_rules('publish_thumbnail', 'Images Thumbnail', 'required|xss_clean');
			// Edit for multiples stories-items
			$this->form_validation->set_rules('publish_item_teks[]', "Stories items texts", 'required');
			$this->form_validation->set_rules('publish_item_gambar[]', "Stories items images", 'required');
			if($this->form_validation->run() == false || $this->session->flashdata('error') != null) {
				$this->error = true;
				$this->error_msg[] = "Error validations";
				$this->session->set_flashdata('error', validation_errors('-', '<br/>'));
				redirect(base_url("dashboard/stories/index"));
				exit;
			}
		}
		//------------- Starting Create Data Object to Post
		$collectData['params_query'] = array(
			'seq'						=> 0,
			'data'						=> array(),
			'stories_seq'				=> 0,
		);
		if ($stories_seq != null) {
			$collectData['params_query']['stories_seq'] += (int)$stories_seq;
		} else {
			# Get unique stories-seq from dashboard database
			if (!$this->error) {
				try {
					$stories_max_seq = $this->mod_firebase->get_stories_params_by('stories', 'stories_seq', 'max');
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get maximum stories-seq from dashboard database: {$ex->getMessage()}.";
				}
			}
			if (!$this->error) {
				if (count($stories_max_seq) > 0) {
					foreach ($stories_max_seq as $seqVal) {
						$collectData['params_query']['stories_seq'] = (int)$seqVal->value;
					}
				}
			}
			if (!$this->error) {
				if ($collectData['params_query']['stories_seq'] != null) {
					try {
						$stories_max_data = $this->mod_firebase->get_stories_params_by('data', 'stories_seq', $collectData['params_query']['stories_seq']);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Cannot get data by maximum stories-seq from dashboard database: {$ex->getMessage()}.";
					}
				} else {
					$collectData['params_query']['seq'] = 0;
				}
			}
			if (!$this->error) {
				if ($collectData['params_query']['stories_seq'] != null) {
					if (count($stories_max_data) > 0) {
						foreach ($stories_max_data as $seqVal) {
							$collectData['params_query']['seq'] = (isset($seqVal->seq) ? (int)$seqVal->seq : 0);
						}
					}
				}
			}
			//==============================================================
			if (!$this->error) {
				if ((int)$collectData['params_query']['seq'] === 0) {
					try {
						$collectData['params_query']['firebase'] = $this->firebase->get_stories();
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Error getting firebase stories from firebase library.";
					}
				}
			}
			if (!$this->error) {
				$firebase_stories = array();
				if ((int)$collectData['params_query']['seq'] === 0) {
					if (isset($collectData['params_query']['firebase']['data']) && (count($collectData['params_query']['firebase']['data']) > 0)) {
						foreach ($collectData['params_query']['firebase']['data'] as $key => &$val) {
							$val['key'] = $key;
						}
					}
				}
			}
			if (!$this->error) {
				$i = 0;
				if ((int)$collectData['params_query']['seq'] === 0) {
					if (isset($collectData['params_query']['firebase']['data']) && (count($collectData['params_query']['firebase']['data']) > 0)) {
						foreach ($collectData['params_query']['firebase']['data'] as $keval) {
							$firebase_stories[$i]['stories_collection'] = 'cerita';
							$firebase_stories[$i]['stories_seq'] = (isset($keval['key']) ? $keval['key'] : 0);
							$firebase_stories[$i]['stories_url_host'] = FIREBASE_URL_HOSTNAME;
							$firebase_stories[$i]['stories_url_path'] = "{$firebase_stories[$i]['stories_collection']}/{$firebase_stories[$i]['stories_seq']}";
							$firebase_stories[$i]['stories_url_full'] = (FIREBASE_URL_PROTOCOL . "{$firebase_stories[$i]['stories_url_host']}/" . $this->base_config['firebase_database_prefix'] . "/{$firebase_stories[$i]['stories_collection']}/{$firebase_stories[$i]['stories_seq']}");
							
							$firebase_stories[$i]['stories_create_by'] = (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0);
							$firebase_stories[$i]['stories_update_by'] = (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0);
							$firebase_stories[$i]['stories_create_datetime'] = $this->DateObject->format('Y-m-d H:i:s');
							$firebase_stories[$i]['stories_update_datetime'] = $this->DateObject->format('Y-m-d H:i:s');
							$i += 1;
						}
					}
				}
			}
			if (!$this->error) {
				if ((int)$collectData['params_query']['seq'] === 0) {
					if (count($firebase_stories) > 0) {
						foreach ($firebase_stories as $keval) {
							// Insert if duplicate, update some parameters, return seq
							$collectData['params_query']['seq'] = $this->mod_firebase->insert_stories_params_by('data', 'duplicate', 'update', $keval);
						}
					}
				}
			}
			// Already get $collectData['params_query']['seq'] > 0
			if (!$this->error) {
				if ((int)$collectData['params_query']['seq'] > 0) {
					try {
						$collectData['params_query']['stories_data'] = $this->mod_firebase->get_stories_params_by('data', 'seq', $collectData['params_query']['seq']);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Error for get data of max seq from firebase-stories: {$ex->getMessage()}";
					}
				} else {
					$this->error = true;
					$this->error_msg[] = "Last seq of firebase-stories must be more than 0.";
				}
			}
			if (!$this->error) {
				if (count($collectData['params_query']['stories_data']) > 0) {
					foreach ($collectData['params_query']['stories_data'] as $keval) {
						$collectData['params_query']['stories_seq'] = (isset($keval->stories_seq) ? (int)$keval->stories_seq : 0);
					}
				}
				// Make new stories-seq by +1
				$collectData['params_query']['stories_seq'] += 1;
			}
		}
		//==================================
		// Main form for PUT into Firebase
		//==================================
		if (!$this->error) {
			$collectData['params_query']['form'] = array(
				'put_action'						=> 'PUT',
				'put_url'							=> base_firebase_url("firebase/cerita"),
				'put_data'							=> array(),
			);
			$collectData['params_query']['form']['put_url'] .= "/{$collectData['params_query']['stories_seq']}";
		}
		# Form Data
		if (!$this->error) {
			$collectData['params_query']['form']['put_data'] = array(
				'deskripsi'				=> $this->input->post('publish_sinopsis'),
				'judul'					=> $this->input->post('publish_judul'),
				'pembaca'				=> 0, // Integer of jumlah-pembaca
				'penulis'				=> $this->input->post('publish_penulis'),
				'penyunting'			=> $this->input->post('publish_editor'),
				'illustrator'			=> $this->input->post('publish_ilustrator'),
				'publisher'				=> $this->input->post('publish_publisher'),
				'sampul'				=> $this->input->post('publish_thumbnail'),
				'thumbnail'				=> $this->input->post('publish_thumbnail'),
				'waktu'					=> 0, // Temporary using 0
				'publish_status'		=> 'NO',
				'licence_type'			=> $this->input->post('publish_licence_type'),
				'licence_url'			=> $this->input->post('publish_licence_url'),
				'tags'					=> $this->input->post('publish_tags'),
			);
			if (($collectData['params_query']['form']['put_data']['publisher'] != FALSE) || ($collectData['params_query']['form']['put_data']['publisher'] != '')) {
				$collectData['params_query']['form']['put_data']['publisher'] = sprintf("%s", $collectData['params_query']['form']['put_data']['publisher']);
			} else {
				$collectData['params_query']['form']['put_data']['publisher'] = (isset($collectData['collect']['userdata']['account_fullname']) ? $collectData['collect']['userdata']['account_fullname'] : '');
			}
			# Tags
			$collectData['params_query']['form']['put_data']['tags'] = explode(",", $collectData['params_query']['form']['put_data']['tags']);
			if (count($collectData['params_query']['form']['put_data']['tags']) === 0) {
				$collectData['params_query']['form']['put_data']['tags'] = array();
			} else {
				foreach ($collectData['params_query']['form']['put_data']['tags'] as &$tag) {
					$tag = preg_replace('/\s+/', ' ', $tag);
				}
			}
			
		}
		if (!$this->error) {
			$collectData['params_query']['form']['put_parent'] = array('kategori' => array());
			$collectData['params_query']['form']['put_parent']['collection'] = array();
			$collectData['params_query']['form']['put_parent_data'] = array(
				'judul'					=> (isset($collectData['params_query']['form']['put_data']['judul']) ? $collectData['params_query']['form']['put_data']['judul'] : ''),
				'publisher'				=> (isset($collectData['params_query']['form']['put_data']['publisher']) ? $collectData['params_query']['form']['put_data']['publisher'] : ''),
				'thumbnail'				=> (isset($collectData['params_query']['form']['put_data']['thumbnail']) ? $collectData['params_query']['form']['put_data']['thumbnail'] : ''),
				'publish_status'		=> (isset($collectData['params_query']['form']['put_data']['publish_status']) ? $collectData['params_query']['form']['put_data']['publish_status'] : 'NO'),
				'penulis'				=> (isset($collectData['params_query']['form']['put_data']['penulis']) ? $collectData['params_query']['form']['put_data']['penulis'] : ''),
				'penyunting'			=> (isset($collectData['params_query']['form']['put_data']['penyunting']) ? $collectData['params_query']['form']['put_data']['penyunting'] : ''),
				'illustrator'			=> (isset($collectData['params_query']['form']['put_data']['illustrator']) ? $collectData['params_query']['form']['put_data']['illustrator'] : ''),
				'sampul'				=> (isset($collectData['params_query']['form']['put_data']['sampul']) ? $collectData['params_query']['form']['put_data']['sampul'] : ''),
				'licence_type'			=> (isset($collectData['params_query']['form']['put_data']['licence_type']) ? $collectData['params_query']['form']['put_data']['licence_type'] : ''),
				'licence_url'			=> (isset($collectData['params_query']['form']['put_data']['licence_url']) ? $collectData['params_query']['form']['put_data']['licence_url'] : ''),
				'tags'					=> (isset($collectData['params_query']['form']['put_data']['tags']) ? $collectData['params_query']['form']['put_data']['tags'] : array()),
			);
			//----
			$collectData['params_query']['form']['put_data']['kategori'] = $this->input->post('publish_kategori');
			$collectData['params_query']['form']['put_data']['kategori_sub'] = $this->input->post('publish_subkategori');
			if (is_array($collectData['params_query']['form']['put_data']['kategori_sub']) && (count($collectData['params_query']['form']['put_data']['kategori_sub']) > 0)) {
				$for_i = 0;
				foreach ($collectData['params_query']['form']['put_data']['kategori_sub'] as $subkatKey => $subkatVal) {
					$kategori_sub_urlpath = "";
					$collectData['params_query']['form']['put_parent']['kategori'][$for_i] = array();
					if (isset($collectData['params_query']['form']['put_data']['kategori'][$subkatKey])) {
						if (is_string($collectData['params_query']['form']['put_data']['kategori'][$subkatKey])) {
							$kategori_sub_urlpath .= $collectData['params_query']['form']['put_data']['kategori'][$subkatKey];
						}
					}
					if (is_array($subkatVal) && (count($subkatVal) > 0)) {
						foreach ($subkatVal as $subkatKeval) {
							$collectData['params_query']['form']['put_parent']['kategori'][$for_i][] = ("{$kategori_sub_urlpath}/{$subkatKeval}");
						}
					}
					$for_i++;
				}
			}
		}
		//-----------------------------------------------------------------
		if (!$this->error) {
			if (count($collectData['params_query']['form']['put_parent']['kategori']) > 0) {
				foreach ($collectData['params_query']['form']['put_parent']['kategori'] as $keval) {
					if (is_array($keval) && (count($keval) > 0)) {
						foreach ($keval as $val) {
							$collectData['params_query']['form']['put_parent']['collection'][] = (is_string($val) ? $val : '');
						}
					}
				}
			}
		}
		if (!$this->error) {
			if (count($collectData['params_query']['form']['put_parent']['collection']) > 0) {
				foreach ($collectData['params_query']['form']['put_parent']['collection'] as &$val) {
					$val .= (isset($collectData['params_query']['stories_seq']) ? "/{$collectData['params_query']['stories_seq']}" : '0');
				}
			}
		}
		//-----------------------------------------------------------------
		# Remove un-necessary index
		if (!$this->error) {
			if (isset($collectData['params_query']['form']['put_data']['kategori'])) {
				unset($collectData['params_query']['form']['put_data']['kategori']);
			}
			if (isset($collectData['params_query']['form']['put_data']['kategori_sub'])) {
				unset($collectData['params_query']['form']['put_data']['kategori_sub']);
			}
		}
		//-----------------------------------------------------------------
		if (!$this->error) {
			if ($stories_seq != null) {
				$collectData['params_query']['form']['put_data']['files'] = array();
				try {
					$collectData['params_query']['stories'] = $this->firebase->get_stories_data_single($stories_seq);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Exception error while get edited stories seq data from firebase.";
				}
			}
		}
		if ($stories_seq != null) {
			//$collectData['params_query']['stories_seq']
			if (!$this->error) {
				if (isset($collectData['params_query']['stories']['cerita'])) {
					if (is_array($collectData['params_query']['stories']['cerita']) && (count($collectData['params_query']['stories']['cerita']) > 0)) {
						foreach ($collectData['params_query']['stories']['cerita'] as $keval) {
							if (isset($keval['files'])) {
								$collectData['params_query']['form']['put_data']['files'] = $keval['files'];
							}
							if (strlen($collectData['params_query']['form']['put_data']['thumbnail']) === 0) {
								$collectData['params_query']['form']['put_data']['thumbnail'] = (isset($keval['thumbnail']) ? $keval['thumbnail'] : '');
							}
							if (strlen($collectData['params_query']['form']['put_parent_data']['thumbnail']) === 0) {
								$collectData['params_query']['form']['put_parent_data']['thumbnail'] = (isset($keval['thumbnail']) ? $keval['thumbnail'] : '');
							}
							if (strlen($collectData['params_query']['form']['put_data']['sampul']) === 0) {
								$collectData['params_query']['form']['put_data']['sampul'] = (isset($keval['sampul']) ? $keval['sampul'] : '');
							}
						}
					}
				}
			}
		}
		
		//--------------------------------------------------------
		// Rewrite Stories Files
		//--------------------------------------------------------
		if (!$this->error) {
			$collectData['params_query']['form']['put_data']['files'] = array();
			$collectData['collect']['stories_files'] = array(
				'texts'			=> $this->input->post('publish_item_teks'),
				'images'		=> $this->input->post('publish_item_gambar'),
			);
			if (!is_array($collectData['collect']['stories_files']['texts'])) {
				$this->error = true;
				$this->error_msg[] = "Stories texts should be in array datatype.";
			}
			if (!is_array($collectData['collect']['stories_files']['images'])) {
				$this->error = true;
				$this->error_msg[] = "Stories images should be in array datatype.";
			}
		}
		if (!$this->error) {
			if (count($collectData['collect']['stories_files']['texts']) !== count($collectData['collect']['stories_files']['images'])) {
				$this->error = true;
				$this->error_msg[] = "Count of texts and images should be same.";
			} else {
				if (count($collectData['collect']['stories_files']['texts']) > 0) {
					$items_i = 0;
					foreach ($collectData['collect']['stories_files']['texts'] as $textsKey => $textsVal) {
						$collectData['params_query']['form']['put_data']['files'][$items_i] = array(
							'gambar' 	=> (isset($collectData['collect']['stories_files']['images'][$textsKey]) ? $collectData['collect']['stories_files']['images'][$textsKey] : ''),
							'teks'		=> $textsVal,
						);
						$items_i += 1;
					}
				}
			}
		}
		# Set Categories of Stories
		if (!$this->error) {
			// Make categories of stories
			try {
				$collectData['collect']['cerita_stories_categories'] = $this->set_stories_categories($collectData['params_query']['stories_seq'], $collectData['collect']['categories']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error while set-story-categories with exception: {$ex->getMessage}";
			}
		}
		
		
		
		//------------------------------------
		// Debug
		//------------------------------------
		/*
		echo "<pre>";
		if (!$this->error) {
			
			
			print_r($collectData);
			
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//------------------------------------------------------------------------------------------------------------
		# PUT to Firebase (cerita)
		if (!$this->error) {
			$action = $collectData['params_query']['form']['put_action'];
			$url = $collectData['params_query']['form']['put_url'];
			$authorize_header = base64_encode("{$this->firebase_credential['USER']}:{$this->firebase_credential['TOKEN']}");
			$this->firebase->add_headers('Authorization', "Basic {$authorize_header}");
			$headers = $this->firebase->create_curl_headers($this->firebase->headers);
			$content = json_encode($collectData['params_query']['form']['put_data'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			
			try {
				$collectData['params_query']['curl_internal'] = $this->firebase->create_curl_request($collectData['params_query']['form']['put_action'], $collectData['params_query']['form']['put_url'], $this->firebase->UA, $headers, $content, 15);
			} catch (Exception $ex){
				$this->error = true;
				$this->error_msg[] = "Error exception while doing PUT data to firebase.";
			}
		}
		# UPDATE to Local Database
		if (!$this->error) {
			if (isset($collectData['params_query']['curl_internal']['response']['code'])) {
				if ((int)$collectData['params_query']['curl_internal']['response']['code'] == 200) {
					$query_params = array(
						'stories_collection'		=> 'cerita',
						'stories_seq'				=> (isset($collectData['params_query']['stories_seq']) ? $collectData['params_query']['stories_seq'] : ''),
						'stories_url_host' 			=> FIREBASE_URL_HOSTNAME,
						'stories_url_path'			=> '',
						'stories_url_full'			=> '',
						'stories_create_by'			=> (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0),
						'stories_update_by'			=> (isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : 0),
						'stories_create_datetime'	=> $this->DateObject->format('Y-m-d H:i:s'),
						'stories_update_datetime'	=> $this->DateObject->format('Y-m-d H:i:s'),
					);
					$query_params['stories_url_path'] .= "{$query_params['stories_collection']}/{$query_params['stories_seq']}";
					$query_params['stories_url_full'] .= (FIREBASE_URL_PROTOCOL . "{$query_params['stories_url_host']}/" . $this->base_config['firebase_database_prefix'] . "/{$query_params['stories_url_path']}");
					// Set stories status as reviewed:
					$query_params['stories_status'] = 'reviewed';
					try {
						$collectData['params_query']['new_stories_seq'] = $this->mod_firebase->insert_stories_params_by('data', 'duplicate', 'update', $query_params);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Error update new stories-seq to local database.";
					}
				}
			}
		}
		#########################################################################
		# PUT to Firebase (kategori)
		#########################################################################
		if (!$this->error) {
			$collectData['collect']['category_full_path_url'] = array();
			if (is_array($collectData['collect']['cerita_stories_categories']['data']) && (count($collectData['collect']['cerita_stories_categories']['data']) > 0)) {
				foreach ($collectData['collect']['cerita_stories_categories']['data'] as $catKey => $catVal) {
					$category_parent_title = $catVal['title'];
					if (isset($catVal['subtitles'])) {
						if (is_array($catVal['subtitles'])) {
							if (count($catVal['subtitles']) > 0) {
								foreach ($catVal['subtitles'] as $subcatVal) {
									$collectData['collect']['category_full_path_url'][] = ($category_parent_title . '/' . $subcatVal . '/' . $collectData['stories_seq']);
								}
							}
						}
					}
				}
			}
			//-------------------------------
			// Remove from current categories
			//-------------------------------
			if (count($collectData['collect']['category_full_path_url']) > 0) {
				$collectData['params_query']['curl_parent_internal'] = array();
				$for_i = 0;
				foreach ($collectData['collect']['category_full_path_url'] as $keval) {
					$remove_from_categories = array(
						'action'					=> 'PUT',
						'url'						=> '',
						'headers'					=> $this->firebase->create_curl_headers($this->firebase->headers),
						'content'					=> array(
							'collection'					=> '',
							//'data'						=> NULL,
						),
					);
					$remove_from_categories['url'] = base_firebase_url("firebase/kategori/put/");
					$remove_from_categories['url'] .= base_permalink($keval);
					$remove_from_categories['content']['collection'] = $keval;
					try {
						$collectData['params_query']['curl_parent_internal'][$for_i] = $this->firebase->create_curl_request($remove_from_categories['action'], $remove_from_categories['url'], $this->firebase->UA, $remove_from_categories['headers'], json_encode($remove_from_categories['content'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), 45);
					} catch (Exception $ex){
						$this->error = true;
						$this->error_msg[] = "Error exception while doing PUT data to firebase on remove-from-kategori.";
					}
					$for_i++;
				}
			}
		}
		if (!$this->error) {
			//-------------------------------------------------
			// Add to categories
			//-------------------------------------------------
			$authorize_header = base64_encode("{$this->firebase_credential['USER']}:{$this->firebase_credential['TOKEN']}");
			$this->firebase->add_headers('Authorization', "Basic {$authorize_header}");
			$put_to_categories = array(
				'action'					=> 'PUT',
				'url'						=> '',
				'headers'					=> $this->firebase->create_curl_headers($this->firebase->headers),
				'content'					=> array(
					'collection'					=> array(),
					'data'							=> (isset($collectData['params_query']['form']['put_parent_data']) ? $collectData['params_query']['form']['put_parent_data'] : array()),
				),
			);
			if (count($collectData['params_query']['form']['put_parent']['collection']) > 0) {
				$collectData['params_query']['curl_parent_internal'] = array();
				$for_i = 0;
				foreach ($collectData['params_query']['form']['put_parent']['collection'] as $keval) {
					$put_to_categories['url'] = base_firebase_url("firebase/kategori/put");
					$put_to_categories['url'] .= "/" . base_permalink($keval);
					$put_to_categories['content']['collection'] = $keval;
					//$put_to_categories['content'] = json_encode($put_to_categories['content'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
					try {
						$collectData['params_query']['curl_parent_internal'][$for_i] = $this->firebase->create_curl_request($put_to_categories['action'], $put_to_categories['url'], $this->firebase->UA, $put_to_categories['headers'], json_encode($put_to_categories['content'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), 15);
					} catch (Exception $ex){
						$this->error = true;
						$this->error_msg[] = "Error exception while doing PUT data to firebase.";
					}
					$for_i++;
				}
			}
		}
		//------------------------------------
		// Debug
		//------------------------------------
		/*
		echo "<pre>";
		if (!$this->error) {
			print_r($collectData);
			
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//------------------------------------
			
			
			
		
		
		if (!$this->error) {
			if ((int)$collectData['params_query']['new_stories_seq'] > 0) {
				$this->session->set_flashdata('success', 'Terimakasih! Cerita yang anda publish, telah kami terima. Kami akan konfirmasi melalui email ketika cerita telah tampil di aplikasi.');
				redirect(base_url("dashboard/stories/edit/{$query_params['stories_seq']}?time=" . time()));
			} else {
				$this->session->set_flashdata('error', "Return new_stories_seq is 0.");
				redirect(base_url('dashboard/stories/index'));
			}
			exit;
		} else {
			$error_to_show = "";
			foreach ($this->error_msg as $keval) {
				if (is_string($keval) || is_numeric($keval)) {
					$error_to_show .= sprintf("%s", $keval);
				} else if (is_object($keval) || is_array($keval)) {
					$error_to_show .= json_encode($keval);
				} else {
					$error_to_show .= "-";
				}
			}
			$this->session->set_flashdata('error', $error_to_show);
			redirect(base_url('dashboard/stories/index'));
			exit;
		}
	
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//============================================================================================
	private function set_stories_categories($stories_seq, $categories) {
		$stories_categories = array(
			'tmp'						=> array(),
			'data'						=> array(),
		);
		if (isset($categories['categories'])) {
			if (is_array($categories['categories']) && (count($categories['categories']) > 0)) {
				$for_i = 0;
				$stories_categories['tmp'][$for_i] = array(
					'title'							=> '',
					'subtitles'						=> array(),
					'subtitles_items'				=> array(),
				);
				foreach ($categories['categories'] as $keval) {
					$stories_categories['tmp'][$for_i]['title'] = (isset($keval['title']) ? $keval['title'] : '');
					if (isset($keval['items'])) {
						$val_i = 0;
						if (count($keval['items']) > 0) {
							foreach ($keval['items'] as $val) {
								if (isset($categories['get_categories']['data'][$keval['title']][$val])) {
									if (array_key_exists($stories_seq, $categories['get_categories']['data'][$keval['title']][$val])) {
										if (!empty($categories['get_categories']['data'][$keval['title']][$val][$stories_seq]) && (is_array($categories['get_categories']['data'][$keval['title']][$val][$stories_seq]))) {
											$stories_categories['tmp'][$for_i]['subtitles'][] = $val;
											$stories_categories['tmp'][$for_i]['subtitles_items'][] = count($categories['get_categories']['data'][$keval['title']][$val]);
										}
									}
								}
								$val_i++;
							}
						}
					}
					$for_i += 1;
				}
			}
		}
		//-------
		if (count($stories_categories['tmp']) > 0) {
			foreach ($stories_categories['tmp'] as &$keval) {
				if (isset($keval['subtitles'])) {
					if (is_array($keval['subtitles'])) {
						$keval['count'] = count($keval['subtitles']);
						if ($keval['count'] > 0) {
							$stories_categories['data'][] = $keval;
						}
					}
				}
			}
		}
		//---
		
		return $stories_categories;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//----------------------------------------------------------------------------
	// Legacy Pages
	//----------------------------------------------------------------------------
	function additem($stories_seq = 0) {
		$stories_seq = ((is_string($stories_seq) || is_numeric($stories_seq)) ? $stories_seq : '0');
		$stories_seq = strtolower(sprintf('%s', $stories_seq));
		$collectData = array(
			'page'				=> 'digitale-dashboard-edit-add',
			'collect'			=> array(),
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
				'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
				'line'					=> $this->take_configuration_item('footer-line-id'),
				'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['stories_seq'] = sprintf("%d", $stories_seq);
			if (intval($collectData['collect']['stories_seq']) > -1) {
				$collectData['collect']['stories'] = $this->firebase->get_stories_data_single($collectData['collect']['stories_seq']);
			}
		}
		if (!$this->error) {
			$this->load->view('public', $collectData);
		} else {
			print_r($this->error_msg);
		}
	}
	function edititem($stories_seq = 0, $item_seq = 0) {
		$stories_seq = ((is_string($stories_seq) || is_numeric($stories_seq)) ? $stories_seq : '0');
		$stories_seq = strtolower(sprintf('%d', $stories_seq));
		$item_seq = ((is_string($item_seq) || is_numeric($item_seq)) ? $item_seq : '0');
		$item_seq = strtolower(sprintf('%d', $item_seq));
		$collectData = array(
			'page'				=> 'digitale-dashboard-edit-edit',
			'collect'			=> array(),
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
				'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
				'line'					=> $this->take_configuration_item('footer-line-id'),
				'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['stories_seq'] = sprintf("%d", $stories_seq);
			$collectData['collect']['item_seq'] = sprintf("%d", $item_seq);
		}
		if (!$this->error) {
			if (intval($collectData['collect']['stories_seq']) > -1) {
				$collectData['collect']['stories'] = $this->firebase->get_stories_data_single($collectData['collect']['stories_seq']);
			}
		}
		if (!$this->error) {
			$collectData['collect']['items'] = array();
			if (isset($collectData['collect']['stories']['data']['items'][$collectData['collect']['item_seq']])) {
				$collectData['collect']['items'] = $collectData['collect']['stories']['data']['items'][$collectData['collect']['item_seq']];
			}
		}
		
		//---
		// Debug
		/*
		if (!$this->error) {
			echo "<pre>";
			print_r($collectData['collect']);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		if (!$this->error) {
			$this->load->view('public', $collectData);
			
		} else {
			print_r($this->error_msg);
		}
	}
	function deleteitem($stories_seq = 0, $item_seq = 0) {
		$stories_seq = ((is_string($stories_seq) || is_numeric($stories_seq)) ? $stories_seq : '0');
		$stories_seq = strtolower(sprintf('%d', $stories_seq));
		$item_seq = ((is_string($item_seq) || is_numeric($item_seq)) ? $item_seq : '0');
		$item_seq = strtolower(sprintf('%d', $item_seq));
		$collectData = array(
			'page'				=> 'digitale-dashboard-edit-delete',
			'collect'			=> array(),
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
			if (!isset($this->mod_adminpanel->localdata['account_role'])) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			if (!$collectData['collect']['userdata']) {
				$this->error = true;
				$this->error_msg[] = "You must login to view this page.";
			}
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'twitter'				=> $this->take_configuration_item('footer-twitter-url'),
				'instagram'				=> $this->take_configuration_item('footer-instagram-url'),
				'line'					=> $this->take_configuration_item('footer-line-id'),
				'googleplay'			=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['stories_seq'] = sprintf("%d", $stories_seq);
			$collectData['collect']['item_seq'] = sprintf("%d", $item_seq);
		}
		if (!$this->error) {
			if (intval($collectData['collect']['stories_seq']) > -1) {
				$collectData['collect']['stories'] = $this->firebase->get_stories_data_single($collectData['collect']['stories_seq']);
			}
		}
		if (!$this->error) {
			$collectData['collect']['items'] = array();
			if (isset($collectData['collect']['stories']['data']['items'][$collectData['collect']['item_seq']])) {
				$collectData['collect']['items'] = $collectData['collect']['stories']['data']['items'][$collectData['collect']['item_seq']];
			}
		}
		
		
		
		if (!$this->error) {
			$this->load->view('public', $collectData);
		} else {
			print_r($this->error_msg);
		}
	}
		

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}