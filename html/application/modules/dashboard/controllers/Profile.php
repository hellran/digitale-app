<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {
	public $isAdmin = FALSE;
	public $isMerchant = FALSE;
	protected $DateObject;
	protected $email_vendor;
	public $error = FALSE, $error_msg = array();
	protected $firebase_credential;
	protected $base_config = array();
	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
		# Load Base Config
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		
		$this->load->model('adminpanel/Model_adminpanel', 'mod_adminpanel');
		$this->load->model('adminpanel/Model_firebase', 'mod_firebase');
		$this->load->model('adminpanel/Model_configuration', 'mod_config');
		$this->load->library('adminpanel/Lib_Rc4Crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('3', '4'))) {
			$this->isAdmin = TRUE;
		}
		if (!$this->isAdmin) {
			if (in_array($this->mod_adminpanel->localdata['account_role'], array('2', '5'))) {
				$this->isMerchant = TRUE;
			}
		}
		$this->DateObject = $this->mod_adminpanel->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'));
		$this->load->library('adminpanel/Lib_Firebase', '', 'firebase');
		//
		$this->email_vendor = $this->base_config['email_vendor'];
		$this->firebase_credential = $this->base_config['firebase_credential'];
	}
	private function take_configuration_item($config_code) {
		$items = false;
		try {
			$config_items = $this->mod_config->get_config_item_by('code', $config_code);
		} catch (Exception $ex) {
			return false;
		}
		if (isset($config_items['data'])) {
			if (is_array($config_items['data']) && (count($config_items['data']) > 0)) {
				foreach ($config_items['data'] as $keval) {
					$items = $keval;
					break;
				}
			}
		}
		return $items;
	}
	//------------------------------------------------------------------------------------------------------------
	function index() {
		$collectData = array(
			'page'						=> 'dashboard-profile-view',
			'collect'					=> array(),
			'base_dashboard_path'		=> 'dashboard',
			'base_home_path'			=> 'home',
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		if (!$this->error) {
			if (!$this->mod_adminpanel->localdata) {
				$this->error = true;
				$this->error_msg[] = "You need login to access this page.";
			} else {
				$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
				$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
				$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
				$collectData['collect']['user-properties'] = array();
				if (!isset($collectData['collect']['userdata']['seq'])) {
					$this->error = true;
					$this->error_msg[] = "Local data user seq not exists.";
				}
			}
		}
		if (!$this->error) {
			try {
				$collectData['collect']['local-properties'] = $this->mod_adminpanel->get_local_user_properties($collectData['collect']['userdata']['seq']);
				if (count($collectData['collect']['local-properties']) > 0) {
					foreach ($collectData['collect']['local-properties'] as $keval) {
						$collectData['collect']['user-properties'][$keval->properties_key] = $keval->properties_value;
					}
				}
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get address details: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			$collectData['collect']['address-params'] = array(
				'country_code'				=> '360',
				'province_code'				=> (isset($collectData['collect']['user-properties']['user_address_province']) ? $collectData['collect']['user-properties']['user_address_province'] : ''),
				'city_code'					=> (isset($collectData['collect']['user-properties']['user_address_city']) ? $collectData['collect']['user-properties']['user_address_city'] : ''),
				'district_code'				=> (isset($collectData['collect']['user-properties']['user_address_district']) ? $collectData['collect']['user-properties']['user_address_district'] : ''),
				'area_code'					=> (isset($collectData['collect']['user-properties']['user_address_area']) ? $collectData['collect']['user-properties']['user_address_area'] : ''),
			);
			$collectData['collect']['address-object'] = array(
				'province'		=> $this->mod_adminpanel->get_province($collectData['collect']['address-params']),
				'city'			=> $this->mod_adminpanel->get_city($collectData['collect']['address-params']),
				'district'		=> $this->mod_adminpanel->get_district($collectData['collect']['address-params']),
				'area'			=> $this->mod_adminpanel->get_area($collectData['collect']['address-params']),
			);
			$collectData['collect']['address-values'] = array();
			if (count($collectData['collect']['address-object']['province']) > 0) {
				foreach ($collectData['collect']['address-object']['province'] as $keval) {
					if ((int)$keval->province_code === (int)$collectData['collect']['address-params']['province_code']) {
						$collectData['collect']['address-values']['province'] = $keval->province_name;
					}
				}
			}
			if (count($collectData['collect']['address-object']['city']) > 0) {
				foreach ($collectData['collect']['address-object']['city'] as $keval) {
					if ((int)$keval->city_code === (int)$collectData['collect']['address-params']['city_code']) {
						$collectData['collect']['address-values']['city'] = $keval->city_name;
					}
				}
			}
			if (count($collectData['collect']['address-object']['district']) > 0) {
				foreach ($collectData['collect']['address-object']['district'] as $keval) {
					if ((int)$keval->district_code === (int)$collectData['collect']['address-params']['district_code']) {
						$collectData['collect']['address-values']['district'] = $keval->district_name;
					}
				}
			}
		}
		if (!$this->error) {
			$this->load->view("{$collectData['base_dashboard_path']}/dashboard.php", $collectData);
		} else {
			$error_to_show = "";
			foreach ($this->error_msg as $keval) {
				if (is_string($keval) || is_numeric($keval)) {
					$error_to_show .= sprintf("%s", $keval);
				} else if (is_object($keval) || is_array($keval)) {
					$error_to_show .= json_encode($keval);
				} else {
					$error_to_show .= "-";
				}
			}
			$this->session->set_flashdata('error', $error_to_show);
			redirect(base_url('home'));
			exit;
		}
	}
	function setting() {
		$this->index();
	}
	function editaction() {
		$collectData = array(
			'page'						=> 'dashboard-profile-setting',
			'collect'					=> array(),
			'base_dashboard_path'		=> 'dashboard',
			'base_home_path'			=> 'home',
		);
		$collectData['profile_edit'] = array();
		$collectData['firebase_credential'] = $this->firebase_credential;
		$collectData['upload_img'] = $this->base_config['upload_img'];
		$collectData['upload_ftp'] = $this->base_config['upload_img']['cdn'];
		if (!$this->error) {
			if (!$this->mod_adminpanel->localdata) {
				$this->error = true;
				$this->error_msg[] = "You need login to access this page.";
			} else {
				$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
				$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
				$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
				$collectData['collect']['user-properties'] = array();
				if (!isset($collectData['collect']['userdata']['seq'])) {
					$this->error = true;
					$this->error_msg[] = "Local data user seq not exists.";
				}
			}
		}
		if (!$this->error) {
			try {
				$collectData['collect']['local-properties'] = $this->mod_adminpanel->get_local_user_properties($collectData['collect']['userdata']['seq']);
				if (count($collectData['collect']['local-properties']) > 0) {
					foreach ($collectData['collect']['local-properties'] as $keval) {
						$collectData['collect']['user-properties'][$keval->properties_key] = $keval->properties_value;
					}
				}
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot get address details: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			$collectData['collect']['address-params'] = array(
				'country_code'				=> '360',
				'province_code'				=> (isset($collectData['collect']['user-properties']['user_address_province']) ? $collectData['collect']['user-properties']['user_address_province'] : ''),
				'city_code'					=> (isset($collectData['collect']['user-properties']['user_address_city']) ? $collectData['collect']['user-properties']['user_address_city'] : ''),
				'district_code'				=> (isset($collectData['collect']['user-properties']['user_address_district']) ? $collectData['collect']['user-properties']['user_address_district'] : ''),
				'area_code'					=> (isset($collectData['collect']['user-properties']['user_address_area']) ? $collectData['collect']['user-properties']['user_address_area'] : ''),
			);
			$collectData['collect']['address-object'] = array(
				'province'		=> $this->mod_adminpanel->get_province($collectData['collect']['address-params']),
				'city'			=> $this->mod_adminpanel->get_city($collectData['collect']['address-params']),
				'district'		=> $this->mod_adminpanel->get_district($collectData['collect']['address-params']),
				'area'			=> $this->mod_adminpanel->get_area($collectData['collect']['address-params']),
			);
			$collectData['collect']['address-values'] = array();
			if (count($collectData['collect']['address-object']['province']) > 0) {
				foreach ($collectData['collect']['address-object']['province'] as $keval) {
					if ((int)$keval->province_code === (int)$collectData['collect']['address-params']['province_code']) {
						$collectData['collect']['address-values']['province'] = $keval->province_name;
					}
				}
			}
			if (count($collectData['collect']['address-object']['city']) > 0) {
				foreach ($collectData['collect']['address-object']['city'] as $keval) {
					if ((int)$keval->city_code === (int)$collectData['collect']['address-params']['city_code']) {
						$collectData['collect']['address-values']['city'] = $keval->city_name;
					}
				}
			}
			if (count($collectData['collect']['address-object']['district']) > 0) {
				foreach ($collectData['collect']['address-object']['district'] as $keval) {
					if ((int)$keval->district_code === (int)$collectData['collect']['address-params']['district_code']) {
						$collectData['collect']['address-values']['district'] = $keval->district_name;
					}
				}
			}
		}
		if (!$this->error) {
			if (isset($collectData['upload_img']['local']) && isset($collectData['upload_img']['cdn'])) {
				try {
					$this->load->library('upload', $collectData['upload_img']['local']);
					$this->upload->initialize($collectData['upload_img']['local']);
					$this->load->library('ftp');
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot load upload and ftp library also initilize that.";
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Config dont have upload image preference or cdn preference";
			}
		}
		//-----------------------------------------------------------------------------
		// Image upload and CDN
		if (!$this->error) {
			$collectData['img_resize_config'] = array();
			$collectData['img_resize_thumb_path'] = '';
			$collectData['img_resize_thumb_name'] = '';
			if (isset($_FILES['user_picture']['tmp_name'])) {
				if ($_FILES['user_picture']['tmp_name'] != null) {
					$size = (isset($_FILES['user_picture']['size']) ? $_FILES['user_picture']['size'] : 0);
					$max_size = (1024 * $this->base_config['upload_img']['local']['max_size']);// size more than 512 KB
					if (intval($size) > $max_size) {
						$this->error = true;
						$this->error_msg[] = 'Max file size is 512 KB';
					} else {
						list($width, $height) = getimagesize($_FILES['user_picture']['tmp_name']);
						if ($this->upload->do_upload('user_picture')) {
							$collectData['up_result'] = $this->upload->data();
							if (isset($collectData['up_result']['full_path'])) {
								$collectData['img_resize_thumb_path'] = (isset($collectData['upload_img']['local']['upload_path']) ? $collectData['upload_img']['local']['upload_path'] : '') . DIRECTORY_SEPARATOR . (isset($collectData['up_result']['raw_name']) ? $collectData['up_result']['raw_name'] : '') . ('_avatar') . (isset($collectData['up_result']['file_ext']) ? $collectData['up_result']['file_ext'] : '');
								$collectData['img_resize_thumb_name'] = (isset($collectData['up_result']['raw_name']) ? $collectData['up_result']['raw_name'] : '') . ('_avatar') . (isset($collectData['up_result']['file_ext']) ? $collectData['up_result']['file_ext'] : '');
								$collectData['img_resize_config']['image_library'] = 'gd2';
								$collectData['img_resize_config']['source_image'] = $collectData['up_result']['full_path'];
								$collectData['img_resize_config']['thumb_marker'] = '_avatar';
								$collectData['img_resize_config']['create_thumb'] = TRUE;
								$collectData['img_resize_config']['maintain_ratio'] = TRUE;
								$collectData['img_resize_config']['overwrite'] = TRUE;
								$collectData['img_resize_config']['new_image'] = (isset($collectData['upload_img']['local']['upload_path']) ? $collectData['upload_img']['local']['upload_path'] : '');
								$collectData['img_resize_config']['master_dim'] = 'width';
								$collectData['img_resize_config']['width'] = 215;
								$collectData['img_resize_config']['height'] = 215;
								$this->load->library('image_lib', $collectData['img_resize_config']);
								if (!$this->image_lib->resize()) {
									$this->error = true;
									$this->error_msg[] = 'Cannot using image-library for resizing image';
								}
								// Send to cdn
								try {
									$this->ftp->connect($collectData['upload_img']['cdn']);
									$collectData['ftp_result'] = $this->ftp->upload($collectData['img_resize_thumb_path'], "{$collectData['upload_img']['cdn']['cdn_full_path']}/{$collectData['img_resize_thumb_name']}");
									$this->ftp->close();
								} catch (Exception $ex) {
									$this->error = true;
									$this->error_msg[] = 'Cannot send image-file via FTP: ' . $ex->getMessage();
								}
								try {
									unlink($collectData['up_result']['full_path']);
									unlink($collectData['img_resize_thumb_path']);
								} catch (Exception $ex) {
									$this->error = true;
									$this->error_msg[] = 'Error exception unlink tmp images.';
								}
								if ($collectData['ftp_result']) {
									$collectData['profile_edit']['account_picture'] = "{$collectData['upload_img']['cdn']['cdn_full_url']}/{$collectData['img_resize_thumb_name']}";
								} else {
									$collectData['profile_edit']['account_picture'] = "FTP Result return FALSE";
									$this->error = true;
									$this->error_msg[] = 'Something wrong with upload to CDN via FTP, please try again';
								}
							} else {
								$this->error = true;
								$this->error_msg[] = $this->upload->display_errors();
							}
						} else {
							$this->error = true;
							$this->error_msg[] = 'ERROR: Cannot do-upload';
							$this->error_msg[] = $this->upload->display_errors();
						}
					}
				}
			}
		}
		//-----------------------------------------------------------------------------
		if (!$this->error) {
			$this->form_validation->set_rules('user_fullname','Full Name','trim|required|max_length[128]|xss_clean');
			$this->form_validation->set_rules('user_nickname','Nickname','trim|required|max_length[128]|xss_clean');
			$this->form_validation->set_rules('user_phonenumber', 'Phone Number', 'min_length[10]|numeric|xss_clean');
			$this->form_validation->set_rules('user_phonemobile', 'Mobile Number', 'required|min_length[10]|numeric|xss_clean');
			
			if ($this->form_validation->run() == FALSE) {
				$this->error = true;
				$this->error_msg[] = validation_errors('<li>', '</li>');
			} else {
				$input_params = array(
					'user_fullname'						=> $this->input->post('user_fullname'),
					'user_nickname'						=> $this->input->post('user_nickname'),
					'user_phonenumber'					=> $this->input->post('user_phonenumber'),
					'user_phonemobile'					=> $this->input->post('user_phonemobile'),
					'user_address'						=> $this->input->post('user_address'),
				);
				$input_params['user_phonenumber'] = sprintf('%s', $input_params['user_phonenumber']);
				$input_params['user_phonemobile'] = sprintf('%s', $input_params['user_phonemobile']);
				$input_params['user_fullname'] = sprintf('%s', $input_params['user_fullname']);
				$query_params = array(
					'account_nickname'				=> ((strlen($input_params['user_nickname']) > 0) ? $input_params['user_nickname'] : ''),
					'account_fullname'				=> ((strlen($input_params['user_fullname']) > 0) ? ucwords(strtolower($input_params['user_fullname'])) : ''),
					'account_address'				=> ((strlen($input_params['user_address']) > 0) ? $input_params['user_address'] : ''),
					'account_phonenumber'			=> ((strlen($input_params['user_phonenumber']) > 0) ? $input_params['user_phonenumber'] : ''),
					'account_phonemobile'			=> ((strlen($input_params['user_phonemobile']) > 0) ? $input_params['user_phonemobile'] : ''),
					'account_edited_by'				=> (isset($collectData['collect']['userdata']['account_username']) ? $collectData['collect']['userdata']['account_username'] : 'system'),
				);
				try {
					$collectData['profile_edit'] = array_merge($collectData['profile_edit'], $query_params);
					$collectData['edit_account_seq'] = $this->mod_adminpanel->edit_user((isset($collectData['collect']['userdata']['seq']) ? $collectData['collect']['userdata']['seq'] : '0'), $collectData['profile_edit']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot editing user to local-account.";
				}
			}
		}
		if (!$this->error) {
			if (isset($collectData['edit_account_seq']) && (intval($collectData['edit_account_seq']) > 0)) {
				$local_data_properties = array();
				$local_data_properties['user_address_province'] = $this->input->post('user_address_province');
				$local_data_properties['user_address_city'] = $this->input->post('user_address_city');
				$local_data_properties['user_address_district'] = $this->input->post('user_address_district');
				$local_data_properties['user_address_area'] = $this->input->post('user_address_area');
				$local_data_properties['user_address'] = $this->input->post('user_address');
				foreach ($local_data_properties as $key => $val) {
					if (strlen($val) > 0) {
						$local_properties_params = array(
							'properties_key'				=> strtolower($key),
							'properties_value'				=> $val,
						);
						$collectData['account_properties_seq'] = $this->mod_adminpanel->update_local_user_properties($collectData['edit_account_seq'], $local_properties_params);
					}
				}
			}
		}
		
		
		//--------------------------------
		// Redirect
		if (!$this->error) {
			$this->session->set_flashdata('success', 'Success saved profile');
			redirect(base_url('dashboard/profile/index'));
		} else {
			$error_to_show = "";
			foreach ($this->error_msg as $keval) {
				if (is_string($keval) || is_numeric($keval)) {
					$error_to_show .= sprintf("%s", $keval);
				} else if (is_object($keval) || is_array($keval)) {
					$error_to_show .= json_encode($keval);
				} else {
					$error_to_show .= "-";
				}
			}
			$this->session->set_flashdata('error', $error_to_show);
			redirect(base_url('dashboard/profile/index'));
			exit;
		}
	}
	function password_edit() {
		$collectData = array(
			'page'						=> 'dashboard-profile-setting',
			'collect'					=> array(),
			'base_dashboard_path'		=> 'dashboard',
			'base_home_path'			=> 'home',
		);
		$collectData['profile_edit'] = array();
		$collectData['firebase_credential'] = $this->firebase_credential;
		$collectData['upload_img'] = $this->base_config['upload_img'];
		$collectData['upload_ftp'] = $this->base_config['upload_img']['cdn'];
		if (!$this->error) {
			if (!$this->mod_adminpanel->localdata) {
				$this->error = true;
				$this->error_msg[] = "You need login to access this page.";
			} else {
				$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
				$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
				$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
				$collectData['collect']['user-properties'] = array();
				if (!isset($collectData['collect']['userdata']['seq'])) {
					$this->error = true;
					$this->error_msg[] = "Local data user seq not exists.";
				}
			}
		}
		if (!$this->error) {
			if ($this->session->userdata('gg_login_account') == FALSE) {
				$this->error = true;
				$this->error_msg[] = "You don't have any logged session";
			} else {
				$this->form_validation->set_rules('user_password_current', 'Current password', 'required|max_length[64]');
				$this->form_validation->set_rules('user_password_new', 'New password', 'required|max_length[64]');
				$this->form_validation->set_rules('user_password_confirm', 'Confirm new password', 'required|matches[user_password_new]|max_length[64]');
				if ($this->form_validation->run() == FALSE) {
					$this->error = true;
					$this->error_msg[] = validation_errors('<li>', '</li>');
				} else {
					$input_params = array(
						'user_password_current'			=> $this->input->post('user_password_current'),
						'user_password_new'				=> $this->input->post('user_password_new'),
						'user_password_confirm'			=> $this->input->post('user_password_confirm'),
					);
					if ($input_params['user_password_new'] !== $input_params['user_password_confirm']) {
						$this->error = true;
						$this->error_msg[] = "New passoword and confirm password should be match.";
					}
				}
			}
		}
		if (!$this->error) {
			$query_params['current_password'] = (strlen($input_params['user_password_current']) ? $input_params['user_password_current'] : '');
			$query_params['update_password'] = (strlen($input_params['user_password_new']) ? $input_params['user_password_new'] : '');
			try {
				$query_params['account_salt'] = $this->rc4crypt->bDecryptRC4($this->mod_adminpanel->localdata['account_hash']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot decrypt of account-hash by rc4: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			$query_params['account_password_string'] = "{$query_params['account_salt']}|{$query_params['current_password']}";
			if (trim(strtolower(sha1($query_params['account_password_string']))) !== trim(strtolower($this->mod_adminpanel->localdata['account_password']))) {
				$this->error = true;
				$this->error_msg[] = "Current password not match";
			} else {
				$query_params['account_salt'] = sprintf("%s", $this->DateObject->format('YmdHisu'));
			}
		}
		if (!$this->error) {
			$query_params['account_hash'] = $this->rc4crypt->bEncryptRC4($query_params['account_salt']);
			$query_params['account_password_string'] = "{$query_params['account_salt']}|{$query_params['update_password']}";
			$query_params['account_password'] = sha1($query_params['account_password_string']);
			if (isset($query_params['account_password_string'])) {
				unset($query_params['account_password_string']);
			}
			if (isset($query_params['current_password'])) {
				unset($query_params['current_password']);
			}
			if (isset($query_params['update_password'])) {
				unset($query_params['update_password']);
			}
			if (isset($query_params['account_salt'])) {
				unset($query_params['account_salt']);
			}
			try {
				$seq = $this->mod_adminpanel->edit_user($this->mod_adminpanel->localdata['seq'], $query_params);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Cannot update password of user: {$ex->getMessage()}.";
			}
		}
		if (!$this->error) {
			// Reset if have tmp_password
			if ($this->session->userdata('tmp_password') != FALSE) {
				$this->session->set_userdata('tmp_password', NULL);
			}
			$this->session->set_flashdata('success', 'Password already changed.');
		} else {
			$error_to_show = "";
			foreach ($this->error_msg as $keval) {
				if (is_string($keval) || is_numeric($keval)) {
					$error_to_show .= sprintf("%s", $keval);
				} else if (is_object($keval) || is_array($keval)) {
					$error_to_show .= json_encode($keval);
				} else {
					$error_to_show .= "-";
				}
			}
			$this->session->set_flashdata('error', $error_to_show);
		}
		redirect(base_url('dashboard/profile/index'));
		exit;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function get_address($type = 'country') {
		$type = (is_string($type) ? strtolower($type) : 'country');
		$input_params = array();
		$address_result = array();
		switch ($type) {
			case 'country':
			default:
				$input_params['country_code'] = $this->input->post('user_address_country');
				$address_result = $this->mod_adminpanel->get_province($input_params);
			break;
			case 'province':
				$input_params['country_code'] = $this->input->post('user_address_country');
				$input_params['province_code'] = $this->input->post('user_address_province');
				$address_result = $this->mod_adminpanel->get_city($input_params);
			break;
			case 'city':
				$input_params['country_code'] = $this->input->post('user_address_country');
				$input_params['province_code'] = $this->input->post('user_address_province');
				$input_params['city_code'] = $this->input->post('user_address_city');
				$address_result = $this->mod_adminpanel->get_district($input_params);
			break;
			case 'district':
				$input_params['country_code'] = $this->input->post('user_address_country');
				$input_params['province_code'] = $this->input->post('user_address_province');
				$input_params['city_code'] = $this->input->post('user_address_city');
				$input_params['district_code'] = $this->input->post('user_address_district');
				$address_result = $this->mod_adminpanel->get_area($input_params);
			break;
			case 'area':
				$input_params['country_code'] = $this->input->post('user_address_country');
				$input_params['province_code'] = $this->input->post('user_address_province');
				$input_params['city_code'] = $this->input->post('user_address_city');
				$input_params['district_code'] = $this->input->post('user_address_district');
				$input_params['area_code'] = $this->input->post('user_address_area');
				$address_result = FALSE;
			break;
		}
		$htmlReturn = "";
		switch (strtolower($type)) {
			case 'country':
			default:
				$htmlReturn .= '<option value="">-- Select Province --</option>\n';
			break;
			case 'province':
				$htmlReturn .= '<option value="">-- Select Kota/Kabupaten --</option>\n';
			break;
			case 'city':
				$htmlReturn .= '<option value="">-- Select Kecamatan --</option>\n';
			break;
			case 'district':
				$htmlReturn .= '<option value="">-- Select Desa/Kelurahan --</option>\n';
			break;
			case 'area':
				$htmlReturn .= '<option value="">-- Select Province --</option>\n';
			break;
		}
		if (count($address_result) > 0) {
			foreach ($address_result as $keval) {
				switch (strtolower($type)) {
					case 'country':
					default:
						$htmlReturn .= "<option value='{$keval->province_code}'>{$keval->province_name}</option>\n";
					break;
					case 'province':
						$htmlReturn .= "<option value='{$keval->city_code}'>{$keval->city_name}</option>\n";
					break;
					case 'city':
						$htmlReturn .= "<option value='{$keval->district_code}'>{$keval->district_name}</option>\n";
					break;
					case 'district':
						$htmlReturn .= "<option value='{$keval->area_name}'>{$keval->area_name}</option>\n";
					break;
					case 'area':
						$htmlReturn .= "<option value='{$keval->province_code}'>{$keval->province_name}</option>\n";
					break;
				}
			}
		}
		echo $htmlReturn;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}