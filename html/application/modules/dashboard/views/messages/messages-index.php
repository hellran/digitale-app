<?php
if (!defined('BASEPATH')) { exit("Page load cannot be directly."); }

?>

<div id="titlebar">
	<div class="row">
		<div class="col-md-12">
			<h2>My Bookmarks</h2>
			<!-- Breadcrumbs -->
			<nav id="breadcrumbs">
				<ul>
					<li><a href="<?= base_url('home');?>">Home</a></li>
					<li><a href="<?= base_url('dashboard');?>">Dashboard</a></li>
					<li>My Messages</li>
				</ul>
			</nav>
		</div>
	</div>
</div>


<div class="row">
	<!-- FLASH MESSAGE-->
	<div class="col-md-12">
		<?php
		if ($this->session->flashdata('error') != false) {
			?>
			<div class="notification error closeable">
				<p>
					<span>Error!</span>
					<?=$this->session->flashdata('error');?>
				</p>
				<a class="close" href="#"></a>
			</div>
			<?php 
		}
		?>
		<?php
		if ($this->session->flashdata('success') != false) {
			?>
			<div class="notification success closeable">
				<p>
					<span>Success!</span>
					<?=$this->session->flashdata('success');?>
				</p>
				<a class="close" href="#"></a>
			</div>
			<?php 
		}
		?>
	</div>
	<!-- //FLASH MESSAGE-->
	
	
	<!-- Listings -->
	<div class="col-lg-12 col-md-12">

		<div class="messages-container margin-top-0">
			<div class="messages-headline">
				<h4>Messages List</h4>
			</div>
				
			<div class="messages-inbox">
				<ul>
					<li class="unread">
						<a href="https://projects.co.id/public/browse_users/view/fbc111/imzers">
							<div class="message-avatar">
								<img src="https://avatars1.githubusercontent.com/u/954765?s=88&v=4" alt="developer-avatar" />
							</div>
							
							<div class="message-by">
								<div class="message-by-headline">
									<h5>Imran Nababan <i>imzers@gmail.com</i></h5>
									<span></span>
								</div>
								<p>
									Please contact developer to add this module(s)
								</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>



