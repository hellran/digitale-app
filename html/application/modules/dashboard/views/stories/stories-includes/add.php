<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly lah.");
}
?>


<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/dropzone.js');?>"></script>
<script type="text/javascript">
	var thumbnailDropzone = new Dropzone("div#dropzone-placeholder-thumbnail", {
		url: '<?= base_url('dashboard/stories/publishThumbnail');?>',
		acceptedFiles: 'image/*',
		addRemoveLinks: true,
		paramName: 'publish_thumbnail',
		clickable: true,
		maxFiles: 10,
		// Remove Links
		dictCancelUpload: 'Cancel Uploading',
		dictRemoveFile: 'Hapus',
		dictCancelUploadConfirmation: 'Really cancel upload?',
		// Callback
		success: function(file, response) {
			$('#publish_thumbnail').attr('value', response);
		}
	});
	$(document).ready(function () {
		var textarea_i = 0;
		var max_fields = 100;
        var wrapper = $('#item-lists-container');
        var add_button = $("#add-stories-item-button");
		$(add_button).click(function(e) {
			e.preventDefault();
			if (textarea_i < max_fields) {
				$(wrapper).append('<tr class="item-list-in-sequences">\
						<td class="row">\
							<div class="item-lists-textarea col-md-12">\
								<div class="col-md-6">\
									<div class="col-md-4">\
										<div class="submit-section">\
											<div id="stories-item' + textarea_i + '" class="dropzone-placeholder-items dropzone" data-img-placeholder="item-img-upload' + textarea_i + '" data-img-input="item-img-input' + textarea_i + '">\
											</div>\
										</div>\
									</div>\
									<div class="col-md-8">\
										<img id="item-img-upload' + textarea_i + '" src="" class="img-responsive items-img" alt="item-image" />\
										<input id="item-img-input' + textarea_i + '" name="publish_item_gambar[' + textarea_i + ']" type="hidden" value="" />\
									</div>\
								</div>\
								<div class="col-md-6">\
									<div class="col-md-11">\
										<textarea class="WYSIWYG" name="publish_item_teks[' + textarea_i + ']"></textarea>\
									</div>\
									<div class="col-md-1">\
										<a class="delitem" href="javascript:;">\
											<i class="fa fa-remove"></i>\
										</a>\
									</div>\
								</div>\
							</div>\
						</td>\
					</tr>');
				$(wrapper).children(':last').find('#stories-item' + textarea_i).each(function () {
					var el_img_placeholder = $(this).attr('data-img-placeholder');
					var el_img_input = $(this).attr('data-img-input');
					itemsDropzone = new Dropzone('div#' + $(this).attr('id'), {
						url: '<?= base_url('dashboard/stories/publishThumbnail');?>',
						acceptedFiles: 'image/*',
						addRemoveLinks: true,
						paramName: 'publish_thumbnail',
						clickable: true,
						maxFiles: 1,
						// Remove Links
						dictCancelUpload: 'Cancel Uploading',
						dictRemoveFile: 'Hapus',
						dictCancelUploadConfirmation: 'Really cancel upload?',
						// Callback
						success: function(file, response) {
							$('#' + el_img_placeholder).attr('src', response);
							$('#' + el_img_input).attr('value', response);
						}
					});
				});
				
				textarea_i++;
			}
		});
		
		$(wrapper).on("click", ".delitem", function(e) {
			e.preventDefault();
			$(this).closest('.item-list-in-sequences').remove();
			textarea_i--;
		});
	});
</script>




