<?php
if (!defined('BASEPATH')) { exit("Page load cannot be directly."); }

?>

<div id="titlebar">
	<div class="row">
		<div class="col-md-12">
			<h2>My Stories</h2>
			<!-- Breadcrumbs -->
			<nav id="breadcrumbs">
				<ul>
					<li><a href="<?= base_url('home');?>">Home</a></li>
					<li><a href="<?= base_url('dashboard');?>">Dashboard</a></li>
					<li>My Stories</li>
				</ul>
			</nav>
		</div>
	</div>
</div>


<div class="row">
	<!-- FLASH MESSAGE-->
	<div class="col-md-12">
		<?php
		if ($this->session->flashdata('error') != false) {
			?>
			<div class="notification error closeable">
				<p>
					<span>Error!</span>
					<?=$this->session->flashdata('error');?>
				</p>
				<a class="close" href="#"></a>
			</div>
			<?php 
		}
		?>
		<?php
		if ($this->session->flashdata('success') != false) {
			?>
			<div class="notification success closeable">
				<p>
					<span>Success!</span>
					<?=$this->session->flashdata('success');?>
				</p>
				<a class="close" href="#"></a>
			</div>
			<?php 
		}
		?>
	</div>
	<!-- //FLASH MESSAGE-->
	
	
	<!-- Listings -->
	<div class="col-lg-12 col-md-12">
		<div class="dashboard-list-box margin-top-0">
			<h4>Active Stories</h4>
			<ul>
				<?php
				if (isset($collect['stories_data'])) {
					if (is_array($collect['stories_data']) && (count($collect['stories_data']) > 0)) {
						foreach ($collect['stories_data'] as $keval) {
							?>
							<li>
								<div class="list-box-listing">
									<div class="list-box-listing-img">
										<a href="<?= base_url($base_home_path . '/stories/view/' . $keval->stories_seq);?>">
											<img class="load-stories-image" src="<?= base_url('templates/digitale/img/logo.png');?>" alt="story image thumb" data-stories-seq="<?=$keval->stories_seq;?>" data-stories-url="<?=$keval->stories_url_full;?>" />
										</a>
									</div>
									<div class="list-box-listing-content">
										<div class="inner">
											<h3>
												<a class="load-stories-title" href="<?= base_url($base_home_path . '/stories/view/' . $keval->stories_seq);?>">
													title....
												</a>
											</h3>
											<span class="load-stories-description">
												description....
											</span>
											<div class="star-rating">
												<div class="rating-counter">
													files....
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="buttons-to-right">
									<a href="<?= base_url('dashboard/stories/edit/' . $keval->stories_seq);?>" class="button gray"><i class="sl sl-icon-note"></i> Edit</a>
								</div>
							</li>
							<?php
						}
					}
				}
				?>
			</ul>
		</div>
	</div>
</div>

<!-- Pagination -->
<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12">
		<div class="pagination-container margin-top-20 margin-bottom-40">
			<nav class="pagination">
				<?= (isset($collect['pagination']) ? $collect['pagination'] : '');?>
			</nav>
		</div>
	</div>
</div>
<!-- //Pagination -->


