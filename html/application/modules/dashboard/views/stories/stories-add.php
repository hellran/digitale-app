<?php
if (!defined('BASEPATH')) { exit("Page load cannot be directly."); }

?>

<div id="titlebar">
	<div class="row">
		<div class="col-md-12">
			<h2>Add Stories</h2>
			<!-- Breadcrumbs -->
			<nav id="breadcrumbs">
				<ul>
					<li><a href="<?= base_url('home');?>">Home</a></li>
					<li><a href="<?= base_url('dashboard');?>">Dashboard</a></li>
					<li>
						<a href="<?= base_url('dashboard/stories/index');?>">My Stories</a>
					</li>
					<li><i class="fa fa-plus"></i> Add</li>
				</ul>
			</nav>
		</div>
	</div>
</div>


<div class="row">
	<!-- FLASH MESSAGE-->
	<div class="col-md-12">
		<?php
		if ($this->session->flashdata('error') != false) {
			?>
			<div class="notification error closeable">
				<p>
					<span>Error!</span>
					<?=$this->session->flashdata('error');?>
				</p>
				<a class="close" href="#"></a>
			</div>
			<?php 
		}
		?>
		<?php
		if ($this->session->flashdata('success') != false) {
			?>
			<div class="notification success closeable">
				<p>
					<span>Success!</span>
					<?=$this->session->flashdata('success');?>
				</p>
				<a class="close" href="#"></a>
			</div>
			<?php 
		}
		?>
	</div>
	<!-- //FLASH MESSAGE-->
	
	
	
	
	<div class="col-lg-12">
		
		<div id="add-listing">
			<form role="form" id="form-publish-new-item" action="<?= base_url("dashboard/stories/addaction") ?>" method="post" enctype="multipart/form-data">
				<!-- Section -->
				<div class="add-listing-section">
					<div class="add-listing-headline">
						<h3>
							<i class="sl sl-icon-doc"></i> Basic Informations
						</h3>
					</div>
					<div class="row with-forms">
						<div class="col-md-12">
							<h5>Stories Title 
								<i class="tip" data-tip-content="Judul cerita"></i>
							</h5>
							<input class="search-field" type="text" id="publish_judul" name="publish_judul" value="" />
						</div>
					</div>
					<div class="row with-forms">
						<div class="col-md-6">
							<h5>Licence Type <span>(optional)</span> <i class="tip" data-tip-content="If stories have licence"></i></h5>
							<input type="text" id="publish_licence_type" name="publish_licence_type" placeholder="Put licence type if any" />
						</div>
						<div class="col-md-6">
							<h5>Licence URL <span>(optional)</span> <i class="tip" data-tip-content="If stories have licence url"></i></h5>
							<input type="text" id="publish_licence_url" name="publish_licence_url" placeholder="Put licence url if any" />
						</div>
					</div>
				</div>
				<!-- Section -->
				<div class="add-listing-section">
					<div class="add-listing-headline">
						<h3>
							<i class="sl sl-icon-book-open"></i> Stories Properties
						</h3>
					</div>
					<div class="row with-forms">
						<div class="col-md-4">
							<h5>Penulis</h5>
							<input type="text" id="publish_penulis" name="publish_penulis" />
						</div>
						<div class="col-md-4">
							<h5>Illustrator</h5>
							<input type="text" id="publish_ilustrator" name="publish_ilustrator" />
						</div>
						<div class="col-md-4">
							<h5>Editor</h5>
							<input type="text" id="publish_editor" name="publish_editor" />
						</div>
					</div>
					<div class="row with-forms">
						<div class="col-md-6">
							<h5>Publisher</h5>
							<input type="text" id="publish_publisher" name="publish_publisher" value="<?= (isset($collect['userdata']['account_fullname']) ? $collect['userdata']['account_fullname'] : '');?>" />
						</div>
						<div class="col-md-6">
							<h5>Tags <span>(optional)</span> <i class="tip" data-tip-content="Tags separated by comma(,)"></i></h5>
							<input type="text" id="publish_tags" name="publish_tags" value="" />
						</div>
					</div>
				</div>
				<!-- Section -->
				<div class="add-listing-section margin-top-45">
					<div class="add-listing-headline">
						<h3><i class="sl sl-icon-picture"></i> Images</h3>
					</div>
					<div class="row with-forms">
						<h5>Thumbnail</h5>
						<div class="col-md-4">
							<!-- Dropzone -->
							<div class="submit-section">
								<div id="dropzone-placeholder-thumbnail" class="dropzone"></div>
							</div>
						</div>
						<div class="col-md-8">
							<input type="text" id="publish_thumbnail" name="publish_thumbnail" value="" readonly="readonly" />
						</div>
					</div>
				</div>
				<!-- Section -->
				<div class="add-listing-section margin-top-45">
					<div class="add-listing-headline">
						<h3><i class="sl sl-icon-docs"></i> Details</h3>
					</div>
					<div class="form">
						<h5>Sinopsis</h5>
						<textarea class="WYSIWYG" name="publish_sinopsis" cols="40" rows="3" id="publish_sinopsis" spellcheck="false"></textarea>
					</div>
					<!-- Checkboxes -->
					
					<?php
					if (isset($collect['categories']['categories'])) {
						if (is_array($collect['categories']['categories']) && (count($collect['categories']['categories']) > 0)) {
							foreach ($collect['categories']['categories'] as $catKey => $keval) {
								?>
								<h5 class="margin-top-30 margin-bottom-10">
									<?= (isset($keval['title']) ? $keval['title'] : '');?>
									<?php
									if (isset($collect['categories']['sub-kategori'][$catKey])) {
										echo '<span>(' . (is_array($collect['categories']['sub-kategori'][$catKey]) ? count($collect['categories']['sub-kategori'][$catKey]) : 0) . ' sub-kategori)</span>';
									}
									?>
								</h5>
								<input type="hidden" id="<?= base_permalink(isset($keval['title']) ? $keval['title'] : '');?>" name="publish_kategori[<?= base_permalink(isset($keval['title']) ? $keval['title'] : '');?>]" data-kategori-title="<?= (isset($keval['title']) ? $keval['title'] : '');?>" value="<?= (isset($keval['title']) ? $keval['title'] : '');?>" />
								<div class="checkboxes in-row margin-bottom-20">
									<?php
									if (isset($keval['items'])) {
										$val_i = 0;
										if (count($keval['items']) > 0) {
											echo '<div class="row with-forms">';
											foreach ($keval['items'] as $val) {
												if ($val_i % 3 == 0) {
													echo '</div>';
													echo '<div class="row with-forms">';
												}
												//--loop
												echo "<div class='col-md-4'>";
												//=============================
												?>
												<input class="form-check-input" id="<?= base_permalink(is_string($val) ? $val : '');?>" type="checkbox" name="publish_subkategori[<?= base_permalink(isset($keval['title']) ? $keval['title'] : '');?>][<?= base_permalink(is_string($val) ? $val : '');?>]" value="<?= (is_string($val) ? $val : '');?>" />
												<label for="<?= base_permalink(is_string($val) ? $val : '');?>" class="form-check-label"><?= (is_string($val) ? $val : '');?></label>
												<?php
												echo "</div>";
												$val_i += 1;
											}
											echo '</div>';
										}
									}
									?>
						
								</div>
								
								<?php
							}
						}
					}
					?>
				</div>
				
				<!-- Section -->
				<div class="add-listing-section margin-top-45">					
					<!-- Headline -->
					<div class="add-listing-headline">
						<h3>
							<i class="sl sl-icon-notebook"></i> Story Items
						</h3>
						<!-- Switcher -->
						<label class="switch">
							<input type="checkbox" checked="checked">
							<span class="slider round"></span>
						</label>
					</div>

					<!-- Switcher ON-OFF Content -->
					<div class="switcher-content">
						<div class="row">
							<div class="col-md-12">
								<table id="item-lists-container">
									<?php
									/*
									<tr class="item-list-in-sequences">
										<td class="row">
											<div class="item-lists-textarea col-md-12">
												<div class="col-md-6">
													<div class="col-md-4">
														<div class="submit-section">
															<div id="stories-item<?=$fileKey;?>" class="dropzone-placeholder-items dropzone" data-img-placeholder="item-img-upload<?=$fileKey;?>" data-img-input="item-img-input<?=$fileKey;?>">
																
															</div>
														</div>
													</div>
													<div class="col-md-8">
														<img id="item-img-upload<?=$fileKey;?>" src="<?= (isset($fileVal['gambar']) ? $fileVal['gambar'] : '');?>" class="img-responsive items-img" alt="item-image" />
														<input id="item-img-input<?=$fileKey;?>" name="publish_item_gambar[<?=$fileKey;?>]" type="hidden" value="<?= (isset($fileVal['gambar']) ? $fileVal['gambar'] : '');?>" />
													</div>
												</div>
												<div class="col-md-6">
													<div class="col-md-11">
														<textarea class="WYSIWYG" name="publish_item_teks[<?=$fileKey;?>]"><?= (isset($fileVal['teks']) ? $fileVal['teks'] : '');?></textarea>
													</div>
													<div class="col-md-1">
														<a class="delitem" href="javascript:;">
															<i class="fa fa-remove"></i>
														</a>
													</div>
												</div>
											</div>
										</td>
									</tr>
									*/
									?>
								</table>
								<a id="add-stories-item-button" href="javascript:;" class="button border">
									<i class="fa fa-plus"></i> Add Item
								</a>
							</div>
						</div>
						
						
						
						
					</div>
					<!-- Switcher ON-OFF Content / End -->
				</div>
				<!-- Section / End -->
				
				
				<button type="submit" class="button preview">
					<i class="fa fa-arrow-circle-right"></i> Send
				</button>
			</form>
		</div>
		
	</div>
	

</div>



