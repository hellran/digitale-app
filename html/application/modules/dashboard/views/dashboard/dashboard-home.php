<?php
if (!defined('BASEPATH')) { exit("Page load cannot be directly."); }

?>

<!-- Titlebar -->
<div id="titlebar">
	<div class="row">
		<div class="col-md-12">
			<h2>Dashboard</h2>
			<span>User Panel Dashboard</span>
			<!-- Breadcrumbs -->
			<nav id="breadcrumbs">
				<ul>
					<li><a href="<?= base_url('home');?>">Home</a></li>
					<li>Dashboard</li>
				</ul>
			</nav>
		</div>
	</div>
</div>



<div class="row">
	<!-- FLASH MESSAGE-->
	<div class="col-md-12">
		<?php
		if ($this->session->flashdata('error') != false) {
			?>
			<div class="notification error closeable">
				<p>
					<span>Error!</span>
					<?=$this->session->flashdata('error');?>
				</p>
				<a class="close" href="#"></a>
			</div>
			<?php 
		}
		?>
		<?php
		if ($this->session->flashdata('success') != false) {
			?>
			<div class="notification success closeable">
				<p>
					<span>Success!</span>
					<?=$this->session->flashdata('success');?>
				</p>
				<a class="close" href="#"></a>
			</div>
			<?php 
		}
		?>
	</div>
	<!-- //FLASH MESSAGE-->
	
	
	<!-- Notice -->
	<!--
	<div class="row">
		<div class="col-md-12">
			<div class="notification success closeable margin-bottom-30">
				<p>Your listing <strong>Hotel Govendor</strong> has been approved!</p>
				<a class="close" href="#"></a>
			</div>
		</div>
	</div>
	-->
	
	
	
	
	
	
	<!-- Content -->
	<div class="row">
		<!-- Item -->
		<div class="col-lg-3 col-md-6">
			<div class="dashboard-stat color-1">
				<div class="dashboard-stat-content">
					<h4>
						<?php
						if (isset($collect['my_stories'])) {
							if (is_array($collect['my_stories'])) {
								echo number_format(count($collect['my_stories']));
							}
						}
						?>
					</h4>
					<span>My Stories</span>
				</div>
				<div class="dashboard-stat-icon">
					<a href="<?= base_url('dashboard/stories/index');?>">
						<i class="im  im-icon-Address-Book2 "></i>
					</a>
				</div>
			</div>
		</div>

		<!-- Item -->
		<div class="col-lg-3 col-md-6">
			<div class="dashboard-stat color-2">
				<div class="dashboard-stat-content">
					<h4>0</h4> 
					<span>My Messages</span>
				</div>
				<div class="dashboard-stat-icon">
					<i class="im  im-icon-Envelope"></i>
				</div>
			</div>
		</div>

		
		<!-- Item -->
		<div class="col-lg-3 col-md-6">
			<div class="dashboard-stat color-3">
				<div class="dashboard-stat-content">
					<h4>0</h4> 
					<span>My Reviews</span>
				</div>
				<div class="dashboard-stat-icon">
					<a href="<?= base_url('dashboard/reviews/index');?>">
						<i class="im im-icon-Add-UserStar"></i>
					</a>
				</div>
			</div>
		</div>

		<!-- Item -->
		<div class="col-lg-3 col-md-6">
			<div class="dashboard-stat color-4">
				<div class="dashboard-stat-content">
					<h4>0</h4>
					<span>My Bookmarks</span>
				</div>
				<div class="dashboard-stat-icon">
					<a href="<?= base_url('dashboard/bookmarks/index');?>">
						<i class="im im-icon-Heart"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
		
	
	<?php
	/*
	<div class="row">
		<!-- Recent Activity -->
		<div class="col-lg-6 col-md-12">
			<div class="dashboard-list-box with-icons margin-top-20">
				<h4>Recent Activities</h4>
				<ul>
					<li>
						<i class="list-box-icon sl sl-icon-layers"></i> Your listing <strong><a href="#">Hotel Govendor</a></strong> has been approved!
						<a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
					</li>

					<li>
						<i class="list-box-icon sl sl-icon-star"></i> Kathy Brown left a review <div class="numerical-rating" data-rating="5.0"></div> on <strong><a href="#">Burger House</a></strong>
						<a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
					</li>

					<li>
						<i class="list-box-icon sl sl-icon-heart"></i> Someone bookmarked your <strong><a href="#">Burger House</a></strong> listing!
						<a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
					</li>

					<li>
						<i class="list-box-icon sl sl-icon-star"></i> Kathy Brown left a review <div class="numerical-rating" data-rating="3.0"></div> on <strong><a href="#">Airport</a></strong>
						<a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
					</li>

					<li>
						<i class="list-box-icon sl sl-icon-heart"></i> Someone bookmarked your <strong><a href="#">Burger House</a></strong> listing!
						<a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
					</li>

					<li>
						<i class="list-box-icon sl sl-icon-star"></i> John Doe left a review <div class="numerical-rating" data-rating="4.0"></div> on <strong><a href="#">Burger House</a></strong>
						<a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
					</li>

					<li>
						<i class="list-box-icon sl sl-icon-star"></i> Jack Perry left a review <div class="numerical-rating" data-rating="2.5"></div> on <strong><a href="#">Tom's Restaurant</a></strong>
						<a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
					</li>
				</ul>
			</div>
		</div>
		
		<!-- Invoices -->
		<div class="col-lg-6 col-md-12">
			<div class="dashboard-list-box invoices with-icons margin-top-20">
				<h4>Invoices</h4>
				<ul>
					
					<li><i class="list-box-icon sl sl-icon-doc"></i>
						<strong>Professional Plan</strong>
						<ul>
							<li class="unpaid">Unpaid</li>
							<li>Order: #00124</li>
							<li>Date: 20/07/2017</li>
						</ul>
						<div class="buttons-to-right">
							<a href="dashboard-invoice.html" class="button gray">View Invoice</a>
						</div>
					</li>
					
					<li><i class="list-box-icon sl sl-icon-doc"></i>
						<strong>Extended Plan</strong>
						<ul>
							<li class="paid">Paid</li>
							<li>Order: #00108</li>
							<li>Date: 14/07/2017</li>
						</ul>
						<div class="buttons-to-right">
							<a href="dashboard-invoice.html" class="button gray">View Invoice</a>
						</div>
					</li>

					<li><i class="list-box-icon sl sl-icon-doc"></i>
						<strong>Extended Plan</strong>
						<ul>
							<li class="paid">Paid</li>
							<li>Order: #00097</li>
							<li>Date: 10/07/2017</li>
						</ul>
						<div class="buttons-to-right">
							<a href="dashboard-invoice.html" class="button gray">View Invoice</a>
						</div>
					</li>
					
					<li><i class="list-box-icon sl sl-icon-doc"></i>
						<strong>Basic Plan</strong>
						<ul>
							<li class="paid">Paid</li>
							<li>Order: #00091</li>
							<li>Date: 30/06/2017</li>
						</ul>
						<div class="buttons-to-right">
							<a href="dashboard-invoice.html" class="button gray">View Invoice</a>
						</div>
					</li>

				</ul>
			</div>
		</div>
	</div>
	*/
	?>
		
		
		
		
</div>



