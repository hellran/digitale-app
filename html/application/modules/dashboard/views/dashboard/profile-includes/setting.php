<?php

?>
<script type="text/javascript">
	//=================================
	// Profile Picture
	$(document).ready( function() {
    	$(document).on('change', '.change-photo-btn :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [label]);
		});

		$('.change-photo-btn :file').on('fileselect', function(event, label) {
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) {
					console.log(log);
				}
		    }
		});

		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
		            $('#user-img-upload').attr('src', e.target.result);
		        }
		        reader.readAsDataURL(input.files[0]);
		    }
		}
		$("#user-img-input").change(function(){
		    readURL(this);
			//$('#item-selected-stories-gambar').attr('value', '');
		});
	});
	//----------------------------------------
	// Address Ajax
	function get_address(address_type) {
		var address_type = address_type;
		address_type = address_type.toLowerCase();
		var ajaxData = {
			'user_address_country': '360',
			'user_address_province': $('#user_address_province').val(),
			'user_address_city': $('#user_address_city').val(),
			'user_address_district': $('#user_address_district').val(),
			'user_address_area': $('#user_address_area').val()
		};
		var ajaxUrl = '<?= base_url('dashboard/profile/get-address');?>';
		if (address_type == 'province') {
			ajaxUrl += '/province';
			$('#user_address_city').html('<option value="">-- Kota/Kabupaten --</option>');
			$('#user_address_district').html('<option value="">-- Kecamatan --</option>');
			$('#user_address_area').html('<option value="">-- Desa/Kelurahan --</option>');
		} else if (address_type == 'city') {
			ajaxUrl += '/city';
			$('#user_address_district').html('<option value="">-- Kecamatan --</option>');
			$('#user_address_area').html('<option value="">-- Desa/Kelurahan --</option>');
		} else if (address_type == 'district') {
			ajaxUrl += '/district';
			$('#user_address_area').html('<option value="">-- Desa/Kelurahan --</option>');
		} else if (address_type == 'area') {
			ajaxUrl += '/area';
		} else {
			ajaxUrl += '/';
			$('#user_address_province').html('<option value="">-- Province --</option>');
		}
		$.ajax({
			type: "POST",
			url: ajaxUrl,
			data: ajaxData,
			cache: false,
			success: function(ajaxReturn){
				if (address_type == 'province') {
					$('#user_address_city').html(ajaxReturn);
					$('#user_address_district').html('<option value="">-- Kecamatan --</option>');
					$('#user_address_area').html('<option value="">-- Desa/Kelurahan --</option>');
				} else if (address_type == 'city') {
					$('#user_address_district').html(ajaxReturn);
					$('#user_address_area').html('<option value="">-- Desa/Kelurahan --</option>');
				} else if (address_type == 'district') {
					$('#user_address_area').html(ajaxReturn);
				} else if (address_type == 'area') {
					//$('#user_address_province').html(ajaxReturn);
				} else {
					$('#user_address_province').html(ajaxReturn);
				}
			}
		});
	}
	$("#user_address_province").change(function () {
		get_address('province');
	});
	$("#user_address_city").change(function () {
		get_address('city');
	});
	$("#user_address_district").change(function () {
		get_address('district');
	});
	$("#user_address_area").change(function () {
		get_address('area');
	});
</script>