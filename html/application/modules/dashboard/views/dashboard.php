<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}

# Load header
$this->load->view('base/base-dashboard/header.php');
# Load header-topbar
$this->load->view('base/base-dashboard/header-topbar.php');

$page = (isset($page) ? $page : 'dashboard');
if (is_string($page)) {
	$page = strtolower($page);
} else {
	$page = 'dashboard';
}

switch (strtolower($page)) {
	//---------------------
	// Dashboard
	//---------------------
	# Dashboard (As Default)
	case 'dashboard-home':
	case 'dashboard-index':
	case 'dashboard':
	default:
		$file_included = 'dashboard/dashboard-home.php';
	break;
	# Profile
	case 'dashboard-profile-setting':
		$file_included = 'dashboard/profile/profile-setting.php';
	break;
	case 'dashboard-profile-view':
		$file_included = 'dashboard/profile/profile-view.php';
	break;
	
	# Stories
	case 'stories-index':
		$file_included = 'stories/stories-index.php';
	break;
	case 'stories-add':
		$file_included = 'stories/stories-add.php';
	break;
	case 'stories-edit':
		$file_included = 'stories/stories-edit.php';
	break;
	case 'stories-lists-published':
	case 'stories-lists-drafted':
	case 'stories-lists-reviewed':
		$file_included = 'stories/stories-lists.php';
	break;
	# Bookmarks
	case 'bookmarks-index':
		$file_included = 'bookmarks/bookmarks-index.php';
	break;
	# Messages
	case 'messages-index':
		$file_included = 'messages/messages-index.php';
	break;
	
	
	
	
	
	
}
# Load file-included
$this->load->view($file_included);

# Load footer
$this->load->view('base/base-dashboard/footer.php');

# Load if any another pages : scripts provided
switch (strtolower($page)) {
	# Dashboard
	case 'dashboard-profile-setting':
	case 'dashboard-profile-view':
		$this->load->view('dashboard/profile-includes/setting.php');
	break;
	# Stories
	case 'stories-index':
		$this->load->view('stories/stories-includes/index.php');
	break;
	case 'stories-add':
		$this->load->view('stories/stories-includes/add.php');
	break;
	case 'stories-edit':
		$this->load->view('stories/stories-includes/edit.php');
	break;
	case 'stories-lists-published':
	case 'stories-lists-drafted':
	case 'stories-lists-reviewed':
		$this->load->view('stories/stories-includes/lists.php');
	break;
	
}
# Load footer-end
$this->load->view('base/base-dashboard/footer-end.php');







