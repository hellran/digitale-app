<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly lah.");
}
?>

<div class="gradient listing-titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Read Story</h2>
				<span><?= (isset($collect['stories_data']['judul']) ? $collect['stories_data']['judul'] : '');?></span>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li>
							<a href="<?= base_url('home');?>">Home</a>
						</li>
						<li>
							Read Story
						</li>
						<li>
							<?= (isset($collect['stories_data']['judul']) ? $collect['stories_data']['judul'] : '');?>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<div class="row sticky-wrapper">
			<div class="col-lg-8 col-md-8 padding-right-30">
				<div id="titlebar" class="listing-titlebar">
					<div class="listing-titlebar-title">
						<h2>
							<?= (isset($collect['stories_data']['judul']) ? $collect['stories_data']['judul'] : '');?>
							<?php
							if (isset($collect['stories_data']['kategori']['data'])) {
								if (is_array($collect['stories_data']['kategori']['data']) && (count($collect['stories_data']['kategori']['data']) > 0)) {
									foreach ($collect['stories_data']['kategori']['data'] as $kategori_data) {
										$kategori_parent = base_permalink($kategori_data['title']);
										if (isset($kategori_data['subtitles'])) {
											if (is_array($kategori_data['subtitles']) && (count($kategori_data['subtitles']) > 0)) {
												foreach ($kategori_data['subtitles'] as $kategori_child) {
													?>
													<a href="<?= base_url("home/categories/subview/{$kategori_parent}/" . base_permalink($kategori_child));?>">
														<span class="listing-tag">
															<?= $kategori_child;?>
														</span>
													</a>
													<?php
												}
											}
										}
									}
								}
							}
							?>
						</h2>
						<span id="stories-deskripsi-placeholder">
							<span class="listing-address">
								loading....
							</span>
						</span>
						<div class="star-rating">
							<div id="stories-files-count" class="rating-counter">
								loading....
							</div>
						</div>
					</div>
				</div>
				
				
				<!-- Listing Nav -->
				<div class="listing-nav-container">
					<ul class="tabs-nav">
						<li><a href="#stories-thumbnail" class="active">Cover</a></li>
						<li><a href="#stories-penulis">Penulis</a></li>
					</ul>
					<div class="tabs-container alt">
						<div class="tab-content" id="stories-thumbnail" style="display: none;">
							<img src="<?= (isset($collect['stories_data']['thumbnail']) ? $collect['stories_data']['thumbnail'] : base_url('templates/digitale/images/loading-image.png'));?>" alt="stories thumbnails" />
						</div>
						<div class="tab-content" id="stories-penulis" style="display: none;">
							<h3 class="listing-desc-headline">Penulis</h3>
							<ul class="listing-features checkboxes margin-top-0">
								<?php
								if (isset($collect['stories_data']['penulis'])) {
									$penulises = explode(',', $collect['stories_data']['penulis']);
									foreach ($penulises as $penulis) {
										?>
										<li><?=$penulis;?></li>
										<?php
									}
								}
								?>
							</ul>
						</div>
					
					
					</div>
				</div>
				
			</div>
			
			<div class="col-lg-4 col-md-4 margin-top-75 sticky">
				<!-- Contact -->
				<div class="boxed-widget">
					<h3><i class="sl sl-icon-pin"></i> Properties</h3>
					<ul class="listing-details-sidebar">
						<li id="stories-publisher">
							<i class="sl sl-icon-book-open"></i>
							<?= (isset($collect['stories_data']['publisher']) ? $collect['stories_data']['publisher'] : '-');?>
						</li>
						<li id="stories-illustrator">
							<i class="sl sl-icon-picture"></i>
							<?= (isset($collect['stories_data']['illustrator']) ? $collect['stories_data']['illustrator'] : '-');?>
						</li>
					</ul>
					<?php
					/*
					<ul class="listing-details-sidebar social-profiles">
						<li><a href="#" class="facebook-profile"><i class="fa fa-facebook-square"></i> Facebook</a></li>
						<li><a href="#" class="twitter-profile"><i class="fa fa-twitter"></i> Twitter</a></li>
					</ul>
					*/
					?>
					<!-- Reply to review popup -->
					<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
						<div class="small-dialog-header">
							<h3>Send Message</h3>
						</div>
						<div class="message-reply margin-top-0">
							<textarea cols="40" rows="3" placeholder="Your message to Burger House"></textarea>
							<button class="button">Send Message</button>
						</div>
					</div>
					<a href="#small-dialog" class="send-message-to-owner button popup-with-zoom-anim"><i class="sl sl-icon-envelope-open"></i> Send Message</a>
				</div>
				
				<!-- Moral Story -->
				<?php
				/*
				<div class="boxed-widget opening-hours margin-top-35">
					<div class="listing-badge now-open">Stories Affect</div>
					<h3><i class="sl sl-icon-briefcase"></i> Moral Story</h3>
					<ul>
						<li>Kebijaksanaan <span>20%</span></li>
						<li>Menabung <span>30%</span></li>
						<li>Berkarya <span>50%</span></li>
					</ul>
				</div>
				*/
				?>
				<!-- Share / Like -->
				<div class="listing-share margin-top-40 margin-bottom-40 no-border">
					<button class="like-button">
						<span class="like-icon"></span> Bookmark this story
					</button> 
					<span>0 people bookmarked this story</span>
					<?php
					/*
					<!-- Share Buttons -->
					<ul class="share-buttons margin-top-40 margin-bottom-0">
						<li><a class="fb-share" href="#"><i class="fa fa-facebook"></i> Share</a></li>
						<li><a class="twitter-share" href="#"><i class="fa fa-twitter"></i> Tweet</a></li>
						<li><a class="gplus-share" href="#"><i class="fa fa-google-plus"></i> Share</a></li>
						<!-- <li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li> -->
					</ul>
					*/
					?>
					<div class="clearfix"></div>
				</div>
			</div>
			
			
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">

			<!-- Categories -->
			<div class="row margin-bottom-25 margin-top-30">
				<div class="col-md-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						<a href="#" class="grid">
							<i class="fa fa-th"></i>
						</a>
						<a href="#" class="list active">
							<i class="fa fa-align-justify"></i>
						</a>
					</div>
				</div>

				<div class="col-md-6">
					<div class="fullwidth-filters">
						<!-- Select -->
						<div class="sort-by">
							<div class="sort-by-select">
								<select id="selected-categories" data-placeholder="Select Categories" class="chosen-select-no-single" name="set_categories">
									<?php
									if (isset($collect['categories_data']['kategori'])) {
										if (is_array($collect['categories_data']['kategori']) && (count($collect['categories_data']['kategori']) > 0)) {
											foreach ($collect['categories_data']['kategori'] as $kategori) {
												?>
												<option value="<?= base_permalink($kategori);?>"><?=$kategori;?></option>
												<?php
											}
										}
									}
									?>
								</select>
							</div>
						</div>
						<!-- //Select -->
					</div>
				</div>
			</div>
			<!-- //Categories -->
			
			<div class="row" id="stories-items" data-stories-seq="<?= (isset($stories_data_from_categories['stories_seq']) ? $stories_data_from_categories['stories_seq'] : '');?>" data-stories-url="<?= sprintf("%s%s/%s/cerita/%d", FIREBASE_URL_PROTOCOL, FIREBASE_URL_HOSTNAME, $base_config['firebase_database_prefix'], (isset($stories_data_from_categories['stories_seq']) ? $stories_data_from_categories['stories_seq'] : 0));?>">
				
			</div>

			
			
		</div>

	</div>
	
	
	
	
</div>





















