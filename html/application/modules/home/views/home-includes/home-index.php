<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly lah.");
}
?>
<script id="stories-categories" type="text/javascript" src="<?= base_url('home/home/get-stories-categories/javascript');?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
		var categories_length = stories_categories.length;
		
		
		// Latest published stories
		$('.carousel-item').each(function() {
			var this_image = this.getElementsByClassName('load-stories-image')[0];
			var this_description = this.getElementsByClassName('load-stories-description')[0];
			var this_publisher = this.getElementsByClassName('listing-badge')[0];
			var this_categories = this.getElementsByClassName('tag')[0];
			var this_title = this.getElementsByClassName('listing-item-stories-title')[0];
			var this_writer = this.getElementsByClassName('listing-item-stories-writer')[0];
			var this_files_count = this.getElementsByClassName('rating-counter')[0];
			
			var src = $(this_image).attr('src');
			var firebasescr = $(this_image).attr('data-stories-url');
			var this_stories_seq = $(this_image).attr('data-stories-seq');
			if (firebasescr.length > 0){
				var img = new Image();
				$.ajax({
					type: "GET",
					url: firebasescr + '.json',
					cache: false,
					dataType: 'json',
					success: function(ajaxReturn) {
						$(img).on("load", function() {
							this_image.src = this.src;
						});
						img.src = ajaxReturn.thumbnail;
						// Set Title
						this_title.innerHTML = ajaxReturn.judul;
						// Set Writer
						this_writer.innerHTML = ajaxReturn.penulis;
						// Set Files Count
						this_files_count.innerHTML = ajaxReturn.files.length + " Items";
						// Set Publisher
						this_publisher.innerHTML = ajaxReturn.publisher;
						
						// Set Categories
						for (cat_index in stories_categories) {
							if (typeof stories_categories[cat_index] !== 'function') {
								if (stories_categories[cat_index].stories[this_stories_seq] == ajaxReturn.judul) {
									this_categories.innerHTML = stories_categories[cat_index].categories;
									break;
								}
							}
						}
						
					}
				});
				
				
				
				
				
				
				
			} else {
				this_image.src = src;
			}
		});

		
	});
</script>