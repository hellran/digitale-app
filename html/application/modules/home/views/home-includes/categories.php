<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly lah.");
}
?>


<script type="text/javascript">
	$(document).ready(function () {
		$('#submit-to-categories-sub').click(function(el) {
			el.preventDefault();
			var selected_subkategori = $('#selected-categories-subview');
			var location_href = '<?= base_url('home/categories/subview');?>' + '/' + selected_subkategori.attr('data-categories-name') + '/' + selected_subkategori.val();
			window.location.href = location_href;
		});
		
		
		// Load each stories loaded
		$('.loading-stories-container').each(function() {
			
			var this_stories = this.getElementsByClassName('listing-item')[0];
			var firebasescr = $(this_stories).attr('data-stories-url');
			var stories_sinopsis = this.getElementsByClassName('listing-item-details')[0];
			var stories_items = this.getElementsByClassName('rating-counter')[0];
			
			var this_stories_seq = $(this_stories).attr('data-stories-seq');
			if (firebasescr.length > 0) {
				$.ajax({
					type: "GET",
					url: firebasescr + '.json',
					cache: false,
					dataType: 'json',
					success: function(ajaxReturn) {
						// Set Sinopsis
						stories_sinopsis.innerHTML = '<ul><li>' + ajaxReturn.deskripsi.substr(0, 32) + '...</li></ul>';
						// Set Items
						stories_items.innerHTML = '(' + ajaxReturn.files.length + ' items)';
						// Set Categories
						/*
						for (cat_index in stories_categories) {
							if (typeof stories_categories[cat_index] !== 'function') {
								if (stories_categories[cat_index].stories[this_stories_seq] == ajaxReturn.judul) {
									this_categories.innerHTML = stories_categories[cat_index].categories;
									break;
								}
							}
						}
						*/
					}
				});
				
				
				
				
				
				
				
			}
		});

		
	});
</script>