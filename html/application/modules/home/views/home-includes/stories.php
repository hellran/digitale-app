<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly lah.");
}
?>
<style type="text/css">
	.stories-inline-item {
		position: relative;
		background: #FFF;
		padding: 40px;
		width: auto;
		max-width: 800px;
		margin: 10px auto;
		text-align: left;
	}
</style>
<script type="text/javascript">
	function nl2br(str, is_xhtml) {   
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';    
		return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
	}
	$(document).ready(function () {
		$('#selected-categories').change(function() {
			var selected_categories = $(this).val();
			window.location.href = '<?= base_url('home/categories/view');?>' + '/' + selected_categories;
		});
		
		var stories_items = $('#stories-items');
		var stories_seq = stories_items.attr('data-stories-seq');
		var stories_url = stories_items.attr('data-stories-url');
		var stories_deskripsi_placeholder = $('#stories-deskripsi-placeholder');
		if (stories_url.length > 0) {
			$.ajax({
				type: "GET",
				url: stories_url + '.json',
				cache: false,
				dataType: 'json',
				success: function(ajaxReturn) {
					stories_deskripsi_placeholder.html('<span class="listing-address">' + ajaxReturn.deskripsi + '</span>');
					$('#stories-files-count').html('<i class="fa fa-bookmark"></i> (' + ajaxReturn.files.length + ' items)');
					$('#stories-deskripsi-tab').html(ajaxReturn.deskripsi);
					var stories_files = [];
					for (file_i in ajaxReturn.files) {
						if (ajaxReturn.files[file_i] != null) {
							// Append stories item
							stories_items.append('\
								<div id="items-file-images' + file_i +'" class="col-lg-12 col-md-12 stories-inline-item mfp-hide">\
									<div class="row">\
										<div class="col-md-7">\
											<img class="stories-image" src="' + ((ajaxReturn.files[file_i].gambar.length > 0) ? ajaxReturn.files[file_i].gambar : '<?= base_url('templates/digitale/images/loading-image.png');?>') + '" alt="file image" />\
										</div>\
										<div class="col-md-5">\
											' + nl2br(ajaxReturn.files[file_i].teks) + '\
										</div>\
									</div>\
								</div>\
								<div class="col-lg-12 col-md-12">\
									<div class="listing-item-container list-layout">\
										<a href="#items-file-images' + file_i +'" class="listing-item">\
											<div class="listing-item-image">\
												<img class="stories-image" src="' + ((ajaxReturn.files[file_i].gambar.length > 0) ? ajaxReturn.files[file_i].gambar : '<?= base_url('templates/digitale/images/loading-image.png');?>') + '" alt="file image" />\
												<span class="tag">' + file_i + '</span>\
											</div>\
											<div class="listing-item-content">\
												<div class="listing-item-inner">' + nl2br(ajaxReturn.files[file_i].teks) + '</div>\
												<span class="like-icon"></span>\
											</div>\
										</a>\
									</div>\
								</div>\
							');
						}
						stories_files.push({
							'src': (ajaxReturn.files[file_i].gambar.length > 0) ? ajaxReturn.files[file_i].gambar : '<?= base_url('templates/digitale/images/loading-image.png');?>',
							'title': nl2br(ajaxReturn.files[file_i].teks)
						});
					}
					$('#stories-publisher').html('<i class="sl sl-icon-book-open"></i> ' + ajaxReturn.publisher);
					$('#stories-illustrator').html('<i class="sl sl-icon-picture"></i> ' + ajaxReturn.illustrator);
					
					
					// Magnific Popup
					$('.listing-item').magnificPopup({
						//midClick: true,
						//type: 'inline'
						items: stories_files,
						gallery: {
							enabled: true
						},
						type: 'image'
					});
				}
			});
		} else {
			alert('empty stories url');
		}
				
		
		
		


		
		
		
	});
	
	
</script>
