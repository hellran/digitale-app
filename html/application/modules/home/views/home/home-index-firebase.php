<?php
if (!defined('BASEPATH')) { exit("Page load cannot be directly."); }


?>





<!-- Latest Stories -->
<section class="fullwidth margin-top-65 padding-top-75 padding-bottom-70" data-background-color="#f8f8f8">
	<div class="container">
		<div class="row">
			<!-- FLASH MESSAGE-->
			<div class="col-md-12">
				<?php
				if ($this->session->flashdata('error') != false) {
					?>
					<div class="notification error closeable">
						<p>
							<span>Error!</span>
							<?=$this->session->flashdata('error');?>
						</p>
						<a class="close" href="#"></a>
					</div>
					<?php 
				}
				?>
				<?php
				if ($this->session->flashdata('success') != false) {
					?>
					<div class="notification success closeable">
						<p>
							<span>Success!</span>
							<?=$this->session->flashdata('success');?>
						</p>
						<a class="close" href="#"></a>
					</div>
					<?php 
				}
				?>
			</div>
			<!-- //FLASH MESSAGE-->
			
			
			<div class="col-md-12">
				<h3 class="headline centered margin-bottom-45">
					Latest Update Published Stories
					<span>Discover latest updated stories</span>
				</h3>
			</div>

			<div class="col-md-12">
				<div class="simple-slick-carousel dots-nav">
					<?php
					if (isset($collect['stories_data']['cerita'])) {
						$cerita_i = 0;
						if (is_array($collect['stories_data']['cerita']) && (count($collect['stories_data']['cerita']) > 0)) {
							foreach ($collect['stories_data']['cerita'] as $cetKey => $cetVal) {
								if ($cerita_i < 8) {
									if (isset($cetVal['judul']) && isset($cetVal['publish_status'])) {
										if (strtoupper($cetVal['publish_status']) === 'YES') {
											?>
											<div class="carousel-item">
												<a href="<?= base_url('stories/read/' . base_permalink($cetVal['judul']));?>" class="listing-item-container">
													<div class="listing-item">
														<img src="<?= (isset($cetVal['thumbnail']) ? $cetVal['thumbnail'] : '');?>" alt="<?=$cetVal['judul'];?>" />
														<?php
														if (isset($cetVal['publisher'])) {
															if (strtolower($cetVal['publisher']) === 'digitale') {
																?>
																<div class="listing-badge now-close"><?=$cetVal['publisher'];?></div>
																<?php
															} else {
																?>
																<div class="listing-badge now-open"><?=$cetVal['publisher'];?></div>
																<?php
															}
														}
														?>
														<div class="listing-item-content">
															<span class="tag">
																<?php
																if (isset($cetVal['kategori']['data'][0]['title'])) {
																	echo $cetVal['kategori']['data'][0]['title'];
																} else {
																	echo "Unknown";
																}
																?>
															</span>
															<h3><?= (isset($cetVal['judul']) ? $cetVal['judul'] : '');?></h3>
															<span>
																<?= (isset($cetVal['penulis']) ? $cetVal['penulis'] : '');?>
															</span>
														</div>
														<span class="like-icon"></span>
													</div>
													<div class="star-rating">
														<div class="rating-counter">
															<?php
															if (isset($cetVal['files'])) {
																if (is_array($cetVal['files'])) {
																	echo count($cetVal['files']) . " Items";
																}
															}
															?>
														</div>
													</div>
												</a>
											</div>
											<?php
											$cerita_i++;
										}
									}
								}
							}
						}
					}
					?>
				</div>
				
			</div>
		</div>
	</div>
</section>
<!-- //Latest Stories -->

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3 class="headline centered margin-top-75">
				Stories Categories
				<span>Browse Stories Categories</span>
			</h3>
		</div>
	</div>
</div>
<!-- Categories Carousel -->
<div class="fullwidth-carousel-container margin-top-25">
	<div class="fullwidth-slick-carousel category-carousel">
		<?php
		$kategori_i = 0;
		if (isset($collect['categories_data']['data']['kategori'])) {
			if (is_array($collect['categories_data']['data']['kategori']) && (count($collect['categories_data']['data']['kategori']) > 0)) {
				foreach ($collect['categories_data']['data']['kategori'] as $katKey => $katVal) {
					if (is_string($katVal)) {
						?>
						<div class="fw-carousel-item">
							<div class="category-box-container">
								<a href="<?= base_url('categories/view/' . base_permalink($katVal));?>" class="category-box" data-background-image="<?= (isset($collect['configuration'][base_permalink($katVal)]->config_value) ? $collect['configuration'][base_permalink($katVal)]->config_value : base_url('templates/digitale/images/slider/kategori.jpg'));?>">
									<div class="category-box-content">
										<h3><?=$katVal;?></h3>
										<span>
											<?php
											if (isset($collect['categories_data']['data']['sub-kategori'][$katKey])) {
												if (is_array($collect['categories_data']['data']['sub-kategori'][$katKey])) {
													echo count($collect['categories_data']['data']['sub-kategori'][$katKey]);
												} else {
													echo "0";
												}
											}
											?>
											Kategori
										</span>
									</div>
									<span class="category-box-btn">Browse</span>
								</a>
							</div>
						</div>
						<?php
					}
					
					/*
					if (is_array($sub_kategori) && (count($sub_kategori) > 0)) {
						foreach ($sub_kategori as $kategori_data) {
							?>
							
							<?php
						}
					}
					*/
					$kategori_i += 1;
				}
			}
		}
		if ($kategori_i < 4) {
			for ($inject_i = 0; $inject_i < (4 - $kategori_i); $inject_i++) {
				if (isset($collect['categories_data']['data']['kategori'][$inject_i])) {
					?>
					<div class="fw-carousel-item">
						<div class="category-box-container half">
							<a href="<?= base_url('categories/view/' . base_permalink($collect['categories_data']['data']['kategori'][$inject_i]));?>" class="category-box" data-background-image="<?= (isset($collect['configuration'][base_permalink($collect['categories_data']['data']['kategori'][$inject_i])]->config_value) ? $collect['configuration'][base_permalink($collect['categories_data']['data']['kategori'][$inject_i])]->config_value : base_url('templates/digitale/images/slider/kategori.jpg'));?>">
								<div class="category-box-content">
									<h3><?=$collect['categories_data']['data']['kategori'][$inject_i];?></h3>
									<span>
										<?php
										if (isset($collect['categories_data']['data']['sub-kategori'][$inject_i])) {
											if (is_array($collect['categories_data']['data']['sub-kategori'][$inject_i])) {
												echo count($collect['categories_data']['data']['sub-kategori'][$inject_i]);
											} else {
												echo "0";
											}
										}
										?>
										Kategori
									</span>
								</div>
								<span class="category-box-btn">Browse</span>
							</a>
						</div>

						<div class="category-box-container half">
							<a href="<?= base_url('categories/view/' . base_permalink($collect['categories_data']['data']['kategori'][$inject_i]));?>" class="category-box" data-background-image="<?= (isset($collect['configuration'][base_permalink($collect['categories_data']['data']['kategori'][$inject_i])]->config_value) ? $collect['configuration'][base_permalink($collect['categories_data']['data']['kategori'][$inject_i])]->config_value : base_url('templates/digitale/images/slider/kategori.jpg'));?>">
								<div class="category-box-content">
									<h3><?=$collect['categories_data']['data']['kategori'][$inject_i];?></h3>
									<span>
										<?php
										if (isset($collect['categories_data']['data']['sub-kategori'][$inject_i])) {
											if (is_array($collect['categories_data']['data']['sub-kategori'][$inject_i])) {
												echo count($collect['categories_data']['data']['sub-kategori'][$inject_i]);
											} else {
												echo "0";
											}
										}
										?>
										Kategori
									</span>
								</div>
								<span class="category-box-btn">Browse</span>
							</a>
						</div>
					</div>
					<?php
				}
			}
		}
		?>
			
	</div>
</div>
<!-- Categories Carousel / End -->


<!-- Home Information -->
<div class="container">

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="headline centered margin-top-80">
				<?= (isset($collect['configuration']['home-description-head']->config_value) ? $collect['configuration']['home-description-head']->config_value : '');?>
				<span class="margin-top-25">
					<?= (isset($collect['configuration']['home-description-body']->config_value) ? $collect['configuration']['home-description-body']->config_value : '');?>
				</span>
			</h2>
		</div>
	</div>

	<div class="row icons-container">
		<div class="col-md-4">
			<div class="icon-box-2 with-line">
				<i class="im im-icon-Eyeglasses-Smiley2"></i>
				<h3>
					<?= (isset($collect['configuration']['home-stage-kiri']->config_name) ? $collect['configuration']['home-stage-kiri']->config_name : '');?>
				</h3>
				<p>
					<?= (isset($collect['configuration']['home-stage-kiri']->config_value) ? $collect['configuration']['home-stage-kiri']->config_value : '');?>
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="icon-box-2 with-line">
				<i class="im im-icon-Speak-2"></i>
				<h3>
					<?= (isset($collect['configuration']['home-stage-tengah']->config_name) ? $collect['configuration']['home-stage-tengah']->config_name : '');?>
				</h3>
				<p>
					<?= (isset($collect['configuration']['home-stage-tengah']->config_value) ? $collect['configuration']['home-stage-tengah']->config_value : '');?>
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="icon-box-2">
				<i class="im im-icon-Spell-Check"></i>
				<h3>
					<?= (isset($collect['configuration']['home-stage-kanan']->config_name) ? $collect['configuration']['home-stage-kanan']->config_name : '');?>
				</h3>
				<p>
					<?= (isset($collect['configuration']['home-stage-kanan']->config_value) ? $collect['configuration']['home-stage-kanan']->config_value : '');?>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- //Home Information -->




























