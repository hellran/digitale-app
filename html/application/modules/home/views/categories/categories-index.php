<?php
if (!defined('BASEPATH')) { exit("Page load cannot be directly."); }


?>

<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Story Categories</h2>
				<span>Discover all stories on <?= (isset($collect['categories_stories'][$categories_name]['kategori']) ? $collect['categories_stories'][$categories_name]['kategori'] : '');?></span>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li>
							<a href="<?= base_url('home');?>">Home</a>
						</li>
						<li>Categories</li>
						<li><?= (isset($collect['categories_stories'][$categories_name]['kategori']) ? $collect['categories_stories'][$categories_name]['kategori'] : '');?></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row">

		<div class="col-lg-9 col-md-8 padding-right-30">
			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-25">
				<form id="go-to-categories-sub" role="form" action="<?= base_url('home/categories/view');?>" method="post">
					<div class="col-md-6 col-xs-6">
						<div class="sort">
							<div class="sort-by-select">
								<select id="selected-categories-subview" data-categories-name="<?=$categories_name;?>" data-placeholder="Categories Child" class="chosen-select-no-single" name="set_subkategori">
									<?php
									if (isset($collect['categories_stories'][$categories_name]['sub_kategori'])) {
										if (is_array($collect['categories_stories'][$categories_name]['sub_kategori']) && (count($collect['categories_stories'][$categories_name]['sub_kategori']) > 0)) {
											foreach ($collect['categories_stories'][$categories_name]['sub_kategori'] as $sub_kategori) {
												?>
												<option value="<?= base_permalink($sub_kategori);?>"><?= $sub_kategori;?></option>	
												<?php
											}
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="layout-switcher">
							<button id="submit-to-categories-sub" type="submit" class="button fullwidth">
								<i class="fa fa-mail-forward"></i>
							</button>
						</div>
					</div>
				</form>
			</div>

			<div class="row">
				<!-- Stories Item -->
				<?php
				if (isset($collect['stories_data'])) {
					if (is_array($collect['stories_data']) && (count($collect['stories_data']) > 0)) {
						foreach ($collect['stories_data'] as $keval) {
							?>
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 loading-stories-container">
								<a href="<?= base_url('home/stories/read/' . (isset($keval['stories_seq']) ? $keval['stories_seq'] : 0));?>" class="listing-item-container">
									<div class="listing-item" data-stories-url="<?= sprintf("%s%s/%s/cerita/%d", FIREBASE_URL_PROTOCOL, FIREBASE_URL_HOSTNAME, $base_config['firebase_database_prefix'], (isset($keval['stories_seq']) ? $keval['stories_seq'] : 0));?>" data-stories-seq="<?=(isset($keval['stories_seq']) ? $keval['stories_seq'] : 0);?>" data-stories-sinopsis="listing-item-details" data-stories-items-count="rating-counter">
										<img src="<?= (isset($keval['stories_data']['thumbnail']) ? $keval['stories_data']['thumbnail'] : base_url('templates/digitale/images/loading-image.png'));?>" alt="stories-images-thumb">
										<div class="listing-badge now-open">
											<?= (isset($keval['stories_data']['publisher']) ? $keval['stories_data']['publisher'] : '');?>
										</div>
										<div class="listing-item-details">
											Loading....
										</div>
										<div class="listing-item-content">
											<span class="tag">
												<?php
												
												if (isset($collect['categories_stories'][$categories_name]['kategori'])) {
													echo $collect['categories_stories'][$categories_name]['kategori'];
												}
												?>
											</span>
											<h3><?= (isset($keval['stories_data']['judul']) ? $keval['stories_data']['judul'] : '');?></h3>
											<span><?= (isset($keval['stories_data']['penulis']) ? $keval['stories_data']['penulis'] : '');?></span>
										</div>
										<span class="like-icon"></span>
									</div>
									<div class="star-rating">
										<div class="rating-counter">
											Loading....
										</div>
									</div>
								</a>
							</div>
							<?php
						}
					}
				}
				
				?>
				
				
				
				


			</div>

			<!-- Pagination -->
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12">
					<!-- Pagination -->
					<div class="pagination-container margin-top-20 margin-bottom-40">
						<nav class="pagination">
							<?php
							if (isset($collect['pagination'])) {
								echo $collect['pagination'];
							}
							?>
						</nav>
					</div>
				</div>
			</div>
			<!-- Pagination / End -->
		</div>


		<!-- Sidebar
		================================================== -->
		<div class="col-lg-3 col-md-4">
			<div class="sidebar">
				<!-- Widget -->
				<div class="widget margin-bottom-40">
					<h3 class="margin-top-0 margin-bottom-30">Categories</h3>
					<?php
					if (isset($collect['categories_data']['kategori'])) {
						if (is_array($collect['categories_data']['kategori']) && (count($collect['categories_data']['kategori']) > 0)) {
							foreach ($collect['categories_data']['kategori'] as $kategori) {
								?>
								<div class="row with-forms">
									<div class="col-md-12">
										<a class="button fullwidth margin-top-25" href="<?= base_url('home/categories/view/' . base_permalink($kategori));?>"><?=$kategori;?></a>
									</div>
								</div>
								<?php
							}
						}
					}
					?>
				</div>
				<!-- Widget / End -->

			</div>
		</div>
		<!-- Sidebar / End -->
	</div>
</div>













