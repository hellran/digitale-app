<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}

?>



<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Digitale App</h2>
				<span>Login to dashboard</span>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li>
							<a href="<?= base_url('home');?>">Home</a>
						</li>
						<li>Login</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>




<div class="container">
	<div class="row">
		<!-- Contact Details -->
		<div class="col-md-4">
			<h4 class="headline margin-bottom-30">About Us</h4>
			<!-- Contact Details -->
			<div class="sidebar-textbox">
				<p>
					<?= (isset($collect['configuration']['home-description-body']->config_value) ? $collect['configuration']['home-description-body']->config_value : '');?>
				</p>
				<ul class="contact-details">
					<li>
						<i class="im im-icon-Phone-2"></i> 
						<strong>Phone:</strong> 
						<span><?= (isset($collect['configuration']['telp-redaksi']->config_value) ? $collect['configuration']['telp-redaksi']->config_value : '');?></span>
					</li>
					<li>
						<i class="im im-icon-Globe"></i> 
						<strong>Web:</strong> 
						<span>
							<a href="<?= base_url();?>"><?= base_url();?></a>
						</span>
					</li>
					<li>
						<i class="im im-icon-Envelope"></i> 
						<strong>E-Mail:</strong> 
						<span>
							<a href="mailto:<?= (isset($collect['configuration']['email-redaksi']->config_value) ? $collect['configuration']['email-redaksi']->config_value : '');?>">
								<span class="__cf_email__"><?= (isset($collect['configuration']['email-redaksi']->config_value) ? $collect['configuration']['email-redaksi']->config_value : '');?></span>
							</a>
						</span>
					</li>
				</ul>
			</div>
		</div>

		<!-- Login Form -->
		<div class="col-md-8">
			<section id="contact">
				<h4 class="headline margin-bottom-35">Login Form</h4>
				<div id="contact-message" class="form-auth-loader">
					<?php
					/*
					<form method="post" action="<?= base_url('dashboard/account/loginaction');?>" id="form-local-login" autocomplete="on">
						<div class="row">
							<div class="col-md-12">
								<div>
									<label for="user-email"><i class="fa fa-envelope"></i> Email Address</label>
									<input name="user_email" type="email" id="user-email" placeholder="Email Address" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" required="required" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div>
									<label for="user-password"><i class="fa fa-lock"></i> Password</label>
									<input name="user_password" type="password" id="user-password" placeholder="Password" required="required" />
								</div>
							</div>
						</div>
						<input id="submit-login-form" type="submit" class="submit button" id="submit" value="Login" />
					</form>
					*/
					?>
					Loading....
				</div>
			</section>
		</div>
		<!-- //Login Form -->
	</div>
</div>







