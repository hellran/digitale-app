<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}

?>
<!-- Firebase App is always required and must be first -->
<script src="https://www.gstatic.com/firebasejs/5.5.6/firebase-app.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/5.5.6/firebase-auth.js"></script>
<!-- Firebase UI -->
<script src="https://cdn.firebase.com/libs/firebaseui/3.1.1/firebaseui.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.1.1/firebaseui.css" />
<script type="text/javascript">
	// Initialize Firebase
	var config = {
		apiKey: "AIzaSyAqx9uHwPamtKyoEjpEtAiRjCya7FwuNus",
		authDomain: "digitale-app.firebaseapp.com",
		databaseURL: "https://digitale-app.firebaseio.com",
		projectId: "digitale-app",
		storageBucket: "digitale-app.appspot.com",
		messagingSenderId: "496551940108"
	};
	firebase.initializeApp(config);
</script>
<script type="text/javascript">
	//-------------------------------------------
	// User already logged-in?
	firebase.auth().onAuthStateChanged(function(user) {
		if (user) {
			// User is signed in.
			var displayName = user.displayName;
			var email = user.email;
			var emailVerified = user.emailVerified;
			var photoURL = user.photoURL;
			var uid = user.uid;
			var providerData = user.providerData;
			var objProfile = {};
			providerData.forEach(function (providerProfile) {
				objProfile.firebase_provider = providerProfile.providerId;
				objProfile.firebase_identity = providerProfile.uid;
				objProfile.firebase_uid = uid;
				objProfile.firebase_email = providerProfile.email;
				objProfile.firebase_fullname = providerProfile.displayName;
				$.ajax({
					type: "POST",
					url: '<?= base_url('dashboard/account/loginfirebase');?>',
					cache: false,
					data: objProfile,
					dataType: 'json',
					success: function(responseFirebase) {
						if (responseFirebase.success == true) {
							window.location.href = '<?= base_url('home');?>';
						} else {
							alert(responseFirebase.msg);
							window.location.href = '<?= base_url('home/logout');?>';
						}
					}
				});
			});
		} else {
			var ui = new firebaseui.auth.AuthUI(firebase.auth());
			var uiConfig = {
				callbacks: {
					signInSuccessWithAuthResult: function(authResult, redirectUrl) {
					  // User successfully signed in.
					  // Return type determines whether we continue the redirect automatically
					  // or whether we leave that to developer to handle.
					  console.log(authResult);
					  return true;
					},
					uiShown: function() {
					  // The widget is rendered.
					  // Hide the loader.
					  document.getElementById('contact-message').style.display = 'none';
					}
				},
				// Will use popup for IDP Providers sign-in flow instead of the default, redirect.
				signInFlow: 'popup',
				signInSuccessUrl: '<?= base_url('dashboard/account/login');?>',
				signInOptions: [
					// Leave the lines as is for the providers you want to offer your users.
					firebase.auth.GoogleAuthProvider.PROVIDER_ID,
					firebase.auth.EmailAuthProvider.PROVIDER_ID,
				],
				// Terms of service url.
				tosUrl: '<?= base_url('home/page/tos');?>',
				// Privacy policy url.
				privacyPolicyUrl: '<?= base_url('home/page/privacy');?>'
			};
			// The start method will wait until the DOM is loaded.
			ui.start('#contact', uiConfig);
		}
	});
	//var ui = new firebaseui.auth.AuthUI(firebase.auth());
	
	
	
</script>





