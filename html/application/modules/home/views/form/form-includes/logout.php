<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}

?>
<!-- Firebase App is always required and must be first -->
<script src="https://www.gstatic.com/firebasejs/5.5.6/firebase-app.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/5.5.6/firebase-auth.js"></script>
<!-- Firebase UI -->
<script src="https://cdn.firebase.com/libs/firebaseui/3.1.1/firebaseui.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.1.1/firebaseui.css" />
<script type="text/javascript">
	// Initialize Firebase
	var config = {
		apiKey: "AIzaSyAqx9uHwPamtKyoEjpEtAiRjCya7FwuNus",
		authDomain: "digitale-app.firebaseapp.com",
		databaseURL: "https://digitale-app.firebaseio.com",
		projectId: "digitale-app",
		storageBucket: "digitale-app.appspot.com",
		messagingSenderId: "496551940108"
	};
	firebase.initializeApp(config);
</script>
<script type="text/javascript">
	//-------------------------------------------
	// User already logged-in?
	firebase.auth().signOut().then(function() {
		window.location.href = '<?= base_url('home');?>';
	}, function(error) {
		alert("Sign-out from firebase failed");
		window.location.href = '<?= base_url('home');?>';
	});
</script>





