<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-----------------------
$page = (isset($page) ? $page : 'digitale');
$page = (is_string($page) ? strtolower($page) : 'digitale');

# Header
$this->load->view('base/base-home/header.php');
# Topbar
$this->load->view('base/base-home/header-topbar.php');





switch ($page) {
	# Home
	case 'digitale-home':
	case 'digitale':
	case 'home':
	default:
		$file_included = 'home/home/home-index.php';
	break;
	# Categories
	case 'categories-index':
		$file_included = 'categories/categories-index.php';
	break;
	case 'categories-subview':
		$file_included = 'categories/categories-subview.php';
	break;
	# Stories
	case 'stories-read':
		$file_included = 'home/stories/stories-read.php';
	break;
	# Form
	case 'form-login':
		$file_included = 'form/form-login.php';
	break;
	case 'form-password-forget':
		$file_included = 'form/form-password-forget.php';
	break;
	case 'form-logout':
		$file_included = 'form/form-logout.php';
	break;
	
	
	
}

# Load file-included
$this->load->view($file_included);

# Footer
$this->load->view('base/base-home/footer.php');
# Load if any another pages : scripts provided
switch (strtolower($page)) {
	# Form
	case 'form-password-forget':
		$this->load->view('form/form-includes/password-forget.php');
	break;
	case 'form-login':
		$this->load->view('form/form-includes/login.php');
	break;
	case 'form-logout':
		$this->load->view('form/form-includes/logout.php');
	break;
	# Categories
	case 'categories-index':
	case 'categories-subview':
		$this->load->view('home/home-includes/categories.php');
	break;
	# Stories
	case 'stories-read':
		$this->load->view('home/home-includes/stories.php');
	break;
	# Home
	case 'digitale-home':
	case 'digitale':
	case 'home':
	default:
		$this->load->view('home/home-includes/home-index.php');
	break;
}
# Footer End
$this->load->view('base/base-home/footer-end.php');










