<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stories extends MY_Controller {
	public $isAdmin = FALSE;
	public $isMerchant = FALSE;
	protected $DateObject;
	protected $email_vendor;
	public $error = FALSE, $error_msg = array();
	protected $firebase_credential;
	protected $base_config = array();
	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
		# Load Base Config
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		
		$this->load->model('adminpanel/Model_adminpanel', 'mod_adminpanel');
		$this->load->model('adminpanel/Model_firebase', 'mod_firebase');
		$this->load->model('adminpanel/Model_configuration', 'mod_config');
		$this->load->library('adminpanel/Lib_Rc4Crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('3', '4'))) {
			$this->isAdmin = TRUE;
		}
		if (!$this->isAdmin) {
			if (in_array($this->mod_adminpanel->localdata['account_role'], array('2', '5'))) {
				$this->isMerchant = TRUE;
			}
		}
		$this->DateObject = $this->mod_adminpanel->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'));
		$this->load->library('adminpanel/Lib_Firebase', '', 'firebase');
		//
		$this->email_vendor = $this->base_config['email_vendor'];
		$this->firebase_credential = $this->base_config['firebase_credential'];
		// Stories categories from consume
		$this->load->model('cli/Model_consume', 'mod_consume');
	}
	private function take_configuration_item($config_code) {
		$items = false;
		try {
			$config_items = $this->mod_config->get_config_item_by('code', $config_code);
		} catch (Exception $ex) {
			return false;
		}
		if (isset($config_items['data'])) {
			if (is_array($config_items['data']) && (count($config_items['data']) > 0)) {
				foreach ($config_items['data'] as $keval) {
					$items = $keval;
					break;
				}
			}
		}
		return $items;
	}
	private function get_stories_categories_from_consumed_firebase() {
		$collectData = array(
			'collect'			=> array(),
		);
		try {
			$collectData['consume_index'] = $this->mod_consume->get_consume_index_by('categories', 'stories_categories');
		} catch (Exception $ex) {
			$this->error = true;
			$this->error_msg[] = "Cannot get stories-categories from mod-consume with exception: {$ex->getMessage()}";
		}
		if (!$this->error) {
			if (isset($collectData['consume_index']->consume_last_seq)) {
				try {
					$collectData['categories_data'] = $this->mod_consume->get_consume_logs_by_table_seq($collectData['consume_index']->consume_table, $collectData['consume_index']->consume_last_seq);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get categories-data by mod-consume with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (isset($collectData['categories_data']->consume_data)) {
				return json_decode($collectData['categories_data']->consume_data, true);
			}
		}
		return false;
	}
	//------------------------------------------------------------------------------------------------------------
	public function get_stories_categories($type = 'javascript') {
		$collectData = array(
			'type'				=> (is_string($type) ? strtolower($type) : 'javascript'),
			'page'				=> 'digitale-home',
			'collect'			=> array(
				'categories_data'		=> array(),
			),
			'base_home_path'	=> 'home',
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'footer-twitter-url'		=> $this->take_configuration_item('footer-twitter-url'),
				'footer-instagram-url'		=> $this->take_configuration_item('footer-instagram-url'),
				'footer-facebook-url'		=> $this->take_configuration_item('footer-facebook-url'),
				'footer-line-id'			=> $this->take_configuration_item('footer-line-id'),
				'google-playstore-link'		=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'copyright',
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
				'telp-redaksi',
				'alamat-redaksi',
				'quote-coo',
				'quote-ceo',
				'home-description-head',
				'home-description-body',
				'home-stage-kiri',
				'home-stage-kanan',
				'home-stage-tengah',
				# HOME Categories
				'dongeng-pengantar-tidur',
				'seri-pengembangan-karakter',
				'cerita-berdasarkan-umur',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['input_query'] = '';
			$collectData['collect']['query_limit'] = array(
				'start'			=> 0,
				'perpage'		=> 16,
			);
		}
		//-----------------------------------------------------
		// Get Story Categories From Firebase
		//-----------------------------------------------------
		if (!$this->error) {
			try {
				$collectData['collect']['categories_data'] = $this->firebase->get_categories_data($collectData['collect']['input_query'], $collectData['collect']['query_limit']['start'], $collectData['collect']['query_limit']['perpage']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			# Build for json construct
			$collectData['collect']['categories_json_data'] = array();
			if (isset($collectData['collect']['categories_data']['kategori'])) {
				if (is_array($collectData['collect']['categories_data']['kategori']) && (count($collectData['collect']['categories_data']['kategori']) > 0)) {
					foreach ($collectData['collect']['categories_data']['kategori'] as $kategori) {
						$kategori_index = base_permalink($kategori);
						$collectData['collect']['categories_json_data'][$kategori_index] = array();
						$collectData['collect']['categories_json_data'][$kategori_index]['categories'] = $kategori;
						// Buld from raw-data
					}
				}
			}
			if (isset($collectData['collect']['categories_data']['get_categories']['data'])) {
				if (is_array($collectData['collect']['categories_data']['get_categories']['data']) && (count($collectData['collect']['categories_data']['get_categories']['data']) > 0)) {
					foreach ($collectData['collect']['categories_data']['get_categories']['data'] as $dataKey => $dataVal) {
						$dataKey = base_permalink($dataKey);
						if (isset($collectData['collect']['categories_json_data'][$dataKey])) {
							$collectData['collect']['categories_json_data'][$dataKey]['subcategories'] = array();
							$collectData['collect']['categories_json_data'][$dataKey]['stories'] = array();
							if (is_array($dataVal) && (count($dataVal) > 0)) {
								foreach ($dataVal as $subKey => $subVal) {
									$collectData['collect']['categories_json_data'][$dataKey]['subcategories'][] = $subKey;
									if (is_array($subVal) && (count($subVal) > 0)) {
										foreach ($subVal as $storyKey => $storyVal) {
											$collectData['collect']['categories_json_data'][$dataKey]['stories'][$storyKey] = (isset($storyVal['judul']) ? $storyVal['judul'] : '');
										}
									}
								}
							}
						}
					}
				}
			}
			// Sort Asc?
			
		}
		
		
	}
	
	
	function read($stories_seq = 0, $pgnumber = 0) {
		$this->view($stories_seq, $pgnumber);
	}
	function view($stories_seq = '', $pgnumber = 0) {
		$collectData = array(
			'page'						=> 'stories-read',
			'collect'					=> array(
				'categories_stories'		=> array(),
				'categories_data'			=> array(),
			),
			'base_home_path'			=> 'home',
			'base_dashboard_path'		=> 'dashboard',
			'pgnumber'					=> (is_numeric($pgnumber) ? (int)$pgnumber : 0),
			'stories_seq'				=> (is_string($stories_seq) ? strtolower($stories_seq) : ''),
			'base_config'				=> $this->base_config,
			'title'						=> 'Read Story: ',
			'stories_type'				=> 'seq',
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		if (is_numeric($collectData['stories_seq'])) {
			$collectData['stories_seq'] = (int)$collectData['stories_seq'];
			$collectData['stories_type'] = 'seq';
		} else if (is_string()) {
			$collectData['stories_seq'] = base_permalink($collectData['stories_seq']);
			$collectData['stories_type'] = 'title';
		} else {
			$collectData['stories_seq'] = 0;
			$collectData['stories_type'] = 'seq';
		}
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'footer-twitter-url'		=> $this->take_configuration_item('footer-twitter-url'),
				'footer-instagram-url'		=> $this->take_configuration_item('footer-instagram-url'),
				'footer-facebook-url'		=> $this->take_configuration_item('footer-facebook-url'),
				'footer-line-id'			=> $this->take_configuration_item('footer-line-id'),
				'google-playstore-link'		=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'copyright',
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
				'telp-redaksi',
				'alamat-redaksi',
				'quote-coo',
				'quote-ceo',
				'home-description-head',
				'home-description-body',
				'home-stage-kiri',
				'home-stage-kanan',
				'home-stage-tengah',
				# HOME Categories
				'dongeng-pengantar-tidur',
				'seri-pengembangan-karakter',
				'cerita-berdasarkan-umur',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['input_query'] = '';
			$collectData['collect']['query_limit'] = array(
				'start'			=> 0,
				'perpage'		=> 16,
			);
		}
		//-----------------------------------------------------
		// Get Story Categories From Consumed Firebase
		//-----------------------------------------------------
		if (!$this->error) {
			try {
				$collectData['collect']['categories_data'] = $this->get_stories_categories_from_consumed_firebase();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			$collectData['stories_data_from_categories'] = array();
			# Build Categories Stories Data
			$collectData['collect']['stories_raw'] = array();
			if (isset($collectData['collect']['categories_data']['kategori'])) {
				if (is_array($collectData['collect']['categories_data']['kategori']) && (count($collectData['collect']['categories_data']['kategori']) > 0)) {
					foreach ($collectData['collect']['categories_data']['kategori'] as $catkey => $kategori) {
						$kategori_permalink = base_permalink($kategori);
						$collectData['collect']['categories_stories'][$kategori_permalink] = array(
							'kategori'				=> $kategori,
							'stories'				=> array(),
							'sub_kategori'			=> array(),
						);
						if (isset($collectData['collect']['categories_data']['sub-kategori'][$catkey])) {
							$collectData['collect']['categories_stories'][$kategori_permalink]['sub_kategori'] = $collectData['collect']['categories_data']['sub-kategori'][$catkey];
						}
						if (count($collectData['collect']['categories_stories'][$kategori_permalink]['sub_kategori']) > 0) {
							foreach ($collectData['collect']['categories_stories'][$kategori_permalink]['sub_kategori'] as $subVal) {
								if (isset($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal])) {
									if (is_array($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal]) && (count($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal]) > 0)) {
										$stories_i = 0;
										foreach ($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal] as $stories_seq => $stories_val) {
											if (isset($stories_val['judul']) && isset($stories_val['publish_status'])) {
												if (is_string($stories_val['judul']) && (strlen($stories_val['judul']) > 0)) {
													if (strtoupper($stories_val['publish_status']) === 'YES') {
														$collectData['collect']['categories_stories'][$kategori_permalink]['stories'][$stories_seq] = array(
															'stories_seq'			=> $stories_seq,
															'stories_data'			=> $stories_val,
														);
														if (strtolower($collectData['stories_type']) === strtolower('judul')) {
															if (base_permalink($stories_val['judul']) === $collectData['stories_seq']) {
																$collectData['stories_data_from_categories'] = $collectData['collect']['categories_stories'][$kategori_permalink]['stories'][$stories_seq];
															}
														} else {
															if ($stories_seq === $collectData['stories_seq']) {
																$collectData['stories_data_from_categories'] = $collectData['collect']['categories_stories'][$kategori_permalink]['stories'][$stories_seq];
															}
														}
														// Loop Stories Integer
														$stories_i++;
													}
												}
											}
										}
									}
								}
							}
						}
						
					}
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Cannot get kategori of categories from firebase api.";
			}
		}
		if (!$this->error) {
			if (!isset($collectData['stories_data_from_categories']['stories_seq'])) {
				$this->error = true;
				$this->error_msg[] = "Stories is not exists or maybe not published yest.";
			} else {
				$collectData['collect']['stories_data'] = $collectData['stories_data_from_categories']['stories_data'];
				$collectData['title'] .= (isset($collectData['collect']['stories_data']['judul']) ? $collectData['collect']['stories_data']['judul'] : '');
			}
		}
		
		//---------------------------
		// Debug
		//---------------------------
		/*
		echo "<pre>";
		if (!$this->error) {
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		
		
		
		
		
		
		if (!$this->error) {
			$this->load->view($collectData['base_home_path'] . '/home.php', $collectData);
			
		} else {
			$action_message = "";
			foreach ($this->error_msg as $error_msg) {
				$action_message .= "- {$error_msg}<br/>";
			}
			$this->session->set_flashdata('action_message', $action_message);
			redirect(base_url("{$collectData['base_home_path']}/home/error"));
			exit;
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
















