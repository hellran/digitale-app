<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	public $isAdmin = FALSE;
	public $isMerchant = FALSE;
	protected $DateObject;
	protected $email_vendor;
	public $error = FALSE, $error_msg = array();
	protected $firebase_credential;
	protected $base_config = array();
	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
		# Load Base Config
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		
		$this->load->model('adminpanel/Model_adminpanel', 'mod_adminpanel');
		$this->load->model('adminpanel/Model_firebase', 'mod_firebase');
		$this->load->model('adminpanel/Model_configuration', 'mod_config');
		$this->load->library('adminpanel/Lib_Rc4Crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('3', '4'))) {
			$this->isAdmin = TRUE;
		}
		if (!$this->isAdmin) {
			if (in_array($this->mod_adminpanel->localdata['account_role'], array('2', '5'))) {
				$this->isMerchant = TRUE;
			}
		}
		$this->DateObject = $this->mod_adminpanel->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'));
		$this->load->library('adminpanel/Lib_Firebase', '', 'firebase');
		//
		$this->email_vendor = $this->base_config['email_vendor'];
		$this->firebase_credential = $this->base_config['firebase_credential'];
		// Stories categories from consume
		$this->load->model('cli/Model_consume', 'mod_consume');
	}
	private function take_configuration_item($config_code) {
		$items = false;
		try {
			$config_items = $this->mod_config->get_config_item_by('code', $config_code);
		} catch (Exception $ex) {
			return false;
		}
		if (isset($config_items['data'])) {
			if (is_array($config_items['data']) && (count($config_items['data']) > 0)) {
				foreach ($config_items['data'] as $keval) {
					$items = $keval;
					break;
				}
			}
		}
		return $items;
	}
	private function get_stories_categories_from_consumed_firebase() {
		$collectData = array(
			'collect'			=> array(),
		);
		try {
			$collectData['consume_index'] = $this->mod_consume->get_consume_index_by('categories', 'stories_categories');
		} catch (Exception $ex) {
			$this->error = true;
			$this->error_msg[] = "Cannot get stories-categories from mod-consume with exception: {$ex->getMessage()}";
		}
		if (!$this->error) {
			if (isset($collectData['consume_index']->consume_last_seq)) {
				try {
					$collectData['categories_data'] = $this->mod_consume->get_consume_logs_by_table_seq($collectData['consume_index']->consume_table, $collectData['consume_index']->consume_last_seq);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get categories-data by mod-consume with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (isset($collectData['categories_data']->consume_data)) {
				return json_decode($collectData['categories_data']->consume_data, true);
			}
		}
		return false;
	}
	//------------------------------------------------------------------------------------------------------------
	public function get_stories_categories($type = 'javascript') {
		$collectData = array(
			'type'				=> (is_string($type) ? strtolower($type) : 'javascript'),
			'page'				=> 'digitale-home',
			'collect'			=> array(
				'categories_data'		=> array(),
			),
			'base_home_path'	=> 'home',
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'footer-twitter-url'		=> $this->take_configuration_item('footer-twitter-url'),
				'footer-instagram-url'		=> $this->take_configuration_item('footer-instagram-url'),
				'footer-facebook-url'		=> $this->take_configuration_item('footer-facebook-url'),
				'footer-line-id'			=> $this->take_configuration_item('footer-line-id'),
				'google-playstore-link'		=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'copyright',
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
				'telp-redaksi',
				'alamat-redaksi',
				'quote-coo',
				'quote-ceo',
				'home-description-head',
				'home-description-body',
				'home-stage-kiri',
				'home-stage-kanan',
				'home-stage-tengah',
				# HOME Categories
				'dongeng-pengantar-tidur',
				'seri-pengembangan-karakter',
				'cerita-berdasarkan-umur',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['input_query'] = '';
			$collectData['collect']['query_limit'] = array(
				'start'			=> 0,
				'perpage'		=> 16,
			);
		}
		//-----------------------------------------------------
		// Get Story Categories From Firebase
		//-----------------------------------------------------
		if (!$this->error) {
			try {
				//$collectData['collect']['categories_data'] = $this->firebase->get_categories_data($collectData['collect']['input_query'], $collectData['collect']['query_limit']['start'], $collectData['collect']['query_limit']['perpage']);
				$collectData['collect']['categories_data'] = $this->get_stories_categories_from_consumed_firebase();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			# Build for json construct
			$collectData['collect']['categories_json_data'] = array();
			if (isset($collectData['collect']['categories_data']['kategori'])) {
				if (is_array($collectData['collect']['categories_data']['kategori']) && (count($collectData['collect']['categories_data']['kategori']) > 0)) {
					foreach ($collectData['collect']['categories_data']['kategori'] as $kategori) {
						$kategori_index = base_permalink($kategori);
						$collectData['collect']['categories_json_data'][$kategori_index] = array();
						$collectData['collect']['categories_json_data'][$kategori_index]['categories'] = $kategori;
						// Buld from raw-data
					}
				}
			}
			if (isset($collectData['collect']['categories_data']['get_categories']['data'])) {
				if (is_array($collectData['collect']['categories_data']['get_categories']['data']) && (count($collectData['collect']['categories_data']['get_categories']['data']) > 0)) {
					foreach ($collectData['collect']['categories_data']['get_categories']['data'] as $dataKey => $dataVal) {
						$dataKey = base_permalink($dataKey);
						if (isset($collectData['collect']['categories_json_data'][$dataKey])) {
							$collectData['collect']['categories_json_data'][$dataKey]['subcategories'] = array();
							$collectData['collect']['categories_json_data'][$dataKey]['stories'] = array();
							if (is_array($dataVal) && (count($dataVal) > 0)) {
								foreach ($dataVal as $subKey => $subVal) {
									$collectData['collect']['categories_json_data'][$dataKey]['subcategories'][] = $subKey;
									if (is_array($subVal) && (count($subVal) > 0)) {
										foreach ($subVal as $storyKey => $storyVal) {
											$collectData['collect']['categories_json_data'][$dataKey]['stories'][$storyKey] = (isset($storyVal['judul']) ? $storyVal['judul'] : '');
										}
									}
								}
							}
						}
					}
				}
			}
			// Sort Asc?
			
		}
		
		if (!$this->error) {
			switch (strtolower($collectData['type'])) {
				case 'html':
					?>
					<div class="fullwidth-slick-carousel category-carousel">
						<?php
						$kategori_i = 0;
						if (isset($collectData['collect']['categories_data']['kategori'])) {
							if (is_array($collectData['collect']['categories_data']['kategori']) && (count($collectData['collect']['categories_data']['kategori']) > 0)) {
								foreach ($collectData['collect']['categories_data']['kategori'] as $katKey => $katVal) {
									if (is_string($katVal)) {
										?>
										<div class="fw-carousel-item">
											<div class="category-box-container">
												<a href="<?= base_url('home/categories/view/' . base_permalink($katVal));?>" class="category-box" data-background-image="<?= (isset($collectData['collect']['configuration'][base_permalink($katVal)]->config_value) ? $collectData['collect']['configuration'][base_permalink($katVal)]->config_value : base_url('templates/digitale/images/slider/kategori.jpg'));?>">
													<div class="category-box-content">
														<h3><?=$katVal;?></h3>
														<span>
															<?php
															if (isset($collectData['collect']['categories_data']['sub-kategori'][$katKey])) {
																if (is_array($collectData['collect']['categories_data']['sub-kategori'][$katKey])) {
																	echo count($collectData['collect']['categories_data']['sub-kategori'][$katKey]);
																} else {
																	echo "0";
																}
															}
															?>
															Kategori
														</span>
													</div>
													<span class="category-box-btn">Browse</span>
												</a>
											</div>
										</div>
										<?php
									}
									
									/*
									if (is_array($sub_kategori) && (count($sub_kategori) > 0)) {
										foreach ($sub_kategori as $kategori_data) {
											?>
											
											<?php
										}
									}
									*/
									$kategori_i += 1;
								}
							}
						}
						if ($kategori_i < 4) {
							for ($inject_i = 0; $inject_i < (4 - $kategori_i); $inject_i++) {
								if (isset($collectData['collect']['categories_data']['kategori'][$inject_i])) {
									?>
									<div class="fw-carousel-item">
										<div class="category-box-container half">
											<a href="<?= base_url('categories/view/' . base_permalink($collectData['collect']['categories_data']['kategori'][$inject_i]));?>" class="category-box" data-background-image="<?= (isset($collectData['collect']['configuration'][base_permalink($collectData['collect']['categories_data']['kategori'][$inject_i])]->config_value) ? $collectData['collect']['configuration'][base_permalink($collectData['collect']['categories_data']['kategori'][$inject_i])]->config_value : base_url('templates/digitale/images/slider/kategori.jpg'));?>">
												<div class="category-box-content">
													<h3><?=$collectData['collect']['categories_data']['kategori'][$inject_i];?></h3>
													<span>
														<?php
														if (isset($collectData['collect']['categories_data']['sub-kategori'][$inject_i])) {
															if (is_array($collectData['collect']['categories_data']['sub-kategori'][$inject_i])) {
																echo count($collectData['collect']['categories_data']['sub-kategori'][$inject_i]);
															} else {
																echo "0";
															}
														}
														?>
														Kategori
													</span>
												</div>
												<span class="category-box-btn">Browse</span>
											</a>
										</div>

										<div class="category-box-container half">
											<a href="<?= base_url('categories/view/' . base_permalink($collectData['collect']['categories_data']['kategori'][$inject_i]));?>" class="category-box" data-background-image="<?= (isset($collectData['collect']['configuration'][base_permalink($collectData['collect']['categories_data']['kategori'][$inject_i])]->config_value) ? $collectData['collect']['configuration'][base_permalink($collectData['collect']['categories_data']['kategori'][$inject_i])]->config_value : base_url('templates/digitale/images/slider/kategori.jpg'));?>">
												<div class="category-box-content">
													<h3><?=$collectData['collect']['categories_data']['kategori'][$inject_i];?></h3>
													<span>
														<?php
														if (isset($collectData['collect']['categories_data']['sub-kategori'][$inject_i])) {
															if (is_array($collectData['collect']['categories_data']['sub-kategori'][$inject_i])) {
																echo count($collectData['collect']['categories_data']['sub-kategori'][$inject_i]);
															} else {
																echo "0";
															}
														}
														?>
														Kategori
													</span>
												</div>
												<span class="category-box-btn">Browse</span>
											</a>
										</div>
									</div>
									<?php
								}
							}
						}
						?>
					</div>
					<?php
				break;
				case 'javascript':
				default:
					header('Content-Type: application/javascript');
					echo sprintf("var %s = %s;",
						"stories_categories",
						json_encode($collectData['collect']['categories_json_data'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
					);
				break;
			}
		} else {
			print_r($this->error_msg);
		}
	}
	function index() {
		$collectData = array(
			'page'				=> 'digitale-home',
			'collect'			=> array(
				'categories_data'		=> array(),
			),
			'base_home_path'	=> 'home',
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'footer-twitter-url'		=> $this->take_configuration_item('footer-twitter-url'),
				'footer-instagram-url'		=> $this->take_configuration_item('footer-instagram-url'),
				'footer-facebook-url'		=> $this->take_configuration_item('footer-facebook-url'),
				'footer-line-id'			=> $this->take_configuration_item('footer-line-id'),
				'google-playstore-link'		=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'copyright',
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
				'telp-redaksi',
				'alamat-redaksi',
				'quote-coo',
				'quote-ceo',
				'home-description-head',
				'home-description-body',
				'home-stage-kiri',
				'home-stage-kanan',
				'home-stage-tengah',
				# HOME Categories
				'dongeng-pengantar-tidur',
				'seri-pengembangan-karakter',
				'cerita-berdasarkan-umur',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['input_query'] = '';
			$collectData['collect']['query_limit'] = array(
				'start'			=> 0,
				'perpage'		=> 16,
			);
		}
		//-----------------------------------------------------
		// Get Latest Published Stories
		if (!$this->error) {
			$collectData['query_stories_params'] = array(
				'paging'		=> array(
					'start'				=> 0,
					'perpage'			=> base_config('rows-per-page'),
				),
				'sorting'		=> array(
					'order_by'			=> 'stories_update_datetime',
					'order_sort'		=> 'DESC',
				),
				'conditions'	=> array(
					'stories_status'	=> 'published',
				),
			);
			try {
				$collectData['collect']['stories_data'] = $this->mod_firebase->get_stories_params_by('data', 'collection', 'cerita', $collectData['query_stories_params']['conditions'], $collectData['query_stories_params']['paging'], $collectData['query_stories_params']['sorting']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error exception get my-stories: {$ex->getMessage()}";
			}
		}
		
		//-----------------------------------------------------
		// Get Story Categories From Firebase
		//-----------------------------------------------------
		if (!$this->error) {
			try {
				//$collectData['collect']['categories_data'] = $this->firebase->get_categories_data($collectData['collect']['input_query'], $collectData['collect']['query_limit']['start'], $collectData['collect']['query_limit']['perpage']);
				$collectData['collect']['categories_data'] = $this->get_stories_categories_from_consumed_firebase();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception: {$ex->getMessage()}";
			}
		}
		//------------------------
		// Debug
		/*
		echo "<pre>";
		if (!$this->error) {
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//------------------------
		

		
		
		
		
		
		
		
		
		
		
		if (!$this->error) {
			$this->load->view($collectData['base_home_path'] . '/home.php', $collectData);
			
		} else {
			$action_message = "";
			foreach ($this->error_msg as $error_msg) {
				$action_message .= "- {$error_msg}<br/>";
			}
			$this->session->set_flashdata('action_message', $action_message);
			redirect(base_url("{$collectData['base_home_path']}/home/error"));
			exit;
		}
	}
	
	function error() {
		$collectData = array(
			'page'					=> 'home-error',
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
		);
		$this->load->view($collectData['base_home_path'] . '/home.php', $collectData);
	}
	function logout() {
		$collectData = array(
			'page'					=> 'form-logout',
			'collect'				=> array(),
			'base_home_path'		=> 'home',
			'base_dashboard_path'	=> 'dashboard',
			'title'					=> 'Home: Logout',
		);
		$this->load->view('home/home.php', $collectData);
	}
}
















