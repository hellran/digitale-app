<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {
	public $isAdmin = FALSE;
	public $isMerchant = FALSE;
	protected $DateObject;
	protected $email_vendor;
	public $error = FALSE, $error_msg = array();
	protected $firebase_credential;
	protected $base_config = array();
	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('form_validation');
		# Load Base Config
		$this->load->config('base/base_config');
		$this->base_config = $this->config->item('base_config');
		
		$this->load->model('adminpanel/Model_adminpanel', 'mod_adminpanel');
		$this->load->model('adminpanel/Model_firebase', 'mod_firebase');
		$this->load->model('adminpanel/Model_configuration', 'mod_config');
		$this->load->library('adminpanel/Lib_Rc4Crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		if (in_array($this->mod_adminpanel->localdata['account_role'], array('3', '4'))) {
			$this->isAdmin = TRUE;
		}
		if (!$this->isAdmin) {
			if (in_array($this->mod_adminpanel->localdata['account_role'], array('2', '5'))) {
				$this->isMerchant = TRUE;
			}
		}
		$this->DateObject = $this->mod_adminpanel->create_dateobject("Asia/Bangkok", 'Y-m-d', date('Y-m-d'));
		$this->load->library('adminpanel/Lib_Firebase', '', 'firebase');
		// Load Firebase credential
		$this->email_vendor = $this->base_config['email_vendor'];
		$this->firebase_credential = $this->base_config['firebase_credential'];
		// Stories categories from consume
		$this->load->model('cli/Model_consume', 'mod_consume');
	}
	private function take_configuration_item($config_code) {
		$items = false;
		try {
			$config_items = $this->mod_config->get_config_item_by('code', $config_code);
		} catch (Exception $ex) {
			return false;
		}
		if (isset($config_items['data'])) {
			if (is_array($config_items['data']) && (count($config_items['data']) > 0)) {
				foreach ($config_items['data'] as $keval) {
					$items = $keval;
					break;
				}
			}
		}
		return $items;
	}
	private function get_stories_categories_from_consumed_firebase() {
		$collectData = array(
			'collect'			=> array(),
		);
		try {
			$collectData['consume_index'] = $this->mod_consume->get_consume_index_by('categories', 'stories_categories');
		} catch (Exception $ex) {
			$this->error = true;
			$this->error_msg[] = "Cannot get stories-categories from mod-consume with exception: {$ex->getMessage()}";
		}
		if (!$this->error) {
			if (isset($collectData['consume_index']->consume_last_seq)) {
				try {
					$collectData['categories_data'] = $this->mod_consume->get_consume_logs_by_table_seq($collectData['consume_index']->consume_table, $collectData['consume_index']->consume_last_seq);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot get categories-data by mod-consume with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			if (isset($collectData['categories_data']->consume_data)) {
				return json_decode($collectData['categories_data']->consume_data, true);
			}
		}
		return false;
	}
	//------------------------------------------------------------------------------------------------------------
	public function get_stories_categories($type = 'javascript') {
		$collectData = array(
			'type'				=> (is_string($type) ? strtolower($type) : 'javascript'),
			'page'				=> 'digitale-home',
			'collect'			=> array(
				'categories_data'		=> array(),
			),
			'base_home_path'	=> 'home',
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'footer-twitter-url'		=> $this->take_configuration_item('footer-twitter-url'),
				'footer-instagram-url'		=> $this->take_configuration_item('footer-instagram-url'),
				'footer-facebook-url'		=> $this->take_configuration_item('footer-facebook-url'),
				'footer-line-id'			=> $this->take_configuration_item('footer-line-id'),
				'google-playstore-link'		=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'copyright',
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
				'telp-redaksi',
				'alamat-redaksi',
				'quote-coo',
				'quote-ceo',
				'home-description-head',
				'home-description-body',
				'home-stage-kiri',
				'home-stage-kanan',
				'home-stage-tengah',
				# HOME Categories
				'dongeng-pengantar-tidur',
				'seri-pengembangan-karakter',
				'cerita-berdasarkan-umur',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['input_query'] = '';
			$collectData['collect']['query_limit'] = array(
				'start'			=> 0,
				'perpage'		=> 16,
			);
		}
		//-----------------------------------------------------
		// Get Story Categories From Firebase
		//-----------------------------------------------------
		if (!$this->error) {
			try {
				//$collectData['collect']['categories_data'] = $this->firebase->get_categories_data($collectData['collect']['input_query'], $collectData['collect']['query_limit']['start'], $collectData['collect']['query_limit']['perpage']);
				$collectData['collect']['categories_data'] = $this->get_stories_categories_from_consumed_firebase();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			# Build for json construct
			$collectData['collect']['categories_json_data'] = array();
			if (isset($collectData['collect']['categories_data']['kategori'])) {
				if (is_array($collectData['collect']['categories_data']['kategori']) && (count($collectData['collect']['categories_data']['kategori']) > 0)) {
					foreach ($collectData['collect']['categories_data']['kategori'] as $kategori) {
						$kategori_index = base_permalink($kategori);
						$collectData['collect']['categories_json_data'][$kategori_index] = array();
						$collectData['collect']['categories_json_data'][$kategori_index]['categories'] = $kategori;
						// Buld from raw-data
					}
				}
			}
			if (isset($collectData['collect']['categories_data']['get_categories']['data'])) {
				if (is_array($collectData['collect']['categories_data']['get_categories']['data']) && (count($collectData['collect']['categories_data']['get_categories']['data']) > 0)) {
					foreach ($collectData['collect']['categories_data']['get_categories']['data'] as $dataKey => $dataVal) {
						$dataKey = base_permalink($dataKey);
						if (isset($collectData['collect']['categories_json_data'][$dataKey])) {
							$collectData['collect']['categories_json_data'][$dataKey]['subcategories'] = array();
							$collectData['collect']['categories_json_data'][$dataKey]['stories'] = array();
							if (is_array($dataVal) && (count($dataVal) > 0)) {
								foreach ($dataVal as $subKey => $subVal) {
									$collectData['collect']['categories_json_data'][$dataKey]['subcategories'][] = $subKey;
									if (is_array($subVal) && (count($subVal) > 0)) {
										foreach ($subVal as $storyKey => $storyVal) {
											$collectData['collect']['categories_json_data'][$dataKey]['stories'][$storyKey] = (isset($storyVal['judul']) ? $storyVal['judul'] : '');
										}
									}
								}
							}
						}
					}
				}
			}
			// Sort Asc?
			
		}
		
		if (!$this->error) {
			switch (strtolower($collectData['type'])) {
				case 'html':
					?>
					<div class="fullwidth-slick-carousel category-carousel">
						<?php
						$kategori_i = 0;
						if (isset($collectData['collect']['categories_data']['kategori'])) {
							if (is_array($collectData['collect']['categories_data']['kategori']) && (count($collectData['collect']['categories_data']['kategori']) > 0)) {
								foreach ($collectData['collect']['categories_data']['kategori'] as $katKey => $katVal) {
									if (is_string($katVal)) {
										?>
										<div class="fw-carousel-item">
											<div class="category-box-container">
												<a href="<?= base_url('categories/view/' . base_permalink($katVal));?>" class="category-box" data-background-image="<?= (isset($collectData['collect']['configuration'][base_permalink($katVal)]->config_value) ? $collectData['collect']['configuration'][base_permalink($katVal)]->config_value : base_url('templates/digitale/images/slider/kategori.jpg'));?>">
													<div class="category-box-content">
														<h3><?=$katVal;?></h3>
														<span>
															<?php
															if (isset($collectData['collect']['categories_data']['sub-kategori'][$katKey])) {
																if (is_array($collectData['collect']['categories_data']['sub-kategori'][$katKey])) {
																	echo count($collectData['collect']['categories_data']['sub-kategori'][$katKey]);
																} else {
																	echo "0";
																}
															}
															?>
															Kategori
														</span>
													</div>
													<span class="category-box-btn">Browse</span>
												</a>
											</div>
										</div>
										<?php
									}

									$kategori_i += 1;
								}
							}
						}
						?>
					</div>
					<?php
				break;
				case 'javascript':
				default:
					header('Content-Type: application/javascript');
					echo sprintf("var %s = %s;",
						"stories_categories",
						json_encode($collectData['collect']['categories_json_data'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
					);
				break;
			}
		} else {
			print_r($this->error_msg);
		}
	}
	function subview($categories_name = '', $subkategori = '', $pgnumber = 0) {
		$collectData = array(
			'page'						=> 'categories-subview',
			'collect'					=> array(
				'collected_stories'			=> array(),
				'categories_stories'		=> array(),
				'categories_data'			=> array(),
			),
			'base_home_path'			=> 'home',
			'base_dashboard_path'		=> 'dashboard',
			'pgnumber'					=> (is_numeric($pgnumber) ? (int)$pgnumber : 0),
			'categories_name'			=> (is_string($categories_name) ? strtolower($categories_name) : ''),
			'subkategori'				=> (is_string($subkategori) ? strtolower($subkategori) : ''),
			'base_config'				=> $this->base_config,
			'title'						=> 'Categories: ',
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		$collectData['categories_name'] = base_permalink($collectData['categories_name']);
		$collectData['subkategori'] = base_permalink($collectData['subkategori']);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'footer-twitter-url'		=> $this->take_configuration_item('footer-twitter-url'),
				'footer-instagram-url'		=> $this->take_configuration_item('footer-instagram-url'),
				'footer-facebook-url'		=> $this->take_configuration_item('footer-facebook-url'),
				'footer-line-id'			=> $this->take_configuration_item('footer-line-id'),
				'google-playstore-link'		=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'copyright',
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
				'telp-redaksi',
				'alamat-redaksi',
				'quote-coo',
				'quote-ceo',
				'home-description-head',
				'home-description-body',
				'home-stage-kiri',
				'home-stage-kanan',
				'home-stage-tengah',
				# HOME Categories
				'dongeng-pengantar-tidur',
				'seri-pengembangan-karakter',
				'cerita-berdasarkan-umur',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['input_query'] = '';
			$collectData['collect']['query_limit'] = array(
				'start'			=> 0,
				'perpage'		=> 16,
			);
		}
		//-----------------------------------------------------
		// Get Story Categories From Firebase
		//-----------------------------------------------------
		if (!$this->error) {
			$collectData['collect']['subcategories_collection'] = array(
				'count'				=> 0,
				'data'				=> array(),
			);
			try {
				//$collectData['collect']['categories_data'] = $this->firebase->get_categories_data($collectData['collect']['input_query'], $collectData['collect']['query_limit']['start'], $collectData['collect']['query_limit']['perpage']);
				$collectData['collect']['categories_data'] = $this->get_stories_categories_from_consumed_firebase();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			# Build Categories Stories Data
			if (isset($collectData['collect']['categories_data']['kategori'])) {
				if (is_array($collectData['collect']['categories_data']['kategori']) && (count($collectData['collect']['categories_data']['kategori']) > 0)) {
					foreach ($collectData['collect']['categories_data']['kategori'] as $catkey => $kategori) {
						$kategori_permalink = base_permalink($kategori);
						$collectData['collect']['categories_stories'][$kategori_permalink] = array(
							'kategori'				=> $kategori,
							'stories'				=> array(),
							'sub_kategori'			=> array(),
						);
						if (isset($collectData['collect']['categories_data']['sub-kategori'][$catkey])) {
							$collectData['collect']['categories_stories'][$kategori_permalink]['sub_kategori'] = $collectData['collect']['categories_data']['sub-kategori'][$catkey];
						}
						if (count($collectData['collect']['categories_stories'][$kategori_permalink]['sub_kategori']) > 0) {
							foreach ($collectData['collect']['categories_stories'][$kategori_permalink]['sub_kategori'] as $subVal) {
								if (isset($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal])) {
									if (is_array($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal]) && (count($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal]) > 0)) {
										$stories_i = 0;
										foreach ($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal] as $stories_seq => $stories_val) {
											if (isset($stories_val['judul']) && isset($stories_val['publish_status'])) {
												if (is_string($stories_val['judul']) && (strlen($stories_val['judul']) > 0)) {
													if (strtoupper($stories_val['publish_status']) === 'YES') {
														$collectData['collect']['categories_stories'][$kategori_permalink]['stories'][$stories_seq] = array(
															'stories_seq'			=> $stories_seq,
															'stories_data'			=> $stories_val,
														);
														$collectData['collect']['collected_stories'][$stories_seq] = array(
															'stories_seq'			=> $stories_seq,
															'stories_data'			=> array(
																'judul'						=> (isset($stories_val['judul']) ? $stories_val['judul'] : ''),
																'thumbnail'					=> (isset($stories_val['thumbnail']) ? $stories_val['thumbnail'] : ''),
																'publisher'					=> (isset($stories_val['publisher']) ? $stories_val['publisher'] : ''),
																'penulis'					=> (isset($stories_val['penulis']) ? $stories_val['penulis'] : ''),
															),
															'subkategori'			=> array(),
															'subcategories'			=> array(),
														);
														if (isset($stories_val['kategori']['data'])) {
															if (is_array($stories_val['kategori']['data']) && (count($stories_val['kategori']['data']) > 0)) {
																foreach ($stories_val['kategori']['data'] as $data_kategori) {
																	if (isset($data_kategori['title'])) {
																		$collectData['collect']['collected_stories'][$stories_seq]['subcategories'][base_permalink($data_kategori['title'])] = array(
																			'categories'			=> $data_kategori['title'],
																			'subtitles'				=> (isset($data_kategori['subtitles']) ? $data_kategori['subtitles'] : array()),
																		);
																	}
																	if (is_array($collectData['collect']['collected_stories'][$stories_seq]['subcategories'][base_permalink($data_kategori['title'])]['subtitles']) && (count($collectData['collect']['collected_stories'][$stories_seq]['subcategories'][base_permalink($data_kategori['title'])]['subtitles']) > 0)) {
																		foreach ($collectData['collect']['collected_stories'][$stories_seq]['subcategories'][base_permalink($data_kategori['title'])]['subtitles'] as $categories_subtitles) {
																			$collectData['collect']['collected_stories'][$stories_seq]['subkategori'][] = array(
																				'permalink'			=> base_permalink($categories_subtitles),
																				'name'				=> $categories_subtitles,
																			);
																		}
																	}
																	
																}
															}
														}
														$stories_i++;
													}
												}
											}
										}
									}
								}
							}
						}
						
					}
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Cannot get kategori of categories from firebase api.";
			}
		}
		if (!$this->error) {
			$permalink_categories_name = base_permalink($collectData['categories_name']);
			$permalink_categories_sub = base_permalink($collectData['subkategori']);
			if (!isset($collectData['collect']['categories_stories'][$permalink_categories_name])) {
				$this->error = true;
				$this->error_msg[] = "Categories name not exists on firebase database.";
			} else {
				$collectData['title'] .= $collectData['collect']['categories_stories'][$permalink_categories_name]['kategori'];
				if (!isset($collectData['collect']['categories_stories'][$permalink_categories_name]['stories'])) {
					$this->error = true;
					$this->error_msg[] = "There is no stories indexed collection.";
				} else {
					if (!isset($collectData['collect']['categories_stories'][$permalink_categories_name]['sub_kategori'])) {
						$this->error = true;
						$this->error_msg[] = "There is no sub-kategori of permalink categories.";
					} else {
						if (is_array($collectData['collect']['categories_stories'][$permalink_categories_name]['sub_kategori']) && (count($collectData['collect']['categories_stories'][$permalink_categories_name]['sub_kategori']) > 0)) {
							foreach ($collectData['collect']['categories_stories'][$permalink_categories_name]['sub_kategori'] as $sub_kategori) {
								if (base_permalink($sub_kategori) === $collectData['subkategori']) {
									$collectData['collect']['subcategories_collection']['name'] = $sub_kategori;
									$collectData['collect']['subcategories_collection']['permalink'] = base_permalink($sub_kategori);
									break;
								}
							}
						}
					}
				}
			}
		}
		if (!$this->error) {
			if (!isset($collectData['collect']['collected_stories'])) {
				$this->error = true;
				$this->error_msg[] = "No stories is collected.";
			} else {
				if (is_array($collectData['collect']['collected_stories']) && (count($collectData['collect']['collected_stories']) > 0)) {
					foreach ($collectData['collect']['collected_stories'] as $collected_stories) {
						if (isset($collected_stories['subkategori'])) {
							if (is_array($collected_stories['subkategori'])) {
								if (isset($collectData['collect']['subcategories_collection']['permalink'])) {
									if (array_search($collectData['collect']['subcategories_collection']['permalink'], array_column($collected_stories['subkategori'], 'permalink')) !== false) {
										$collectData['collect']['subcategories_collection']['data'][$collected_stories['stories_seq']] = $collected_stories;
									}
								}
							}
						}
					}
				}
				krsort($collectData['collect']['subcategories_collection']['data'], SORT_NUMERIC);
				$collectData['collect']['subcategories_collection']['count'] = count($collectData['collect']['subcategories_collection']['data']);
				// Make pagination start and per-page
				if ((int)$collectData['collect']['subcategories_collection']['count'] > 0) {
					$collectData['pagination'] = array(
						'page'		=> (isset($collectData['pgnumber']) ? $collectData['pgnumber'] : 1),
						'start'		=> 0,
					);
					$collectData['pagination']['page'] = (is_numeric($collectData['pagination']['page']) ? sprintf("%d", $collectData['pagination']['page']) : 1);
					if ($collectData['pagination']['page'] > 0) {
						$collectData['pagination']['page'] = (int)$collectData['pagination']['page'];
					} else {
						$collectData['pagination']['page'] = 1;
					}
					$collectData['pagination']['start'] = $this->mod_adminpanel->get_pagination_start($collectData['pagination']['page'], base_config('rows_per_page'), $collectData['collect']['subcategories_collection']['count']);
				} else {
					$collectData['pagination'] = array(
						'page'		=> 1,
						'start'		=> 0,
					);
				}
				// Get stories by paging
				$collectData['get_paging_params'] = array(
					'start'			=> $collectData['pagination']['start'], 
					'perpage'		=> base_config('rows_per_page'),
				);
				try {
					$collectData['collect']['stories_data'] = array_slice($collectData['collect']['subcategories_collection']['data'], $collectData['get_paging_params']['start'], $collectData['get_paging_params']['perpage']);
				} catch (Exception $ex) {
					$this->error = true;
					$this->error_msg[] = "Cannot array slice of all stories collection with exception: {$ex->getMessage()}.";
				}
			}
		}
		if (!$this->error) {
			try {
				$collectData['collect']['pagination'] = $this->mod_adminpanel->generate_pagination_for('home', base_url("{$collectData['base_home_path']}/categories/subview/{$collectData['categories_name']}/{$collectData['subkategori']}"), $collectData['pagination']['page'], base_config('rows_per_page'), $collectData['collect']['subcategories_collection']['count'], $collectData['pagination']['start']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error while get pagination with exception: {$ex->getMessage()}";
			}
		}
		
		
		
		
		
		
		
		
		
		//------------------------
		// Debug
		/*
		echo "<pre>";
		if (!$this->error) {
			//unset($collectData['collect']['categories_stories'][$collectData['categories_name']]['stories']);
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//------------------------
		
		if (!$this->error) {
			$this->load->view($collectData['base_home_path'] . '/home.php', $collectData);
			
		} else {
			$action_message = "";
			foreach ($this->error_msg as $error_msg) {
				$action_message .= "- {$error_msg}<br/>";
			}
			$this->session->set_flashdata('action_message', $action_message);
			redirect(base_url("{$collectData['base_home_path']}/home/error"));
			exit;
		}
	}
	function view($categories_name = '', $pgnumber = 0) {
		$collectData = array(
			'page'						=> 'categories-index',
			'collect'					=> array(
				'categories_stories'		=> array(),
				'categories_data'			=> array(),
			),
			'base_home_path'			=> 'home',
			'base_dashboard_path'		=> 'dashboard',
			'pgnumber'					=> (is_numeric($pgnumber) ? (int)$pgnumber : 0),
			'categories_name'			=> (is_string($categories_name) ? strtolower($categories_name) : ''),
			'base_config'				=> $this->base_config,
			'title'						=> 'Categories: ',
		);
		$collectData['firebase_credential'] = $this->firebase_credential;
		$collectData['categories_name'] = base_permalink($collectData['categories_name']);
		if (!$this->error) {
			$collectData['collect']['ggdata'] = $this->mod_adminpanel->userdata;
			$collectData['collect']['userdata'] = $this->mod_adminpanel->localdata;
			$collectData['collect']['datetime'] = $this->mod_adminpanel->get_datetime();
		}
		if (!$this->error) {
			$collectData['collect']['configuration'] = array(
				'footer-twitter-url'		=> $this->take_configuration_item('footer-twitter-url'),
				'footer-instagram-url'		=> $this->take_configuration_item('footer-instagram-url'),
				'footer-facebook-url'		=> $this->take_configuration_item('footer-facebook-url'),
				'footer-line-id'			=> $this->take_configuration_item('footer-line-id'),
				'google-playstore-link'		=> $this->take_configuration_item('google-playstore-link'),
			);
			$take_config_items = array(
				'copyright',
				'nama-pendiri',
				'email-pendiri',
				'email-redaksi',
				'telp-redaksi',
				'alamat-redaksi',
				'quote-coo',
				'quote-ceo',
				'home-description-head',
				'home-description-body',
				'home-stage-kiri',
				'home-stage-kanan',
				'home-stage-tengah',
				# HOME Categories
				'dongeng-pengantar-tidur',
				'seri-pengembangan-karakter',
				'cerita-berdasarkan-umur',
			);
			foreach ($take_config_items as $keval) {
				$collectData['collect']['configuration'][$keval] = $this->take_configuration_item($keval);
			}
			$collectData['collect']['input_query'] = '';
			$collectData['collect']['query_limit'] = array(
				'start'			=> 0,
				'perpage'		=> 16,
			);
		}
		//-----------------------------------------------------
		// Get Latest Published Stories
		if (!$this->error) {
			$collectData['query_stories_params'] = array(
				'paging'		=> array(
					'start'				=> 0,
					'perpage'			=> base_config('rows-per-page'),
				),
				'sorting'		=> array(
					'order_by'			=> 'stories_update_datetime',
					'order_sort'		=> 'DESC',
				),
				'conditions'	=> array(
					'stories_status'	=> 'published',
				),
			);
		}
		
		//-----------------------------------------------------
		// Get Story Categories From Firebase
		//-----------------------------------------------------
		if (!$this->error) {
			try {
				//$collectData['collect']['categories_data'] = $this->firebase->get_categories_data($collectData['collect']['input_query'], $collectData['collect']['query_limit']['start'], $collectData['collect']['query_limit']['perpage']);
				$collectData['collect']['categories_data'] = $this->get_stories_categories_from_consumed_firebase();
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Get categories data with error-exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			# Build Categories Stories Data
			$collectData['collect']['stories_raw'] = array();
			if (isset($collectData['collect']['categories_data']['kategori'])) {
				if (is_array($collectData['collect']['categories_data']['kategori']) && (count($collectData['collect']['categories_data']['kategori']) > 0)) {
					foreach ($collectData['collect']['categories_data']['kategori'] as $catkey => $kategori) {
						$kategori_permalink = base_permalink($kategori);
						$collectData['collect']['categories_stories'][$kategori_permalink] = array(
							'kategori'				=> $kategori,
							'stories'				=> array(),
							'sub_kategori'			=> array(),
						);
						if (isset($collectData['collect']['categories_data']['sub-kategori'][$catkey])) {
							$collectData['collect']['categories_stories'][$kategori_permalink]['sub_kategori'] = $collectData['collect']['categories_data']['sub-kategori'][$catkey];
						}
						if (count($collectData['collect']['categories_stories'][$kategori_permalink]['sub_kategori']) > 0) {
							foreach ($collectData['collect']['categories_stories'][$kategori_permalink]['sub_kategori'] as $subVal) {
								if (isset($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal])) {
									if (is_array($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal]) && (count($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal]) > 0)) {
										$stories_i = 0;
										foreach ($collectData['collect']['categories_data']['get_categories']['data'][$kategori][$subVal] as $stories_seq => $stories_val) {
											if (isset($stories_val['judul']) && isset($stories_val['publish_status'])) {
												if (is_string($stories_val['judul']) && (strlen($stories_val['judul']) > 0)) {
													if (strtoupper($stories_val['publish_status']) === 'YES') {
														$collectData['collect']['categories_stories'][$kategori_permalink]['stories'][$stories_seq] = array(
															'stories_seq'			=> $stories_seq,
															'stories_data'			=> $stories_val,
														);
														$stories_i++;
													}
												}
											}
										}
									}
								}
							}
						}
						
					}
				}
			} else {
				$this->error = true;
				$this->error_msg[] = "Cannot get kategori of categories from firebase api.";
			}
		}
		if (!$this->error) {
			$permalink_categories_name = base_permalink($collectData['categories_name']);
			if (!isset($collectData['collect']['categories_stories'][$permalink_categories_name])) {
				$this->error = true;
				$this->error_msg[] = "Categories name not exists on firebase database.";
			} else {
				$collectData['title'] .= $collectData['collect']['categories_stories'][$permalink_categories_name]['kategori'];
				if (!isset($collectData['collect']['categories_stories'][$permalink_categories_name]['stories'])) {
					$this->error = true;
					$this->error_msg[] = "There is no stories index collection.";
				} else {
					$collectData['collect']['categories_stories']['count'] = count($collectData['collect']['categories_stories'][$permalink_categories_name]['stories']);
					krsort($collectData['collect']['categories_stories'][$permalink_categories_name]['stories'], SORT_NUMERIC);
					// Make pagination start and per-page
					if ((int)$collectData['collect']['categories_stories']['count'] > 0) {
						$collectData['pagination'] = array(
							'page'		=> (isset($collectData['pgnumber']) ? $collectData['pgnumber'] : 1),
							'start'		=> 0,
						);
						$collectData['pagination']['page'] = (is_numeric($collectData['pagination']['page']) ? sprintf("%d", $collectData['pagination']['page']) : 1);
						if ($collectData['pagination']['page'] > 0) {
							$collectData['pagination']['page'] = (int)$collectData['pagination']['page'];
						} else {
							$collectData['pagination']['page'] = 1;
						}
						$collectData['pagination']['start'] = $this->mod_adminpanel->get_pagination_start($collectData['pagination']['page'], base_config('rows_per_page'), $collectData['collect']['categories_stories']['count']);
					} else {
						$collectData['pagination'] = array(
							'page'		=> 1,
							'start'		=> 0,
						);
					}
					// Get stories by paging
					$collectData['get_paging_params'] = array(
						'start'			=> $collectData['pagination']['start'], 
						'perpage'		=> base_config('rows_per_page'),
					);
					try {
						$collectData['collect']['stories_data'] = array_slice($collectData['collect']['categories_stories'][$permalink_categories_name]['stories'], $collectData['get_paging_params']['start'], $collectData['get_paging_params']['perpage']);
					} catch (Exception $ex) {
						$this->error = true;
						$this->error_msg[] = "Cannot array slice of all stories collection with exception: {$ex->getMessage()}.";
					}
				}
			}
		}
		if (!$this->error) {
			try {
				$collectData['collect']['pagination'] = $this->mod_adminpanel->generate_pagination_for('home', base_url("{$collectData['base_home_path']}/categories/view/{$collectData['categories_name']}"), $collectData['pagination']['page'], base_config('rows_per_page'), $collectData['collect']['categories_stories']['count'], $collectData['pagination']['start']);
			} catch (Exception $ex) {
				$this->error = true;
				$this->error_msg[] = "Error while get pagination with exception: {$ex->getMessage()}";
			}
		}
		if (!$this->error) {
			if (!isset($collectData['collect']['categories_stories'][$collectData['categories_name']]['sub_kategori'])) {
				$this->error = true;
				$this->error_msg[] = "No sub-categories of categories";
			} else {
				if (is_array($collectData['collect']['categories_stories'][$collectData['categories_name']]['sub_kategori']) && (count($collectData['collect']['categories_stories'][$collectData['categories_name']]['sub_kategori']) > 0)) {
					foreach ($collectData['collect']['categories_stories'][$collectData['categories_name']]['sub_kategori'] as $sub_kategori) {
						
					}
				}
			}
		}
		//--------------------------------------------------------
		// Debug
		/*
		echo "<pre>";
		if (!$this->error) {
			//unset($collectData['collect']['categories_stories'][$collectData['categories_name']]['stories']);
			print_r($collectData);
		} else {
			print_r($this->error_msg);
		}
		exit;
		*/
		//--------------------------------------------------------
	
		

		
		
		
		
		
		
		
		
		
		
		if (!$this->error) {
			$this->load->view($collectData['base_home_path'] . '/home.php', $collectData);
			
		} else {
			$action_message = "";
			foreach ($this->error_msg as $error_msg) {
				$action_message .= "- {$error_msg}<br/>";
			}
			$this->session->set_flashdata('action_message', $action_message);
			redirect(base_url("{$collectData['base_home_path']}/home/error"));
			exit;
		}
	}
	
	
	
	
	
}
















