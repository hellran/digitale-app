<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }


if (!function_exists('base_permalink')) {
	function base_permalink($url) {
		$url = strtolower($url);
		$url = preg_replace('/&.+?;/', '', $url);
		$url = preg_replace('/\s+/', '_', $url);
		$url = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '_', $url);
		$url = preg_replace('|%|', '_', $url);
		$url = preg_replace('/&#?[a-z0-9]+;/i', '', $url);
		$url = preg_replace('/[^%A-Za-z0-9 \_\-]/', '_', $url);
		$url = preg_replace('|_+|', '-', $url);
		$url = preg_replace('|-+|', '-', $url);
		$url = trim($url, '-');
		$url = (strlen($url) > 128) ? substr($url, 0, 128) : $url;
		return $url;
	}
}
if (!function_exists('base_config')) {
	function base_config($config_name = '') {
		$config_name = base_permalink($config_name);
		$CI = &get_instance();
		$CI->load->config('base/base_config');
		$base_config = $CI->config->item('base_config');
		if (isset($base_config[$config_name])) {
			return $base_config[$config_name];
		}
		return "";
	}
}
if (!function_exists('form_close')) {
	function form_close() {
		return '</form>';
	}
}
if (!function_exists('base_safe_text')) {
	function base_safe_text($text, $length, $allow_nl = false) {
		$text = htmlspecialchars($text, ENT_QUOTES);
		$text = trim(chop($text));
		$text = $allow_nl ? $text : preg_replace("/[\r|\n]/", "", $text);
		$text = substr($text, 0, $length);
		return $text;
	}
}
if (!function_exists('callback_date_valid_ymd')) {
	function callback_date_valid_ymd($date) {
		$day = (int) substr($date, 8, 2);
		$month = (int) substr($date, 5, 2);
		$year = (int) substr($date, 0, 4);
		return checkdate($month, $day, $year);
	}
}
//====================================================================================
if (!function_exists('set_sidebar_active')) {
	function set_sidebar_active($link_name, $param_name = 'service', $match = null) {
		if (!isset($match)) {
			$AltoRouter = new AltoRouter();
			$CI = &get_instance();
			$CI->load->config('base_dashboard');
			$base_dashboard = $CI->config->item('base_dashboard');
			if (isset($base_dashboard['altorouter']['mapping'])) {
				if (is_array($base_dashboard['altorouter']['mapping']) && (count($base_dashboard['altorouter']['mapping']) > 0)) {
					foreach ($base_dashboard['altorouter']['mapping'] as $val) {
						if (is_array($val) && (count($val) > 3)) {
							$AltoRouter->map($val[0], $val[1], $val[2], $val[3]);
						}
					}
				}
			}
			$match = $AltoRouter->match();
		}
		$param_name = (is_string($param_name) ? strtolower($param_name) : 'service');
		switch ($param_name) {
			case 'version':
				$link_name = (is_string($link_name) ? $link_name : 'dashboard');
				if (isset($match['params'][$param_name])) {
					if (is_string($match['params'][$param_name])) {
						if (strtolower($match['params'][$param_name]) === strtolower($link_name)) {
							return "active";
						}
					}
				}
			break;
			case 'service':
				$link_name = (is_string($link_name) ? $link_name : 'dashboard');
				if (isset($match['params'][$param_name])) {
					if (is_string($match['params'][$param_name])) {
						if (strtolower($match['params'][$param_name]) === strtolower($link_name)) {
							return "active";
						}
					}
				}
			break;
			case 'method':
				$link_name = (is_array($link_name) ? $link_name : array('service' => 'dashboard', 'method' => 'index'));
				if (isset($link_name['service']) && isset($link_name['method'])) {
					if (isset($match['params']['service']) && isset($match['params']['method'])) {
						if (is_string($match['params']['service']) && is_string($match['params']['method'])) {
							if (($match['params']['service'] == $link_name['service']) && ($match['params']['method'] == $link_name['method'])) {
								return 'active';
							}
						}
					}
				}
			break;
			case 'methodsub':
				$link_name = (is_array($link_name) ? $link_name : array('service' => 'dashboard', 'method' => 'index'));
				if (isset($link_name['service']) && isset($link_name['method'])) {
					if (isset($match['params']['service']) && isset($match['params']['method'])) {
						if (is_string($match['params']['service']) && is_string($match['params']['method'])) {
							if (($match['params']['service'] == $link_name['service']) && (strpos($match['params']['method'], $link_name['method']) !== FALSE)) {
								return 'active';
							}
						}
					}
				}
			break;
			case 'methodchild':
				$link_name = (is_array($link_name) ? $link_name : array('service' => 'dashboard', 'method' => 'index', 'segment' => 'all'));
				if (isset($link_name['service']) && isset($link_name['method']) && isset($link_name['segment'])) {
					if (isset($match['params']['service']) && isset($match['params']['method']) && isset($match['params']['segment'])) {
						if (is_string($match['params']['service']) && is_string($match['params']['method']) && is_string($match['params']['segment'])) {
							if (($match['params']['service'] == $link_name['service']) && ($match['params']['method'] == $link_name['method']) && ($match['params']['segment'] == $link_name['segment'])) {
								return 'active';
							}
						}
					}
				}
			break;
		}
		return '';
	}
}
if (!function_exists('base_firebase_url')) {
	function base_firebase_url($path = '') {
		$firebase_url = ConstantConfig::$firebase_url;
		$firebase_url .= "/";
		$firebase_url .= $path;
		return $firebase_url;
	}
}
//============================================
// Legacy Digitale
//============================================
if (!function_exists('show_header_title')) {
	function show_header_title($view_path) {
		$header_title = "";
		switch (strtolower($view_path)) {
			case 'adminpanel/form-login':
				$header_title .= "Digitale Space";
			break;
			case 'user/user-includes':
				$header_title .= "Digitale Space";
			break;
			case 'user/user-includes/long':
				$header_title .= "Digitale Space";
			break;
			case 'user/user-includes/short':
				$header_title .= "DS";
			break;
			
			case 'dtu/merchant-transaction':
				$header_title .= "Merchant Transactions";
				$header_title .= "<small>Transaction data with selected merchant</small>";
			break;
			
			// Public
			case 'public/digitale':
				$header_title .= "Digitale";
			break;
			
			default:
				$header_title .= "DEFAULT TITLE";
			break;
		}
	
		return $header_title;
	}
}
if (!function_exists('show_footer_string')) {
	function show_footer_string($view_path) {
		$footer_string = "";
		switch (strtolower($view_path)) {
			case 'user/user-includes/name':
				$footer_string = "Digitale Space";
			break;
			case 'user/user-includes/version':
				$footer_string = "Version 1.0";
			break;
			case 'user/user-includes/short':
				$footer_string = "DS";
			break;
			
			case 'dtu/merchant-transaction':
				$footer_string = "DS";
			break;
			
			
			
			default:
				$footer_string = "DEFAULT STRING";
			break;
		}
		return $footer_string;
	}
}
//=====================================================================





if (!function_exists('base_rc4encrypt_string')) {
	function base_rc4encrypt_string($text) {
		$text = (is_string($text) || is_numeric($text)) ? sprintf("%s", $text) : '';
		if (strlen($text) == 0) {
			return false;
		}
		$CI = &get_instance();
		$CI->load->library('dashboard/Lib_rc4crypt', ConstantConfig::$rc4crypt_keys['ENCRYPT_KEY'], 'rc4crypt');
		return $CI->rc4crypt->bEncryptRC4($text);
	}
}






