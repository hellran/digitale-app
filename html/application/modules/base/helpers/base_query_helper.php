<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

if (!function_exists('query_get_categories')) {
	function query_get_categories($search_text = '', $start = 0, $perpage = 100) {
		$CI = &get_instance();
		$CI->load->library('adminpanel/Lib_Firebase', '', 'firebase');
		$CI->load->model('cli/Model_consume', 'mod_consume');
		try {
			$consume_index = $CI->mod_consume->get_consume_index_by('categories', 'stories_categories');
		} catch (Exception $ex) {
			return false;
		}
		if (isset($consume_index->consume_last_seq)) {
			$categories_data = $CI->mod_consume->get_consume_logs_by_table_seq($consume_index->consume_table, $consume_index->consume_last_seq);
			if (isset($categories_data->consume_data)) {
				return json_decode($categories_data->consume_data, true);
			}
		}
		return false;
	}
}

if (!function_exists('query_get_logindata')) {
	function query_get_logindata() {
		$CI = &get_instance();
		$CI->load->model('adminpanel/Model_adminpanel', 'mod_adminpanel');
		if (isset($CI->mod_adminpanel->localdata['account_role'])) {
			return $CI->mod_adminpanel->localdata;
		} else {
			return false;
		}
	}
}
if (!function_exists('query_get_configuration_item')) {
	function query_get_configuration_item($config_code) {
		$CI = &get_instance();
		$CI->load->model('adminpanel/Model_configuration', 'mod_config');
		$items = false;
		try {
			$config_items = $CI->mod_config->get_config_item_by('code', $config_code);
		} catch (Exception $ex) {
			return false;
		}
		if (isset($config_items['data'])) {
			if (is_array($config_items['data']) && (count($config_items['data']) > 0)) {
				foreach ($config_items['data'] as $keval) {
					$items = $keval;
					break;
				}
			}
		}
		return $items;
	}
}
if (!function_exists('query_get_stories_count')) {
	function query_get_stories_count($user_seq = 0) {
		$user_seq = (is_numeric($user_seq) ? (int)$user_seq : 0);
		if ($user_seq === 0) {
			return false;
		}
		$CI = &get_instance();
		$CI->load->config('base/base_config');
		$base_config = $CI->config->item('base_config');
		$db_core = $CI->load->database('core', TRUE);
		$sql = sprintf("SELECT stories_status, COUNT(seq) AS value FROM %s WHERE (stories_create_by = '%d') GROUP BY stories_status ORDER BY stories_status ASC",
			'firebase_stories',
			$db_core->escape_str($user_seq)
		);
		try {
			$sql_query = $db_core->query($sql);
		} catch (Exception $ex) {
			throw $ex;
			return false;
		}
		$result_db = array();
		foreach ($sql_query->result() as $row) {
			$result_db[$row->stories_status] = $row->value;
		}
		return $result_db;
	}
}





















