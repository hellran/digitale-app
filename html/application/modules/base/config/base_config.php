<?php
if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
$config['base_config'] = array(
	'site-name'				=> 'Digitale App',
	'site-name-short'		=> 'DA',
	'site-version'			=> 'v.2.1',
	'site-copyright'		=> '2018 - Digitale App',
	'site-developer'		=> 'https://nababan.net',
	'site-template'			=> 'templates/listeo/',
	'base_path'				=> 'home',
	'base_password_forget'	=> 5,
	'email_vendor'			=> 'localsmtp',
	'email_template'		=> (dirname(APPPATH) . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'email-template.html'),
	'super_admin_role'		=> 4,
	'admin_role'			=> array(4, 6),
	'editor_role'			=> array(3, 4, 5, 6),
	'merchant_role'			=> array(3, 5),
	'rows-per-page'			=> 10,
);

$config['base_config']['upload_img'] = array(
	'local'		=> array(
		'upload_dir'			=> (FCPATH . 'media'),
		'upload_path' 			=> (FCPATH . 'media/images'),
		'allowed_types'			=> 'jpg|jpeg|png|gif|png',
		'max_size' 				=> ((1024 * 1024) * 32),
		'max_width' 			=> 32600,
		'max_height' 			=> 32600,
		'encrypt_name'			=> TRUE,
		'file_name'				=> (uniqid() . time()),
	),
	'cdn'		=> array(
		'hostname' 				=> 'jolteon.rapidplex.com',
		'username' 				=> 'digita12',
		'password' 				=> '06zH6neTj0',
		'debug'					=> TRUE,
		'cdn_url' 				=> 'https://static.digitale-app.com',
		'cdn_path' 				=> '/digitale/static.digitale.space',
		'cdn_full_url'			=> 'https://static.digitale-app.com/files/firebase/assets/images',
		'cdn_full_path' 		=> '/digitale/static.digitale.space/files/firebase/assets/images',
		'default_img' 			=> 'https://static.digitale-app.com/files/firebase/team_logo/demo_304x304.jpg',
		'default_tour_img' 		=> 'https://static.digitale-app.com/files/firebase/tour_logo/demo_304x304.jpg',
		'default_upload_team' 	=> 'https://static.digitale-app.com/files/firebase/team_logo/default_myteam_condition.jpg',
	),
	'resize'			=> array(
		'path'				=> array(),
		'library'			=> array(
			'image_library' 	=> 'gd2',
			'source_image'		=> '',
			'thumb_marker'		=> '_thumb',
			'create_thumb'		=> true,
			'maintain_ratio'	=> true,
			'overwrite'			=> true,
			'new_image'			=> (FCPATH . 'media' . DIRECTORY_SEPARATOR . 'images'),
			'master_dim'		=> 'width',
			'width'				=> 640,
			'height'			=> 720,
		),
		
	),
);



#### Legacy Digitale
$config['base_config']['firebase_database_prefix'] = ConstantConfig::THIS_SERVER_MODE;
$config['base_config']['firebase_credential'] = array(
	'URL'			=> 'https://digitale-app.firebaseio.com/',
	'USER'			=> 'digitale-space',
	'TOKEN'			=> 'RxqB7cNlhEwy4txOetkFfL0P7SRCokggXzgAIUS5',
	'TIMEDOUT'		=> 15,
);
$config['base_config']['email_vendors'] = array(
	'digitale'				=> array(
		'mail_hostname'					=> 'ssl://jolteon.rapidplex.com',
		'mail_port'						=> 465,
		'mail_address'					=> 'info@digitale.space',
		'mail_username'					=> 'info@digitale.space',
		'mail_password'					=> 'bVAT~4NXov{7',
		'mail_name'						=> 'Digitale Space',
	),
);
$config['base_config']['email_vendor'] = 'digitale';












// Tables
$config['base_config']['tables'] = array(
	'logged'						=> 'account_logged',
	'logged_log'					=> 'account_logged_log',
	'admin'							=> 'account_admin',
	'roles'							=> 'account_roles',
	'users'							=> 'account_users',
	
	# app link
	'app_link'						=> 'app_links',
	# configuration
	'site_config'					=> 'site_configuration',
);