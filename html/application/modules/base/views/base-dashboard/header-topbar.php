<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly");
}

$section = 'dashboard';
if (isset($page)) {
	$page = (is_string($page) ? strtolower($page) : 'dashboard-home');
} else {
	$page = 'dashboard-home';
}
switch (strtolower($page)) {
	//---------------------------
	// Dashboard
	//---------------------------
	# Home
	case 'dashboard-home':
	case 'dashboard-index':
	case 'dashboard':
	default:
		$section = 'dashboard';
	break;

	# Profile
	case 'dashboard-profile-view':
	case 'dashboard-profile-setting':
		$section = 'profile';
	break;
	
	# Stories
	case 'stories-index':
	case 'stories-edit':
	case 'stories-lists-published':
	case 'stories-lists-drafted':
	case 'stories-lists-reviewed':
		$section = 'stories';
	break;
	case 'stories-add':
		$section = 'stories-add';
	break;
	# Bookmarks
	case 'bookmarks-index':
	case 'bookmarks-edit':
	case 'bookmarks-add':
		$section = 'bookmarks';
	break;
	# Messages
	case 'messages-index':
	case 'messages-edit':
	case 'messages-add':
		$section = 'messages';
	break;
}

$story_categories = query_get_categories('', 0, 10);
$get_logindata = query_get_logindata();
$google_playstore = query_get_configuration_item('google-playstore-link');
if (isset($collect['userdata']['seq'])) {
	$story_items = query_get_stories_count($collect['userdata']['seq']);
} else {
	$story_items = false;
}

?>
<header id="header-container" class="fixed fullwidth dashboard">
	<div id="header" class="not-sticky">
		<div class="container">
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="<?= base_url('home');?>">
						<img src="<?= base_url('templates/digitale/images/logo/dashboard-191x59-dash.png');?>" alt="digitale-app logo" />
					</a>
					<a href="<?= base_url('home');?>" class="dashboard-logo">
						<img src="<?= base_url('templates/digitale/images/logo/dashboard-191x59.png');?>" alt="digitale-app dashboard" />
					</a>
				</div>
				<!-- Mobile Navigation -->
				<div class="menu-responsive">
					<i class="fa fa-reorder menu-trigger"></i>
				</div>
				<!-- Main Navigation -->
				<nav id="navigation" class="style-1">
					<ul id="responsive">
						<li>
							<a class="<?= (strtolower($page) === 'dashboard-home') ? 'current' : '';?>" href="<?= base_url('home');?>">
								Home
							</a>
						</li>

						<li>
							<a class="<?= (strtolower($section) === 'stories') ? 'current' : '';?>" href="<?= base_url('dashboard/stories/index');?>">My Stories</a>
							<ul>
								<li>
									<a href="<?= base_url('dashboard/stories/published');?>">Published</a>
								</li>
								<li>
									<a href="<?= base_url('dashboard/stories/reviewed');?>">Reviewed</a>
								</li>
								<li>
									<a href="<?= base_url('dashboard/stories/drafted');?>">Drafted</a>
								</li>
							</ul>
						</li>
						<li>
							<a class="<?= (in_array(strtolower($section), array('profile', 'messages', 'bookmarks')) ? 'current' : '');?>" href="<?= base_url('dashboard/profile/index');?>">
								My Profile
							</a>
							<ul>
								<li>
									<a href="<?= base_url('dashboard/profile/view');?>">Profile</a>
								</li>
								<li>
									<a href="<?= base_url('dashboard/messages/index');?>">Messages</a>
								</li>
								<li>
									<a href="<?= base_url('dashboard/bookmarks/index');?>">Bookmarks</a>
								</li>
							</ul>
						</li>
					</ul>
				</nav>
				<div class="clearfix"></div>
				<!-- Main Navigation / End -->
				
			</div>
			<!-- Left Side Content / End -->

			<!-- Right Side Content / End -->
			<div class="right-side">
				<!-- Header Widget -->
				<div class="header-widget">
					<!-- User Menu -->
					<div class="user-menu">
						<?php
						if (isset($collect['userdata']['seq'])) {
							?>
							<div class="user-name">
								<span>
									<img src="<?=$collect['userdata']['account_picture'];?>" alt="user-profile avatar" />
								</span>
								<?=$collect['userdata']['account_fullname'];?>
							</div>
							<ul>
								<li>
									<a href="<?= base_url('dashboard/profile/setting');?>">
										<i class="sl sl-icon-settings"></i> Setting
									</a>
								</li>
								<li>
									<a href="<?= base_url('dashboard/messages/index');?>">
										<i class="sl sl-icon-envelope-open"></i> Messages
									</a>
								</li>
								<li>
									<a href="<?= base_url('dashboard/account/logout');?>">
										<i class="sl sl-icon-power"></i> Logout
									</a>
								</li>
							</ul>
							<?php
						}
						?>
					</div>
					<a href="<?= base_url('dashboard/stories/publish');?>" class="button border with-icon">
						Add Story
						<i class="sl sl-icon-plus"></i>
					</a>
				</div>
				<!-- Header Widget / End -->
			</div>
			<!-- Right Side Content / End -->
		</div>
	</div>
	<!-- Header / End -->
</header>
<div class="clearfix"></div>
<!-- Dashboard -->
<div id="dashboard">
	<a href="javascript:;" class="dashboard-responsive-nav-trigger">
		<i class="fa fa-reorder"></i> Dashboard Navigation
	</a>
	
	<div class="dashboard-nav">
		<ul data-submenu-title="Main">
			<li class="<?= ((strtolower($section) === 'dashboard') ? 'active' : '');?>">
				<a href="<?= base_url('dashboard/dashboard/index');?>">
					<i class="sl sl-icon-settings"></i> Dashboard
				</a>
			</li>
			<li class="<?= ((strtolower($page) === 'messages-index') ? 'active' : '');?>">
				<a href="<?= base_url('dashboard/messages/index');?>">
					<i class="sl sl-icon-envelope-open"></i> My Messages 
					<span class="nav-tag messages">0</span>
				</a>
			</li>
		</ul>
		<ul data-submenu-title="Listings">
			<li class="<?= ((strtolower($section) === 'stories') ? 'active' : '');?>">
				<a>
					<i class="sl sl-icon-layers"></i> My Stories
				</a>
				<ul>
					<li>
						<a href="<?= base_url('dashboard/stories/published');?>">
							Published <span class="nav-tag green"><?= (isset($story_items['published']) ? $story_items['published'] : 0);?></span>
						</a>
					</li>
					<li>
						<a href="<?= base_url('dashboard/stories/reviewed');?>">
							Reviewed <span class="nav-tag yellow"><?= (isset($story_items['reviewed']) ? $story_items['reviewed'] : 0);?></span>
						</a>
					</li>
					<li>
						<a href="<?= base_url('dashboard/stories/drafted');?>">
							Drafted <span class="nav-tag red"><?= (isset($story_items['drafted']) ? $story_items['drafted'] : 0);?></span>
						</a>
					</li>
				</ul>	
			</li>
			<li class="<?= ((strtolower($page) === 'bookmarks-index') ? 'active' : '');?>">
				<a href="<?= base_url('dashboard/bookmarks/index');?>">
					<i class="sl sl-icon-heart"></i> My Bookmarks
				</a>
			</li>
			<li class="<?= ((strtolower($page) === 'stories-add') ? 'active' : '');?>">
				<a href="<?= base_url('dashboard/stories/publish');?>">
					<i class="sl sl-icon-plus"></i> Add Story
				</a>
			</li>
		</ul>	
		<ul data-submenu-title="Account">
			<li class="<?= ((strtolower($section) === 'profile') ? 'active' : '');?>">
				<a href="<?= base_url('dashboard/profile/setting');?>">
					<i class="sl sl-icon-user"></i> My Profile
				</a>
			</li>
			<li>
				<a href="<?= base_url('dashboard/account/logout');?>">
					<i class="sl sl-icon-power"></i> Logout
				</a>
			</li>
		</ul>
	</div>
	<!-- Content -->
	<div class="dashboard-content">
		

