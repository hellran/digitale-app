<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly");
}


?>

		<div class="row">
			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">
					&copy; <?= base_config('site-copyright');?>
				</div>
			</div>
		</div>
	</div>
	<!-- //Content -->
</div>
<!-- //Wrapper -->


<!-- Scripts -->
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/jquery-2.2.0.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/jpanelmenu.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/chosen.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/slick.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/rangeslider.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/magnific-popup.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/waypoints.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/counterup.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/jquery-ui.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/tooltips.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/custom.js');?>"></script>
<!-- //Scripts -->