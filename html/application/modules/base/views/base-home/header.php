<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="<?= base_config("Site Author");?>">
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('templates/digitale/icon/apple-icon-57x57.png');?>" />
	<link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('templates/digitale/icon/apple-icon-60x60.png');?>" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('templates/digitale/icon/apple-icon-72x72.png');?>" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('templates/digitale/icon/apple-icon-76x76.png');?>" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('templates/digitale/icon/apple-icon-114x114.png');?>" />
	<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('templates/digitale/icon/apple-icon-120x120.png');?>" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('templates/digitale/icon/apple-icon-144x144.png');?>" />
	<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('templates/digitale/icon/apple-icon-152x152.png');?>" />
	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('templates/digitale/icon/apple-icon-180x180.png');?>" />
	<link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url('templates/digitale/icon/android-icon-192x192.png');?>" />
	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('templates/digitale/images/logo/icon-32x32.png');?>" />
	<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('templates/digitale/icon/favicon-96x96.png');?>" />
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('templates/digitale/images/logo/icon-16x16.png');?>" />
	<?php
	if (isset($title)) {
		?>
		<title>
			<?=$title;?>
		</title>
		<?php
	} else {
		?>
		<title>Digitale App</title>
		<?php
	}
	?>
	<link rel="stylesheet" href="<?= base_url('templates/listeo/css/bootstrap.css');?>" />
	<link rel="stylesheet" href="<?= base_url('templates/listeo/css/icons.css');?>" />
	<link rel="stylesheet" href="<?= base_url('templates/listeo/css/revolutionslider.css');?>" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:600,700" />
    <link rel="stylesheet" href="<?= base_url('templates/listeo/css/style-min.css');?>" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="<?= base_url('templates/listeo/css/colors/main.css');?>" id="colors" />
</head>
<body>
	<div id="wrapper">






