<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>

<?php

/*
echo "<pre>";
print_r($collect['configuration']);
echo "</pre>";
exit;

*/
?>

<?php
$section = 'home';
if (isset($page)) {
	$page = (is_string($page) ? strtolower($page) : 'digitale-home');
} else {
	$page = 'digitale-home';
}
switch (strtolower($page)) {
	//---------------------------
	// Home
	//---------------------------
	# Home
	case 'digitale-home':
	case 'digitale':
	case 'home':
	default:
		$section = 'dashboard';
	break;
	# About
	case 'home-about':
		$section = 'about';
	break;
	
	//---------------------------
	// Dashboard
	//---------------------------
	# Users
	case 'user-list':
	case 'user-add':
	case 'user-edit':
	case 'user-view':
		$section = 'users';
	break;
	# Profile
	case 'profile-edit':
	case 'profile-view':
		$section = 'profile';
	break;
	
	//---------------------------
	// Dashboard
	//---------------------------

}

$story_categories = query_get_categories('', 0, 10);
$get_logindata = query_get_logindata();
$google_playstore = query_get_configuration_item('google-playstore-link');
?>

<header id="header-container">
	<div id="header">
		<div class="container">
			<!-- Left Side Content -->
			<div class="left-side">
				<!-- Logo -->
				<div id="logo">
					<a href="<?= base_url('home');?>">
						<img src="<?= base_url('templates/digitale/images/logo/dashboard-191x59-dash.png');?>" alt="digitale-app logo" />
					</a>
				</div>

				<!-- Mobile Navigation -->
				<div class="menu-responsive">
					<i class="fa fa-reorder menu-trigger"></i>
				</div>

				<!-- Main Navigation -->
				<nav id="navigation" class="style-1">
					<ul id="responsive">
						<li>
							<a class="<?= (strtolower($page) === 'digitale-home') ? 'current' : '';?>" href="<?= base_url('home');?>">
								Home
							</a>
						</li>
						<li>
							<a href="javascript:;">Categories</a>
							<ul>
								<?php
								if (isset($story_categories['kategori'])) {
									if (is_array($story_categories['kategori']) && (count($story_categories['kategori']) > 0)) {
										foreach ($story_categories['kategori'] as $catKey => $catVal) {
											?>
											<li>
												<a href="<?= base_url('home/categories/view/' . base_permalink($catVal));?>"><?=$catVal;?></a>
												<?php
												if (isset($story_categories['sub-kategori'][$catKey])) {
													?>
													<ul>
														<?php
														if (is_array($story_categories['sub-kategori'][$catKey]) && (count($story_categories['sub-kategori'][$catKey]) > 0)) {
															foreach ($story_categories['sub-kategori'][$catKey] as $catChild) {
																?>
																<li>
																	<a href="<?= base_url('home/categories/subview/' . base_permalink($catVal) . '/' . base_permalink($catChild));?>"><?=$catChild;?></a>
																</li>
																<?php
															}
														}
														?>
													</ul>
													<?php
												}
												?>
											</li>
											<?php
										}
									}
								}
								?>
							</ul>
						</li>
						<?php
						if ($get_logindata != false) {
							?>
							<li>
								<a href="javascript:;">Dashboard</a>
								<ul>
									<li>
										<a href="<?= base_url('dashboard/dashboard/index');?>">My Dashboard</a>
									</li>
									<li>
										<a href="<?= base_url('dashboard/stories/index');?>">My Stories</a>
									</li>
									<li>
										<a href="<?= base_url('dashboard/profile/index');?>">My Profile</a>
									</li>
								</ul>
							</li>
							<?php
						}
						?>
					</ul>
				</nav>
				<div class="clearfix"></div>
				<!-- Main Navigation / End -->
				
			</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content -->
			<div class="right-side">
				<div class="header-widget">
					<?php
					if ($get_logindata != false) {
						?>
						<a href="<?= base_url('dashboard/account/logout');?>" class="sign-in">
							<i class="sl sl-icon-logout"></i> Logout
						</a>
						<?php
					} else {
						?>
						<a href="<?= base_url('dashboard/account/login');?>#sign-in-dialog" class="sign-in">
							<i class="sl sl-icon-login"></i> Login
						</a>
						<?php
					}
					if ($google_playstore != false) {
						?>
						<a href="<?= (isset($google_playstore->config_value) ? $google_playstore->config_value : '');?>" class="button border with-icon">
							<i class="sl sl-icon-cloud-download"></i> Download
						</a>
						<?php
					}
					?>
				</div>
			</div>
			<!-- Right Side Content / End -->

			<!-- Sign In Popup -->
			<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
				<div class="small-dialog-header">
					<h3>Sign In</h3>
				</div>

				<!--Tabs -->
				<div class="sign-in-form style-1">
					<ul class="tabs-nav">
						<li class=""><a href="#tab-login-local">Log In</a></li>
						<li><a href="#tab-register-local">Register</a></li>
					</ul>

					<div class="tabs-container alt">
						<!-- Login -->
						<div class="tab-content" id="tab-login-local" style="display: none;">
							<form method="post" class="login" action="<?= base_url('dashboard/account/loginaction');?>">
								<p class="form-row form-row-wide">
									<input type="hidden" name="user_server" value="local" />
									<label for="user_email">Username:
										<i class="im im-icon-Email"></i>
										<input type="text" class="input-text" name="user_email" id="user_email" value="" />
									</label>
								</p>
								<p class="form-row form-row-wide">
									<label for="user_password">Password:
										<i class="im im-icon-Lock-2"></i>
										<input class="input-text" type="password" name="user_password" id="user_password"/>
									</label>
									<span class="lost_password">
										<a href="<?= base_url('dashboard/account/passwordForget');?>">Lost Your Password?</a>
									</span>
								</p>

								<div class="form-row">
									<input type="submit" class="button border margin-top-5" name="login" value="Login" />
									<div class="checkboxes margin-top-10">
										<input id="remember-me" type="checkbox" name="check">
										<label for="remember-me">Remember Me</label>
									</div>
								</div>
							</form>
						</div>

						<!-- Register -->
						<div class="tab-content" id="tab-register-local" style="display: none;">
							<form id="form-digitale-register" method="post" class="register" action="<?= base_url('dashboard/account/registeraction');?>">
								<p class="form-row form-row-wide">
									<label for="user_fullname">Name:
										<i class="im im-icon-Male"></i>
										<input type="text" class="input-text" name="user_fullname" id="user_fullname" value="" />
									</label>
								</p>
								<p class="form-row form-row-wide">
									<label for="user_email">Email Address:
										<i class="im im-icon-Mail"></i>
										<input type="text" class="input-text" name="user_email" id="user_email" value="" />
									</label>
								</p>
								<p class="form-row form-row-wide">
									<label for="user_password">Password:
										<i class="im im-icon-Lock-2"></i>
										<input class="input-text" type="password" name="user_password" id="user_password" />
									</label>
								</p>
								<p class="form-row form-row-wide">
									<label for="user_password_confirm">Repeat Password:
										<i class="im im-icon-Lock-2"></i>
										<input class="input-text" type="password" name="user_password_confirm" id="user_password_confirm"/>
									</label>
								</p>
								<p>
									<input type="hidden" id="user_phonenumber" name="user_phonenumber" value="" />
									<input type="hidden" id="user_phonemobile" name="user_phonemobile" value="" />
									<input type="hidden" id="user_username" name="user_username" value="" />
									<input type="hidden" id="user_address" name="user_address" value="" />
									<input type="hidden" id="user_address_province" name="user_address_province" value="" />
									<input type="hidden" id="user_address_city" name="user_address_city" value="" />
									<input type="hidden" id="user_address_district" name="user_address_district" value="" />
									<input type="hidden" id="user_address_area" name="user_address_area" value="" />
								</p>
								<button id="save-this-item" type="button" class="button border fw margin-top-10">
									Register
								</button>
							</form>
						</div>

					</div>
				</div>
			</div>
			<!-- Sign In Popup / End -->

		</div>
	</div>
</header>
<div class="clearfix"></div>