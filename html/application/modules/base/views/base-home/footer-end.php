<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}

?>
		<script type="text/javascript">
		//==========================================
		// Register Action
		//==========================================
		$('#save-this-item').click(function(el) {
			el.preventDefault();
			var objData = {};
			var user_fullname = $('#user_fullname').val();
			$.ajax({
				url: '<?= base_url('dashboard/account/registerUsername');?>',
				type: 'POST',
				cache: false,
				data : {
					'user_name': user_fullname
				},
				dataType: "json",
				success : function(response) {
					if (response != null) {
						if ((response.username != 'undefined') && (response.username.length > 0)) {
							var user_phonenumber = $('#user_phonenumber');
							user_phonenumber.attr('value', '');
							var user_phonemobile = $('#user_phonemobile');
							user_phonemobile.attr('value', '081212345678');
							var user_username = $('#user_username');
							user_username.attr('value', response.username);
							var user_address = $('#user_address');
							user_address.attr('value', '');
							var user_address_province = $('#user_address_province');
							user_address_province.attr('value', '');
							var user_address_city = $('#user_address_city');
							user_address_city.attr('value', '');
							var user_address_district = $('#user_address_district');
							user_address_district.attr('value', '');
							var user_address_area = $('#user_address_area');
							user_address_area.attr('value', '');
							
							
							$('#form-digitale-register').submit();
						}
					}
				}
			});
			
			
			
			
			
			
			
			
			
		});

		</script>
	</body>
</html>