<?php
if (!defined('BASEPATH')) {
	exit("Cannot load script directly.");
}
?>
	<!-- Footer
	================================================== -->
	<div id="footer" class="sticky-footer">
		<!-- Main -->
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-6">
					<img class="footer-logo" src="<?= base_url('templates/digitale/images/logo/dashboard-191x59-dash.png');?>" alt="footer-logo" />
					<br><br>
					<p>
						<?= (isset($collect['configuration']['footer-description']->config_value) ? $collect['configuration']['footer-description']->config_value : '');?>
					</p>
				</div>

				<div class="col-md-4 col-sm-6 ">
					<h4>Helpful Links</h4>
					<ul class="footer-links">
						<li><a href="<?= base_url('dashboard/dashboard/index');?>">Dashboard</a></li>
						<li><a href="<?= base_url('dashboard/account/register');?>">Sign Up</a></li>
					</ul>

					<ul class="footer-links">
						<li><a href="#">Pricing</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
					<div class="clearfix"></div>
				</div>		

				<div class="col-md-3  col-sm-12">
					<h4>Contact Us</h4>
					<div class="text-widget">
						<span>
							<?= (isset($collect['configuration']['alamat-redaksi']->config_value) ? $collect['configuration']['alamat-redaksi']->config_value : '');?>
						</span> 
						<br/>
						Phone: <span><?= (isset($collect['configuration']['telp-redaksi']->config_value) ? $collect['configuration']['telp-redaksi']->config_value : '');?></span>
						<br/>
						E-Mail: 
						<span>
							<a href="<?= (isset($collect['configuration']['email-redaksi']->config_value) ? "mailto:{$collect['configuration']['email-redaksi']->config_value}" : '');?>">
								<?= (isset($collect['configuration']['email-redaksi']->config_value) ? $collect['configuration']['email-redaksi']->config_value : '');?>
							</a> 
						</span>
					</div>

					<ul class="social-icons margin-top-20">
						<li>
							<a class="facebook" href="<?= (isset($collect['configuration']['footer-facebook-url']->config_value) ? $collect['configuration']['footer-facebook-url']->config_value : '');?>">
								<i class="icon-facebook"></i>
							</a>
						</li>
						<li>
							<a class="twitter" href="<?= (isset($collect['configuration']['footer-twitter-url']->config_value) ? $collect['configuration']['footer-twitter-url']->config_value : '');?>">
								<i class="icon-twitter"></i>
							</a>
						</li>
						<li>
							<a class="gplus" href="<?= (isset($collect['configuration']['footer-googleplay-url']->config_value) ? $collect['configuration']['footer-googleplay-url']->config_value : '');?>">
								<i class="icon-gplus"></i>
							</a>
						</li>
						<li>
							<a class="instagram" href="<?= (isset($collect['configuration']['footer-instagram-url']->config_value) ? $collect['configuration']['footer-instagram-url']->config_value : '');?>">
								<i class="icon-instagram"></i>
							</a>
						</li>
					</ul>

				</div>

			</div>
			
			<!-- Copyright -->
			<div class="row">
				<div class="col-md-12">
					<div class="copyrights">
						<?= (isset($collect['configuration']['copyright']->config_value) ? $collect['configuration']['copyright']->config_value : '');?>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- Footer / End -->

	<!-- Back To Top Button -->
	<div id="backtotop">
		<a href="#"></a>
	</div>
</div>


<!-- jQuery -->
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/jquery-2.2.0.min.js');?>"></script>
<!-- ListEO -->
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/jpanelmenu.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/chosen.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/slick.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/rangeslider.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/magnific-popup.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/waypoints.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/counterup.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/jquery-ui.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/tooltips.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('templates/listeo/scripts/custom.js');?>"></script>







